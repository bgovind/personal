Association for India�s Development � Clemson Chapter � Website Development and Maintenance: December 2015 - Present:

Association for India�s Development is a U.S based NGO that focuses on education, healthcare, women empowerment, agriculture 
and many more areas at different projects in India. Clemson chapter is a student chapter based out of Clemson and is recognized 
by Clemson University as a graduate student organization.

I am the webmaster of organization. The official webpage https://aid.people.clemson.edu and the organization�s admin portal was 
designed, developed and is currently being maintained by me.

Technologies used: HTML5, CSS, JavaScript, PHP, MySQL
