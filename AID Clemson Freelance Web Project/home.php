<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
	session_start();
	include "mysqlClass.inc.php";
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AID Clemson Admin Home</title>
</head>
<style>
th, td {
    padding: 5px;
}
</style>
<body>
<div>
<p><img src="img/aid-logo-main-site.png" height="110" width="300"/> </p>
</div>
<?php

if(isset($_SESSION['username']))
{	
	?>
	<div style="background-color: #cce6ff;">
	<div style="margin: auto 1cm auto 1cm;">
	<h3>Welcome Admin!!</h3>
	<table>
	<tr>
	<td><a href="home.php">Home</a></td><td><a href="add.php">Admin Settings</a></td><td><a href="volunteer.php">Volunteer List</a></td><td><a href="meeting_mins.php">Meeting Minutes</a></td></tr>
	</table>
	<form method="post" action="">
	<input type="submit" name="logout" value="Logout">
	</form>
	<?php
	if(isset($_POST['logout']))
	{
		session_destroy();
		header('Location: admin_login.php');
	}
	?>
	<br>
	<br>
	<h4>Alumni Drive Donation Details</h4>
	<br>
	<table border="1" style="text-align:center;">
	<tr><th><b>First Name</b></th><th><b>Last Name</b></th><th><b>Email</b></th><th><b>Phone</b></th><th><b>Graduated Year</b></th><th><b>Amount in $</b></th><th><b>Donated On</b></th></tr>
	<?php
	$alumni_donations = mysql_query("select * from table");
	$total = 0;
	while($row_alumni = mysql_fetch_row ($alumni_donations))
	{
		$fn = $row_alumni[0];
		$ln = $row_alumni[1];
		$amount = $row_alumni[2];
		$email = $row_alumni[3];
		$phone = $row_alumni[4];
		$year = $row_alumni[5];
		$d_on = $row_alumni[6];
		$total = $total + $amount;
		?>
		<tr>
		<td><?php echo $fn; ?></td>
		<td><?php echo $ln; ?></td>
		<td><?php echo $email; ?></td>
		<td><?php echo $phone; ?></td>
		<td><?php echo $year; ?></td>
		<td><b><?php echo $amount; ?></b></td>
		<td><?php echo $d_on; ?></td>
		</tr>
		<?php
	}
?>
	</table>
	<p>Total Amount Donated Alumni as of <?php echo date("Y/m/d"); ?> : <b> $&nbsp;<?php echo $total; ?> </b></p>
	<br>
	<br>
	<br>
	</div>
	</div>
	
<?php
}
else
{
	header('Location: admin_login.php');
}
?>

</body>
</html>