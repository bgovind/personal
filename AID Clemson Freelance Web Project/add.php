<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
	session_start();
	include "mysqlClass.inc.php";
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AID Clemson Admin - Settings</title>
</head>
<style>
th, td {
    padding: 5px;
}
</style>
<body>
<div>
<p><img src="img/aid-logo-main-site.png" height="110" width="300"/> </p>
</div>
<?php

if(isset($_SESSION['username']))
{	
	?>
	<div style="background-color: #cce6ff;">
	<div style="margin: auto 1cm auto 1cm;">
	<h3>Welcome Admin!!</h3>
	<table>
	<tr>
	<td><a href="home.php">Home</a></td><td><a href="add.php">Admin Settings</a></td><td><a href="volunteer.php">Volunteer List</a></td><td><a href="meeting_mins.php">Meeting Minutes</a></td></tr>
	</table>
	<form method="post" action="">
	<input type="submit" name="logout" value="Logout">
	</form>
	<?php
	if(isset($_POST['logout']))
	{
		session_destroy();
		header('Location: admin_login.php');
	}
	?>
	<br>
	<br>
	<h4>Admin Settings</h4>
	<br>
	<h5>Change Password</h5>
	<form method = "post" action = "add.php">
	<table>
	<tr>
	<td>Old Password:</td><td><input type="password" name="oldp"></td></tr>
	<tr>
	<td>New Password:</td><td><input type="password" name="np1"></td></tr>
	<tr>
	<td>Confirm New Password:</td><td><input type="text" name="np2"></td></tr>
	<tr>
	<td></td><td><input type="submit" name="resetp" value="Reset Password"></td></tr>
	</table>
	<?php
	if(isset($_POST['resetp']))
	{
		if($_POST['oldp'] == '' || $_POST['np1'] == '' || $_POST['np2'] == '')
		{
			echo "One or more fields missing";
		}
		else
		{
			$check_oldp = mysql_query("select password from table where username='".$_SESSION['username']."'");
			$curr_pwd = mysql_fetch_row($check_oldp);
			if ($_POST['oldp'] == $curr_pwd[0])
			{
				if($_POST['np1'] == $_POST['np2'])
				{
					$ch_pwd = mysql_query("update table set password = '".$_POST['np1']."' where username ='".$_SESSION['username']."'");
					echo "Password has been Reset";
				}
				else
				{
					echo "New Passwords do not match";
				}
			}
			else
			{
				echo "Current Password Wrong.";
			}
		}
		
	}
	?>
	<br>
	<br>
	<br>
	</div>
	</div>
<?php
}
else
{
	header('Location: admin_login.php');
}
?>

</body>
</html>