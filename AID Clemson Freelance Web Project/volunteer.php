<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
	session_start();
	include "mysqlClass.inc.php";
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AID Clemson Admin - Volunteers List</title>
</head>
<style>
th, td {
    padding: 5px;
}
</style>
<body>
<div>
<p><img src="img/aid-logo-main-site.png" height="110" width="300"/> </p>
</div>
<?php

if(isset($_SESSION['username']))
{	
	?>
	<div style="background-color: #cce6ff;">
	<div style="margin: auto 1cm auto 1cm;">
	<h3>Welcome Admin!!</h3>
	<table>
	<tr>
	<td><a href="home.php">Home</a></td><td><a href="add.php">Admin Settings</a></td><td><a href="volunteer.php">Volunteer List</a></td><td><a href="meeting_mins.php">Meeting Minutes</a></td></tr>
	</table>
	<form method="post" action="">
	<input type="submit" name="logout" value="Logout">
	</form>
	<?php
	if(isset($_POST['logout']))
	{
		session_destroy();
		header('Location: admin_login.php');
	}
	?>
	<br>
	<br>
	<h4>Registered Volunteer Details</h4>
	<br>
	<table border="1" style="text-align:center;">
	<tr><th><b>First Name</b></th><th><b>Last Name</b></th><th><b>Email</b></th><th><b>Graduation Year</b></th></tr>
	<?php
	$list = mysql_query("select * from table");
	while($row_v = mysql_fetch_row ($list))
	{
		$vfn = $row_v[0];
		$vln = $row_v[1];
		$vemail = $row_v[2];
		$vyear = $row_v[3];
		?>
		<tr>
		<td><?php echo $vfn; ?></td>
		<td><?php echo $vln; ?></td>
		<td><?php echo $vemail; ?></td>
		<td><?php echo $vyear; ?></td>
		</tr>
		<?php
	}
?>
	</table>
	<br>
	<br>
	<br>
	</div>
	</div>
<?php
}
else
{
	header('Location: admin_login.php');
}
?>

</body>
</html>