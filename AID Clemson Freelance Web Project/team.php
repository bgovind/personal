<!DOCTYPE html>
<?php
	session_start();
	include "mysqlClass.inc.php";
?>
<html> 
<Head>
<link rel="stylesheet" type="text/css" href="stylesheet.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script>
		$(function() {

			$("#slideshow > div:gt(0)").hide();

			setInterval(function() {
			  $('#slideshow > div:first')
			    .fadeOut(1000)
			    .next()
			    .fadeIn(1000)
			    .end()
			    .appendTo('#slideshow');
			},  3000);

		});
</script>
<Title> AID Clemson Chapter </Title>
</Head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "http://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="logo">
<img src="img/front.jpg"></img>
</div>

<div class="nav">
<a class="inactive" href="index.html">Home</a>
<a class="inactive" href="projects.html">Projects</a>
<a class="inactive" href="events.html">Events</a>
<a class="active" href="team.php">Volunteers</a>
<a class="inactive" href="donate.html">Donate</a>
<a class="inactive" href="alumni.php">Alumni Drive</a>
<a class="inactive" href="http://www.aidindia.org" target="_blank">AID.org</a>

</div>

<div class="latest">
<br><br><br><br><br><br><br><br><br>
<h3 style="margin: 0.5cm auto auto 1cm;">Executive Committee:</h3>
<p style="margin: 0.5cm auto auto 1cm;">
President: <a href="mailto:araghav@g.clemson.edu">Achyut Raghavendra</a><br><br>
Vice President: <a href="mailto:Karan1793@gmail.com">Karan Agarwal</a><br><br>
Secretary: <a href="mailto:ssudars@g.clemson.edu">Senbagaraman Sudarsanam</a><br><br>
Treasurer: <a href="mailto:smritib@g.clemson.edu">Smriti Bhargava</a><br><br>
GSG Representative: <a href="mailto:nakul.ravi@gmail.com">Nakul R.Kumar</a><br><br>
Project co-ordinator: <a href="mailto:rishwanth4@gmail.com">Rishwanth S</a><br><br>
Fundraising co-ordinator: Sumithra <br><br>
Webmaster: <a href="mailto:bgovind@g.clemson.edu">Bharat Kumar Govindaradjou</a><br><br>
<h3 style="margin: 0.5cm 320px auto 1cm;">You can also contact us at: <a href="mailto:aid@clemson.edu">aid@clemson.edu</a></h3><br><br>
</p>

<div id="volunteer" style="margin: 0.5cm 320px auto 1cm; background-color: light-grey;">
<h4> Wish to volunteer? Fill the volunteer form below and we will be in touch!! </h4>

<form action="team.php#volunteer" method="post">
<table style="margin: 0.5cm 0cm auto 1cm; text-align: left;">
	<tr><td>First Name: </td><td><input type="text" name="fname"></td></tr>
	<tr><td>Last Name: </td><td><input type="text" name="lname"></td></tr>
	<tr><td>Graduation year: <i>e.g 2005</i></td><td><input type="number" name="year"></td></tr>
	<tr><td>Email: </td><td><input type="email" name="email"></td></tr>
	<tr><td></td><td><input name="submit" type="submit" value="Register"></td></tr>
</table>
</form>

<?php
if(isset($_POST['submit'])) 
{ 
	if($_POST['fname'] == "")
	{
		?>
		<p style="text-align: center; background-color: orange;">
		<?php echo "First Name is a mandatory field"; ?>
		</p>
		<?php
	}
	else if($_POST['lname'] == "")
	{
		?>
		<p style="text-align: center;;background-color: orange;">
		<?php echo "Last Name is a mandatory field"; ?>
		</p>
		<?php
	}
	else if($_POST['year'] == "")
	{
		?>
		<p style="text-align: center;;background-color: orange;">
		<?php echo "Graduation year is a mandatory field"; ?>
		</p>
		<?php
	}
	else if($_POST['email'] == "")
	{
		?>
		<p style="text-align: center; background-color: orange;">
		<?php echo "Email ID is a mandatory field"; ?>
		</p>
		<?php
	}
	else
	{
		$vfn = $_POST['fname'];
		$vln = $_POST['lname'];
		$vem = $_POST['email'];
		$vyr = $_POST['year'];
		$insert = mysql_query("insert into table values('$vfn', '$vln', '$vem', '$vyr')");
		mail($_POST['email'], "Confirmation : AID Clemson Volunteer Registration" ,"Thank you for registering as a volunteer with Association for India's development, Clemson University Chapter. We will be in touch soon.", "From: aid@clemson.edu");
		?>
		<p style="margin: 0.5cm 2cm auto 1cm;background-color: #90EE90;"> Thank you for registerting as a volunteer!! A Confirmation email has been sent to the email ID that you provided. Your details are now saved in our records and we will contact you soon!!</p>
		<?php
	}
}
?>
<br>
<br>
</div>
</div>

<div class="footer">
Copyright &copy; 2010 AID Clemson. Design by <a href="mailto:bgovind@clemson.edu">Bharat Kumar Govindaradjou</a>.
</div>

<div id="sidebar">
<h4 style="text-align: center;font-size: 80%;">Wise Words:</h4>

<div id="slideshow">
<div>
<p style="text-align: justify; margin: 0cm 0.25cm auto 0.25cm;font-size: 80%;">"Never doubt that a small group of thoughtful, committed citizens can change the world; indeed, it's the only thing that ever has. "</p>
</div>

<div>
<p style="text-align: justify; margin: 0cm 0.25cm auto 0.25cm;font-size: 80%;">"Strength does not come from physical capacity. It comes from an indomitable will."</p>
</div>

<div>
<p style="text-align: justify; margin: 0cm 0.25cm auto 0.25cm;font-size: 80%;">"You must be the change you wish to see in the world."</p>
</div>

<div>
<p style="text-align: justify; margin: 0cm 0.25cm auto 0.25cm;font-size: 80%;">"The best way to find yourself is to lose yourself in the service of others."</p>
</div>



</div>

<div class="sidebar-nav">
<a href="http://cu-cisa.org" target="_blank">CISA</a>
<a href="http://www.clemson.edu" target="_blank">Clemson University</a>
</div>

<h4 style="text-align:center;font-size: 80%;">Want to help us?</h4>
<div class="rlinks">
<a href="https://aid.people.clemson.edu/team.php#volunteer">Register now to volunteer!!</a><br>
</div>

<h4 style="text-align:center;font-size: 80%;">Meetings:</h4>
<p style="text-align: center;font-size: 80%;">Every Sunday: 3PM - 4PM EST @ LeMans Apartments, Clemson</p>

<div class="rlinks">
<a href="https://aid.people.clemson.edu/admin_login.php" target="_blank">Admin Portal</a><br>
</div>

<div class="fb-page" style="margin: 0.05cm auto auto auto;" data-href="https://www.facebook.com/aidclemsonchapter/" data-width="350px" data-height="250" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/aidclemsonchapter/"><a href="https://www.facebook.com/aidclemsonchapter/">AID Clemson</a></blockquote></div></div>


</div>

</body>
</html>
