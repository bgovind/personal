<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
	session_start();
	include "mysqlClass.inc.php";
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AID Clemson Admin - Meeting Minutes</title>
</head>
<style>
th, td {
    padding: 5px;
}
</style>
<body>
<div>
<p><img src="img/aid-logo-main-site.png" height="110" width="300"/> </p>
</div>
<?php

if(isset($_SESSION['username']))
{	
	?>
	<div style="background-color: #cce6ff;">
	<div style="margin: auto 1cm auto 1cm;">
	<h3>Welcome Admin!!</h3>
	<table>
	<tr>
	<td><a href="home.php">Home</a></td><td><a href="add.php">Admin Settings</a></td><td><a href="volunteer.php">Volunteer List</a></td><td><a href="meeting_mins.php">Meeting Minutes</a></td></tr>
	</table>
	<form method="post" action="">
	<input type="submit" name="logout" value="Logout">
	</form>
	<?php
	if(isset($_POST['logout']))
	{
		session_destroy();
		header('Location: admin_login.php');
	}
	?>
	<br>
	<br>
	<h4>Record Meeting Minutes</h4>
	<br>
	<form method="post" action="meeting_mins.php#message">
	<textarea name="mm" rows="20" cols="100" placeholder="Enter the meeting minutes here."></textarea>
	<table>
	<tr><td>
	Posted by: </td><td><input name="posted_by" type="text"></td></tr>
	<tr><td>
	Send to: </td><td><input name="email" type="email"></td></tr>
	<tr><td>
	</td><td><input type="submit" name="record" value="Record & Publish"></td></tr>
	</table>
	</form>
	<div id="message">
	<?php
	if(isset($_POST['record']))
	{
		if($_POST['mm'] == '' || $_POST['posted_by'] == '' || $_POST['email'] == '')
		{
			echo "Enter meeting minutes with your name and then click Record & Publish button";
		}
		else
		{
			$mom = $_POST['mm'];
			$user = $_POST['posted_by'];
			$on_date = date("Y/m/d");
			$record_mm = mysql_query("insert into table values('$mom', '$user', '$on_date')");
			$subject = "AID Clemson Meeting Minutes - " . $on_date;
			$email = $_POST['email'];
			mail($email, $subject , $_POST['mm'] , "From: aid@clemson.edu");
			?>
			<p style="background-color: #90EE90;">New Meeting minutes has been recorded and a copy has been sent to your email. Please share it.</p>
			<?php
		}
		
	}
	?>
	</div>
	<h4>Get Previous Meeting Minutes</h4>
	<form method="post" action="meeting_mins.php#results">
	<select name="on_date">
	<?php
	$get_dates = mysql_query("select on_date from table");
	while($date_list = mysql_fetch_row($get_dates))
	{
		?>
		<option value="<?php echo $date_list[0]; ?>"><?php echo $date_list[0]; ?></option>
		<?php
	}
	?>
	<input type = "submit" name="go" value="Go">
	</form>
	<div id="results">
	<?php
	if(isset($_POST['go']))
	{
		?>
		<p><b>Results..</b></p>
		<?php
		$od = $_POST['on_date'];
		$get_mm = mysql_query("select * from table where on_date='$od'");
		while($mm_list = mysql_fetch_row($get_mm))
		{
			?>
			<b><?php echo $mm_list[1]; ?></b>&nbsp;on&nbsp;<b><?php echo $mm_list[2]; ?>:</b>
			<p style="text-align: justify; margin: auto 1cm auto 1cm;"><?php echo $mm_list[0]; ?></p>
			<br>
			<?php
		}
		
	}
	?>
	</div>
	</div>
	</div>
	<br>
	<br>
	<br>
	<?php
}
else
{
	header('Location: admin_login.php');
}
?>

</body>
</html>