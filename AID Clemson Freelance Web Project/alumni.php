<!DOCTYPE html>
<?php
	session_start();
	include "mysqlClass.inc.php";
?>

<html> 
<Head>
<link rel="stylesheet" type="text/css" href="stylesheet.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script>
		$(function() {

			$("#slideshow > div:gt(0)").hide();

			setInterval(function() {
			  $('#slideshow > div:first')
			    .fadeOut(1000)
			    .next()
			    .fadeIn(1000)
			    .end()
			    .appendTo('#slideshow');
			},  3000);

		});
</script>
<Title> AID Clemson Chapter </Title>
</Head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "http://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="logo">
<img src="img/front.jpg"></img>
</div>

<div class="nav">
<a class="inactive" href="index.html">Home</a>
<a class="inactive" href="projects.html">Projects</a>
<a class="inactive" href="events.html">Events</a>
<a class="inactive" href="team.php">Volunteers</a>
<a class="inactive" href="donate.html">Donate</a>
<a class="active" href="alumni.php" target="_blank">Alumni Drive</a>
<a class="inactive" href="http://www.aidindia.org" target="_blank">AID.org</a>

</div>

<div class="latest">
<br><br><br><br><br><br><br><br><br>
<h3 style="margin: 0.5cm auto auto 1cm;">Welcome to AID Clemson Alumni Donation Page!!</h3>
<br>
<div style="width:81%; height:450px">
<form action="alumni.php" method="post">
	<p style="margin: 0.5cm 0cm auto 1cm;">AID Clemson has been making tremendous efforts in raising funds for its projects back in India. Thanks to a successful Indian origin alumni base that Clemson University is blessed with, AID Clemson aims to build on the bond that alumni like you have with the university and its community of current volunteers. Help us give back to our society by donating now!! Please fill this form to start with.</p>
	<br>
	<table style="margin: 0.5cm 0cm auto 1cm; text-align: left;">
	<tr><td>First Name: </td><td><input type="text" name="fname"></td></tr>
	<tr><td>Last Name: </td><td><input type="text" name="lname"></td></tr>
	<tr><td>Graduated year: <i>e.g 2005</i></td><td><input type="number" name="year"></td></tr>
	<tr><td>Email: </td><td><input type="email" name="email"></td></tr>
	<tr><td>Phone: <i>123456789 (Max 10 characters)</i></td><td><input type="number" name="phone" maxlength="10"></td></tr>
	<tr><td>Donation amount in $: </td><td><input type="number" name="amount"></td></tr>
	<tr><td></td><td><input name="submit" type="submit" value="Proceed"></td></tr>
	</table>
</form>

<?php
if(isset($_POST['submit'])) 
{ 
	if($_POST['fname'] == "")
	{
		?>
		<p style="text-align: center; background-color: orange;">
		<?php echo "First Name is a mandatory field"; ?>
		</p>
		<?php
	}
	else if($_POST['lname'] == "")
	{
		?>
		<p style="text-align: center;;background-color: orange;">
		<?php echo "Last Name is a mandatory field"; ?>
		</p>
		<?php
	}
	else if($_POST['year'] == "")
	{
		?>
		<p style="text-align: center;;background-color: orange;">
		<?php echo "Graduation year is a mandatory field"; ?>
		</p>
		<?php
	}
	else if($_POST['email'] == "")
	{
		?>
		<p style="text-align: center; background-color: orange;">
		<?php echo "Email ID is a mandatory field"; ?>
		</p>
		<?php
	}
	else if( $_POST['phone'] == "") 
	{
		?>
		<p style="text-align: center; background-color: orange;">
		<?php echo "Phone number id mandatory"; ?>
		</p>
		<?php
	}
	else if($_POST['amount'] == "")
	{
		?>
		<p style="text-align: center; background-color: orange;">
		<?php echo "Enter donation amount";?>
		</p>
		<?php
	}
	else
	{
		$fn = $_POST['fname'];
		$ln = $_POST['lname'];
		$em = $_POST['email'];
		$ph = $_POST['phone'];
		$amt = $_POST['amount'];
		$yr = $_POST['year'];
		$date = date("Y/m/d");
		$insert = mysql_query("insert into table values('$fn', '$ln', '$amt', '$em', '$ph', '$yr', '$date')");
		?>
		<p style="margin: 0.5cm 2cm auto 1cm;background-color: #90EE90;"> Thank you!! Your details are now saved in our records and you will be redirected to the <a href="https://secure.aidindia.org/donate/clemson">AID Secure Donation Page</a> in a few seconds...</p>
		<?php
		header( "refresh:4; url=https://secure.aidindia.org/donate/clemson" ); 
	}
}
?>
</div>

</div>

<div class="footer">
Copyright &copy; 2010 AID Clemson. Design by <a href="mailto:backgroundovind@clemson.edu">Bharat Kumar Govindaradjou</a>.
</div>

<div id="sidebar">
<h4 style="text-align: center;font-size: 80%;">Wise Words:</h4>

<div id="slideshow">
<div>
<p style="text-align: justify; margin: 0cm 0.25cm auto 0.25cm;font-size: 80%;">"Never doubt that a small group of thoughtful, committed citizens can change the world; indeed, it's the only thing that ever has. "</p>
</div>

<div>
<p style="text-align: justify; margin: 0cm 0.25cm auto 0.25cm;font-size: 80%;">"Strength does not come from physical capacity. It comes from an indomitable will."</p>
</div>

<div>
<p style="text-align: justify; margin: 0cm 0.25cm auto 0.25cm;font-size: 80%;">"You must be the change you wish to see in the world."</p>
</div>

<div>
<p style="text-align: justify; margin: 0cm 0.25cm auto 0.25cm;font-size: 80%;">"The best way to find yourself is to lose yourself in the service of others."</p>
</div>



</div>

<div class="sidebar-nav">
<a href="http://cu-cisa.org" target="_blank">CISA</a>
<a href="http://www.clemson.edu" target="_blank">Clemson University</a>
</div>

<h4 style="text-align:center;font-size: 80%;">Want to help us?</h4>
<div class="rlinks">
<a href="https://aid.people.clemson.edu/team.php#volunteer">Register now to volunteer!!</a><br>
</div>

<h4 style="text-align:center;font-size: 80%;">Meetings:</h4>
<p style="text-align: center;font-size: 80%;">Every Sunday: 3PM - 4PM EST @ LeMans Apartments, Clemson</p>

<div class="rlinks">
<a href="https://aid.people.clemson.edu/admin_login.php" target="_blank">Admin Portal</a><br>
</div>

<div class="fb-page" style="margin: 0.05cm auto auto auto;" data-href="https://www.facebook.com/aidclemsonchapter/" data-width="350px" data-height="250" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/aidclemsonchapter/"><a href="https://www.facebook.com/aidclemsonchapter/">AID Clemson</a></blockquote></div></div>


</div>

</body>
</html>
