<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bharat Kumar's Webpage</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  
  <script>
  $(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 800);
          return false;
        }
      }
    });
  });
	</script>
	
	<script>
	
	$('.slide img:gt(0)').hide();
	
	$(window).load(function(){
	$('.image img:gt(0)').hide();

	$('.next').click(function() 
	{
    $('.image img:first-child').fadeOut(1000).next().fadeIn(1000).end().appendTo('.image');
	});

	$('.prev').click(function() {
    $('.image img:first-child').fadeOut(1000);
    $('.image img:last-child').prependTo('.image').fadeOut(1000);
    $('.image img:first-child').fadeIn(1000);
	});
	});

	</script>
  
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
	
	
	.links{color: #154360;}
	
	.links a:hover{background-color: #111;}
   
	
	th, td {
    padding: 10px;
	word-wrap: break-all;
	vertical-align: middle;
	border-bottom: 1px solid #2471A3;
	}
	
	table{	
	table-layout: fixed;
	word-wrap: break-word;
	}
	
	
		
	ul.topnav.menu {
	display: none;
	}

	/* Float the list items side by side */
	ul.topnav li {float: left;}
	
	

	/* Style the links inside the list items */
	ul.topnav li a {
    display: inline-block;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    transition: 0.3s;
	}

	

	/* Hide the list item that contains the link that should open and close the topnav on small screens */
	ul.topnav li.icon {display: none;}
	
	ul.topnav li.menu {display: none;}
	
	
    @media screen and (max-height: 1080px) {
		
	ul.topnav {
    list-style-type: none;
    margin: 0 auto;
	width: 1000px;
    padding: 0;
    overflow: hidden;
    background-color: #333;
	}
	
	h2{text-align: center;}
	
    .row.content {height: auto;}
	
	.large{
	background-color: #333;
	height:48px;
	}
	
	.logo {display: inline-block;}
	
	
	/* Change background color of links on hover */
	ul.topnav li a:hover {background-color: #2471A3; color: black;}
	
	.menu{display: none;}
	
	.container-content{ margin: 10% 10% auto 10%; font-family:verdana; }
	
	.contact{padding-right: 20%; padding-left: 20%;}

    }
	
	@media screen and (max-width: 1080px) {
		
	
	h2{text-align: left;}
	
	.logo {display: none;}
	
	ul.topnav {
    list-style-type: none;
    padding: 0;
	width: 550px;
    overflow: hidden;
    background-color: #333;
	}
		
	ul.topnav li:not(:first-child) {display: none;}
	ul.topnav li.icon {
    display: inline-block;
	color: white;
	}
	
	ul.topnav li.menu{display: none; color:white;}
	
	
	ul.topnav li.menu {
	color:white;
	}

	ul.topnav.responsive {position: relative; background-color: #333;}
	ul.topnav.responsive li.icon {
	position: absolute;
	left: 50px;
    top: 0;
	}
	
	ul.topnav.responsive li {
    float: none;
    display: inline;
	}
	ul.topnav.responsive li a {
    display: block;
    text-align: left;
	}
	
	ul.topnav.responsive li a:hover {background-color: #333 ; color: white;}
	
	.large{
	background-color: #333;
	height:52px;
	}
	
    .row.content {height: auto;}
	  
	.container-content{float: right; margin: 23% 10px auto 10px; }
	  
	.col-sm-9{float: none;}
	
	.vol-header{display: none;}
	
	.contact{padding-right: 2%; padding-left: 2%; vertical-align: middle;}
	  
    }
	
	
	
	.img-effect{
	-moz-box-shadow: 0 0 20px #888;
	-webkit-box-shadow: 0 0 20px #888;
	box-shadow: 0 0 20px #888;
	}
	
	#highlight-img{
	opacity: 1;
	}
	
	#highlight-img img:hover{
	opacity: .65;
	}
	
	.next{
	-moz-box-shadow: 0 0 10px #888;
	-webkit-box-shadow: 0 0 10px #888;
	box-shadow: 0 0 10px #888;
	}
	
	.prev{
	-moz-box-shadow: 0 0 10px #888;
	-webkit-box-shadow: 0 0 10px #888;
	box-shadow: 0 0 10px #888;
	}
	
	textarea, input{
	text-align:center;
	}
	
	.footer {
	position: relative;
	float:none;
	clear:both;
	width:100%;
	bottom:0;	
   	background: #2874a6;
	font-size: 120%;
   	text-align: center;
	padding-top: 10px;
	padding-bottom: 25px;
	height: 70px;
	color: white;
	}
	
	
  </style>
</head>
<body>

<div class="container-fluid">
  <div class="row content">
	<div style="background: white; position: fixed; width: 100%;">
	<h4 style="color: #154360; font-family: Arial, Helvetica, sans-serif; text-align: center;"><b>Welcome to Bharat's Webpage</b></h4>
	<div class="large">
		<ul class="topnav">
		<li class="menu"><a href="#"><b>Menu</b></a></li>
        <li><a href="#section2"><b>Education</b></a></li>
        <li><a href="#section3"><b>Experience</b></a></li>
        <li><a href="#section4"><b>Projects</b></a></li>
		<li><a href="#section5"><b>Technical Skillset</b></a></li>
		<li><a href="#section6"><b>Résumé</b></a></li>
		<li><a href="#section7"><b>Freelance & Volunteering</b></a></li>
		<li><a href="#section8"><b>Awards</b></a></li>
		<li><a href="#section9"><b>Contact</b></a></li>
		<li><a href="#section1"><b>Top</b></a></li>
		<li class="icon">
			<a href="javascript:void(0);" onclick="myFunction()"><img src="img/menu.png" width="23px" height="23px"></img></a>
		</li>
		</ul><br>
	</div>
	</div>
	<script>
	function myFunction() {
    document.getElementsByClassName("topnav")[0].classList.toggle("responsive");
	}
	</script>
		

    <div class="container-content">
	
		<p id="section1" style="text-align: center; "><img style="border-radius: 50%;" class="img-effect" src="img/bgovind.jpg" width="240px" height="260px"></img></p>
	  <hr>
      <p style="width: 100%; text-align: center; font-size: 120%;">Hi! Thanks for visiting my webpage. I am Bharat Kumar Govindaradjou, a highly motivated Computer Science grad from Clemson University's class of 2017. Born in Chennai, a city on the southeastern coast of India, I spent my childhood and my early career there as a software professional. I currently live in the city of Atlanta, Georgia where I am a part of Infor's Retail Technology team located in the ever bustling Midtown area. A passionate software professional eternally in search of real world problems to solve, I always make time for catching the latest in the world of science and technology. I am also a big fan of tradional Indian music, western pop, a movie-buff and a Potterhead!</p>      
      <br>
      <h2 id="section2"><img src="img/grad_cap.JPG" width="65px" height="40px">Education</h2>
	  <hr>
      <table style="width:100%; background:  #EBF5FB;">
	  <tr>
	  <td><a id="highlight-img" href="http://www.clemson.edu/ces/departments/computing/" target="_blank"><img class="logo" src="img/paw.jpg" width="40px" height="40px"></img></a>&nbsp;<b>Clemson University, Clemson, SC</b></td>
	  <td align="center"><b>Master of Science in Computer Science</b></td><td align="right"><b>May 2017</b></td>
	  </tr>
	  <tr>
	  </tr>
	  </table>
	  <br>
	  <table style="width:100%; background:  #EBF5FB;">
	  <tr>
	  <td><a id="highlight-img" href="https://www.annauniv.edu/" target="_blank"><img class="logo" src="img/anna.png" width="40px" height="40px"></img></a>&nbsp;<b>Anna University, Chennai, India</b></td>
	  <td align="center"><b>Bachelor of Engineering in Electrical & Electronics Engineering</b></td><td align="right"><b>April 2013</b></td>
	  </tr>
	  <tr>
	  </tr>
	  </table>
	  
	  <h2 id="section3"><img src="img/exp.png" width="65px" height="60px">Experience</h2>
	  <hr>
	  <table style="width:100%; background:  #f2f3f4;">
	  <tr style="background: #2471A3; border-radius: 10px;color: white;">
	  <td><a id="highlight-img" href="http://www.infor.com/industries/retail/" target="_blank"><img class="logo" src="img/infor.png" width="40px" height="40px"></img></a>&nbsp;<b>Infor, Atlanta, GA</b></td>
	  <td align="center"><b>Software Engineer</b></td><td align="right"><b>June 2017 - Present</b></td>
	  </tr>
	  </table>
	  <table style="width:100%; background: #EBF5FB;">
	  <tr>
	  <td><b>Duties & Responsibilities:</b></td></tr>
	  <tr>
	  <td style="text-align: justify;">
	  New member of Infor's Retail suite of Software Development, Reliability and Production Engineering team located in Midtown Atlanta.
	  </td>
	  </tr>
	  </table>
	  <table style="width:100%; background:  #f2f3f4;">
	  <tr style="background: #2471A3; border-radius: 10px;color: white;">
	  <td><a id="highlight-img" href="https://www.clemson.edu/mobile/" target="_blank"><img class="logo" src="img/paw.jpg" width="40px" height="40px"></img></a>&nbsp;<b>Clemson Univesity, Anderson, SC</b></td>
	  <td align="center"><b>Software Development Intern</b></td><td align="right"><b>Jan 2017 - May 2017</b></td>
	  </tr>
	  </table>
	  <table style="width:100%; background: #EBF5FB;">
	  <tr>
	  <td><b>Duties & Responsibilities:</b></td></tr>
	  <tr>
	  <td style="text-align: justify;">
	  Web and Mobile Development for Clemson Computing & Information Technology - Mobile Innovation Team. Technologies used - Android, ReactJS, Go, PostGres, PHP, JIRA, BitBucket.
	  </td>
	  </tr>
	  </table>
      <table style="width:100%; background:  #f2f3f4;">
	  <tr style="background: #2471A3; border-radius: 10px;color: white;">
	  <td><a id="highlight-img" href="http://us.blackberry.com/home.html" target="_blank"><img class="logo" src="img/bb.png" width="40px" height="40px"></img></a>&nbsp;<b>BlackBerry Limited, Cary, NC</b></td>
	  <td align="center"><b>Software Test Specialist Intern</b></td><td align="right"><b>Aug 2016 - Dec 2016</b></td>
	  </tr>
	  </table>
	  <table style="width:100%; background: #EBF5FB;">
	  <tr>
	  <td><b>Duties & Responsibilities:</b></td></tr>
	  <tr>
	  <td style="text-align: justify;">
	  Automated Test Development using Robot Framework for BlackBerry's Android Devices. Technologies used - Robot/RIDE using Python, Android Studio, Gerrit, JIRA.
	  </td>
	  </tr>
	  <tr>
	  <td>
	  Complete Roles & Responsibilities and Performance Evaluation from BlackBerry :&nbsp;<a href="BharatKumar_Govindaradjou_Fall2016_Coop_Manager_Evaluation.pdf" class="btn btn-success" target="_blank">Download</a>
	  </td>
	  </tr>
	  </table>
	  <table style="width:100%; background:  #f2f3f4;">
	  <tr style="background: #2471A3; border-radius: 10px;color: white;">
	  <td><a id="highlight-img" href="https://www.cognizant.com/quality-engineering-and-assurance" target="_blank"><img class="logo" src="img/cognizant.png" width="40px" height="40px"></img></a>&nbsp;<b>Cognizant QE&A, Chennai, India</b></td>
	  <td align="center"><b>Programmer Analyst</b></td><td align="right"><b>Sept 2013 - May 2015</b></td>
	  </tr>
	  </table>
	  <table style="width:100%; background: #EBF5FB;">
	  <tr>
	  <td><b>Duties & Responsibilities:</b></td></tr>
	  <tr>
	  <td style="text-align: justify;">
	  Test Planning, Test Design & Development, Manual & Automated System Testing, Database Testing, Test Framework Development and Customization, Bug Tracking, Defect Triaging, Risk Management, Project Management, Resource Training, Project Documentation.
	  </td>
	  </tr>
	  <tr>
	  <td>
	  Complete Roles & Responsibilities letter issued by Cognizant Human Resources :&nbsp;<a href="BharatKumarGovindaradjou_CognizantRoles&ResponsibilitiesLetter.pdf" class="btn btn-success" target="_blank">Download</a>
	  </td>
	  </tr>
	  </table>
	  
	  <h2 id="section4"><img src="img/projects.jpg" width="68px" height="62px">Projects</h2>
	  <hr>
	  <button style="width: 100%; text-align: left;background: #2471A3; padding: 5px 5px 5px 5px;" type="button" class="btn-info" data-toggle="collapse" data-target="#project1">
	  <img class="logo" src="img/p.JPG" width="30px" height="30px"></img>&nbsp;<span style="color: white;"><b>MeTube - A Database Driven Multimedia Hosting Website</b></span>
	  <span style="float: right; color: white;"><b>Jan 2016 - Present</b></span></button>
		<div id="project1" class="collapse" style="text-align: justify;width: 100%;padding: 10px 10px 10px 10px; background:  #EBF5FB;">
		<p>MeTube is a database driven multimedia content hosting website on the lines on Youtube. MeTube can host a wide variety of content including Videos, Images and Audio files. Some key highlights of its capabilities are its ability to form word cloud and provide a holistic view of top trending search terms on the website, media recommendation system, categorized playlists. On top of this MeTube provides a social network like platform to chat with other users, add them as contact/friends and get involved in public conversations on topics of interest.</p>
		<p>The website and the database are currently hosted on the School of Computing Lab Machines and is available on the open internet <a href="https://people.cs.clemson.edu/~bgovind" target="_blank">here</a></p>
		<p>Implemented using - HTML, CSS, bootstrap, javascript for the front end and PHP, MySQL on the server side.</p>
		<p>The website is still evolving and will include additional functionalities in the future as current open bugs are fixed.</p>
		<p>Demo: <a id="highlight-img" href="https://people.cs.clemson.edu/~bgovind" target="_blank"><img src="img/demo.png" width="30px" height="30px" /></a> Git Repo: <a id="highlight-img" href="https://bitbucket.org/bgovind/personal/src/f093b2f87d9381273c4c88058029d403512c05c5/MeTube/?at=master" target="_blank"><img src="img/git.png" width="30px" height="30px"/></a></p>
		</div>
	  <button style="width: 100%; text-align: left;background: #2471A3;border-radius: 5px; padding: 5px 5px 5px 5px;" type="button" class="btn-info" data-toggle="collapse" data-target="#project2">
	  <img class="logo" src="img/p.JPG" width="30px" height="30px"></img>&nbsp;<span style="color: white;"><b>XVCL Based Data & Keyword Driven Automated Software Verification Framework</b></span>
	  <span style="float: right; color: white;"><b>Oct 2015 - Dec 2015</b></span></button>
		<div id="project2" class="collapse" style="text-align: justify;width: 100%;padding: 10px 10px 10px 10px; background:  #EBF5FB;">
		<p>The objective of this project was to compare the use of XVCL (XML-based Variant Configuration Language) based data and keyword driven automated framework in the V&V of a simple java based software product line with normal V&V and emphasize that using reusable meta-components reduces V&V cost of a software project.</p>
		<p>Application Development: Eclipse IDE with JAVA, Apache Tomcat 7.0 for development of JSPs.</p>
		<p>Test Framework: XVCL (XML Based Variant Configuration Language), JUnit 4 with Mockito, Selenium Webdriver on Eclipse IDE.</p>
		<p>Demo: <a id="highlight-img" href="https://youtu.be/6uxsuH0Ze9A" target="_blank"><img src="img/demo.png" width="30px" height="30px" /></a> Git Repo: <a id="highlight-img" href="https://bitbucket.org/bgovind/personal/src/2c2a99bac618d09174baa475a95cb33a0f3fe2a5/XVCLBasedTestFramework/" target="_blank"><img src="img/git.png" width="30px" height="30px"/></a></p>
		</div>
	  <button style="width: 100%; text-align: left;background: #2471A3;border-radius: 5px; padding: 5px 5px 5px 5px;" type="button" class="btn-info" data-toggle="collapse" data-target="#project3">
	  <img class="logo" src="img/p.JPG" width="30px" height="30px"></img>&nbsp;<span style="color: white;"><b>Hangman - A Ruby Sinatra based SaaS Application on Heroku</b></span>
	  <span style="float: right; color: white;"><b>June 2016</b></span></button>
		<div id="project3" class="collapse" style="text-align: justify;width: 100%;padding: 10px 10px 10px 10px; background:  #EBF5FB;">
		<p>The objective of this project was to develop a simple SaaS application accessible on the cloud using the Ruby-Sinatra framework.</p>
		<p>A precursor to my upcoming works using the Ruby on Rails framework, this hangman application was developed using the Test Driven Development methodology primarily found in Agile Software Development Model. The Tests were developed using <a href="http://rspec.info/">RSpec</a> and the application developed after test development. Cucumber was used to perform system testing. The application uses random words from a 3rd party service thereby demonstrating a simple version of Servive Oriented Architecture.</p>
		<p>Implementation Details: Ruby, Sinatra, RSpec using Ruby, Cucumber using Ruby, Heroku PaaS, Test Driven Development, SOA</p>
		<p>Demo: <a id="highlight-img" href="https://pure-anchorage-29084.herokuapp.com/new" target="_blank"><img src="img/demo.png" width="30px" height="30px" /></a> Git Repo: <a id="highlight-img" href="https://bitbucket.org/bgovind/personal/src/1d641828aab2aca902d3898f86a0a1a1b6175002/rubyrails/hw-sinatra-saas-hangperson-master/?at=master" target="_blank"><img src="img/git.png" width="30px" height="30px"/></a></p>
		</div>
	  <button style="width: 100%; text-align: left;background: #2471A3;border-radius: 5px; padding: 5px 5px 5px 5px;" type="button" class="btn-info" data-toggle="collapse" data-target="#project4">
	  <img class="logo" src="img/p.JPG" width="30px" height="30px"></img>&nbsp;<span style="color: white;"><b>Android Development</b></span>
	  <span style="float: right; color: white;"><b>Jan 2017 - April 2017</b></span></button>
		<div id="project4" class="collapse" style="text-align: justify;width: 100%;padding: 10px 10px 10px 10px; background:  #EBF5FB;">
		<br></br>
		<p><h4>Project 2: Currency Converter V2.0</h4></p>
		<p>The Currency Converter is an android application that converts USD amount to Indian Rupee (INR), Great Britain Pound (GBP), Euro (EURO) and Chinese Yuan (CYN). The app consists of three pages, Main Landing page, History Page (Version V2.0)and Info Page. The info page can be accessed by using the Info option from overflow menu. The main landing page has two EditText fields (1 to enter USD and editable, 1 to display converted exchange amount and disabled), 4 Buttons (INR, GBP, EUR, CNY & Reset). Based on currency button clicked the app converts the amount in USD to field to equivalent exchange and displays it in a toast and also a non-editable EditText field (Non-editable field is not populated when there is no internet). The History page gives your 10 recent exchnage rates and amounts. You can edit the USD values for individual entries to get an updated Exchange amount for old exchage rate.</p>
		<p><b>Key Features in V2.0:</b></p>
		<p>1. V2.0 now has a history page that gives you 10 of your most recent exchange entries (Read). These are stored in an internal SQLITE DB when you check exchange amount on main page (Create) and you can access them anytime you want, even when you are not connected to internet. You can tap on each USD value to update it (Update) and get an exchange value for old exchange rate that was prevalent on that particular date (Date Column). You can also clear (Delete) your history whenver you wish using Clear History button. </p>
		<p>2. The app uses realtime exchange values from "fixer.io" API webservice and obtains latest currency exchange values in JSON and parses the JSON objects to get exchange amount when connected to internet.</p>
		<p>3. When there is no internet the app uses approximate default exchange value for each currency and displays the exchange value along with a message to user that internet connectivity is needed for latest accurate exchange values.</p>
		<p><b>Acknowledgements: </b></p>
		<p>1. stackoverflow.com & developer.android.com - Using JSON from webserive to get realtime Currency Exchange values and parse JSON data in Async Task.</p>
		<p>2. fixer.io - API used to get JSON data of currency exchange values. </p>
		<p>3. stackoverflow.com - for dynamically updating layout object IDs to update table from multiple activities.</p>
		<p>Click <a id="highlight-img" href="android682/android.html#project2" target="_blank">here</a> to view screenshots from Currency Converter V2.0. The Android Project Zip is available <a id="highlight-img" href="android682/2/bgovind.a2.zip" target="_blank">here</a>.<p/>
		<p>Click <a id="highlight-img" href="#" target"_blank">here</a> for Doxygen Documentation. </a>
		<p>Demo: <a id="highlight-img" href="android682/2/CurrencyConverter_v2.apk" target="_blank"><img src="img/demo.png" width="30px" height="30px" /></a> Git Repo: <a id="highlight-img" href="https://bitbucket.org/bgovind/personal/src/33d36ff9352ec6aded53dc21e99df09f8c5954bd/Android_Projects/CurrencyConverter/?at=master" target="_blank"><img src="img/git.png" width="30px" height="30px"/></a></p>
		
		<br></br>
		<p><h4>DB Design: A simple Schema</h4></p>
		<p>A Simple 3 table schema for a database that stores report records of patients in a hospital.</p>
		<p>Click <a href="img/bgovind.a3.schema.JPG" target="_blank">here</a> to download the schema diagram.</p>
		
		<br></br>
		<p><h4>Project 1: Currency Converter V1.0</h4></p>
		<p>The Currency Converter is an android application that converts USD amount to Indian Rupee (INR), Great Britain Pound (GBP), Euro (EURO) and Chinese Yuan (CYN). The app consists of two pages, Main Landing page and Info Page. The info page can be accessed by using the Info option from overflow menu. The main landing page has two EditText fields (1 to enter USD and editable, 1 to display converted exchange amount and disabled), 4 Buttons (INR, GBP, EUR, CNY & Reset). Based on currency button clicked the app converts the amount in USD to field to equivalent exchange and displays it in a toast and also a non-editable EditText field (Non-editable field is not populated when there is no internet).</p>
		<p><b>Key Feature:</b> The app uses realtime exchange values from "fixer.io" API webservice and obtains latest currency exchange values in JSON and parses the JSON objects to get exchange amount when connected to internet.</p>
		<p>When there is no internet the app uses approximate default exchange value for each currency and displays the exchange value along with a message to user that internet connectivity is needed for latest accurate exchange values.</p>
		<p><b>Acknowledgements: </b></p>
		<p>1. stackoverflow.com & developer.android.com - Using JSON from webserive to get realtime Currency Exchange values and parse JSON data in Async Task.</p>
		<p>2. fixer.io - API used to get JSON data of currency exchange values. </p>
		<p>Click <a id="highlight-img" href="android682/android.html" target="_blank">here</a> to view screenshots from Currency Converter V1.0. The Android Project Zip is available <a id="highlight-img" href="android682/1/bgovind.a1.zip" target="_blank">here</a>.<p/>
		<p>Demo: <a id="highlight-img" href="android682/1/CurrencyConverter_v1.apk" target="_blank"><img src="img/demo.png" width="30px" height="30px" /></a> Git Repo: <a id="highlight-img" href="https://bitbucket.org/bgovind/personal/src/23fceb1b7c094c1467045cdf4d0a28d53ce4b01a/Android_Projects/CurrencyConverter/?at=master" target="_blank"><img src="img/git.png" width="30px" height="30px"/></a></p>
		
		
		
		
		
		</div>
		
	  <button style="width: 100%; text-align: left;background: #2471A3;border-radius: 5px; padding: 5px 5px 5px 5px;" type="button" class="btn-info" data-toggle="collapse" data-target="#project5">
	  <img class="logo" src="img/p.JPG" width="30px" height="30px"></img>&nbsp;<span style="color: white;"><b>Data Driven Game Development with C++</b></span>
	  <span style="float: right; color: white;"><b>Jan 2017 - April 2017</b></span></button>
		<div id="project5" class="collapse" style="text-align: justify;width: 100%;padding: 10px 10px 10px 10px; background:  #EBF5FB;">
		<p>A lot of Object Oriented programming using C++ 11 & above with emphasis on design patterns. The singleton, factory, object pooling were used in development of a Quidditch game from the world of Harry Potter 2D game using SDL 2.0 framework. </p>
		<p>Demo: <a id="highlight-img" href="#" target="_blank"><img src="img/demo.png" width="30px" height="30px" /></a> Git Repo: <a id="highlight-img" href="https://bitbucket.org/bgovind/personal/src/ad9360f7a399656d79d1c05b88fea7709aa62d0d/DataDrivenGameDesign_CPP/?at=master" target="_blank"><img src="img/git.png" width="30px" height="30px"/></a></p>
		</div>
	  
	  
	<h2 id="section5"><img src="img/skill.png" width="60px" height="45px">Technical Skillset</h2>
	<hr>
	<table style="width:100%; background: #EBF5FB; text-align: left;">
	<tr>
	<td><b>Programming/Scripting/Web</b></td>
	<td>Java (Test Scripting), C, C++, Python, SQL, VB Scripting, HTML5, CSS3, PHP, Ruby on Rails, Sinatra, JavaScript, jQuery, Bootstrap</td>
	</tr>
	<tr>
	<td><b>Testing</b></td>
	<td>Selenium Webdriver with Java (Using Eclipse IDE), Robot Framework using Python, Cucumber (Behavior Driven Development with Ruby), RSpec (Test Driven Development with Ruby), HP Unified Functional Testing (QTP), SOAP UI, JUnit4, Perfecto Mobile, Appium</td>
	</tr>
	<tr>
	<td><b>Tools & Applications</b></td>
	<td>AWS, HP Quality Center, JIRA, Bugzilla, phpMyAdmin, MySQL Workbench, Hudson & Jenkins CI, Apache Ant (Using Eclipse IDE), Visual Studio Tools for Apache Cordova, Git (Github, Bitbucket, Gerrit)</td>
	</tr>
	<tr>
	<td><b>Project Management & Processes</b></td>
	<td>Microsoft Project, Eclipse Process Framework (for WBS), HP Project & Portfolio Management, Peoplesoft Human Resource Management Applications</td>
	</tr>
	</table>
	
	<h2 id="section6"><img src="img/resume.png" width="60px" height="45px">Résumé</h2>
	<hr>
	<table style="width:100%; background: #EBF5FB; text-align: center;">
	<tr><td><a id="highlight-img" target="_blank" href="BharatKumarGovindaradjou_Resume_Updated.pdf"><img src="img/pdfr.png" width="40px" height="40px"></img></a>&nbsp;&nbsp;<a id="highlight-img" target="_blank" href="BharatKumarGovindaradjou_Resume_Updated.docx"><img src="img/wordr.png" width="40px" height="40px"></a></td></tr>
	</table>
	
	<h2 id="section7"><img src="img/volunteer.png" width="60px" height="45px"><span class="vol-header">Freelance & </span>Volunteering</h2>
	<hr>
	<table style="width:100%; background: #EBF5FB; text-align: justify;">
	<tr>	
	<td width="5%"><a id="highlight-img" href="http://www.clemson.edu/campus-life/campus-services/sds/" target="_blank"><img class="logo" src="img/paw.jpg" width="40px" height="40px"></img></a></td><td>Test Proctor at Clemson University, <a style="text-decoration: none;" href="http://www.clemson.edu/campus-life/campus-services/sds/" target="_blank">Test Proctoring Center</a> of Student Accessibility Services - January 2016 - July 2016.</td></tr>
	<tr><td width="5%"><a id="highlight-img" href="https://aid.people.clemson.edu" target="_blank"><img class="logo" src="img/aid.png" width="40px" height="40px"></img></a></td><td>Webmaster for Clemson University's student chapter of "Association for India's Development" a US based charity organization. I designed and developed the organization's <a style="text-decoration: none;" href="https://aid.people.clemson.edu" target="_blank">webpage</a> using a combination of HTML5-CSS and currently maintaining it. I'm was an active volunteer during my time at Clemson - Oct 2015 to April 2017</td></tr>
	<tr><td width="5%"><a id="highlight-img" href="https://www.cognizant.com" target="_blank"><img class="logo" src="img/cognizant.png" width="40px" height="40px"></img></a></td><td>Former member of Cognizant's Corporate Social Responsibility Arm - "Outreach" that created audio books for visually challenged school children - 2014 - 2015</td></tr>
	</td></tr>
	</table>
	
	<h2 id="section8"><img src="img/award.png" width="60px" height="45px">Awards</h2>
	<hr>
	<table style="width:100%; text-align: center;">
	<tr><td style="border-bottom: none;">

		<div id="slide">
		<div class="image">
		<img class="img-effect" style="width: 50%; height: auto;" src="award/1.PNG"></img>
		<img class="img-effect" style="width: 50%; height: auto;" src="award/2.PNG"></img>
		<img class="img-effect" style="width: 50%; height: auto;" src="award/3.PNG"></img>
		<img class="img-effect" style="width: 50%; height: auto;" src="award/4.PNG"></img>
		</div>
		
		<br>
		
		<div id="highlight-img">
		<img class="prev" src="award/prev.png" width="40px" height="40px" alt="Previous"></img>&nbsp;
		<img class="next" src="award/next.png" width="40px" height="40px" alt="Next"></img> 
		</div>
		</div>
	</td>
	</tr>
	</table>

		
    <h2 id="section9"><img src="img/contact.png" width="50px" height="45px">&nbsp;Contact</h2>
	<hr>

	<table style="width:100%; background: #EBF5FB; text-align: center;">
	<td class="contact">
	<form method="post" action="#section9">
	<div class="form-group">
	Email:&nbsp;<input class="form-control" name="email" type="email" placeholder="abc@xyz.com"/><br>
	Subject:&nbsp;<input class="form-control" name="subject" type="text" placeholder="Type the subject here"/><br>
	Message:&nbsp;<textarea class="form-control" name="message" rows="3" cols="30" placeholder="Type your message here"></textarea><br>
	</div>
	<input class="btn btn-success" type="submit" name="send" value="Send"/>
	</form>
	<?php

	// use wordwrap() if lines are longer than 70 characters
	//$msg = wordwrap($msg,70);

	// send email
	if(isset($_POST['send']))
	{
		if((!empty($_POST['message'])) && (!empty($_POST['subject'])) && (!empty($_POST['email'])))
		{
			mail("bgovind@g.clemson.edu", $_POST['subject'] ,$_POST['message'], "From:" . $_POST['email']);
		?>
			<br>
			<p style="text-align: center; background: green; color: white;"><b><?php echo "Your message has been successfully sent to Bharat.";?></b></p>
		<?php
		}
		else
		{
		?>
			<br>
			<p style="text-align: center;background: red; color: white;"><?php echo "Your Email ID, Subject and Contents are mandatory.";?> </p>
		<?php
		}
	}
	?>
	</td>
	</tr>
	<tr><td style="text-align: center;border-bottom:none;"><b>Mobile : +1(864)-624-3131</b></td></tr>
	<tr><td style="text-align: center;"><b>Email : bgovind@g.clemson.edu</b></td></tr>
	<tr><td style="text-align: center;"><a id="highlight-img" href="http://www.facebook.com/bharatkumar.g.161" target="_blank"><img src="img/fb.png" width="40px" height="40px"/></a>&nbsp;&nbsp;<a id="highlight-img" href="https://www.linkedin.com/in/bharatkg0792" target="_blank"><img src="img/linkedin.png" width="40px" height="40px"/></a>&nbsp;&nbsp;<a id="highlight-img" href="https://www.youtube.com/channel/UCrRl2e-hRTuUThKNavhSdWg" target="_blank"><img src="img/yt.png" width="40px" height="40px"/></a></td></tr>
	</table>
	<br>

	  
      
   </div>
  </div>
</div>

<div class="footer">
<b>&copy; Bharat Kumar Govindaradjou</b>
<br>
  <b>Visitor Count</b>&nbsp;<a href="http://www.digits.net" target="_blank">
    <img src="http://counter.digits.net/?counter={4c50cde7-e5b5-d4e4-1940-431582c0d4d7}&template=simple" 
     alt="Hit Counter by Digits" border="0"  />
  </a>

</div>


</body>
</html>