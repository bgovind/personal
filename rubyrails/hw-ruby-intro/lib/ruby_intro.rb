# When done, submit this entire file to the autograder.

# Part 1

#sum of elements in an array

def sum(arr)
  if arr.empty? == true               #return 0 if array is empty
    return 0
  else
    j=0
    sum =0
    x= arr.length
    x.times do |j| sum = sum + arr[j]
      j+=1
    end
    return sum                        #return sum of elements
  end
end

#sum of the maximum two elements in an array

def max_2_sum(arr)
  if arr.empty? == true               #return 0 if array is empty
    return 0
  elsif arr.length == 1               
    return arr[0]                     #return the element if array has only one element
  else
    arr_sorted = arr.sort
    size = arr_sorted.length
    sum = arr_sorted[size-1] + arr_sorted[size-2]     #return the sum of max elements
    return sum
  end
end

#check sum of pair in an array

def sum_to_n? (arr,n)
  count = 0
  if arr.empty? == true               #return false if the array is empty
    return false
  else
    arr.combination(2).each do |x, y|   #find each pair of combination in given array
    count+=1 if x+y == n                #check if sum of pair equals given "n"
    end
  end
  return count > 0 ? true : false       #return true if even one pair exists
end

# Part 2

#Append a string to the method and print

def hello(name)
  return "Hello, #{name}"
end

#Check if a string starts with a consonant

def starts_with_consonant?(s)
  if s.empty? == true
    return false                        #return false if array is empty
  else
    #return true if first letter matches a consonant
    return s[0].match(/[b-d]|[f-h]|[j-n]|[p-t]|[v-z]/i).nil? ? false : true
  end
end

#check if a given binary number is a multiple of 4

def binary_multiple_of_4?(s)
  #return false if anything other than 0 or 1 exists in the given string
  if (s.empty? == true || s.include?('/[a-z]|[2-9]|[!@$%#&*:;.<>\"/\'/]/i') == true)
    return false
  else
    #return true for 0 and true if the binary number is a multiple of 4
    return s!= "0" ? (s =~ /^[10]*00$/ ? true : false) : true
  end
end

# Part 3

#create a class and get isbn and price of a book in a specific format

class BookInStock
  attr_accessor :isbn
  attr_accessor :price
  def initialize(isbn,price)
    if (isbn.empty? || price < 0 || price == 0)
      raise ArgumentError               #throw ArgumentError if price or isbn is wrong
    else
      @isbn = isbn.to_s
      @price = price.to_f
    end
  end
  
  #display as "$0.00" format
  def price_as_string
    return "$"'%.2f' % @price
  end
end

#EndOfFile