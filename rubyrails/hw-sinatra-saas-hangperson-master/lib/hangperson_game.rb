class HangpersonGame

  # add the necessary class methods, attributes, etc. here
  # to make the tests in spec/hangperson_game_spec.rb pass.

  # Get a word from remote "random word" service

  # def initialize()
  # end
  #attr_accessor :word
  #attr_accessor :guesses
  #attr_accessor :wrong_guesses
  
  def initialize(word)
    @word = word
    @guesses = ""
    @wrong_guesses = ""
  end
  
  def word
    @word
  end
  
  def guesses
    @guesses
  end
  
  def wrong_guesses
    @wrong_guesses
  end

  #get random word from service
  def self.get_random_word
    require 'uri'
    require 'net/http'
    uri = URI('http://watchout4snakes.com/wo4snakes/Random/RandomWord')
    Net::HTTP.post_form(uri ,{}).body
  end
  
  def guess(letter)
    #Raise ArgumentError for incorrect values like numbers, special characters and blanks
    if letter == '' or letter == nil or /[^A-Za-z]/.match(letter) != nil
      raise ArgumentError
    else
      char = letter.downcase
      if @guesses.include?(char) or @wrong_guesses.include?(char)
        return false
      end
      if @word.include?(char)
        @guesses = @guesses + letter
      else
        @wrong_guesses = @wrong_guesses + letter
      end
    end
  end
  
  #Build word based on guess
  def word_with_guesses
    @word.gsub(/[^ #{@guesses}]/, '-')
  end
  
  #return game result
  def check_win_or_lose
    return :win if word_with_guesses == @word
    return :lose if @wrong_guesses.length >= 7
  end

end