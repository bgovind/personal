FactoryGirl.define do
  factory :movie do
    title 'title' # default values
    rating 'PG'
    release_date { 10.years.ago }
    director nil
  end
end