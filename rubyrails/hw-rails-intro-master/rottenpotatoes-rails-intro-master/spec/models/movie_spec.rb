require 'spec_helper'

describe Movie do
  describe 'finding similar movies' do
    it "should find movies that have same director" do
      movie1 = FactoryGirl.build(:movie, :id => 1, :director => 'lol')
      movie2 = FactoryGirl.build(:movie, :id => 2, :director => 'notlol')
      movie3 = FactoryGirl.build(:movie, :id => 3, :director => 'lol')
      movie4 = FactoryGirl.build(:movie, :id => 4, :director => nil)
      movies = Movie.similar_movies(1)
      expect(movies).to eq([movie1, movie3])
    end
  end
end
