require 'rails_helper'

describe MoviesController do

  describe 'finding similar movies' do
    it 'should generate RESTful route for Find Similar Movies' do
        expect(:get => "/search_directors/1").to route_to(
          :controller => "movies",
          :action => "search_directors",
          :id => "1")
    end
  
    it 'should call the controller method with correct id' do
      expect(controller).to receive(:search_directors)
      get :search_directors, :id => '1'
      expect(controller.params[:id]).to eql('1')
    end
    
    it "should call model method if movie has a director" do
      expect(Movie).to receive(:similar_movies).with('1')
      get :search_directors, :id => '1'
    end

    it "should not call model method if movie does not has a director" do
      expect(Movie).to_not receive(:similar_movies).with('2')
      get :search_directors, :id => '2'
    end
    
  end
end