Feature: create and destroy movies

  As a movie buff
  So that I can add and remove movies
  I want to create and destroy movies from the database

Background: movies in database

  Given the following movies exist:
  | title        | rating | director     | release_date |
  | Star Wars    | PG     | George Lucas |   1977-05-25 |
  | Blade Runner | PG     | Ridley Scott |   1982-06-25 |
  | Alien        | R      |              |   1979-05-25 |
  | THX-1138     | R      | George Lucas |   1971-03-11 |

Scenario: add new movie
  Given I am on the add new movie page
  When I fill in "Title" with "Alien2"
  And I press "Save Changes"
  Then I should be on the RottenPotatoes home page
  And I should see "Alien2"
  
Scenario: delete a movie
  Given I am on the details page for "Alien"
  When I press "Delete"
  Then I should be on the RottenPotatoes home page
  And I should not see "Alien2"