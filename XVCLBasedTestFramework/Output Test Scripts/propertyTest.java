

//RetailTest
package com.example.tests;

import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;

import java.io.FileInputStream;
import jxl.Sheet;
import jxl.Workbook;

public class propertyTest
{
   
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();


  @Before
  public void setUp() throws Exception 
  {
    driver = new ChromeDriver();
    baseUrl = "http://localhost:8080/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  //Test Case 1 - Property Billing
  @Test
  public void property_main() throws Exception 
  {
	  //Load the test data from Excel for Data driven testing
	  FileInputStream fs = new FileInputStream("E:/testdata.xls");
	  Workbook wb = Workbook.getWorkbook(fs);
	  Sheet sh = wb.getSheet("Property_main");
	  
	  //Execute the test steps
	  driver.get(baseUrl + "/ProductLineBillingSystem/index.jsp");
	    driver.findElement(By.xpath("(//input[@name='radiosgroup'])[2]")).click();
	    driver.findElement(By.name("submit")).click();
	    driver.findElement(By.name("trancid")).clear();
	    driver.findElement(By.name("trancid")).sendKeys(sh.getCell(0,1).getContents());
	    driver.findElement(By.name("timestmp")).clear();
	    driver.findElement(By.name("timestmp")).sendKeys(sh.getCell(1,1).getContents());
	    driver.findElement(By.name("cshid")).clear();
	    driver.findElement(By.name("cshid")).sendKeys(sh.getCell(2,1).getContents());
	    driver.findElement(By.name("cardtype")).clear();
	    driver.findElement(By.name("cardtype")).sendKeys(sh.getCell(3,1).getContents());
	    driver.findElement(By.name("cardno")).clear();
	    driver.findElement(By.name("cardno")).sendKeys(sh.getCell(4,1).getContents());
	    driver.findElement(By.xpath(sh.getCell(5,1).getContents())).click();
	    driver.findElement(By.name("cdcardno")).clear();
	    driver.findElement(By.name("cdcardno")).sendKeys(sh.getCell(6,1).getContents());
	    for(int i=1;i<=sh.getRows()-1;i++)
	    {
	    driver.findElement(By.name("item"+i)).clear();
	    driver.findElement(By.name("item"+i)).sendKeys(sh.getCell(7,i).getContents());
	    driver.findElement(By.name("name"+i)).clear();
	    driver.findElement(By.name("name"+i)).sendKeys(sh.getCell(8,i).getContents());
	    driver.findElement(By.name("qty"+i)).clear();
	    driver.findElement(By.name("qty"+i)).sendKeys(sh.getCell(9,i).getContents());
	    driver.findElement(By.name("price"+i)).clear();
	    driver.findElement(By.name("price"+i)).sendKeys(sh.getCell(10,i).getContents());
	    driver.findElement(By.name("dprice"+i)).clear();
	    driver.findElement(By.name("dprice"+i)).sendKeys(sh.getCell(11,i).getContents());
	    }
	    
	    driver.findElement(By.name("Submit")).click();
	    String subtotal, propertytax, esttax, total;
	    
	    subtotal = driver.findElement(By.name("subtotal")).getAttribute("value");
	    assertTrue(subtotal.contains(sh.getCell(12,1).getContents()));
	    propertytax = driver.findElement(By.name("servicetaxval")).getAttribute("value");
	    assertTrue(propertytax.contains(sh.getCell(14,1).getContents()));
	    esttax = driver.findElement(By.name("esttaxval")).getAttribute("value");
	    assertTrue(esttax.contains(sh.getCell(16,1).getContents()));
	    total = driver.findElement(By.name("total")).getAttribute("value");
	    assertTrue(total.contains(sh.getCell(17,1).getContents()));
	    
	    driver.findElement(By.xpath("//input[@value='Print']")).click();	    

  }
  
  //Test Case 2
  @Test
  public void property_validations() throws Exception
  {
	  //Load the test data from Excel for Data driven testing
	  FileInputStream fs = new FileInputStream("E:/testdata.xls");
	  Workbook wb = Workbook.getWorkbook(fs);
	  Sheet sh = wb.getSheet("Property_validations");
	  Sheet val = wb.getSheet("Validation_msg");
	  //Execute Test Steps
	  driver.get(baseUrl + "/ProductLineBillingSystem/index.jsp");
	    driver.findElement(By.xpath("(//input[@name='radiosgroup'])[2]")).click(); 
	    driver.findElement(By.name("submit")).click();
	    driver.findElement(By.name("trancid")).clear();
	    driver.findElement(By.name("Submit")).click();
	    assertEquals(val.getCell(2,1).getContents(), closeAlertAndGetItsText());
	    driver.findElement(By.name("trancid")).sendKeys(sh.getCell(0,1).getContents());
	    driver.findElement(By.name("timestmp")).clear();
	    driver.findElement(By.name("timestmp")).sendKeys(sh.getCell(1,1).getContents());
	    driver.findElement(By.name("cshid")).clear();
	    assertEquals(val.getCell(2,2).getContents(), closeAlertAndGetItsText());
	    driver.findElement(By.name("timestmp")).sendKeys(sh.getCell(2,1).getContents());
	    driver.findElement(By.name("cshid")).clear();
	    driver.findElement(By.name("cshid")).sendKeys(sh.getCell(3,1).getContents());
	    assertEquals(val.getCell(2,3).getContents(), closeAlertAndGetItsText());
	    driver.findElement(By.name("cshid")).sendKeys(sh.getCell(4,1).getContents());
	    driver.findElement(By.name("cardtype")).clear();
	    driver.findElement(By.name("Submit")).click();
	    assertEquals(val.getCell(2,4).getContents(), closeAlertAndGetItsText());
	    driver.findElement(By.name("cardtype")).sendKeys(sh.getCell(5,1).getContents());
	    driver.findElement(By.name("cardno")).clear();
	    driver.findElement(By.name("Submit")).click();
	    assertEquals(val.getCell(2,5).getContents(), closeAlertAndGetItsText());
	    driver.findElement(By.name("cardno")).sendKeys(sh.getCell(6,1).getContents());
	    assertEquals(val.getCell(2,6).getContents(), closeAlertAndGetItsText());
	    driver.findElement(By.name("cardno")).sendKeys(sh.getCell(7,1).getContents());
	    driver.findElement(By.xpath(sh.getCell(8,1).getContents())).click();
	    driver.findElement(By.name("Submit")).click();
	    assertEquals(val.getCell(2,7).getContents(), closeAlertAndGetItsText());
	    driver.findElement(By.name("cdcardno")).clear();
	    driver.findElement(By.name("cdcardno")).sendKeys(sh.getCell(11,1).getContents());
	   
	    driver.findElement(By.name("Submit")).click();
	    
	    driver.findElement(By.name("cdcardno")).clear();
	    driver.findElement(By.xpath(sh.getCell(9,1).getContents())).click();
	    driver.findElement(By.name("Submit")).click();
	    assertEquals(val.getCell(2,7).getContents(), closeAlertAndGetItsText());
	    driver.findElement(By.name("cdcardno")).clear();
	    driver.findElement(By.name("cdcardno")).sendKeys(sh.getCell(11,1).getContents());
	    driver.findElement(By.name("Submit")).click();
	    
	    driver.findElement(By.xpath(sh.getCell(10,1).getContents())).click();
	    driver.findElement(By.name("cdcardno")).clear();
	    driver.findElement(By.name("Submit")).click();	    
    
  }
  
 
  @After
  public void tearDown() throws Exception 
  {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) 
    {
      fail(verificationErrorString);
    }
  }
  

  private boolean isElementPresent(By by) 
  {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) 
    {
      return false;
    }
  }

  private boolean isAlertPresent() 
  {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) 
    {
      return false;
    }
  }

  private String closeAlertAndGetItsText() 
  {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
  
  private int AlertpresentandClose()
  {
	  Alert alert = driver.switchTo().alert();
	  try {
	      alert.dismiss();
	      return 1;
	    } catch (NoAlertPresentException e) 
	    {
	      return 0;
	    }  
  }
}







