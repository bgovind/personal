package com.example.tests;

import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class tcgen {
	
	private WebDriver driver;
	  private String baseUrl;
	  private boolean acceptNextAlert = true;
	  private StringBuffer verificationErrors = new StringBuffer();
	  
	  @Before
	  public void setUp() throws Exception 
	  {
	    driver = new ChromeDriver();
	    baseUrl = "http://localhost:8080/";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  }

	@Test
	public void test() throws BiffException, IOException 
	{
		FileInputStream fs = new FileInputStream("E:/testdata.xls");
		  Workbook wb = Workbook.getWorkbook(fs);
		  Sheet sh = wb.getSheet("new");
		  driver.get(baseUrl + "/ProductLineBillingSystem/index.jsp");
		  for(int i=1;i<=sh.getRows()-1;i++)
		  {
			  String action = sh.getCell(1,i).getContents();
			  String obj= sh.getCell(2,i).getContents();
			  String data=sh.getCell(3,i).getContents();
			  execute(i,action, obj, data,sh);
			  
			  boolean alertstatus=isAlertPresent();
		  
			  if(alertstatus==true)
		  {
			  assertEquals(sh.getCell(4,i).getContents(), closeAlertAndGetItsText());
			  
		  }
		  }
	}
	public void execute(int i, String action, String obj, String data, Sheet sh)
	{
		if (action.equalsIgnoreCase("click"))
		{
			driver.findElement(By.name(obj)).click();
		}
		else if (action.equalsIgnoreCase("Enter data"))
		{
			driver.findElement(By.name(obj)).sendKeys(data);
		}
		else if (action.equals("clear"))
		{
			driver.findElement(By.name(obj)).clear();
		}
		else if(action.equals("radio"))
		{
			driver.findElement(By.xpath(obj)).click();
			 
		}
		else if(action.equals("checkvalue"))
		{
			String val = driver.findElement(By.name(obj)).getAttribute("value");
		    assertTrue(val.contains(sh.getCell(4,i).getContents()));
		}
		else if(action.equals("Print"))
		{
			driver.findElement(By.xpath(obj)).click();
		}
	}
	
	private String closeAlertAndGetItsText() 
	  {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  
	   }
	
	private boolean isAlertPresent() 
	  {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) 
	    {
	      return false;
	    }
	  }

}
