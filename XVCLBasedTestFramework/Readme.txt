XVCL Based Data and Keyword Driven Automated Test Framework
 
CPSC 873 Software Verification Validation and Measurement - Fall 2015

Highlights of the Framework:

1.Usage of XVCL for generating core assets – System Test scripts
2.Data Driven
3.Action Driven
4.Automated
5.Easier to incorporate requirement changes

About XVCL:
1.Is a meta-programming technique
2.Effective reuse mechanisms
3.Uses “composition with adaptation” rules to generate a specific program from generic, reusable meta-components.

Technologies & Tools used:
1.Eclipse IDE
2.JUnit 4 for Selenium
3.JSP
4.XML for configuration of meta components using XVCL
5.Microsoft Excel

Repo Contents:

edu.xvcl.core_3.2.3_bin - XVCL Processor
ProductLineBillingSystem - JSP Based Simple Online Billing System
workspace - Eclipse IDE Project Workspace
XVCL Meta Components - Contains Meta components which when given as input to XVCL processor provide output automated test script
testdata.xls - contains user defined keyword, actions and data that drive the test script
PPT

View Demo at https://www.youtube.com/watch?v=6uxsuH0Ze9A 