<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<SCRIPT LANGUAGE= "javascript">
function mycal(){
	var frm = document.forms[0];
	//Begin general details validation->Not empty
	if(frm.trancid.value==null || frm.trancid.value==''){
		alert("Please enter Trnsaction ID.")
		return false;
	}
	if(frm.timestmp.value==null || frm.timestmp.value==''){
		alert("Please enter Time Stamp.")
		return false;
	}
	if(frm.cshid.value==null || frm.cshid.value==''){
		alert("Please enter Cashier's ID.")
		return false;
	}
	if(frm.cardtype.value==null || frm.cardtype.value==''){
		alert("Please enter ID Card Type.")
		return false;
	}
	if(frm.cardno.value==null || frm.cardno.value==''){
		alert("Please enter ID Card Number.")
		return false;
	}
	
	//Vlidation for payment card no in case of debit and credit only
	var radios = document.getElementsByName('paymenttype');
	for (var i = 0, length = radios.length; i < length; i++) {
	    if (radios[i].checked) {
	        if(radios[i].value=='c' || radios[i].value=='d'){
	        	if(frm.cdcardno.value==null || frm.cdcardno.value==''){
	        		alert("Please enter Payment Card Number.")
	        		return false;
	        	}
	        }
	        
	    }
	}
	//End of general vlidation
	//Begin validation for name, qty, price value based on item no.
	if(frm.item1.value!=null && frm.item1.value!=''){
		if(frm.name1.value!=null && frm.name1.value!=''){	
		}
		else{
			alert("Please enter first item's name.");
			return false;
		}
		if(frm.qty1.value!=null && frm.qty1.value!=''){	
		}
		else{
			alert("Please enter first item's quantity");
			return false;
		}
		if(frm.price1.value!=null && frm.price1.value!='' ){	
		}
		else{
			alert("Please enter first item's price.");
			return false;
		}
	}
	
	if(frm.item2.value!=null && frm.item2.value!=''){
		if(frm.name2.value!=null && frm.name2.value!=''){	
		}
		else{
			alert("Please enter second item's name.");
			return false;
		}
		if(frm.qty2.value!=null && frm.qty2.value!=''){	
		}
		else{
			alert("Please enter second item's quantity");
			return false;
		}
		if(frm.price2.value!=null && frm.price2.value!=''){	
		}
		else{
			alert("Please enter second item's price.");
			return false;
		}
	}
	
	if(frm.item3.value!=null && frm.item3.value!=''){
		if(frm.name3.value!=null && frm.name3.value!=''){	
		}
		else{
			alert("Please enter third item's name.");
			return false;
		}
		if(frm.qty3.value!=null && frm.qty3.value!=''){	
		}
		else{
			alert("Please enter third item's quantity");
			return false;
		}
		if(frm.price3.value!=null && frm.price3.value!=''){	
		}
		else{
			alert("Please enter third item's price.");
			return false;
		}
	}
	
	if(frm.item4.value!=null && frm.item4.value!=''){
		if(frm.name4.value!=null && frm.name4.value!=''){	
		}
		else{
			alert("Please enter fourth item's name.");
			return false;
		}
		if(frm.qty4.value!=null && frm.qty4.value!=''){	
		}
		else{
			alert("Please enter fourth item's quantity");
			return false;
		}
		if(frm.price4.value!=null && frm.price4.value!=''){	
		}
		else{
			alert("Please enter fourth item's price.");
			return false;
		}
	}
		

	if(frm.item5.value!=null && frm.item5.value!=''){
		if(frm.name5.value!=null  && frm.name5.value!=''){	
		}
		else{
			alert("Please enter fifth item's name.");
			return false;
		}
		if(frm.qty5.value!=null  && frm.qty5.value!=''){	
		}
		else{
			alert("Please enter fifth item's quantity");
			return false;
		}
		if(frm.price5.value!=null && frm.price5.value!=''){	
		}
		else{
			alert("Please enter fifth item's price.");
			return false;
		}
	}
	//Ends validation for name, qty, price based on item number.	
//Begin validation for discounted value based on original price.
	if(frm.price1.value!=null){
		if(frm.dprice1.value==null ){
			alert("Please enter the Discounted price for first item.");
			return false;
		}
	}
	if(frm.price2.value!=null){
		if(frm.dprice2.value==null){
			alert("Please enter the Discounted price for second item.");
			return false;
		}
	}
	
	if(frm.price3.value!=null){
		if(frm.dprice3.value==null ){
			alert("Please enter the Discounted price for third item.");
			return false;
		}
	}
	
	if(frm.price4.value!=null){
		if(frm.dprice4.value==null ){
			alert("Please enter the Discounted price for fourth item.");
			return false;
		}
	}
	if(frm.price5.value!=null){
		if(frm.dprice5.value==null){
			alert("Please enter the Discounted price for fifth item.");
			return false;
		}
	}
	//Ends validation for discounted value based on original price.	
	//Begin normalizing other remaining fields taht are used for calculations.
	if(frm.price1.value==null || frm.price1.value==0){
		frm.dprice1.value=0; 
		frm.price1.value=0;
		frm.qty1.value=0;
	}
	
	if(frm.price2.value==null || frm.price2.value==0){
		frm.dprice2.value=0; 
		frm.price2.value=0;
		frm.qty2.value=0;
	}
	
	if(frm.price3.value==null || frm.price3.value==0){
		frm.dprice3.value=0; 
		frm.price3.value=0;
		frm.qty3.value=0;
	}
	
	if(frm.price4.value==null || frm.price4.value==0){
		frm.dprice4.value=0; 
		frm.price4.value=0;
		frm.qty4.value=0;
	}
	
	if(frm.price5.value==null || frm.price5.value==0){
		frm.dprice5.value=0; 
		frm.price5.value=0;
		frm.qty5.value=0;
	}
	//Ends normalizing other remaining fields that are used for calculations.
	
	//alert(frm.dprice1.value+"|"+frm.price1.value);
	if(frm.price1.value!=null && frm.price1.value!='' && frm.dprice1.value!=null && frm.dprice1.value!=''){
	if(eval(frm.dprice1.value)>eval(frm.price1.value)){
		alert("Discounted Price for first item can't be more than the Original price.");
		return false;
	}
	}
	if(frm.price2.value!=null && frm.price2.value!='' && frm.dprice2.value!=null && frm.dprice2.value!=''){
	if(eval(frm.dprice2.value)>eval(frm.price2.value)){
		alert("Discounted Price for second item can't be more than the Original price.");
		return false;
	}
	}
	if(frm.price3.value!=null && frm.price3.value!='' && frm.dprice3.value!=null && frm.dprice3.value!=''){
	if(eval(frm.dprice3.value)>eval(frm.price3.value)){
		alert("Discounted Price for third item can't be more than the Original price.");
		return false;
	}
	}
	if(frm.price4.value!=null && frm.price4.value!='' && frm.dprice4.value!=null && frm.dprice4.value!=''){
	if(eval(frm.dprice4.value)>eval(frm.price4.value)){
		alert("Discounted Price for fourth item can't be more than the Original price.");
		return false;
	}
	}
	if(frm.dprice5.value!=null && frm.dprice5.value!='' && frm.price5.value!=null && frm.price5.value!=''){
	if(eval(frm.dprice5.value)>eval(frm.price5.value)){
		alert("Discounted Price for fifth item can't be more than the Original price.");
		return false;
	}
	}
	
	//Begin calculation for subtotal
	var dp1=eval(frm.dprice1.value*frm.qty1.value);
	var dp2=eval(frm.dprice2.value*frm.qty2.value);
	var dp3=eval(frm.dprice3.value*frm.qty3.value);
	var dp4=eval(frm.dprice4.value*frm.qty4.value);
	var dp5=eval(frm.dprice5.value*frm.qty5.value);
	var subt=eval(dp1)+eval(dp2)+eval(dp3)+eval(dp4)+eval(dp5);
	//alert(subt+"=" +frm.dprice1.value+"+"+frm.dprice2.value+"+"+frm.dprice3.value+"+"+frm.dprice4.value+"+"+frm.dprice5.value);
	frm.subtotal.value=subt;
	//alert(frm.subtotal.value);
	frm.servicetax.value="11%";
	frm.sintax.value="20.5%";
	var totalPriceBeforeDiscount=eval(frm.price1.value*frm.qty1.value)+ eval(frm.price2.value*frm.qty2.value)+eval(frm.price3.value*frm.qty3.value)+eval(frm.price4.value*frm.qty4.value)+eval(frm.price5.value*frm.qty5.value);
	//alert(totalPriceBeforeDiscount);
	frm.savings.value=eval(totalPriceBeforeDiscount)-eval(subt);
	frm.total.value=eval(eval(0.11*subt)+eval(0.205*subt)+subt);
}

function printcmd(){
	window.print();
}

function back(){
	var frm = document.forms[0];
	window.location.replace("index.jsp");
}

//Fields for allow numbers only onkeypress
function isNumber(evt) {
	 var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)){
		        	alert("Please enter numeric values only.");
		            return false;
		        }
		      else
		        return true;
}

//Checking the time format
function checkTimeStamp(){
	var frm = document.forms[0];
	 re = /^\d{1,2}:\d{2}([ap]m)?$/;
	if(frm.timestmp.value != '' && !frm.timestmp.value.match(re)) {
	      alert("Invalid time format.");
	      frm.timestmp.value='';
	      frm.timestmp.focus();
	      return false;
	    }
}

function formatForNumberAmt(nStr) {
	var result = nStr.toFixed(2);
	result += '';
	  x = result.split('.');
	  x1 = x[0];
	  x2 = x.length > 1 ? '.' + x[1] : '';
	  var rgx = /(\d+)(\d{3})/;
	  while (rgx.test(x1)) {
	    x1 = x1.replace(rgx, '$1' + ',' + '$2');
	  }
	  alert(x1+x2);
	  return x1 + x2;
}

</SCRIPT>
</head>
<body>
<h1>Welcome to Product Line for Billing System -Cigarettes/Alcohol Billing</h1>
<form action="Controller" method="post">
<input type="button" value="Back" onclick="back()" >
<input type="button" value="Print" onclick="printcmd()" ><br>
<img src="logo.png" alt="walmart"><br>

405 <br>
College Avenue,<br>
STE 160Clemson, <br>
SC 29631<br>
<br>
Transaction ID:<input type="text" name="trancid" maxlength="15"><br>
Time Stamp:<input type="text" name="timestmp" onchange="checkTimeStamp()" maxlength="5"><br>
Cashier's ID:<input type="text" name="cshid"  maxlength="5" onkeypress="javascript:return isNumber (event)"><br>

Customer's ID/Age Proof Type: <input type="text" name="cardtype" maxlength="10"><br>
Customer's ID/Age Proof Number: <input type="text" name="cardno" onkeypress="javascript:return isNumber (event)"maxlength="16"><br>

Payment Mode:<INPUT TYPE="radio" name="paymenttype" VALUE="d" checked="checked">Debit Card
<INPUT TYPE="radio" name="paymenttype" VALUE="c">Credit Card 
<INPUT TYPE="radio" name="paymenttype" VALUE="csh">Cash<br>
Card No: <input type="text" name="cdcardno" maxlength="16" onkeypress="javascript:return isNumber (event)"><br>

<table>
<tr>
	<td>
		Item No
	</td>
	<td>
		Name
	</td>
	<td>
		Quantity
	</td>
	<td>
		Price
	</td>
	<td>
		Discounted Price
	</td>
</tr>

<tr>
	<td>
		<input type="text" name="item1" onkeypress="javascript:return isNumber (event)" maxlength="8">
	</td>
	<td>
		<input type="text" name="name1" maxlength="25">
	</td>
	<td>
		<input type="text" name="qty1"  onkeypress="javascript:return isNumber (event)" maxlength="5">
	</td>
	<td>
		<input type="text" name="price1"  onchange="return formatForNumberAmt(this)" onkeypress="javascript:return isNumber (event)">
	</td>
	<td>
		<input type="text" name="dprice1"  onchange="return formatForNumberAmt(this)" onkeypress="javascript:return isNumber (event)">
	</td>
</tr>


<tr>
	<td>
		<input type="text" name="item2" onkeypress="javascript:return isNumber (event)" maxlength="8">
	</td>
	<td>
		<input type="text" name="name2" maxlength="25">
	</td>
	<td>
		<input type="text" name="qty2"  onkeypress="javascript:return isNumber (event)" maxlength="5">
	</td>
	<td>
		<input type="text" name="price2"  onchange="return formatForNumberAmt(this)" onkeypress="javascript:return isNumber (event)">
	</td>
	<td>
		<input type="text" name="dprice2"  onchange="return formatForNumberAmt(this)" onkeypress="javascript:return isNumber (event)">
	</td>
</tr>


<tr>
	<td>
		<input type="text" name="item3" onkeypress="javascript:return isNumber (event)" maxlength="8">
	</td>
	<td>
		<input type="text" name="name3" maxlength="25">
	</td>
	<td>
		<input type="text" name="qty3"  onkeypress="javascript:return isNumber (event)" maxlength="5">
	</td>
	<td>
		<input type="text" name="price3"  onchange="return formatForNumberAmt(this)" onkeypress="javascript:return isNumber (event)">
	</td>
	<td>
		<input type="text" name="dprice3"  onchange="return formatForNumberAmt(this)" onkeypress="javascript:return isNumber (event)">
	</td>
</tr>


<tr>
	<td>
		<input type="text" name="item4" onkeypress="javascript:return isNumber (event)" maxlength="8">
	</td>
	<td>
		<input type="text" name="name4" maxlength="25">
	</td>
	<td>
		<input type="text" name="qty4"  onkeypress="javascript:return isNumber (event)" maxlength="5">
	</td>
	<td>
		<input type="text" name="price4"  onchange="return formatForNumberAmt(this)" onkeypress="javascript:return isNumber (event)">
	</td>
	<td>
		<input type="text" name="dprice4"  onchange="return formatForNumberAmt(this)" onkeypress="javascript:return isNumber (event)">
	</td>
</tr>
<tr>
	<td>
		<input type="text" name="item5" onkeypress="javascript:return isNumber (event)" maxlength="8">
	</td>
	<td>
		<input type="text" name="name5" maxlength="25">
	</td>
	<td>
		<input type="text" name="qty5"  onkeypress="javascript:return isNumber (event)" maxlength="5">
	</td>
	<td>
		<input type="text" name="price5"  onchange="return formatForNumberAmt(this)" onkeypress="javascript:return isNumber (event)">
	</td>
	<td>
		<input type="text" name="dprice5"  onchange="return formatForNumberAmt(this)" onkeypress="javascript:return isNumber (event)">
	</td>
</tr>
<tr>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		Sub Total: 
	</td>
	<td>
		<input type="text" name="subtotal">
	</td>
</tr>
<tr>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		Sales Tax:
	</td>
	<td>
		<input type="text" name="servicetax">
	</td>
</tr>
<tr>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		Sin Tax:
	</td>
	<td>
		<input type="text" name="sintax">
	</td>
</tr>
<tr>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		Total: 
	</td>
	<td>
		<input type="text" name="total">
	</td>
</tr>


<tr>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		Your Savings: 
	</td>
	<td>
		<input type="text" name="savings">
	</td>
</tr>
<tr>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		<br>
	</td>
	<td>
		<input type="button" name="Submit" value="submit" onclick="mycal()">
	</td>
</tr>
</table>
</form> 
</body>
</html>