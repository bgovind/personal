XVCL PROCESSOR README
=====================

1. Introduction
---------------
XVCL processor version 3.2.3 enables you to process x-frames.  The processor
is written in Java and uses JDOM parsing framework.  Bugs fixes will be
updated regularly. See the Summary of Changes for additional information
pertaining to this release.


2. Conformance
--------------
This implementation conforms to the "XVCL language specification (version
2.10)".


3. Installation
---------------
To use this software, you need to download and install JRE1.5 or above from
http://www.java.sun.com. JDOM v1.0, DTDParser v1.2.1, log4j v1.2.13 and
commons logging v1.1 used in this software are included in the package.

You shall unzip the XVCL package into a directory where XVCL is to be
installed. Some subdirectories and files are created:

.\docs\               has examples of configuration files/APIs/XVCL Spec
.\lib\                libraries used in this software are located here.
.\default             default DTD for XVCL language.
.\logging.properties  configuration files for logging.
.\processor.prepeties property file required by XVCL Processor.
.\xvcl-plugin.xml     plugin file used by XVCL Processor in standalone mode.
.\xvcl.jar            the XVCL Processor.


4. Running XVCL Processor
-------------------------
After installing the XVCL package, you can run the XVCL processor as follows:

    java -jar xvcl.jar [-B|L|T|V|M] [external variable map] [XVCL SPecifiCation file]

[XVCL specification file] - your specification x-frame (SPC)
For example, if your XVCL installation directory is "c:\xvcl" and your SPC
name is "c:\myxvcl\myspec.xvcl", to run the processor you can input:

    java -jar xvcl.jar c:\myxvcl\myspec.xvcl

NOTE: if XVCL SPecification file is in current working directory, please prefix
"." to the file name. For example,

    java -jar xvcl.jar .\myspec.xvcl

Option -B    makes the processor trim all the white spaces (blank, space,
			 etc.) in the Textual Content around an XVCL command (any command
			 with the exception of <value-of>) are trimmed.

Option -L    facilitates the user to get the ideas on what files the processor
			 creates or modifies during the processing. With option -L, the
			 processor creates a log file listing the names of all the output
			 files (full path names). By default, this file is created in
			 "[dir to SPC]\.xvcl\[SPC name]\output.log".

Option -T    includes the comment line with the name of an x-frame in the
             output emitted from that x-frame.

Option -V    runs processor in validate only mode. The processor will process
             SPC without writing any output to disks.
             
Option -M    supplies an external variable map containing variable definitions
             to the processor. The file is in java properties format
             (variable-name=variable-value). Variables containing non-escape ","
             will be treated as multi-value variables. The evaluation of
             variable value will be deferred. If external variable map location
             is not specified, the default file, "external.map" in project
             folder or classpath, will be used.

For detailed description, please refer to the XVCL specification.


5. Bug Reports and Feedback
---------------------------
To directly submit a bug or request a feature, please email at:
xvcl@comp.nus.edu.sg
or visit the forums at http://xvcl.comp.nus.edu.sg/forum/
