#include <SDL_image.h>
#include <sstream>
#include <string>
#include <iomanip>
#include "hud.h"
#include "gamedata.h"
#include "ioMod.h"
#include "renderContext.h"
#include "clock.h"

Hud& Hud::getInstance() {
  static Hud instance;
  return instance;
}

Hud::~Hud() 
{}

Hud::Hud() : renderer( RenderContext::getInstance()->getRenderer() )
{}


//Draw Hud
void Hud::drawHud() const {
	std::stringstream strm;
	std::stringstream strm2;
	strm << "Seconds: " << Clock::getInstance().getSeconds();
	strm2 << "FPS: " << Clock::getInstance().getFps();
	strm2 << " Avg FPS: " << Clock::getInstance().getAvgFps();
	SDL_Surface *surface = IMG_Load("images/hud_bg2.jpg");
 	if ( !surface ) {
    		throw std::string("Couldn't load ");
  	}

	//Resizable HUD. This routine scales the font size inside the HUD based on the height of the HUD from Game XML ad
        int scaledFontSize = 0;
	int hudHeight = 0;
	if(Gamedata::getInstance().getXmlInt("hud/height") < 150)
	{
		hudHeight = 150;
		scaledFontSize = Gamedata::getInstance().getXmlInt("font/size");
		
	}
	else
	{;
		hudHeight = Gamedata::getInstance().getXmlInt("hud/height");
        	scaledFontSize = Gamedata::getInstance().getXmlInt("font/size")*((Gamedata::getInstance().getXmlInt("hud/height"))/150);
	}
	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);
	int hudWidth = hudHeight+10;
	SDL_Rect dest = {Gamedata::getInstance().getXmlInt("hud/startLoc/x"), Gamedata::getInstance().getXmlInt("hud/startLoc/y"), hudWidth, hudHeight };
  	SDL_RenderCopy(renderer, texture, NULL, &dest);
	
	IOmod::getInstance().writeText("Controls", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+40, Gamedata::getInstance().getXmlInt("hud/startLoc/y")+5, scaledFontSize);
	IOmod::getInstance().writeText("Toggle HUD: F1", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(19*(Gamedata::getInstance().getXmlInt("hud/height"))/150), scaledFontSize);

	IOmod::getInstance().writeText("Pause Game: P", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(33*(Gamedata::getInstance().getXmlInt("hud/height"))/150), scaledFontSize);
										
										
	IOmod::getInstance().writeText("Move Up: W", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(47*(Gamedata::getInstance().getXmlInt("hud/height"))/150), scaledFontSize);
	IOmod::getInstance().writeText("Move Down: S", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(61*(Gamedata::getInstance().getXmlInt("hud/height"))/150), scaledFontSize);
										
 	IOmod::getInstance().writeText("Move Right: D", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(75*(Gamedata::getInstance().getXmlInt("hud/height"))/150), scaledFontSize);
										
	IOmod::getInstance().writeText("Move Left: A", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(89*(Gamedata::getInstance().getXmlInt("hud/height"))/150), scaledFontSize);
										
	IOmod::getInstance().writeText("Toggle Font Color: R", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(103*(Gamedata::getInstance().getXmlInt("hud/height"))/150), scaledFontSize);
										
	IOmod::getInstance().writeText(strm.str(), Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(117*(Gamedata::getInstance().getXmlInt("hud/height"))/150), scaledFontSize);
										
	IOmod::getInstance().writeText(strm2.str(), Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(131*(Gamedata::getInstance().getXmlInt("hud/height"))/150), scaledFontSize);
										
	SDL_DestroyTexture(texture); 
}



  
