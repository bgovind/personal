#ifndef PLAYER__H
#define PLAYER__H
#include <string>
#include <vector>
#include <cmath>
#include "playerdraw.h"

class Player : public Playerdraw {
public:
  Player(const std::string&);
  Player(const Player&);

  virtual void draw() const;
  virtual void update(Uint32 ticks);
  
  virtual const Frame* getFrame() const { 
    return frames[currentFrame]; 
  }
  virtual void right();
  virtual void left();
  virtual void stop();
  virtual void up();
  virtual void down();
  virtual void ne();
  virtual void nw();
  virtual void se();
  virtual void sw();

protected:
  const std::vector<Frame *> frames;

  unsigned currentFrame;
  unsigned numberOfFrames;
  unsigned frameInterval;
  Vector2f initialVelocity;
  const float slowdown;

  float timeSinceLastFrame;
  int worldWidth;
  int worldHeight;
  int frameWidth;
  int frameHeight;

  void advanceFrame(Uint32 ticks);

};
#endif
