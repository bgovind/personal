#include <random>
#include <functional>
#include "scaledSprite.h"
#include "gamedata.h"
#include "renderContext.h"

void ScaledSprite::advanceFrame(Uint32 ticks) 
{
	timeSinceLastFrame += ticks;


	//Flip two way ScaledSprite to move in opposite direction
	if (numberOfFrames == 16 && timeSinceLastFrame > frameInterval) 
	{
		if ( getVelocityX() <= 0)
		{
			if(currentFrame <= numberOfFrames/2)
			{
    				if(currentFrame <= numberOfFrames / 2){ 
                    			currentFrame = (currentFrame + (numberOfFrames / 2) + 1) % numberOfFrames; 
                		} 
				
			}
			else
			{
				currentFrame = (currentFrame + 1) % numberOfFrames; 
    				if(currentFrame <= numberOfFrames / 2){ 
                    			currentFrame = (currentFrame + (numberOfFrames / 2) + 1) % numberOfFrames; 
               			 } 
			}
		}
		else
 		{
			currentFrame = (currentFrame+1) % (numberOfFrames/2);
				
		}
		timeSinceLastFrame = 0;
	}

}

ScaledSprite::ScaledSprite( const std::string& name, const float scaleVel) :
  Drawable(name, 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/startLoc/x"), 
                    Gamedata::getInstance().getXmlInt(name+"/startLoc/y")-(scaleVel*200)), //Provide difference in Y location for each owl 
           Vector2f((Gamedata::getInstance().getXmlInt(name+"/speedX")*(scaleVel+0.4)), //Scale velocity for owls sprites based on scale
                    Gamedata::getInstance().getXmlInt(name+"/speedY")) 
           ),
  frames( RenderContext::getInstance()->getFrames(name) ),

  currentFrame(1),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval")),
  timeSinceLastFrame( 0 ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),
  frameWidth(frames[0]->getWidth()),
  frameHeight(frames[0]->getHeight()),
  scale(1)

{ }

ScaledSprite::ScaledSprite(const ScaledSprite& s) :
  Drawable(s), 
  frames(s.frames),
  currentFrame(s.currentFrame),
  numberOfFrames( s.numberOfFrames ),
  frameInterval( s.frameInterval ),
  timeSinceLastFrame( s.timeSinceLastFrame ),
  worldWidth( s.worldWidth ),
  worldHeight( s.worldHeight ),
  frameWidth( s.frameWidth ),
  frameHeight( s.frameHeight ),
  scale(s.scale)

  { }

inline namespace{
  constexpr float SCALE_EPSILON = 2e-7;
}

void ScaledSprite::draw() const { 
  if(getScale() < SCALE_EPSILON) return;
  frames[currentFrame]->draw(getX(), getY(), scale); 
}

void ScaledSprite::update(Uint32 ticks) { 
  advanceFrame(ticks);

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);

  if ( getY() < 150) {
    setVelocityY( fabs( getVelocityY() ) );
  }
  if ( getY() > (0.8*worldHeight)-(frameHeight*scale)) {
    setVelocityY( -fabs( getVelocityY() ) );
  }

  if ( getX() < 0) {
    setVelocityX( fabs( getVelocityX() ) );
  }
  if ( getX() > worldWidth-(frameWidth*scale)) {
    setVelocityX( -fabs( getVelocityX() ) );
  }  

}

