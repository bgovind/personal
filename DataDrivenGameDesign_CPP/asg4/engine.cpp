#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <algorithm>
#include "sprite.h"
#include "multisprite.h"
#include "multisprite2.h"
#include "scaledSprite.h"
#include "player.h"
#include "gamedata.h"
#include "engine.h"
#include "frameGenerator.h"

class SpriteLess
{
	public:
	bool operator()(const Drawable* lhs, const Drawable* rhs)
	{
		return lhs->getScale() < rhs->getScale();
	}

};

Engine::~Engine() { 
  std::cout << "Terminating program" << std::endl;
 
  for (auto& l : sprites )
  {
	delete l;
  }

  for (auto& n : player)
  {
	delete n;
  }
  for (auto &o : scaledSprites)
  {
	delete o;
  }

}

Engine::Engine() :
  rc( RenderContext::getInstance() ),
  io( IOmod::getInstance() ),
  clock( Clock::getInstance() ),
  renderer( rc->getRenderer() ),

  pitch("pitch", Gamedata::getInstance().getXmlInt("pitch/factor") ),
  cloud("cloud", Gamedata::getInstance().getXmlInt("cloud/factor") ),
  mountain("mountain", Gamedata::getInstance().getXmlInt("mountain/factor") ),

  viewport( Viewport::getInstance() ),
  player(),
  sprites(),
  scaledSprites(),
  hud(Hud::getInstance()),

  currentSprite(-1),
  makeVideo( false ),
  textColorState(false),
  toggleHUD(true),
  flagCount(0),
  keyState(false),
  hudResetUnder10(false)
{
 
  //Create Harry Player multisprite
  player.push_back(new Player("harry"));

  //Create Snitch multisprite
  sprites.push_back(new MultiSprite("snitch"));

  //Dynamically generate flag multisprites based on world size
  flagCount = ((Gamedata::getInstance().getXmlInt("world/width")/1280)*2)+1;
  int flagX = 147;
  for (int i = 1; i <= flagCount; ++i)
  {
	if (i%2 != 0)
	{
        	sprites.push_back( new MultiSprite2("flag", flagX, 19) );
		flagX+=871;
  	}
	else
	{
		sprites.push_back( new MultiSprite2("flag", flagX, 19) );
		flagX+=409;
	}
  }

  //Create Owl Sprites and scale using scale factor from min to max using Mean and SD
  for ( int i = 0; i < Gamedata::getInstance().getXmlInt("owl/count"); ++i ) {
    float scale = Gamedata::getInstance().getRandFloat(0.3, 0.85);
    auto* s = new ScaledSprite("owl", scale);
   
    while(scale < 0.1f) scale = Gamedata::getInstance().getRandFloat(0.3, 0.85);
    s->setScale(scale);
    scaledSprites.push_back(s);
  }
  
  std::vector<Drawable*>::iterator ptr = scaledSprites.begin();
  sort(ptr, scaledSprites.end(), SpriteLess());
  for ( Drawable* sprite : scaledSprites ) {
    Drawable* thisone = dynamic_cast<Drawable*>(sprite);
    if(thisone)
    {
	std::cout << thisone -> getScale() << std::endl;
    }
  }
  
  //Create Bludger Sprites based on count from Game XML
  for(int i = 0 ; i < Gamedata::getInstance().getXmlInt("bludger/count") ; ++i)
  {
  	sprites.push_back( new Sprite("bludger") );
  }
  
  switchSprite();
  std::cout << "Loading complete" << std::endl;
}

void Engine::draw(bool textColorState, bool toggleHUD) const {
  cloud.draw();
  //Draw smallest Owls behind mountains & trees
  for (unsigned i = 0; i < scaledSprites.size()/3; ++i){
    //std::cout<< scaledSprites[i]->getVelocityX() << std::endl;
    
    scaledSprites[i]->draw();
  }
  mountain.draw();
  //Draw mid sized owls behind quidditch pitch
  for (unsigned i = (scaledSprites.size()/3); i < ((scaledSprites.size())*2/3); ++i){

    //std::cout<< scaledSprites[i]->getVelocityX() << std::endl;
    scaledSprites[i]->draw();
  }
  
  pitch.draw();

  clock.calcAvgFps();
  clock.getFps();


  clock.getAvgFps();
  for(auto* s : sprites) s->draw();

  //Draw largest owls
  for (unsigned i = ((scaledSprites.size())*2/3); i < scaledSprites.size(); ++i){

    //std::cout<< scaledSprites[i]->getVelocityX() << std::endl;
    scaledSprites[i]->draw();
  }
  
  //Draw player - Harry
  for(auto* r : player) r->draw();
  
  //Toggle Font color with R Key
  if(textColorState)
  {
   io.writeText(Gamedata::getInstance().getXmlStr("nameTitle"), 10 , 680, {255,0,0,0});
  }

  else
  {
  //io.writeText("Press R to change font color to Red", (Gamedata::getInstance().getXmlInt("view/width")*0.5) - 180 , Gamedata::getInstance().getXmlInt("view/height")/30);
  io.writeText(Gamedata::getInstance().getXmlStr("nameTitle"), 10, 680);
  }

  //Toggle HUD with F1 Key
  if(toggleHUD)
  {
	hud.drawHud();
  }
  else
  {
	//Draw default text when HUD id toggled OFF
 	if(textColorState)
  	{
		io.writeText("Toggle HUD: F1", 10, 10, {255,0,0,0});
		io.writeText("Toggle Font color: R", 10, 30, {255,0,0,0});
	}
	else
	{
		io.writeText("Toggle HUD: F1", 10, 10);
		io.writeText("Toggle Font color: R", 10, 30);
	}
  }

  viewport.draw();

  SDL_RenderPresent(renderer);
}

void Engine::update(Uint32 ticks) {
  for(auto* s : sprites) s->update(ticks);
  for(auto* r : scaledSprites) r->update(ticks);

   cloud.update();
   pitch.update();
   mountain.update();
     
  for(auto* n: player) n->update(ticks);

  viewport.update();
}

void Engine::switchSprite(){
  ++currentSprite;
  currentSprite = currentSprite % player.size();
  Viewport::getInstance().setObjectToTrack(player[currentSprite]);
}

void Engine::play() {
  SDL_Event event;
  const Uint8* keystate;
  bool done = false;

  Uint32 ticks = clock.getElapsedTicks();
  FrameGenerator frameGen;

  //Game Loop
  while ( !done ) 
  {

    //Display HUD for first 10 seconds once game starts. Toggle with F1.
    //Logic to handle toggling HUD F1 within 10 seconds.
    if( clock.getSeconds() <= 10 &&  hudResetUnder10 == false) { toggleHUD = true; }
      else if ( clock.getSeconds() > 10 && !hudResetUnder10){ toggleHUD = false;}
      else if ( toggleHUD == false){ toggleHUD = false;}
      else if ( toggleHUD == true) {toggleHUD = true;}

    while ( SDL_PollEvent(&event) ) 
    {
      keystate = SDL_GetKeyboardState(NULL);
      if (event.type ==  SDL_QUIT) { done = true; break; }
      	
      if(event.type == SDL_KEYUP)
      {
	keyState = false;
      }
      else if(event.type == SDL_KEYDOWN) 
      {
	
        if (keystate[SDL_SCANCODE_ESCAPE] || keystate[SDL_SCANCODE_Q]) 
        {
          done = true;
          break;
        }
        if ( keystate[SDL_SCANCODE_P] ) 
        {
          if ( clock.isPaused() ) clock.unpause();
          else clock.pause();
        }
        if ( keystate[SDL_SCANCODE_V] ) 
        {
          clock.toggleSloMo();
        }
        if ( keystate[SDL_SCANCODE_T] ) 
        {
          switchSprite();
        }
        if (keystate[SDL_SCANCODE_F4] && !makeVideo) 
        {
          std::cout << "Initiating frame capture" << std::endl;
          makeVideo = true;
        }
        else if (keystate[SDL_SCANCODE_F4] && makeVideo) 
        {
          std::cout << "Terminating frame capture" << std::endl;
          makeVideo = false;
        }
	if(keystate[SDL_SCANCODE_R] && !textColorState)
    	{
		textColorState = true;
    	}
    	else if (keystate[SDL_SCANCODE_R] && textColorState) 
    	{
		textColorState = false;
    	}
	if(keystate[SDL_SCANCODE_F1] && toggleHUD && !keyState)
    	{
		hudResetUnder10 = true;
		keyState = true;
		toggleHUD = false;
    	}
    	else if (keystate[SDL_SCANCODE_F1] && !toggleHUD && !keyState) 
    	{
		hudResetUnder10 = true;
		keyState = true;
		toggleHUD = true;
	}

      }
    }
    ticks = clock.getElapsedTicks();

    //Player Control
      
    //Stop player when opposing keys are pressed
    if(keystate[SDL_SCANCODE_A] && keystate[SDL_SCANCODE_D])
    {
       for(auto* h : player) h->stop();	 
    }
    else if (keystate[SDL_SCANCODE_W] && keystate[SDL_SCANCODE_S])
    {
	for(auto* h : player) h->stop();
    }
    
    //Player control key combinations for diagonal movement
    else if (keystate[SDL_SCANCODE_W] && keystate[SDL_SCANCODE_D])
    {
	for(auto* h : player) h->ne();
    }
    else if (keystate[SDL_SCANCODE_W] && keystate[SDL_SCANCODE_A])
    {
	for(auto* h : player) h->nw();
    }
    else if (keystate[SDL_SCANCODE_S] && keystate[SDL_SCANCODE_D])
    {
	for(auto* h : player) h->se();
    }
    else if (keystate[SDL_SCANCODE_S] && keystate[SDL_SCANCODE_A])
    {
	for(auto* h : player) h->sw();
    }

    //Player control up, down, forward, backward
    else if (keystate[SDL_SCANCODE_A])
    {
	for(auto* h : player) h->left();
    }
    else if (keystate[SDL_SCANCODE_D])
    {
	for(auto* h : player) h->right();
    }
    else if (keystate[SDL_SCANCODE_W])
    {
	for(auto* h : player) h->up();
    }
    else if (keystate[SDL_SCANCODE_S])
    {
	for(auto* h : player) h->down();
    }
    
    if ( ticks > 0 ) 
    {
      clock.incrFrame();
      draw(textColorState, toggleHUD);	
      update(ticks);
      if ( makeVideo ) 
      {
        frameGen.makeFrame();
      }
    }
    
  }
}
