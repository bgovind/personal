#include <iostream>
#include <string>
#include <SDL.h>

class Hud {
public:
  static Hud& getInstance();
  ~Hud();
 
  //Draw Hud Function
  void drawHud() const;
  SDL_Renderer* getRenderer() const { return renderer; }
 
private:
  SDL_Renderer* renderer;
  Hud();
  Hud(const Hud&);
  Hud& operator=(const Hud&);
};
