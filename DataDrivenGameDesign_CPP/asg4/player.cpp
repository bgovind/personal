#include "player.h"
#include "gamedata.h"
#include "renderContext.h"

void Player::advanceFrame(Uint32 ticks) 
{
	timeSinceLastFrame += ticks;


	//Item 11: Flip two way Player to move in opposite direction
	if (numberOfFrames == 4 && timeSinceLastFrame > frameInterval) 
	{
		if ( getVelocityX() <= 0)
		{
			if(currentFrame > numberOfFrames/2)
			{
    				currentFrame = (currentFrame+(numberOfFrames/2)+1) % numberOfFrames;
				
			}
			else
			{
    				currentFrame = (currentFrame+1) % numberOfFrames;
			}
		}
		else
 		{

			currentFrame = (currentFrame+1) % (numberOfFrames/2);
				
		}
		timeSinceLastFrame = 0;
	}
	
}

Player::Player( const std::string& name) :
  Playerdraw(name, 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/startLoc/x"), 
                    Gamedata::getInstance().getXmlInt(name+"/startLoc/y")), 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/speedX"),
                    Gamedata::getInstance().getXmlInt(name+"/speedY"))
           ),
  frames( RenderContext::getInstance()->getFrames(name) ),

  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval")),
  initialVelocity(getVelocity()),
  slowdown(0.9),
  timeSinceLastFrame( 0 ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),
  frameWidth(frames[0]->getWidth()),
  frameHeight(frames[0]->getHeight())
{ }

Player::Player(const Player& s) :
  Playerdraw(s), 
  frames(s.frames),
  currentFrame(s.currentFrame),
  numberOfFrames( s.numberOfFrames ),
  frameInterval( s.frameInterval ),
  initialVelocity(s.initialVelocity),
  slowdown(s.slowdown),
  timeSinceLastFrame( s.timeSinceLastFrame ),
  worldWidth( s.worldWidth ),
  worldHeight( s.worldHeight ),
  frameWidth( s.frameWidth ),
  frameHeight( s.frameHeight )
  { }

void Player::draw() const { 
  frames[currentFrame]->draw(getX(), getY());
}

void Player::update(Uint32 ticks) {
  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  	setPosition(getPosition() + incr);
  
  advanceFrame(ticks);
  if ( getY() < 0) {
    setVelocityY( fabs( getVelocityY() ) );
  }
  if ( getY() > worldHeight-frameHeight) {
    setVelocityY( -fabs( getVelocityY() ) );
  }

  if ( getX() < 0) {
    setVelocityX( fabs( getVelocityX() ) );
  }
  if ( getX() > worldWidth-frameWidth) {
    setVelocityX( -fabs( getVelocityX() ) );
  } 

  stop();

}

void Player::stop()
{
	setVelocityX(slowdown * getVelocityX());
	setVelocityY(0);
}

void Player::right()
{
	if(getX() < worldWidth-frameWidth)
	{
		setVelocityX(initialVelocity[0]);
	}

}

void Player::left()
{
	if(getX() > 0)
	{
		setVelocityX(-initialVelocity[0]);
	}
}

void Player::up()
{
	if(getY() > 0)
	{
		setVelocityY(-initialVelocity[0]);
	}
}

void Player::down()
{
	if(getY() < worldHeight - frameHeight)
	{
		setVelocityY(initialVelocity[0]);
	}
}

void Player::ne()
{
	if(getY() > 0 && getX() < worldWidth-frameWidth)
	{
		setVelocityY(-initialVelocity[0]);
		setVelocityX(initialVelocity[0]);
	}
	else if(getY() <= 0 && getX() < worldWidth-frameWidth)
	{
		setVelocityX(initialVelocity[0]);
	}
}

void Player::nw()
{
	if(getY() > 0 && getX() > 0)
	{
		setVelocityY(-initialVelocity[0]);
		setVelocityX(-initialVelocity[0]);
	}
	else if (getY() <= 0 && getX() > 0)
	{
		setVelocityX(-initialVelocity[0]);
	}
}

void Player::se()
{
	if(getY() < worldHeight - frameHeight && getX() < worldWidth-frameWidth)
	{
		setVelocityY(initialVelocity[0]);
		setVelocityX(initialVelocity[0]);
	}
	else if (getY() >= worldHeight - frameHeight && getX() < worldWidth-frameWidth)
	{
		setVelocityX(initialVelocity[0]);
	}
}

void Player::sw()
{
	if(getY() < worldHeight - frameHeight && getX() > 0)
	{
		setVelocityY(initialVelocity[0]);
		setVelocityX(-initialVelocity[0]);
	}
	else if (getY() >= worldHeight - frameHeight && getX() > 0)
	{
		setVelocityX(-initialVelocity[0]);
	}
}
