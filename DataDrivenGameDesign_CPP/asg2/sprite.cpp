#include<iostream>
#include<string>
#include "sprite.h"

Sprite::~Sprite() {

	std::cout<< "Sprite Destroyed" << std::endl;

}

Sprite::Sprite() : winTitle("Quidditch"), xPos(100), yPos(100), winWIDTH(1280), winHEIGHT(720), flag(0), done(false), nKeys(-1), frameTime(0), fps(17u), bgPath("images/axis.png"), window(SDL_CreateWindow(winTitle, xPos, yPos, winWIDTH, winHEIGHT, flag)), renderer(SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC)), background(getTexture()), harry(new Harry(renderer, background, window))
{
	std::cout << "Initialized" << std::endl;

}


void Sprite::start()
{
	
	SDL_Event event;
	
  	const Uint8* keystate;
	//int nKeys=-1;
	//bool done = false;
	while ( !done )

	{
		while (SDL_PollEvent(&event)){
      			if (event.type == SDL_QUIT) {
        			done = true;
      			}
      			keystate = SDL_GetKeyboardState(&nKeys);
      			if (keystate[SDL_SCANCODE_ESCAPE]) { 
        			done = true; 
      			}
			
		}
		frameTime ++;
				
		draw();
	}
	
	delete harry;
	SDL_DestroyTexture(background);
	//SDL_DestroyTexture(harry&);
	SDL_DestroyRenderer(renderer);
  	SDL_DestroyWindow(window);
  	SDL_Quit();
	

}


void Sprite::draw()
{
	
	if ( fps/frameTime == 2)
	{
		frameTime = 0;
		harry->effects();
	}
	harry->draw();
	harry->update();
	//harry.update();
	//SDL_RenderClear(renderer);
	//SDL_RenderCopy(renderer, background, NULL, NULL);
	//SDL_RenderPresent(renderer);

}


SDL_Texture* Sprite::getTexture()
{
  try {
    SDL_Texture *texture = IMG_LoadTexture(renderer, bgPath);
	std::cout<< "image path" << bgPath << std::endl;
 	std::cout<<"Texture created" << std::endl;
    if ( texture == NULL ) {
      throw std::string("Couldn't load ") + bgPath;
    }
    return texture;
  }
  catch( const std::string& msg ) { 
    std::cout << msg << std::endl; 
    std::terminate();
  }
}
	
