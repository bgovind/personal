#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "bludger.h"
#include<string>

Bludger::~Bludger() {
	SDL_DestroyTexture(bludger);
	std::cout<< "Bludger Destroyed" << std::endl;
}

Bludger::Bludger(SDL_Renderer *renderer) : 
bludgerPath("images/bludger.png"), 
imageWidth(420), 
imageHeight(420), 
X_POS(0), 
Y_POS(0), 
xP(0), 
yP(0), 
X_VEL(300.0), 
Y_VEL(300.0),
DT(17u), 
myRenderer(renderer), 
bludger(getBludgerTexture())
{
	std::cout << "Bludger Created" << std::endl;	
}

Bludger::Bludger(const Bludger& bludger2) : 
bludgerPath(bludger2.bludgerPath), 
imageWidth(bludger2.imageWidth), 
imageHeight(bludger2.imageHeight), 
X_POS(bludger2.X_POS), 
Y_POS(bludger2.Y_POS), 
xP(350), 
yP(0), 
X_VEL(250), 
Y_VEL(250),
DT(bludger2.DT), 
myRenderer(bludger2.myRenderer), 
bludger(bludger2.getBludgerTexture())
{
	std::cout << "Bludger2 Created" << std::endl;	
}



Bludger& Bludger::operator=(const Bludger& rhs)
{
	if ( this == &rhs ) return *this;
	myRenderer = rhs.myRenderer;
	std::cout<< "Assignment called" << std::endl;
	return *this;
}

//Add Bludger texture to renderer
void Bludger::draw()
{
	SDL_Rect bSrcRect = {X_POS, Y_POS, imageWidth, imageHeight}; 
	SDL_Rect bDstRect = {xP, yP, 50, 50};

	
	SDL_RenderCopy(myRenderer, bludger, &bSrcRect, &bDstRect);
	
	std::cout<<"Drawing Bludger" << std::endl;
}

//Update Bludger position on renderer
void Bludger::update()
{
	float x = xP;
  	float y = yP;
	
	float dx = X_VEL * DT * 0.001;
  	x += dx;
	float dy = Y_VEL * DT * 0.001;
  	y += dy;

	std::cout << "Updating Bludgers" << std::endl;

	xP = x;
	yP = y;
}

//Get Bludger image to create Bludger Texture
SDL_Texture* Bludger::getBludgerTexture() const
{
  try {
    SDL_Texture *texture = IMG_LoadTexture(myRenderer, bludgerPath);
	std::cout<< "Bludger path" << bludgerPath << std::endl;
 	std::cout<<"Bludger Texture created" << std::endl;
    if ( texture == NULL ) {
      throw std::string("Couldn't load ") + bludgerPath;
    }
    return texture;
  }
  catch( const std::string& msg ) { 
    std::cout << msg << std::endl; 
    std::terminate();
  }
}
