#include<iostream>
#include<string>
#include <cmath>
#include "animation.h"


Animation::~Animation() 
{
	std::cout<< "Animation Destroyed" << std::endl;
}


//Initialize animation along with each individual sprite texture
Animation::Animation() : 
winTitle("Quidditch"), 
xPos(100), yPos(100), 
winWIDTH(1280), 
winHEIGHT(720), 
flag(0), 
done(false), 
nKeys(-1), 
frameTime(0), 
fps(60u), //Cap FPS
bgPath("images/axis.png"), 
makeVideo(false), // Enable/Disable framegen for creating video
window(SDL_CreateWindow(winTitle, xPos, yPos, winWIDTH, winHEIGHT, flag)), 
renderer(SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC)), 
background(getTexture()), 
harry(new Harry(renderer)), 
snitch(new Snitch(renderer, background)), 
shadow(new Shadow(renderer)),
bludger(new Bludger(renderer)),
bludger2(new Bludger(*bludger)),
frameGen(new FrameGenerator(renderer, window))
{
	std::cout << "Animation Initialized" << std::endl;
}


//Game Loop
void Animation::start()
{
	SDL_Event event;
	
  	const Uint8* keystate;

	unsigned int frameCount = 0;
	const int PERIOD = std::ceil(1000.0f/fps);
  	unsigned int startTime = SDL_GetTicks();      
  	int prevTicks = startTime; 

	while ( !done )

	{
		while (SDL_PollEvent(&event)){
      			if (event.type == SDL_QUIT) {
        			done = true;
      			}
      			keystate = SDL_GetKeyboardState(&nKeys);
      			if (keystate[SDL_SCANCODE_ESCAPE]) { 
        			done = true; 
      			}
			if (keystate[SDL_SCANCODE_F4]) { 
       				makeVideo = true;
      			}
			
		}
		int currTicks = SDL_GetTicks();
    		int deltaTicks = currTicks-prevTicks;
    		if(deltaTicks < PERIOD) continue;		

		++frameTime;	
		draw();
	 	prevTicks=currTicks;
    		++frameCount;
	}
	unsigned int totalTicks = SDL_GetTicks() - startTime;
  	std::cout << "fps: "<< frameCount/(totalTicks*0.001) << std::endl;
	
	delete frameGen;
	delete bludger2;
	delete bludger;
	delete shadow;
	delete snitch;
	delete harry;
	
	SDL_DestroyTexture(background);
	SDL_DestroyRenderer(renderer);
  	SDL_DestroyWindow(window);
  	SDL_Quit();
}

//Invoke draw and update for each texture (harry, snitch, shadow and bludgers)
void Animation::draw()
{
	
	if ( makeVideo ) frameGen->makeFrame();	
	if ( fps/frameTime == 4)
	{
		frameTime = 0;
		harry->effects();
		snitch->effects();
	}
	SDL_RenderClear(renderer);
	snitch->draw();
	shadow->draw();
	harry->draw();
	bludger->draw();
	bludger2->draw();
	snitch->update();
	shadow->update();	
	harry->update();
	bludger2->update();
	bludger->update();	
	SDL_RenderPresent(renderer);
}

//Create Background Texture using background image
SDL_Texture* Animation::getTexture()
{
  try {
    SDL_Texture *texture = IMG_LoadTexture(renderer, bgPath);
	std::cout<< "image path" << bgPath << std::endl;
 	std::cout<<"Texture created" << std::endl;
    if ( texture == NULL ) {
      throw std::string("Couldn't load ") + bgPath;
    }
    return texture;
  }
  catch( const std::string& msg ) { 
    std::cout << msg << std::endl; 
    std::terminate();
  }
}
	
