//Bludger
#ifndef BLUDGER_H
#define BLUDGER_H

#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "animation.h"
#include <string>

class Bludger
{
	private:
	private:
	const char* bludgerPath;
	int imageWidth;
	int imageHeight;
	

	int X_POS;
	int Y_POS;

	int xP;
	int yP;

	float X_VEL;
	float Y_VEL;

	const unsigned int DT;

	SDL_Renderer *myRenderer;
	SDL_Texture *bludger;

	SDL_Texture *getBludgerTexture() const;
	
	
	public:

	Bludger(); //Explicitly disallow creation of compiler generated default constructor
	Bludger(SDL_Renderer *myRenderer);
	Bludger(const Bludger& bludger2); //Copy Constructor to make copy of a bludger
	Bludger& operator=(const Bludger& bludger2);
	~Bludger();
	
	void draw();
	void update();

};

#endif
