//Snitch
#ifndef SNITCH_H
#define SNITCH_H

#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "animation.h"
#include <string>

class Snitch
{
	private:
	const char* snitchPath;
	int imageWidth;
	int imageHeight;

	int X_POS;
	int Y_POS;
	int xP;
	int yP;
	float X_VEL;
	float Y_VEL;

	const float val;
	const float lo;
	const float hi;
	const unsigned int DT;

	inline float clampX(const float val, const float lo, const float hi);
	inline float clampY(const float val, const float hi, const float lo);

	SDL_Renderer *myRenderer;
	SDL_Texture *snitch;
	SDL_Texture *mybackground;
	SDL_Texture *getSnitchTexture();
		
	Snitch(const Snitch&);
	Snitch operator = (const Snitch&);

	public:
	Snitch(); //Explicitly disallow creation of compiler generated default constructor
	Snitch(SDL_Renderer *myRenderer, SDL_Texture *mybackground);
	~Snitch();
	
	void effects();
	void draw();
	void update();

};

#endif
	
	

