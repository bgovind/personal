#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "snitch.h"
#include<string>

Snitch::~Snitch() {
	SDL_DestroyTexture(snitch);
	std::cout<< "Snitch Destroyed" << std::endl;
}

Snitch::Snitch(SDL_Renderer *renderer, SDL_Texture *background) : 
snitchPath("images/yellowstar1.png"), 
imageWidth(692), 
imageHeight(562), 
X_POS(0), 
Y_POS(0), 
xP(250), 
yP(470), 
X_VEL(200.0), 
Y_VEL(200.0), 
val(0.0), 
lo(0.0), 
hi(0.0), 
DT(17u),
myRenderer(renderer), 
snitch(getSnitchTexture()),
mybackground(background)
{
	std::cout << "Snitch Created" << std::endl;
}


//Add Snitch texture to renderer
void Snitch::draw()
{
	SDL_Rect sSrcRect = {X_POS, Y_POS, imageWidth, imageHeight}; 
	SDL_Rect sDstRect = {xP, yP, 50, 50};
	SDL_RenderCopy(myRenderer, mybackground, NULL, NULL);
	SDL_RenderCopy(myRenderer, snitch, &sSrcRect, &sDstRect);
	std::cout<<"Drawing snitch" << std::endl;
}

//Update Snitch position on renderer
void Snitch::update()
{
	float x = xP;
  	float y = yP;
	
	float dx = X_VEL * DT * 0.001;
  	x += dx;
	float dy = Y_VEL * DT * 0.001;
  	y -= dy;


	x = clampX(x,0.f,950);
	y = clampY(y,720.f,100);

	xP = x;
	yP = y;

	
}

inline float Snitch::clampX(const float val, const float lo, const float hi)
{
  return val <= lo ? lo : ( val >= hi ? hi : val);
}


inline float Snitch::clampY(const float val, const float hi, const float lo)
{
  return val >= hi ? hi : ( val <= lo ? lo : val);
}

//Change coordinates from sprite input image to create flutter effect
void Snitch::effects()
{
	Y_POS = Y_POS + 562;
	
	if (Y_POS >= 610)
	{
		Y_POS = 0; 
	}

}


//Get Snitch image to create Snitch Texture
SDL_Texture* Snitch::getSnitchTexture()
{
  try {
    SDL_Texture *texture = IMG_LoadTexture(myRenderer, snitchPath);
	std::cout<< "Snitch path" << snitchPath << std::endl;
 	std::cout<<"Snitch Texture created" << std::endl;
    if ( texture == NULL ) {
      throw std::string("Couldn't load ") + snitchPath;
    }
    return texture;
  }
  catch( const std::string& msg ) { 
    std::cout << msg << std::endl; 
    std::terminate();
  }
}




