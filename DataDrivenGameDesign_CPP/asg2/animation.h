//Animation Class.. Starts animation and initializes sprites
#ifndef ANIMATION__H
#define ANIMATION__H

#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "harry.h"
#include "snitch.h"
#include "shadow.h"
#include "bludger.h"
#include "frameGenerator.h"

class Harry;
class Snitch;
class Shadow;
class Bludger;

class Animation
{

	private:
	
	const char* winTitle;
	const int xPos;
	const int yPos;
	const int winWIDTH;
	const int winHEIGHT;
	const int flag;
	bool done;
	int nKeys;
	int frameTime;
	const unsigned int fps;
	const char* bgPath;
	bool makeVideo;
	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_Texture *background;
	SDL_Texture *getTexture();
	bool clock();


	
	Harry *harry;
	Snitch *snitch;
	Shadow *shadow;
	Bludger *bludger;
	Bludger *bludger2;
	FrameGenerator *frameGen;
	void draw();

		
	Animation(const Animation&);
	Animation operator = (const Animation&);

	public:
	

	Animation();
	~Animation();

	void start();
	
};



#endif


	
