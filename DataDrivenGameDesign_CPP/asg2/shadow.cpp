#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "shadow.h"
#include<string>

Shadow::~Shadow() {
	SDL_DestroyTexture(shadow);
	std::cout<< "Shadow Destroyed" << std::endl;
}

Shadow::Shadow(SDL_Renderer *renderer) : 
shadowPath("images/shadow.png"), 
imageWidth(200), 
imageHeight(50), 
X_POS(0), 
Y_POS(0), 
xP(0), 
yP(680), 
X_VEL(210.0), 
Y_VEL(210.0), 
val(0.0), 
lo(0.0), 
hi(0.0), 
DT(17u),
myRenderer(renderer), 
shadow(getShadowTexture())
{
	std::cout << "Shadow Created" << std::endl;
}

//Add Shadow texture to renderer
void Shadow::draw()
{
	SDL_Rect shtRect = {xP, yP, 200, 25};
	SDL_RenderCopy(myRenderer, shadow, NULL, &shtRect);
	std::cout<<"Drawing shadow" << std::endl;
}

//Update Shadow position on renderer
void Shadow::update()
{
	static float x = xP;
  	static float y = yP;
	
	float dx = X_VEL * DT * 0.001;
  	x += dx;

	x = clampX(x,0.f,1000-150);

	xP = x;
	yP = y;
	
}

inline float Shadow::clampX(const float val, const float lo, const float hi)
{
  return val <= lo ? lo : ( val >= hi ? hi : val);
}

//Get Shadow image to create Shadow Texture
SDL_Texture* Shadow::getShadowTexture()
{
  try {
    SDL_Texture *texture = IMG_LoadTexture(myRenderer, shadowPath);
	std::cout<< "Shadow path" << shadowPath << std::endl;
 	std::cout<<"Shadow Texture created" << std::endl;
    if ( texture == NULL ) {
      throw std::string("Couldn't load ") + shadowPath;
    }
    return texture;
  }
  catch( const std::string& msg ) { 
    std::cout << msg << std::endl; 
    std::terminate();
  }
}

