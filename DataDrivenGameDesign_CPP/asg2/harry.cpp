#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "harry.h"
#include<string>

Harry::~Harry() {
	SDL_DestroyTexture(harry);
	std::cout<< "Harry Destroyed" << std::endl;
}

Harry::Harry(SDL_Renderer *renderer) : 
harryPath("images/harry1.png"), 
imageWidth(298), 
imageHeight(300), 
X1_POS(0), 
Y1_POS(0), 
x1P(0), 
y1P(500), 
X1_VEL(210.0), 
Y1_VEL(210.0),  
val(0.0), 
lo(0.0), 
hi(0.0), 
DT(17u), 
myRenderer(renderer), 
harry(getHarryTexture())
{
	std::cout << "Harry Created" << std::endl;	
}

//Add Harry texture to renderer

void Harry::draw()
{
	SDL_Rect hSrcRect = {X1_POS, Y1_POS, imageWidth, imageHeight}; 
	SDL_Rect hDstRect = {x1P, y1P, 175, 200};

	
	SDL_RenderCopy(myRenderer, harry, &hSrcRect, &hDstRect);
	
	std::cout<<"Drawing Harry" << std::endl;
}

//Update Harry position on renderer
void Harry::update()
{
	static float x1 = x1P;
  	static float y1 = y1P;
	
	float dx1 = X1_VEL * DT * 0.001;
  	x1 += dx1;
	float dy1 = Y1_VEL * DT * 0.001;
  	y1 -= dy1;

	x1 = clampX(x1,0.f,1000-150);
	y1 = clampY(y1,550.f,10);

	x1P = x1;
	y1P = y1;
	
}

inline float Harry::clampX(const float val, const float lo, const float hi)
{
  return val <= lo ? lo : ( val >= hi ? hi : val);
}

inline float Harry::clampY(const float val, const float hi, const float lo)
{
  return val >= hi ? hi : ( val <= lo ? lo : val);
}

//Change coordinates from Harry input image to create flutter effect
void Harry::effects()
{
	Y1_POS = Y1_POS + 315;

	if ( Y1_POS >= 600)
	{
		Y1_POS = 0;
	}
}

//Get Harry image to create Harry Texture
SDL_Texture* Harry::getHarryTexture()
{
  try {
    SDL_Texture *texture = IMG_LoadTexture(myRenderer, harryPath);
	std::cout<< "Harry path" << harryPath << std::endl;
 	std::cout<<"Harry Texture created" << std::endl;
    if ( texture == NULL ) {
      throw std::string("Couldn't load ") + harryPath;
    }
    return texture;
  }
  catch( const std::string& msg ) { 
    std::cout << msg << std::endl; 
    std::terminate();
  }
}
