#ifndef HUD__H
#define HUD__H
#include <string>
#include "drawable.h"

class Hud : public Drawable {
public:
  Hud(const std::string&);
  Hud(const Hud&);
  virtual ~Hud() { } 
  Hud& operator=(const Hud&);

  virtual const Frame* getFrame() const { return frame; }
  virtual void draw() const;
  virtual void update(Uint32 ticks);
  SDL_Renderer* getRenderer() const { return renderer; }
private:
  const Frame * frame;
  int worldWidth;
  int worldHeight;
  int getDistance(const Hud*) const;
  SDL_Renderer* renderer;
};
#endif
