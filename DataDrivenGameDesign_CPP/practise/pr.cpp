#include<cstring>
#include<iostream>
class string
{
public:
 string() {std::cout << "default" << std::endl;}
 string(const char*)  {std::cout << "convert" << std::endl;}
 string(const string&)  {std::cout << "copy" << std::endl;}
 ~string() {std::cout << "destroy" << std::endl;}

 string& operator=(const string&)
 {
  std::cout << "assign" << std::endl;
 }
};

void crash(string s) { }
int main()
{
 string x("cat"), y =x;
//crash(y);
//string y;
//y=x;
//string* z = new string("dog");
//delete z;

} 
