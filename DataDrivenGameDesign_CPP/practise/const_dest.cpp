#include <iostream>

class Binary
{
 public:
	Binary() : number(0), myCount(count)
	{
		++count;
		std::cout << "default:" << myCount << std::endl;
	}
	
	Binary(int n) : number(n), myCount(count)
	{
		++count;
		std::cout << "convert:" << myCount << std::endl;
	}
	Binary(const Binary& bin) : number(bin.number), myCount(count)
	{
		++count;
		std::cout << "copy:" << myCount << std::endl;
	}
	~Binary() { std::cout << "destructor:" << myCount << std::endl; }
	
	Binary& operator=(const Binary&)
	{
		std::cout << "assignment" << std::endl;
		return *this;
	}

	int getNumber() const {return number;}

	void increment() { ++number; }

 private:
	int number;
	int myCount;
	static int count;

};

Binary increment (Binary bin)
{
	bin.increment();
	return bin;
}

int Binary::count = 0;

int main()
{
	Binary a(17), b = a;
	b = increment(a);
	//std::cout << b << std::endl;
}


