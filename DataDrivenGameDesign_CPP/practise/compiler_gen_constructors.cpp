#include <iostream>

class Binary 
{
	public:
	int getNumber() const { return number; }
	void setNumber(int n) { number = n; }
	private:
	int number;
};

int main()
{
	Binary a, b = a;
	a.setNumber(19);
	std::cout << b.getNumber() << std::endl;
}
