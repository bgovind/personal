#include<iostream>

class Sum
{
 public:
 static Sum& getInstance()
 {
	//if (!instance) 
        //{instance = new Sum;}
	static Sum instance;
	return instance;
 }
 int operator()(int a, int b) const
	{return a+b;}
 private:
 Sum() {}
 Sum(const Sum&) = delete;
 Sum& operator=(const Sum&) = delete;
 ~Sum() {}
};

//Sum* Sum::getInstance()


int main()
{
 Sum& sum = Sum::getInstance();
 std::cout << sum(3,6) << std::endl;
}
