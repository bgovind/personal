#include<iostream>

class Sum
{
 public:
 static Sum* getInstance()
 {
	if (!instance) 
        {instance = new Sum;}
	return instance;
 }
 int operator()(int a, int b) const
	{return a+b;}
 private:
 static Sum* instance;
 Sum() {}
 Sum(const Sum&) = delete;
 Sum& operator=(const Sum&) = delete;
};

//Sum* Sum::getInstance()


Sum* Sum::instance = nullptr;

int main()
{
 Sum* sum = Sum::getInstance();
 std::cout << (*sum)(3,6) << std::endl;
 delete sum;
}
