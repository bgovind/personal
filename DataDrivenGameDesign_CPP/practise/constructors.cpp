#include <cstdio>
#include <iostream>

using std:: cout;
using std::endl;

class Student
{
	public:
	Student() {cout << "default" << endl;}
	Student(const char *b) {cout <<"convert" <<endl;}
	/*Student& operator = (const Student &){
		cout << "assign" << endl;
		return *this;
	}
*/
	
	private:
	char *buf;
};

class TestStudent
{
	Student str;
	public:
	TestStudent(const char* s)
	{
		str = s;
	}
};

int main()
{
	TestStudent t1("cat");
}
