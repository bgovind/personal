#include <iostream>
#include <functional>
#include <ctime>

// Write:
//   (1) maybe neg
//   (2) incr (param to fun)
//   (3) incr (with global)
//   (4) factorial (non recursive)
//   (5) factorial (recursive)

// [capture clause] (parameters) -> return-type {body}

int main() {
  srand( time(0) );
  int x = rand() % 100;
  int z = 99;

  auto isEven=[](int x)->bool{ return !(x%2);};
  std::cout << "x " << x << ", " << isEven(x) << std::endl;

  auto incr = [](int& x)->int{ return ++x; };
  std::cout << "x is: " << incr(x) << std::endl;
  std::cout << "x still is: " << x << std::endl;

  auto incrz = [&z]()->int{ return ++z; };
  std::cout << "z is: " << incrz() << std::endl;

  auto factorial = [](int x)->int{
    int f = 1;
    for (int i = 2; i <=x; ++i) f *= i;
    return f;
  };
  std::cout << "factorial(5) is: " << factorial(5) << std::endl;

  std::function<int(int)> fact;
  fact = [&fact](int x) { if(x<=1) return 1; else return x*fact(x-1);};
  std::cout << "fact(5) is: " << fact(5) << std::endl;
  std::cout << "fact(1) is: " << fact(1) << std::endl;
  std::cout << "fact(0) is: " << fact(0) << std::endl;

}
