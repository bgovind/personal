#include <iostream>
#include <functional>
#include <list>
#include <cstdlib>
#include <algorithm>

class Movie{

public:
Movie(const std::string& t) : title(t) {}
const std::string getTitle() const { return title; }
bool operator<(const Movie& m) const {
    return title < m.title;
  }
bool operator>(const Movie& m) const {
    return title > m.title;
  }
private:
std::string title;
};

std::ostream& operator<<(std::ostream& out, const Movie& m){
	return out<< m.getTitle();
}

class MovieLess{
public:
bool operator()(const Movie a, const Movie b) const {
	return a.getTitle() > b.getTitle();

}

};

class MovieTitles{
public:
MovieTitles() : titles() {}
void addTitle(const std::string& t) { titles.push_back(t);}
void sortTitles(){
	sort(titles.begin(), titles.end(), [](const Movie a, const Movie b)->bool{return a < b;});
	//sort(titles.begin(), titles.end());
	//sort(titles.begin(), titles.end(), MovieLess());
}
void print(){
	for (unsigned int i = 0; i < titles.size(); ++i) {
    std::cout << titles[i].getTitle()  << ", ";
  }
  std::cout << std::endl;
}


private:
std::vector<Movie> titles;
};


int main(){
MovieTitles titles;
titles.addTitle("Scream");
titles.addTitle("Harry Potter");
titles.addTitle("X-Men");
titles.addTitle("Avengers");
titles.print();
titles.sortTitles();
titles.print();
}
