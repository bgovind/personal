#include<cstdlib>
#include<iostream>
#include<list>

class Number {
	public:
	Number() : n (rand() % 100) {}
	int getNumber() const { return n; }
	bool operator<(const Number& rhs) const { return n < rhs.n; }
	private:
	int n;
};

std::ostream& operator<<(std::ostream& out, const Number* n)
{
	return out<< n->getNumber();
}

void print(std::list<Number*> numbers)
{
	for(Number* n : numbers)
	{
		std::cout<< n << "," ;
  	}
	std::cout << std::endl;
}

void removeMedian(std::list<Number*> numbers)
{
	std::list<Number*>::iterator ptrSlow = numbers.begin();
	std::list<Number*>::iterator ptrFast = numbers.begin();
	std::cout << "Slow Iterator" << std::endl;
	while(ptrFast != numbers.end())
	{
		
		std::cout << "Fast: " << (*ptrFast) << std::endl;
		++ptrSlow;
		
		std::cout<< "Slow: "<< (*ptrSlow) << std::endl;
		if(++ptrFast == numbers.end() ) std::cout << "Median :" << (*ptrSlow) << std::endl;

		
		//++ptrFast;
		++ptrFast;
		
		
	}
	
	std::cout<< std::endl;
}

int main()
{
	std::list<Number*> numbers;
	for(unsigned int i = 0; i < 100 ; ++i)
	{
		numbers.push_back( new Number() );
	}

	print(numbers);

	

	numbers.sort([](const Number* a, const Number* b) -> bool { return (*a) < (*b); });

	std::cout<< "sorted " << std::endl;

	print(numbers);

	removeMedian(numbers);


	//print(numbers);

	std::cout<< "List size : " << numbers.size() << std::endl;
	


}
