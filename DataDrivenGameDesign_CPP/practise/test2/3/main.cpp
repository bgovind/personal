#include <iostream>
#include <string>
#include <vector>
#include <sstream>

void print(std::vector<std::string>& numbers){
    for(unsigned int i = 0; i < numbers.size(); ++i)
    {
        std::cout<< numbers[i] << std::endl;
    }
}

void init(std::vector<std::string>& numbers){
    for(unsigned int i = 0; i < 4; ++i){
        std::stringstream strm;
        strm<<i;
        numbers.push_back(strm.str());
    }
}

int main(){
    std::vector<std::string> numbers;
    init (numbers);
    print (numbers);
}
