#include<iostream>
class Number
{
 public:
 Number() {std::cout<< "default" << std::endl;}
 Number(float ) {std::cout<< "conversion" << std::endl;}
 Number(const Number&) {std::cout<< "copy" << std::endl;}
 ~Number() {std::cout<< "destruct" << std::endl;}
 Number& operator=(const Number&) { std::cout << "assign" << std::endl;
 return *this;
}
};

class Student
{
public:
 Student (float g) { gpa = g; }
private:
 Number gpa;
};

int main(){
Student npc(3.4);
}
