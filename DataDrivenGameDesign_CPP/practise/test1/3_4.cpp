#include<iostream>
#include<vector>
class A
{
	public:
	A(){}
	virtual void foo() const { std::cout<< "I am foo in A" << std::endl;}
	void bar() const { std::cout << "I am bar in A" << std::endl;}
};

class B: public A
{
	public:
	B() : A() {}
	virtual void foo() const { std::cout<< "I am foo in B" << std::endl; }
	void bar() const {std::cout << "I am bar in B" << std::endl; }
};
int main()
{
	std::vector<A*> vec;
	vec.push_back( new B);
	vec[0]-> foo();
	vec[0]-> bar();
}
