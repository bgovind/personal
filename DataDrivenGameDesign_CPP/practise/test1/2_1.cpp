#include <iostream>
class Title
{
	public:
	Title() { std::cout << "default" << std::endl; }
	Title(const char*) { std::cout << "convert" << std::endl; }
	Title(const Title&) { std::cout << "copy" << std::endl; }
	~Title() { std::cout << "destruct" << std::endl;}
	Title& operator=(const Title&)
	{
		std::cout << "assign" << std::endl;
		return *this;
	}
};

class NPC
{
	public:
	NPC(const char* t) //: gpa(g)
	{
	 //std::cout << "convert 1" << std::endl;
	 title = t;
	}
	//~Student() { std::cout << "destruct 1" << std::endl; }
	private:
	Title title;
};

int main()
{
	//Student npc(3.4);
	NPC* npc = new NPC("Some text");
}
	
