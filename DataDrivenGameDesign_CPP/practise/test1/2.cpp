#include <iostream>
class Number
{
	public:
	Number() { std::cout << "default" << std::endl; }
	Number(float) { std::cout << "convert" << std::endl; }
	Number(const Number&) { std::cout << "copy" << std::endl; }
	~Number() { std::cout << "destruct" << std::endl;}
	Number& operator=(const Number&)
	{
		std::cout << "assign" << std::endl;
		return *this;
	}
};

class Student
{
	public:
	Student(float g) //: gpa(g)
	{
	 //std::cout << "convert 1" << std::endl;
	 gpa = g;
	}
	//~Student() { std::cout << "destruct 1" << std::endl; }
	private:
	Number gpa;
};

int main()
{
	Student npc(3.4);
}
	
