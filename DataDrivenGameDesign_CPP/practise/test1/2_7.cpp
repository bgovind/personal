#include<iostream>
class Number
{
public:
	Number(int n) : number(n) {}
	int getNumber() const {return number;}
private:
	int number;
	Number& operator=(const Number&);
	Number(const Number&);
};

void printNumber(const Number& number)
{
	std::cout << number.getNumber() << std::endl;
}

int main()
{
	Number number(17);
	printNumber(number);
}
