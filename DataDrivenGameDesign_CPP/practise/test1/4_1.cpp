#include <cstring>
#include <iostream>
using std::cout; using std::endl;
class string {
public:
string() : buf(new char['\0']){ std::cout << "default" << std::endl;}
string(const char* s) : buf(new char[strlen(s)+1]) { std::cout<< "convert" << std::endl;
strcpy(buf, s);}
string(const string& p) : buf(new char[strlen(p.buf)+1]) { std::cout<< "copy" << std::endl; strcpy(buf,p.buf );}
~string() { delete [] buf; }
const char* getBuf() const { return buf; }
string& operator=(const string& rhs)
{
	if(this == &rhs) return *this;
	delete [] buf;
	std::cout<< "assign" << std::endl;
	buf = new char[strlen(rhs.buf)+1];
	strcpy(buf, rhs.buf);
	return *this;
	
}
private:
char * buf;
};
int main() {
string a("dog"), b("cat");
b = a;
cout << a.getBuf() << endl;
cout << b.getBuf() << endl;
}
