#include<iostream>
#include<vector>
#include<cstdlib>
const int MAX = 100;
const int LETTERS = 26;
void init(std::vector<char>& vec)
{
	for (unsigned int i = 0; i < MAX; ++i)
	{
	vec.push_back(rand()%LETTERS + 'A');
	}
}

void print(const std::vector<char>& vec)
{
	for (unsigned int i = 0; i < vec.size(); ++i)
	{
	std::cout << vec[i] << "";
	}
	std::cout<< std::endl;
}

void eraseVowels(std::vector<char>& vec)
{
	for(unsigned int i = 0; i < vec.size(); ++i)
	{
		vec.pop_back();
	}

}

int main()
{
	std::vector<char> vec;
	init(vec);
	print(vec);
	eraseVowels(vec);
	print(vec);
}
