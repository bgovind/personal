#include<iostream>
#include<cstring>
class string
{
	public:
	/*string()  { std::cout << "default" << std::endl;}*/
	string (const char* s) : buf(new char[strlen(s)+1]) 
	{
		std::cout << "convert" << std::endl;
		strcpy(buf, s);
	}
	/*string(const string& p) : buf(new char[strlen(p.buf)+1]) 
	{
		std::cout << "copy" << std::endl;
    		strcpy(buf, p.buf);
  	}*/
	/*~string() { std::cout << "destroy" << std::endl; }*/
	char* getBuf() const { return buf; }
	void setBuf(const char* s)
	{
		delete [] buf;
		buf = new char[strlen(s)+1];
		strcpy(buf, s);

	}
	/*string& operator=(const string& s)
	{
		std::cout<< "assign" << std::endl;
		if(this == &s) return *this;
		delete [] buf;
		buf = new char[strlen(s.buf)+1];
		strcpy(buf, s.buf);
		return *this;
	}*/
	private:
	char* buf;
};

/*std::ostream& operator<<(std::ostream& out, const string& s)
{
	return out<<s.getBuf();
}*/

int main()
{
	string a("cat"), b("dog");// = a ;
	b = a ;
	b.setBuf("rat");
	//std::cout<< a << std::endl;
	std::cout<< a.getBuf() << std::endl;
	std::cout<< b.getBuf() << std::endl;
}

