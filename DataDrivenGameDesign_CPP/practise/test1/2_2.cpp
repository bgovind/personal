#include<iostream>
class Title
{
	public:
	Title() { std::cout << "default" << std::endl; }
	Title(const char*) { std::cout << "convert" << std::endl; }
	Title(const Title&) { std::cout << "copy" << std::endl; }
	~Title() { std::cout << "destruct" << std::endl;}
	Title& operator=(const Title&)
	{
		std::cout << "assign" << std::endl;
		return *this;
	}
};

int main()
{
	Title x("cat"), y;
	y = x;
	/*Title y;
	y = *x;
	delete x;*/
}
