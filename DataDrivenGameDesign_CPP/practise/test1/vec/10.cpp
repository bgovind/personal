// 1. size v capacity
// 2. value semantics
// 3. reserve 
// 4. push_back 
// 5. explicit
// 6. emplace_back
// 7. ranged for loops
// 8. Iterators/auto: erase

#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>

const int MAX = 3;

class Number {
public:
  Number() : number(0) {
    std::cout << "default" << std::endl;
  }
  Number(int n) : number(n) {
    std::cout << "convert" << std::endl;
  }
  Number(const Number& a) : number(a.number) {
    std::cout << "copy" << std::endl;
  }
  Number& operator=(const Number& rhs) {
    if ( this != &rhs ) {
      number = rhs.number;
    }
    std::cout << "assign" << std::endl;
    return *this;
  }
  int getNumber() const { return number; }
private:
  int number;
};
std::ostream& operator<<(std::ostream& out, const Number& n) {
  return out << n.getNumber();
}

void print(const std::vector<Number> & vec) 
{
  for( const Number& item : vec )
 {
    std::cout << item  << ", ";
  }
  std::cout << std::endl;
}

void init(std::vector<Number>& vec)
{
	for(unsigned int i = 0; i < MAX; i++)
	{
		vec.emplace_back(Number(i+1));
	}
}

int main() {
  std::vector<Number> vec;
  vec.reserve(4);
  init(vec);
  vec.emplace_back(Number(1));
  vec.emplace_back(Number(2));
  //vec.emplace_back(2);
  /*vec.emplace_back(2);
  vec.emplace_back(3);
  vec.emplace_back(4);*/
  print(vec);
  std::cout << vec.size() <<std::endl;
  std::cout << vec.capacity() <<std::endl;

}
