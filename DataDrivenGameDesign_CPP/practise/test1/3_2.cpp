#include<iostream>
#include<cstdlib>
#include<vector>

const int MAX = 2;
class A
{
	public:
	A() { std::cout << "default" << std::endl;}
	A(const A&) { std::cout << "copy" << std::endl; }
};

template <typename T>
void print(std::vector<T>& vec)
{
	std::cout<< "size: " << vec.size() << std::endl;
	std::cout << "cap:" << vec.capacity() << std::endl;
}

int main()
{
	std::vector<int> vec1;
	std::vector<int> vec2;
	vec2.reserve(MAX);
	vec1.push_back(rand() % 100);
	vec2.push_back(rand() % 100);
	std::vector<A> vec3(MAX);
	vec3.push_back(A());
	print(vec1);
	print(vec2);
	print(vec3);
}
