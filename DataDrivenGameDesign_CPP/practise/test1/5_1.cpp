#include <iostream>
#include <vector>
using std::string; using std::cout; using std::endl;
class A {
public:
A(const string& n) : name(n) {}
const string getName() const { return name; }
virtual int getAge() const { return 111; }
private:
string name;
};
class B : public A {
public:
B(const string& n, int a) : A(n), age(a) {}
const string getName() const { return "Torvalds"; }
int getAge() const { return age; }
private:
int age;
};
int main() {
std::vector<A*> people;
people.push_back(new B("Abe", 21));
people.push_back(new A("Bill"));
cout << people[0]->getAge() << endl;
cout << people[1]->getAge() << endl;
}
