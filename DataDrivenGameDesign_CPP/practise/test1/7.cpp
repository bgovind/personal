#include <iostream>

class Number
{
	public:
	Number() { std::cout << "default" << std::endl; }
	Number(int) { std::cout << "convert" << std::endl; }
	Number(const Number&) { std::cout << "copy" << std::endl; }
	~Number() { std::cout << "destruct" << std::endl;}
	Number& operator=(const Number&)
	{
		std::cout << "assign" << std::endl;
		return *this;
	}
};

void f(Number n) {}

int main()
{
	Number a(17), b = a;
	f(a);
	Number* number = new Number(3);
	delete number;
}
