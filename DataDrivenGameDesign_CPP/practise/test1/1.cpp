#include <iostream>
class Bird
{
	public:
	Bird () : wingSpan(0), speed(0) { std::cout << "default" << std::endl;}
	Bird (int w) : wingSpan(w), speed(2*wingSpan)
	{
		std::cout << "Conversion" << std::endl;
	}
	~Bird() { std::cout << "destroy" << std::endl; }
	int getSpeed() { return speed; }
	int getwing() { return wingSpan; }
	

	private:
	int wingSpan;
	int speed;

};

int main()
{
	Bird robin;
	std::cout << robin.getSpeed() << std::endl;
	std::cout << robin.getwing() << std::endl;
}
