// How many things are wrong with this class?

#include <cstring>
#include <iostream>
class string {
public:
  string() : buf(new char[1]) { buf[0] = '\0'; }
  string(const char* b) : buf(new char[strlen(b)+1]) { 
    strcpy(buf, b); 
  }
  string(const string& s) : buf(new char[strlen(s.buf)+1]) { 
    strcpy(buf, s.buf); 
  }
  ~string() { delete [] buf; }
  const char* getBuf() const { return buf; }
  //void setBuf(const char* b) {
    //buf = const_cast<char*>(b);
  //}
private:
  char * buf;
  string& operator=(const string&);
};

void crash(string s) {}

int main() {
  string x("cat"), y = x;
  crash(y);
}
