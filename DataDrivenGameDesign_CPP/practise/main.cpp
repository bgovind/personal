#include <iostream>

using namespace std;

class dog
{
	private:
	bool happy;
	string dogName;
	public:
	void dogStatus();
	dog(): dogName(""), happy(true) {cout << "Initialised and allocated" << endl;};
	dog(string dogName, bool happy, int *dog2ID): dogName(dogName), happy(happy) {*dog2ID = *dog2ID + 1; cout<< "Dog ID: " << *dog2ID << endl;}; 
	~dog();
	
};

dog::~dog()
{
	cout << "Deallocated" << endl;
}


void dog::dogStatus()
{
	if (happy)
	{
		cout << dogName << " Bow bow" << endl;
	}
	else
	{
		cout << dogName << " GRRRR" << endl;
	}
}


int main()
{
	cout << "Program Start" << endl;
	{	
		int dog2ID = 2;
		dog *dog1 = new dog();
		dog1->dogStatus();
		dog dog2("Snowy", false, &dog2ID);
		dog2.dogStatus();
		cout<< "Dog 2ID: " << dog2ID << endl;
		delete dog1;
		
	}
	cout << "Program End" << endl;
	int a = 1;

	cout << ++a << endl;
	
	return 0;
}
