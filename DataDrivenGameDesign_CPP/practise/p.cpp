#include<iostream>
class Pokeman{
public:
 Pokeman(): combatPower(0){
std::cout<< "default" << std::endl;
}
Pokeman(int c) : combatPower(c)
{
std::cout<< "conversion"<< std::endl;
}
Pokeman(const Pokeman& p) : combatPower(p.combatPower) {
std::cout<< "copy" << std::endl;
}
Pokeman& operator=(const Pokeman&){
std::cout << "assignment" << std::endl;
return *this;
}

unsigned int getCP() const {return combatPower;}
private:
unsigned int combatPower;
};

void display(const Pokeman pokeman){
std::cout<< pokeman.getCP() << std::endl;
}

int main(){
Pokeman snorlax(2840), dragnite = snorlax;
display(dragnite);
}
