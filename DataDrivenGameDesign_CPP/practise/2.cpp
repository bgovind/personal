#include<iostream>
int incrCount(int count)
{
++count;
std::cout << "count in func:" << count << std::endl;
return count;
}

void makeswitch(int & count)
{
	switch(count)
	{
	 case 2: ++count;
	 case 3: ++count;
	 case 4: ++count;
	 case 5: ++count;
	 default: ++count;
	}
}

int main()
{
 int count = 10;
 count = incrCount(count);
 std::cout<< count << std::endl;
 
 count = 8;
 count = (count%5)?2:3;
std::cout << "count in main:" << count << std::endl;
 makeswitch(count);
 std::cout << count << std::endl;
}
