#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include "sprite.h"
#include "multisprite.h"
#include "multisprite2.h"
#include "gamedata.h"
#include "engine.h"
#include "frameGenerator.h"

Engine::~Engine() { 
  std::cout << "Terminating program" << std::endl;
 
  for (auto& m : sprites ) //Item 6: Ranged For Loop
  {
	delete m;
  }

}

Engine::Engine() :
  rc( RenderContext::getInstance() ),
  io( IOmod::getInstance() ),
  clock( Clock::getInstance() ),
  renderer( rc->getRenderer() ),

  //Item 4: Pitch, Cloud and Mountain Backgrounds for parallax scrolling
  pitch("pitch", Gamedata::getInstance().getXmlInt("pitch/factor") ),
  cloud("cloud", Gamedata::getInstance().getXmlInt("cloud/factor") ),
  mountain("mountain", Gamedata::getInstance().getXmlInt("mountain/factor") ),
  viewport( Viewport::getInstance() ),
  sprites(),
  currentSprite(-1),

  makeVideo( false ),
  textColorState(false),
  flagCount(0)
{

  //Item 10: Create multisprites
  sprites.push_back( new MultiSprite("harry") ); //Item 11: Harry is a two way multisprite
  sprites.push_back( new MultiSprite("snitch") );

  //Dynamically generate flag multisprites based on world width
  flagCount = ((Gamedata::getInstance().getXmlInt("world/width")/1280)*2)+1;

  int flagX = 147;

  for (int i = 1; i <= flagCount; ++i)
  {
	if (i%2 != 0)
	{
		//Item 11: Create dynamic instances of flag multisprite of type Multisprite2 based on flagCount 
        	sprites.push_back( new MultiSprite2("flag", flagX, 19) );
		flagX+=871;
  	}
	else
	{
		sprites.push_back( new MultiSprite2("flag", flagX, 19) );
		flagX+=409;
	}
  }
  //
  

  //Item 10: Generate bludger sprites based on number given in XML
  for(int i = 0 ; i < Gamedata::getInstance().getXmlInt("bludger/count") ; ++i)
  {
  	sprites.push_back( new Sprite("bludger") );
  }
  

  switchSprite();
  std::cout << "Loading complete" << std::endl;
}

void Engine::draw(bool textColorState) const {
  cloud.draw();
  mountain.draw();
  pitch.draw();
  std::stringstream strm;
  std::stringstream strm2;

  //Item 9: Call calcAvgFps in Clock class to calculate average fps (Engine is a friend class of Clock)
  clock.calcAvgFps();
  strm << "FPS: " << clock.getFps();

  //Item 9: Call getter in Clock class to get and print average fps
  strm2 << "Average FPS: " << clock.getAvgFps();
  for(auto* s : sprites) s->draw();

  //Item 5: Call Overloaded writeText function to print text in red font
  if(textColorState)
  {
   io.writeText("Press D to change font color to Default", (Gamedata::getInstance().getXmlInt("view/width")*0.5) - 195 , Gamedata::getInstance().getXmlInt("view/height")/20, {255,0,0,0});
   io.writeText(strm.str(), (Gamedata::getInstance().getXmlInt("view/width")*0.5) - 140 , Gamedata::getInstance().getXmlInt("view/height")/12, {255,0,0,0});
   io.writeText(strm2.str(), (Gamedata::getInstance().getXmlInt("view/width")*0.5) - 15, Gamedata::getInstance().getXmlInt("view/height")/12, {255,0,0,0});

   //Item 1: Print Name and Animation in Red font
   io.writeText(Gamedata::getInstance().getXmlStr("screenTitle"), (Gamedata::getInstance().getXmlInt("view/width")*0.45)-105 , Gamedata::getInstance().getXmlInt("view/height")/8.5, {255,0,0,0});
  }
  //Item 5: Call default writeText function to print text in default font color specified in XML
  else
  {
  io.writeText("Press R to change font color to Red", (Gamedata::getInstance().getXmlInt("view/width")*0.5) - 180 , Gamedata::getInstance().getXmlInt("view/height")/20);
  io.writeText(strm.str(), (Gamedata::getInstance().getXmlInt("view/width")*0.5) - 140 , Gamedata::getInstance().getXmlInt("view/height")/12 );
  io.writeText(strm2.str(), (Gamedata::getInstance().getXmlInt("view/width")*0.5) -15 , Gamedata::getInstance().getXmlInt("view/height")/12 );

  //Item 1: Print Name and Animation from default font color specified in XML
  io.writeText(Gamedata::getInstance().getXmlStr("screenTitle"), (Gamedata::getInstance().getXmlInt("view/width")*0.45)-105 , Gamedata::getInstance().getXmlInt("view/height")/8.5);
  }

  viewport.draw();
  SDL_RenderPresent(renderer);
}

void Engine::update(Uint32 ticks) {
  for(auto* s : sprites) s->update(ticks);
  cloud.update();
  pitch.update();
  mountain.update();
  viewport.update(); // always update viewport last
}

void Engine::switchSprite(){
  ++currentSprite;
  currentSprite = currentSprite % sprites.size();
  Viewport::getInstance().setObjectToTrack(sprites[currentSprite]);
}

void Engine::play() {
  SDL_Event event;
  const Uint8* keystate;
  bool done = false;
  Uint32 ticks = clock.getElapsedTicks();
  FrameGenerator frameGen;

  while ( !done ) 
  {
    while ( SDL_PollEvent(&event) ) 
    {
      keystate = SDL_GetKeyboardState(NULL);
      if (event.type ==  SDL_QUIT) { done = true; break; }
      if(event.type == SDL_KEYDOWN) 
      {
        if (keystate[SDL_SCANCODE_ESCAPE] || keystate[SDL_SCANCODE_Q]) 
        {
          done = true;
          break;
        }
        if ( keystate[SDL_SCANCODE_P] ) 
        {
          if ( clock.isPaused() ) clock.unpause();
          else clock.pause();
        }
        if ( keystate[SDL_SCANCODE_S] ) 
        {
          clock.toggleSloMo();
        }
        if ( keystate[SDL_SCANCODE_T] ) 
        {
          switchSprite();
        }
        if (keystate[SDL_SCANCODE_F4] && !makeVideo) 
        {
          std::cout << "Initiating frame capture" << std::endl;
          makeVideo = true;
        }
        else if (keystate[SDL_SCANCODE_F4] && makeVideo) 
        {
          std::cout << "Terminating frame capture" << std::endl;
          makeVideo = false;
        }
      }
    }
    ticks = clock.getElapsedTicks();

    //Item 5: Key Events for Toggling font color
    if(keystate[SDL_SCANCODE_R] && !textColorState)
    {
	textColorState = true;
        
    }
    else if (keystate[SDL_SCANCODE_D] && textColorState) 
    {
	textColorState = false;
    }
    if ( ticks > 0 ) 
    {
      clock.incrFrame();
      draw(textColorState);
      update(ticks);
      if ( makeVideo ) 
      {
        frameGen.makeFrame();
      }
    }
  }
}
