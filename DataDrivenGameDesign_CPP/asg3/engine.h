#include <vector>
#include <SDL.h>
#include "ioMod.h"
#include "renderContext.h"
#include "clock.h"
#include "world.h"
#include "viewport.h"

class Engine {
public:
  Engine ();
  ~Engine ();
  void play();
  void switchSprite();

private:
  const RenderContext* rc;
  const IOmod& io;
  Clock& clock;

  SDL_Renderer * const renderer;
  
  //Item 4: Pitch, Cloud and Mountain Backgrounds for parallax scrolling
  World pitch;
  World cloud;
  World mountain;

  Viewport& viewport;

  std::vector<Drawable*> sprites;

  int currentSprite;
  bool makeVideo;
  bool textColorState;
  int flagCount;

  void draw(bool) const;
  void update(Uint32);

  //Item 7: Explicitly disallow  use of compiler generated functions
  Engine(const Engine&) = delete;
  Engine& operator=(const Engine&) = delete;
};
