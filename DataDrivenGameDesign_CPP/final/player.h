#ifndef PLAYER__H
#define PLAYER__H
#include <string>
#include <vector>
#include <list>
#include <cmath>
#include "playerdraw.h"
#include "explodingSprite.h"
#include "drawable.h"

class Player : public Playerdraw {
public:
  Player(const std::string&);
  Player(const std::string&, int, int, int, int);
  Player(const Player&);
  Player& operator=(const Player&);
  ~Player();

  virtual void draw() const;
  virtual void update(Uint32 ticks);
  virtual void explode();
  
  virtual const Frame* getFrame() const { 
    return frames[currentFrame]; 
  }
  virtual void right();
  virtual void left();
  virtual void stop();
  virtual void up();
  virtual void down();
  virtual void ne();
  virtual void nw();
  virtual void se();
  virtual void sw();
  
  bool getExplosionStatus() const { return explosionStatus; }
  
  void setHealthZero (){ health = 0; }; //Set Health to 0 when player loses
  void reduceHealth() { if(health < 2) health = 0; else health = health - 1.25; } //Reduce health when player is hit by a bludger
  void increaseHealth() { if(health > 90) health = 100; else health = health + 5; } //Increase health when player shoots a dementor
  int getHealth() const { return health; }
  void initObservers(Playerdraw* obj) { observers.push_back(obj); } //Create Observer List if the class object is of type harry player(subject)
  void printObserverSize() { std::cout<<"Observer List size" << observers.size() << std::endl; }
  void notifyHarryPosition(Vector2f subjectPosition) { subjectPositionX = subjectPosition[0]; //Set Position of the subject to observer if class object is of type dementor (observer)
						       subjectPositionY = subjectPosition[1];}

protected:
  enum MODE {NORMAL, SMART}; //Observer modes
  MODE currentMode;

  ExplodingSprite* explosion;
  const std::vector<Frame *> frames;

  unsigned currentFrame;
  unsigned numberOfFrames;
  unsigned frameInterval;
  Vector2f initialVelocity;
  const float slowdown;

  float timeSinceLastFrame;
  int worldWidth;
  int worldHeight;
  int frameWidth;
  int frameHeight;
  bool explosionStatus;

  int health;

  std::list<Playerdraw*> observers;
  float subjectPositionX;
  float subjectPositionY;
  int observerInfluence;

  void advanceFrame(Uint32 ticks);
  float getDistance(float,float,float,float);

  Vector2f makeVelocity(int ,int) const;

};
#endif
