#include <iostream>
#include <string>
#include <SDL.h>
#include <SDL_ttf.h>

class HuDFonts {
public:
  static HuDFonts& getInstance();
  ~HuDFonts();
  SDL_Texture* readTexture(const std::string& filename);
  SDL_Surface* readSurface(const std::string& filename);
  void writeText(const std::string&, int, int) const;

  //Overloaded writeText function for toggling font color to Red and back
  void writeText(const std::string&, int, int, SDL_Color) const;
  
  //Overloaded writeText function to scaled font size based on HUD size
  void writeText(const std::string&, int, int, int) const;
  SDL_Renderer* getRenderer() const { return renderer; }
 
private:
  int init;
  SDL_Renderer* renderer;
  TTF_Font* font;
  SDL_Color textColor;
  HuDFonts();
  HuDFonts(const HuDFonts&);
  HuDFonts& operator=(const HuDFonts&);
};
