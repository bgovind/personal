#include <iostream>
#include <string>
#include <SDL.h>

class Hud {
public:
  static Hud& getInstance();
  ~Hud();
 
  //Draw Hud Function
  void drawHud() const;
  void drawWinHuD() const;
  void drawLostHuD() const;
  void drawModePoolHuD(bool, unsigned, unsigned) const;
  SDL_Renderer* getRenderer() const { return renderer; }
 
private:
  SDL_Surface* surface;
  SDL_Renderer* renderer;
  SDL_Texture* texture1;
  SDL_Texture* texture2;

  Hud();
  Hud(const Hud&);
  Hud& operator=(const Hud&);
};
