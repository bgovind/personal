#include <SDL_image.h>
#include <sstream>
#include <string>
#include <iomanip>
#include "health.h"
#include "gamedata.h"
#include "ioMod.h"
#include "renderContext.h"
#include "clock.h"

Health& Health::getInstance() {
  static Health instance;
  return instance;
}

Health::~Health() 
{}

Health::Health() : 
 renderer( RenderContext::getInstance()->getRenderer() )
{}


//Draw HealthBar
void Health::operator[](int width) {
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    if((2*width) >= 150)
    	SDL_SetRenderDrawColor( renderer, 36, 255, 40, 180 );
    else if ((2*width) >= 100 && (2*width) < 150)
	SDL_SetRenderDrawColor( renderer, 80, 160, 40, 180 );
    else if ((2*width) >= 50 && (2*width) < 100)
	SDL_SetRenderDrawColor( renderer, 160, 80, 40, 180 );
    else if ((2*width) >= 0 && (2*width) < 50)
	SDL_SetRenderDrawColor( renderer, 255, 36, 40, 180 );

    SDL_Rect hudRect = {500, 40, width*2, 40};
    SDL_RenderFillRect( renderer, &hudRect );

    SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255 );
    SDL_Rect hudColor = { 500, 40, 200, 40};
    SDL_RenderDrawRect( renderer, &hudColor );
}



  
