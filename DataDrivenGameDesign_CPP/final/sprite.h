#ifndef SPRITE__H
#define SPRITE__H
#include <string>
#include "drawable.h"

class ExplodingSprite;
class Sprite : public Drawable {
public:
  Sprite(const std::string&, int, int, int , int );
  Sprite(const std::string&, 
         const Vector2f&, const Vector2f&, const Frame*);
  Sprite(const Sprite&);
  virtual ~Sprite() { } 
  Sprite& operator=(const Sprite&);

  virtual const Frame* getFrame() const { return frame; }
		void   setFrame( const  Frame* f ) { frame = f; }
  virtual void draw() const;
  virtual void update(Uint32 ticks);

  inline float getScale()const{ return scale; }
  void  setScale(float s){ scale = s; }
  void explode();

private:
  ExplodingSprite* explosion;
  const Frame * frame;
  int worldWidth;
  int worldHeight;
  int frameWidth;
  int frameHeight;
  float scale;
  int getDistance(const Sprite*) const;
  Vector2f makeVelocity(int ,int) const;
};
#endif
