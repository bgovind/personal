#include <string>
#include <iostream>
#include "player.h"
#include "bulletPool.h"

class ShootingSprite : public Player {
public:
  ShootingSprite(const std::string& n);
  ShootingSprite(const std::string& n, int, int, int ,int);
  ShootingSprite(const ShootingSprite& s);

  virtual void update(Uint32 ticks);
  virtual void draw() const;
  void shoot();
  virtual bool collidedWith(const Playerdraw*) const;
  unsigned int bulletCount() const { return bullets.bulletCount(); }
  unsigned int freeCount()  const { return bullets.freeCount(); }

private:
  std::string bulletName;
  BulletPool bullets;
  float minSpeed;
  ShootingSprite& operator=(const ShootingSprite&);
};
