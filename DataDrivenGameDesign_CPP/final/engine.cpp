#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <iomanip>
#include <algorithm>
#include <ctime>
#include "sprite.h"
#include "multisprite.h"
#include "multisprite2.h"
#include "scaledSprite.h"
#include "player.h"
#include "gamedata.h"
#include "engine.h"
#include "frameGenerator.h"
#include "collisionStrategy.h"
#include "shootingSprite.h"


//Function object for sorting Owl multisprites based on scale.
class SpriteLess
{
	public:
	bool operator()(const Drawable* lhs, const Drawable* rhs)
	{
		return lhs->getScale() < rhs->getScale();
	}

};

Engine::~Engine() { 
  std::cout << "Terminating program" << std::endl;
 
  for (auto& l : sprites )
  {
	delete l;
  }

  for (auto& l : dementors )
  {
	delete l;
  }

  for (auto& l : bludgers )
  {
	delete l;
  }

  for (auto& n : player)
  {
	delete n;
  }
  for (auto &o : scaledSprites)
  {
	delete o;
  }

  delete strategy;
}

Engine::Engine() :
  rc( RenderContext::getInstance() ),
  io( IOmod::getInstance() ),
  ioHuD ( HuDFonts::getInstance() ),
  clock( Clock::getInstance() ),
  renderer( rc->getRenderer() ),

  pitch("pitch", Gamedata::getInstance().getXmlInt("pitch/factor") ),
  cloud("cloud", Gamedata::getInstance().getXmlInt("cloud/factor") ),
  mountain("mountain", Gamedata::getInstance().getXmlInt("mountain/factor") ),
  directions("directions", Gamedata::getInstance().getXmlInt("directions/factor") ),

  viewport( Viewport::getInstance() ),
  player(),
  sprites(),
  dementors(),
  bludgers(),
  scaledSprites(),
  sound(SDLSound::getInstance()),
  hud(Hud::getInstance()),
  health(Health::getInstance()),

  currentSprite(-1),
  makeVideo( false ),
  strategy( new PerPixelCollisionStrategy ),
  textColorState(false),
  toggleHUD(true),
  flagCount(0),
  keyState(false),
  hudResetUnder5(false),
  collisionDementor(false),
  collisionBludger(false),
  directHit(0),
  reset(false),
  godMode(false),
  result(false),
  lost(false)
{
 
  //Create Harry Player multisprite. This is the subject and is a two way multisprite that can explode and shoot spells
  player.push_back(new ShootingSprite("harry"));

  //Create Snitch multisprite
  sprites.push_back(new MultiSprite("snitch"));


  //Dynamically generate flag multisprites based on world size. These are static multisprites
  flagCount = ((Gamedata::getInstance().getXmlInt("world/width")/1280)*2)+1;
  int flagX = 147;
  for (int i = 1; i <= flagCount; ++i)
  {
	if (i%2 != 0)
	{
        	sprites.push_back( new MultiSprite2("flag", flagX, 19) );
		flagX+=871;
  	}
	else
	{
		sprites.push_back( new MultiSprite2("flag", flagX, 19) );
		flagX+=409;
	}
  }

  //Create Owl Sprites and scale using scale factor. For Painter's Algorithm. These are two way multisprites and do not shoot or explode
  for ( int i = 0; i < Gamedata::getInstance().getXmlInt("owl/count"); ++i ) {
    float scale = Gamedata::getInstance().getRandFloat(0.3, 0.85);
    auto* s = new ScaledSprite("owl", scale);
   
    while(scale < 0.1f) scale = Gamedata::getInstance().getRandFloat(0.3, 0.85);
    s->setScale(scale);
    scaledSprites.push_back(s);
  }

  //Create dementor multisprites and randomize their movement. These are two way multisprites that can explode and are also the observers
  srand( time(0) );
  for(int i = 0; i < Gamedata::getInstance().getXmlInt("dementor/count"); ++i )
  {
	dementors.push_back(new ShootingSprite("dementor", 
						Gamedata::getInstance().getXmlInt("dementor/speedX") + (rand()%2? -50 : 50),
                    				Gamedata::getInstance().getXmlInt("dementor/speedY") + (rand()%2? -50 : 50),
						Gamedata::getInstance().getXmlInt("dementor/startLoc/x"),
						Gamedata::getInstance().getXmlInt("dementor/startLoc/y")));
        static_cast<Player*>(player[0])->initObservers(dementors[i]);
  }

  //Print Observer list size
  static_cast<Player*>(player[0])->printObserverSize();

  //Create Bludger Sprites based on count from Game XML
  int bludgerX = Gamedata::getInstance().getXmlInt("bludger/startLoc/x");
  int bludgerXSpeed = Gamedata::getInstance().getXmlInt("bludger/speedX");
  int bludgerYSpeed = Gamedata::getInstance().getXmlInt("bludger/speedY");
  for(int i = 0 ; i < Gamedata::getInstance().getXmlInt("bludger/count") ; ++i)
  {
  	bludgers.push_back( new Sprite("bludger", bludgerX + (rand()%2? -10 : 10) , Gamedata::getInstance().getXmlInt("bludger/startLoc/x"), bludgerXSpeed, bludgerYSpeed ) );
	bludgerX = bludgerX+600;
	bludgerYSpeed = bludgerYSpeed + 250;
  }

  //Scale Owl Sprites  and sort them using Function Object Spriteless
  std::vector<Drawable*>::iterator ptr = scaledSprites.begin();
  sort(ptr, scaledSprites.end(), SpriteLess());
  for ( auto* sprite : scaledSprites ) {
    Drawable* thisone = dynamic_cast<Drawable*>(sprite);
    if(thisone)
    {
	std::cout << thisone -> getScale() << std::endl;
    }
  } 
  
  switchSprite();
  std::cout << "Loading complete" << std::endl;
}

//Draw
void Engine::draw() const {
  cloud.draw();

  //Draw smallest Owls behind mountains & trees
  for (unsigned i = 0; i < scaledSprites.size()/3; ++i){ 
    scaledSprites[i]->draw();
  }

  mountain.draw();

  //Draw mid sized owls behind quidditch pitch
  for (unsigned i = (scaledSprites.size()/3); i < ((scaledSprites.size())*2/3); ++i){
    scaledSprites[i]->draw();
  }
  
  directions.draw();
  
  pitch.draw();

  clock.calcAvgFps();
  clock.getFps();
  clock.getAvgFps();

  //Draw Snitch & Flags
  for(auto* s : sprites) s->draw();

  //Draw largest owls
  for (unsigned i = ((scaledSprites.size())*2/3); i < scaledSprites.size(); ++i){
    scaledSprites[i]->draw();
  }
  
  for(auto* b: bludgers) b->draw();
  
  //Draw player - Harry
  player[0] -> draw();


  //Draw Dementors
  for(auto* d: dementors) d->draw();
   

  //God Mode & Bullet Pool HuD
  hud.drawModePoolHuD(godMode, static_cast<ShootingSprite*>(player[0]) -> bulletCount(), static_cast<ShootingSprite*>(player[0]) -> freeCount() );
  

  //Toggle HUD with F1 Key
  if(toggleHUD)
  {
	hud.drawHud();
  }
  else
  {
	//Draw default text when HUD id toggled OFF
 	if(textColorState)
  	{
		io.writeText("Toggle Help: F1", 10, 10, {255,36,0,0});
		io.writeText("Toggle Font color: C", 10, 30, {255,36,0,0});
	}
	else
	{
		io.writeText("Toggle Help: F1", 10, 10);
		io.writeText("Toggle Font color: C", 10, 30);
	}
  }


  //Health Bar
  std::stringstream strm1;
  strm1 << "Harry Health: " << static_cast<Player*>(player[0])->getHealth();
  health[static_cast<Player*>(player[0])->getHealth()];


  //Toggle TextColor  
  if(textColorState)
  {
   	io.writeText(Gamedata::getInstance().getXmlStr("nameTitle"), 10 , 680, {255,36,0,0});
	io.writeText(strm1.str(), 530, 48, {255,36,0,0});
  }

  else
  {
  	io.writeText(Gamedata::getInstance().getXmlStr("nameTitle"), 10, 680);
	io.writeText(strm1.str(), 530, 48);
  }

  //Check Result
  if (result) {
	hud.drawWinHuD();
  }
  
  //Check if Lost
  if(lost) {
	hud.drawLostHuD();	
  }

  viewport.draw();

  SDL_RenderPresent(renderer);
}


//Update
void Engine::update(Uint32 ticks) {
  for(auto* s : sprites) s->update(ticks);
  for(auto* d : dementors) d-> update(ticks);
  for(auto* b : bludgers) b-> update(ticks);
  for(auto* r : scaledSprites) r->update(ticks);

   cloud.update();
   pitch.update();
   directions.update();
   mountain.update();
     
  for(auto* n: player) n->update(ticks);

  viewport.update();
}

void Engine::switchSprite(){
  ++currentSprite;
  currentSprite = currentSprite % player.size();
  Viewport::getInstance().setObjectToTrack(player[currentSprite]);
}

void Engine::checkForCollisions() {
 
  //Check if Harry has collided with a Bludger and reduce Harry's health if True
  std::vector<Drawable*>::const_iterator it1 = bludgers.begin();
  while ( it1 != bludgers.end() ) 
  {
    if ( strategy->execute(*player[0], **it1) ) 
    {
      collisionBludger = true;
      
      if((static_cast<Player*>(player[0])->getHealth())  == 0 ) 
      { 
	lost = true;
        static_cast<Player*>(player[0])-> setHealthZero();
	for(auto* r : player) r->explode(); 
      }
      else { static_cast<Player*>(player[0])->reduceHealth(); }
      break;
    }
    else
    {
	collisionBludger = false;
    }
    ++it1;
  }

  //Check if Harry has collided with a Dementor and explode Harry if True
  std::vector<Playerdraw*>::const_iterator it2 = dementors.begin();
  while ( it2 != dementors.end() ) 
  {
    if( !(static_cast<Player*>(*it2) -> getExplosionStatus()) && !(static_cast<Player*>(player[0]) -> getExplosionStatus()) )
    {
    	if ( strategy->execute(*player[0], **it2) ) 
    	{
      		collisionDementor = true;
		sound[1];
      		if( !(static_cast<Player*>(*it2) -> getExplosionStatus()))  for(auto* r : player) r->explode();
      		lost = true;
		static_cast<Player*>(player[0])-> setHealthZero();
    	}
    	else
    	{
      		collisionDementor = false;
      		lost = false;
    	}
    }
    ++it2;
  }
  
}

void Engine::checkDirectHit() {
  //Check if a Bullet has successfully hit a dementor and explode dementor if True 
  std::vector<Playerdraw*>::iterator it3 = dementors.begin();
  while ( it3 != dementors.end() ) 
  {
    if( !(static_cast<Player*>(*it3) -> getExplosionStatus()))
    {
    	if ( static_cast<ShootingSprite*>(player[0]) -> collidedWith(*it3)  ) 
    	{
	        directHit = true;
                static_cast<Player*>(player[0])->increaseHealth();
		sound[1]; //Play Dementor explosion sound
      		(*it3)->explode();
		break;
	}
	else
	{
		directHit = false;
	}
    }
    ++it3;
  }

}

//Check if player has caught the snitch
void Engine::checkVictory() {
	if ( strategy->execute(*player[0], *sprites[0]) ) {
      		result = true;
    	}
    	else{
		result = false;
   	 }
}

//Genenate new dementors when existing ones are killed by the player
void Engine::generateDementor() {

  if(player[0]->getX() < (((Gamedata::getInstance().getXmlInt("world/width")-3000))))
  {
	int inactiveDementorCount = 0;
	std::vector<Playerdraw*>::iterator it = dementors.begin();
	while( it != dementors.end())
	{
		if( static_cast<Player*>(*it) -> getExplosionStatus() )
		{
			++inactiveDementorCount;
		}
		++it;
	}

	if(inactiveDementorCount >  (0.75*(dementors.size())))
	{			
			Playerdraw* obj = new ShootingSprite("dementor", 
						Gamedata::getInstance().getXmlInt("dementor/speedX")*(rand()%2? -1 : 1),
                    				Gamedata::getInstance().getXmlInt("dementor/speedY") + (rand()%2? -50 : 50),
						(player[0]->getX())+1500, 50);		
			dementors.push_back(obj);
        		static_cast<Player*>(player[0])->initObservers(obj);
	}
   }
}

void Engine::play() {
  SDL_Event event;
  const Uint8* keystate;
  bool done = false;

  Uint32 ticks = clock.getElapsedTicks();
  FrameGenerator frameGen;

  clock.startClock();

  //Game Loop
  while ( !done ) 
  {

    //Display HUD for first 5 seconds once game starts. Toggle with F1.
    //Logic to handle toggling HUD F1 within 5 seconds.
    if( clock.getSeconds() <= 5 &&  hudResetUnder5 == false) { toggleHUD = true; }
      else if ( clock.getSeconds() > 5 && !hudResetUnder5){ toggleHUD = false;}
      else if ( toggleHUD == false){ toggleHUD = false;}
      else if ( toggleHUD == true) {toggleHUD = true;}

    while ( SDL_PollEvent(&event) ) 
    {
      keystate = SDL_GetKeyboardState(NULL);
      if (event.type ==  SDL_QUIT) { done = true; break; }
      	
      if(event.type == SDL_KEYUP)
      {
	keyState = false;
      }
      else if(event.type == SDL_KEYDOWN) 
      {

	//Use Esc or Q for quitting game window
        if (keystate[SDL_SCANCODE_ESCAPE] || keystate[SDL_SCANCODE_Q]) 
        {
          done = true;
          break;
        }
	
	//Toggle Pause / unpause using P
        if ( keystate[SDL_SCANCODE_P] && !(static_cast<Player*>(player[0]) -> getExplosionStatus()) ) 
        {
          if ( clock.isPaused() ) clock.unpause();
          else clock.pause();
        }

	//Use Space bar to shoot
	if ( keystate[SDL_SCANCODE_SPACE] & !(static_cast<Player*>(player[0]) -> getExplosionStatus()) && result == false && !clock.isPaused()) {
	  static_cast<ShootingSprite*>(player[0])->shoot();
	  sound[0]; //Play Spell incantation audio
        }

	//God Mode Toggling using G
        if(!(static_cast<Player*>(player[0]) -> getExplosionStatus()))
	{
		if(keystate[SDL_SCANCODE_G] && godMode == false) {
			godMode = true;
		} else if(keystate[SDL_SCANCODE_G] && godMode == true ) {
			godMode = false;
		}
	}

	//Reset Game using R
	if(keystate[SDL_SCANCODE_R])
	{
		reset = true;
		done = true;
		clock.resetGame();
		break;
	}

	//Record video using F4
        if (keystate[SDL_SCANCODE_F4] && !makeVideo) 
        {
          std::cout << "Initiating frame capture" << std::endl;
          makeVideo = true;
        }
        else if (keystate[SDL_SCANCODE_F4] && makeVideo) 
        {
          std::cout << "Terminating frame capture" << std::endl;
          makeVideo = false;
        }

	//Toggle Font color from Black to Red or otherwise using C
	if(keystate[SDL_SCANCODE_C] && !textColorState)
    	{
		textColorState = true;
    	}
    	else if (keystate[SDL_SCANCODE_C] && textColorState) 
    	{
		textColorState = false;
    	}

	//Toggle HUD using F1
	if(keystate[SDL_SCANCODE_F1] && toggleHUD && !keyState)
    	{
		hudResetUnder5 = true;
		keyState = true;
		toggleHUD = false;
    	}
    	else if (keystate[SDL_SCANCODE_F1] && !toggleHUD && !keyState) 
    	{
		hudResetUnder5 = true;
		keyState = true;
		toggleHUD = true;
	}

      }

     //Left Mouse Click for Shooting Patronous Spell
     if(event.type == SDL_MOUSEBUTTONDOWN)
     {
	if ( event.button.button == SDL_BUTTON_LEFT && !(static_cast<Player*>(player[0]) -> getExplosionStatus()) && result == false && !clock.isPaused()) 
	{
          static_cast<ShootingSprite*>(player[0])->shoot();
	  sound[0]; //Play Spell incantation audio
        }
     } else if ( event.type == SDL_MOUSEBUTTONUP )
	{
		//
	}
    }
    ticks = clock.getElapsedTicks();

    //Player Control
      
    //Stop player when opposing keys are pressed
    if(!(static_cast<Player*>(player[0])->getExplosionStatus()))
    {
    if(keystate[SDL_SCANCODE_A] && keystate[SDL_SCANCODE_D])
    {
	for(auto* h : player) h->stop(); 
    }
    else if (keystate[SDL_SCANCODE_W] && keystate[SDL_SCANCODE_S])
    {
	for(auto* h : player) h->stop();
    }
    
    //Player control key combinations for diagonal movement
    else if (keystate[SDL_SCANCODE_W] && keystate[SDL_SCANCODE_D])
    {
	for(auto* h : player) h->ne();
    }
    else if (keystate[SDL_SCANCODE_W] && keystate[SDL_SCANCODE_A])
    {
	for(auto* h : player) h->nw();
    }
    else if (keystate[SDL_SCANCODE_S] && keystate[SDL_SCANCODE_D])
    {
	for(auto* h : player) h->se();
    }
    else if (keystate[SDL_SCANCODE_S] && keystate[SDL_SCANCODE_A])
    {
	for(auto* h : player) h->sw();
    }

    //Player control up, down, forward, backward
    else if (keystate[SDL_SCANCODE_A])
    {
	for(auto* h : player) h->left();
    }
    else if (keystate[SDL_SCANCODE_D])
    {
	for(auto* h : player) h->right();
    }
    else if (keystate[SDL_SCANCODE_W])
    {
	for(auto* h : player) h->up();
    }
    else if (keystate[SDL_SCANCODE_S])
    {
	for(auto* h : player) h->down();
    }
    }
    
    if ( ticks > 0 ) 
    {
      clock.incrFrame();
      if(godMode == false) checkForCollisions(); //Check for collisions if God mode is not enabled
      checkDirectHit(); //Check is a spell shot by player has hit a dementor
      checkVictory(); //Check if player has won by catching snich
      draw(); //Draw Game elements
      update(ticks);  //Update Game Elements
      generateDementor();	//Generate Dementors when existing dementors are killed

      if (result) clock.pause();

      if ( makeVideo ) 
      {
        frameGen.makeFrame();
      }
    }
    
  }

  //Reset Game using R
  if(reset)
  {
	reset = false;
	Engine newGame;
	newGame.play();

  }
 
}
