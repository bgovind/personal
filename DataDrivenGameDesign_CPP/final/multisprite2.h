#ifndef MULTISPRITE2__H
#define MULTISPRITE2__H
#include <string>
#include <vector>
#include <cmath>
#include "drawable.h"

class MultiSprite2 : public Drawable {
public:
  MultiSprite2(const std::string&, int, int);
  MultiSprite2(const MultiSprite2&);

  virtual void draw() const;
  virtual void update(Uint32 ticks);
  //virtual void explode();
  virtual const Frame* getFrame() const { 
    return frames[currentFrame]; 
  }

protected:
  const std::vector<Frame *> frames;

  unsigned currentFrame;
  unsigned numberOfFrames;
  unsigned frameInterval;
  float timeSinceLastFrame;
  int worldWidth;
  int worldHeight;
  int frameWidth;
  int frameHeight;

  void advanceFrame(Uint32 ticks);

};
#endif
