#include <SDL_image.h>
#include "HuDFonts.h"
#include "gamedata.h"
#include "renderContext.h"

HuDFonts& HuDFonts::getInstance() {
  static HuDFonts instance;
  return instance;
}

HuDFonts::~HuDFonts() { 
  TTF_CloseFont(font);
  TTF_Quit(); 
}

HuDFonts::HuDFonts() : 
  init(TTF_Init()),
  renderer( RenderContext::getInstance()->getRenderer() ),
  font(TTF_OpenFont(Gamedata::getInstance().getXmlStr("hudfont/file").c_str(),
                    Gamedata::getInstance().getXmlInt("hudfont/size"))),
  textColor({0xff, 0, 0, 0})
{
  if ( init == -1 ) {
    throw std::string("error: Couldn't init font");
  }
  if (font == NULL) {
    throw std::string("error: font not found");
  }

}

SDL_Texture* HuDFonts::readTexture(const std::string& filename) {
  SDL_Texture *texture = IMG_LoadTexture(renderer, filename.c_str());
  if ( texture == NULL ) {
    throw std::string("Couldn't load ") + filename;
  }
  return texture;
}

SDL_Surface* HuDFonts::readSurface(const std::string& filename) {
  SDL_Surface *surface = IMG_Load(filename.c_str());
  if ( !surface ) {
    throw std::string("Couldn't load ") + filename;
  }
  return surface;
}

void HuDFonts::writeText(const std::string& msg, int x, int y) const {
  int textWidth;
  int textHeight;
  SDL_Color textColor;

  textColor.r = Gamedata::getInstance().getXmlInt("font/red");
  textColor.g = Gamedata::getInstance().getXmlInt("font/green");
  textColor.b = Gamedata::getInstance().getXmlInt("font/blue");
  textColor.a = Gamedata::getInstance().getXmlInt("font/alpha");

  SDL_Surface* surface = 
    TTF_RenderText_Solid(font, msg.c_str(), textColor);

  SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);

  textWidth = surface->w;
  textHeight = surface->h;
  SDL_FreeSurface(surface);
  SDL_Rect dst = {x, y, textWidth, textHeight};

  SDL_RenderCopy(renderer, texture, NULL, &dst);
  SDL_DestroyTexture(texture);
}


//Item 5: Overloaded writeText function for toggling font color to Red and back
void HuDFonts::writeText(const std::string& msg, int x, int y, SDL_Color textColor) const {
  int textWidth;
  int textHeight;

  SDL_Surface* surface = 
    TTF_RenderText_Solid(font, msg.c_str(), textColor);

  SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);

  textWidth = surface->w;
  textHeight = surface->h;
  SDL_FreeSurface(surface);
  SDL_Rect dst = {x, y, textWidth, textHeight};

  SDL_RenderCopy(renderer, texture, NULL, &dst);
  SDL_DestroyTexture(texture);
}

void HuDFonts::writeText(const std::string& msg, int x, int y, int scaledSize) const {
  int textWidth;
  int textHeight;

  SDL_Color textColor;

  textColor.r = Gamedata::getInstance().getXmlInt("font/red");
  textColor.g = Gamedata::getInstance().getXmlInt("font/green");
  textColor.b = Gamedata::getInstance().getXmlInt("font/blue");
  textColor.a = Gamedata::getInstance().getXmlInt("font/alpha");

  TTF_Font* scaledFont;
  scaledFont = TTF_OpenFont(Gamedata::getInstance().getXmlStr("hudfont/file").c_str(), scaledSize);

  SDL_Surface* surface = 
    TTF_RenderText_Solid(scaledFont, msg.c_str(), textColor);

  SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);

  textWidth = surface->w;
  textHeight = surface->h;
  SDL_FreeSurface(surface);
  SDL_Rect dst = {x, y, textWidth, textHeight};

  SDL_RenderCopy(renderer, texture, NULL, &dst);
  TTF_CloseFont(scaledFont);
  SDL_DestroyTexture(texture);
  
}



  
