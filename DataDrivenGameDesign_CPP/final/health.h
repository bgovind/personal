#include <iostream>
#include <string>
#include <SDL.h>

class Health {
public:
  static Health& getInstance();
  ~Health();
 
  SDL_Renderer* getRenderer() const { return renderer; }
  void operator[](int);
 
private:
  SDL_Renderer* renderer;

  Health();
  Health(const Health&);
  Health& operator=(const Health&);
};
