#include <SDL.h>
#include <string>
#include <vector>

class Engine;

class Clock {
public:
  static Clock& getInstance();
  unsigned int getTicks() const;
  unsigned int getSeconds() const { return getTicks()/1000;  }
  int getFps() const;

  int getAvgFps() const;   //Item 9: Getter function returns calculated average fps to Engine Class
  void calcAvgFps(); //Item 9: Function to calculate average fps 


private:
  friend class Engine;

  bool started;
  bool paused;

  const bool FRAME_CAP_ON;
  const Uint32 PERIOD;

  unsigned int frames;
  int avgFps;

  unsigned int timeAtStart;
  unsigned int timeAtPause;
  unsigned int currTicks;
  unsigned int prevTicks;
  unsigned int ticks;
  unsigned int frameMax; //Item 9: Get frameMax from XML to calculate fps of last frameMax frames
  std::deque<unsigned int> capturefps; //Item 9: Deque to hold fps of last frameMax frames
  int count;
  //bool reset;

  unsigned int getElapsedTicks();
  void incrFrame();
  void toggleSloMo();
  

  bool isStarted() const { return started; }
  bool isPaused() const  { return paused;  }
  unsigned int getFrames() const  { return frames;  }
  
  unsigned int capFrameRate() const;

  

  void startClock();
  void pause();
  void unpause();
  void resetGame();

  Clock();
  Clock(const Clock&);
  Clock&operator=(const Clock&);
  ~Clock();
};
