#include <cmath>
#include <random>
#include <ctime>
#include <functional>
#include "sprite.h"
#include "gamedata.h"
#include "renderContext.h"
#include "explodingSprite.h"

Sprite::Sprite(const std::string& name, int xPos, int yPos, int xSpeed, int ySpeed) :
  Drawable(name,
           Vector2f(xPos, yPos), 
           Vector2f(xSpeed, ySpeed)),
  explosion(NULL),
  frame( RenderContext::getInstance()->getFrame(name) ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),
  frameWidth(frame->getWidth()),
  frameHeight(frame->getHeight()),
  scale(1)
{ }

Sprite::Sprite(const std::string& name, const Vector2f& pos, const Vector2f& vel, const Frame* frm) :
  Drawable(name, pos, vel),
  explosion(NULL),
  frame( frm ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),
  frameWidth(frame->getWidth()),
  frameHeight(frame->getHeight()),
  scale(1)
{ }


//Item 10: Randomize the movement of bludgers
Vector2f Sprite::makeVelocity(int vx, int vy) const
{
	srand( time(0) );
	float a = Gamedata::getInstance().getRandInRange(vx -100, vx + 100);
	float b = Gamedata::getInstance().getRandInRange(vy - 100, vy + 100);
	a = a * (rand()%2? -1 : 1);
	b = b * (rand()%2? -1 : 1);
	vx += a;
	vy += b;
	//std::cout<< a << "," << b << std::endl;
	return Vector2f(a, b);
}

Sprite::Sprite(const Sprite& s) :
  Drawable(s), 
  explosion(s.explosion),
  frame(s.frame),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),
  frameWidth(s.getFrame()->getWidth()),
  frameHeight(s.getFrame()->getHeight()),
  scale(s.scale)
{ }

//Overloaded assignment operator provided by Dr.Malloy
Sprite& Sprite::operator=(const Sprite& rhs) {
  Drawable::operator=( rhs );
  explosion = rhs.explosion;
  frame = rhs.frame;
  worldWidth = rhs.worldWidth;
  worldHeight = rhs.worldHeight;
  frameWidth = rhs.frameWidth;
  frameHeight = rhs.frameHeight;
  std::cout<< "assign" << std::endl;
  return *this;
}

void Sprite::explode() { 
  if ( explosion ) return;
  explosion = new ExplodingSprite(*this); 
}

void Sprite::draw() const { 
  frame->draw(getX(), getY()); 
}

void Sprite::update(Uint32 ticks) { 
  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);

  if ( getY() < 50) {
    setVelocityY( std::abs( getVelocityY() ) );
  }
  if ( getY() > 0.9*(worldHeight-frameHeight)) {
    setVelocityY( -std::abs( getVelocityY() ) );
  }

  if ( getX() < 0) {
    setVelocityX( std::abs( getVelocityX() ) );
  }
  if ( getX() > worldWidth-frameWidth) {
    setVelocityX( -std::abs( getVelocityX() ) );
  }  
}
