#include <SDL_image.h>
#include <sstream>
#include <string>
#include <iomanip>
#include "hud.h"
#include "gamedata.h"
#include "ioMod.h"
#include "HuDFonts.h"
#include "renderContext.h"
#include "clock.h"

Hud& Hud::getInstance() {
  static Hud instance;
  return instance;
}

Hud::~Hud() 
{ }

Hud::Hud() : 
 surface(IMG_Load("images/hud_bg2.jpg")),
 renderer( RenderContext::getInstance()->getRenderer() ),
 texture1(SDL_CreateTextureFromSurface(renderer, surface)),
 texture2(SDL_CreateTextureFromSurface(renderer, surface))
{ SDL_FreeSurface(surface); }

//Draw Hud
void Hud::drawHud() const {
	std::stringstream strm;
	std::stringstream strm2;
	strm << "Seconds: " << Clock::getInstance().getSeconds();
	strm2 << "FPS: " << Clock::getInstance().getFps();
	strm2 << " Avg FPS: " << Clock::getInstance().getAvgFps();
	//SDL_Surface *surface = IMG_Load("images/hud_bg2.jpg");
 	if ( !surface ) {
    		throw std::string("Couldn't load ");
  	}

	//Resizable HUD. This routine scales the font size inside the HUD based on the height of the HUD from Game XML ad
        int scaledFontSize = 0;
	int hudHeight = 0;
	if(Gamedata::getInstance().getXmlInt("hud/height") < 210)
	{
		hudHeight = 210;
		scaledFontSize = Gamedata::getInstance().getXmlInt("font/size");
		
	}
	else
	{
		hudHeight = Gamedata::getInstance().getXmlInt("hud/height");
        	scaledFontSize = Gamedata::getInstance().getXmlInt("font/size")*((Gamedata::getInstance().getXmlInt("hud/height"))/210);
	}
	int hudWidth = hudHeight+5;
	SDL_Rect dest = {Gamedata::getInstance().getXmlInt("hud/startLoc/x"), Gamedata::getInstance().getXmlInt("hud/startLoc/y"), hudWidth, hudHeight };
  	SDL_RenderCopy(renderer, texture1, NULL, &dest);
	IOmod::getInstance().writeText("Toggle Help: F1", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10, Gamedata::getInstance().getXmlInt("hud/startLoc/y")+5, scaledFontSize);
	IOmod::getInstance().writeText("Pause Game: P", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(20*(Gamedata::getInstance().getXmlInt("hud/height"))/210), scaledFontSize);

	IOmod::getInstance().writeText("Restart Game: R", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(35*(Gamedata::getInstance().getXmlInt("hud/height"))/210), scaledFontSize);

	IOmod::getInstance().writeText("Record Video: F4", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(50*(Gamedata::getInstance().getXmlInt("hud/height"))/210), scaledFontSize);
										
										
	IOmod::getInstance().writeText("Move Up: W", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(65*(Gamedata::getInstance().getXmlInt("hud/height"))/210), scaledFontSize);
	IOmod::getInstance().writeText("Move Down: S", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(80*(Gamedata::getInstance().getXmlInt("hud/height"))/210), scaledFontSize);
										
 	IOmod::getInstance().writeText("Move Right: D", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(95*(Gamedata::getInstance().getXmlInt("hud/height"))/210), scaledFontSize);
										
	IOmod::getInstance().writeText("Move Left: A", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(110*(Gamedata::getInstance().getXmlInt("hud/height"))/210), scaledFontSize);

	IOmod::getInstance().writeText("Shoot Spell: Mouse Left/Space", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(125*(Gamedata::getInstance().getXmlInt("hud/height"))/210), scaledFontSize);
										
	IOmod::getInstance().writeText("Toggle Font Color: C", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(140*(Gamedata::getInstance().getXmlInt("hud/height"))/210), scaledFontSize);
 
	IOmod::getInstance().writeText("Toggle God Mode: G", Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(155*(Gamedata::getInstance().getXmlInt("hud/height"))/210), scaledFontSize);
										
	IOmod::getInstance().writeText(strm.str(), Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(170*(Gamedata::getInstance().getXmlInt("hud/height"))/210), scaledFontSize);
										
	IOmod::getInstance().writeText(strm2.str(), Gamedata::getInstance().getXmlInt("hud/startLoc/x")+10 , Gamedata::getInstance().getXmlInt("hud/startLoc/y")
										+(185*(Gamedata::getInstance().getXmlInt("hud/height"))/210), scaledFontSize);
										
	//SDL_DestroyTexture(texture);
}

//HUD to be displayed when player wins game
void Hud::drawWinHuD() const {
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor( renderer, 180, 180, 180, 180 );
	SDL_Rect hudWin = {300, 270, 675 , 150};
        SDL_RenderFillRect( renderer, &hudWin );
	HuDFonts::getInstance().writeText("Harry has caught the Golden Snitch!", 325, 290, {255, 0, 36, 0});
	HuDFonts::getInstance().writeText("Press R to Try Again", 425, 350, {255, 0, 36, 0});
}

//HUD to be displayed when player loses game
void Hud::drawLostHuD() const {
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor( renderer, 180, 180, 180, 180 );
	SDL_Rect hudLost = {250, 285, 785 , 100};
        SDL_RenderFillRect( renderer, &hudLost );
	HuDFonts::getInstance().writeText("Harry has lost! Press R to help him again", 275, 300, {255, 0, 36, 0});

}

//HUD for displaying Bulletpool and God Mode Status
void Hud::drawModePoolHuD(bool godMode, unsigned int activeList, unsigned int freeList) const{
	SDL_Rect dest = { 1040, 8, 150 , 70 };
  	SDL_RenderCopy(renderer, texture2, NULL, &dest);
  	std::stringstream stream;
  		stream << "Active Spells: " << activeList;
  		IOmod::getInstance().
    		writeText(stream.str(), 1050, 10);
  		stream.clear();
  		stream.str("");
  		stream << "Spells Conjured: " << freeList;
  		IOmod::getInstance().
    		writeText(stream.str(), 1050, 30);
 	if(godMode)
  	{
		IOmod::getInstance().writeText("God Mode ON", 1050, 50);
  	}
  	else {
		IOmod::getInstance().writeText("God Mode OFF", 1050, 50);
  	}
}




  
