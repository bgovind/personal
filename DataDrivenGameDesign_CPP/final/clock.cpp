#include <cmath>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <queue>
#include "clock.h"
#include "gamedata.h"
#include "ioMod.h"

Clock& Clock::getInstance() {
  static Clock clock; 
  return clock;
}

Clock::~Clock()
{
	capturefps.clear();
}

Clock::Clock() :
  started(false), 
  paused(false), 
  FRAME_CAP_ON(Gamedata::getInstance().getXmlBool("frameCapOn")), 
  PERIOD(Gamedata::getInstance().getXmlInt("period")), 
  frames(0),
  avgFps(0),
  timeAtStart(0), 
  timeAtPause(0),
  currTicks(0), prevTicks(0), ticks(0), 
  frameMax(Gamedata::getInstance().getXmlInt("avgFrameMax")), 
  capturefps(),
  count(0)
  {
  
}

Clock::Clock(const Clock& c) :
  started(c.started), 
  paused(c.paused), 
  FRAME_CAP_ON(c.FRAME_CAP_ON), 
  PERIOD(c.PERIOD),
  frames(c.frames),
  avgFps(c.avgFps),
  timeAtStart(c.timeAtStart), timeAtPause(c.timeAtPause),
  currTicks(c.currTicks), prevTicks(c.prevTicks), ticks(c.ticks), 
  frameMax(Gamedata::getInstance().getXmlInt("avgFrameMax")), 
  capturefps(c.capturefps),
  count(c.count)
{
  startClock();
}

void Clock::toggleSloMo() {
  throw( std::string("Slow motion is not implemented yet") );
}

unsigned int Clock::getTicks() const { 
  if (paused) return timeAtPause;
  else return SDL_GetTicks() - timeAtStart; 
}

unsigned int Clock::getElapsedTicks() { 
  if (paused) return 0;

  currTicks = getTicks();
  ticks = currTicks-prevTicks;

  if ( FRAME_CAP_ON ) {
    if ( ticks < PERIOD ) return 0;
    prevTicks = currTicks;
    return ticks;
  }
  else {
    prevTicks = currTicks;
    return ticks;
  }

}

int Clock::getFps() const { 
  if ( getSeconds() > 0 ) return frames/getSeconds();  
  else if ( getTicks() > 5000 and getFrames() == 0 ) {
    throw std::string("Can't getFps if you don't increment the frames");
  }
  else return 0;
}


//Item 9: Calculate Average fps
void Clock::calcAvgFps()
{

		unsigned int size = capturefps.size();

		int fps = getFps();

		++count;

                int fpsSum = 0;

		capturefps.emplace_back(fps);
	
		if (frames > frameMax)
		{	
			if(size >  frameMax)
			{
				capturefps.pop_front();
				for (int i : capturefps)
				{
					fpsSum+=i;
				}
				avgFps = fpsSum/frameMax;
			}
		}
		else if (frames <= frameMax)
		{
			for (int i : capturefps)
			{
				fpsSum+=i;
			}
			avgFps = fpsSum/count;
		}
}


//Item 9: Return calculated average fps to Engine class
int Clock::getAvgFps() const 
{
	if(frames > 0)
	{	
		return avgFps;
	}
	else if ( getTicks() > 5000 and getFrames() == 0 ) 
	{	
    		throw std::string("Can't getFps if you don't increment the frames");
  	}
	else return 0;
        
}

void Clock::incrFrame() { 
  if ( !paused ) {
    ++frames; 
  }
}

void Clock::resetGame() {
  capturefps.clear();
  frames = 0;
  avgFps = 0;
  timeAtStart = 0;
  timeAtPause = 0;
  currTicks = 0;
  prevTicks = 0;
  ticks = 0;
  count = 0;
}

void Clock::startClock() { 
  started = true; 
  paused = false; 
  frames = 0;
  timeAtPause = timeAtStart = SDL_GetTicks(); 
  prevTicks = 0;
}

void Clock::pause() {
  if( started && !paused ) {
    timeAtPause = SDL_GetTicks() - timeAtStart;
    paused = true;
  }
}
void Clock::unpause() {
  if( started && paused ) {
    timeAtStart = SDL_GetTicks() - timeAtPause;
    paused = false;
  }
}

