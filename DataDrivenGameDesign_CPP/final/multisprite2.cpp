#include "multisprite2.h"
#include "gamedata.h"
#include "renderContext.h"

void MultiSprite2::advanceFrame(Uint32 ticks) 
{
	timeSinceLastFrame += ticks;
	
	if (numberOfFrames == 10 && timeSinceLastFrame > frameInterval) 
	{
		if ( getVelocityX() <= 0)
		{
			if(currentFrame > numberOfFrames/2)
			{
    				currentFrame = (currentFrame+(numberOfFrames/2)+1) % numberOfFrames;
				
			}
			else
			{
    				currentFrame = (currentFrame+1) % numberOfFrames;
			}
		}
		else
 		{
			currentFrame = (currentFrame+1) % (numberOfFrames/2);
				
		}
		timeSinceLastFrame = 0;
	}

	
}

MultiSprite2::MultiSprite2( const std::string& name, int xPos, int yPos) :
  Drawable(name, 
           Vector2f(xPos, yPos), 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/speedX"),
                    Gamedata::getInstance().getXmlInt(name+"/speedY"))
           ),
  frames( RenderContext::getInstance()->getFrames(name) ),

  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval")),
  timeSinceLastFrame( 0 ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),
  frameWidth(frames[0]->getWidth()),
  frameHeight(frames[0]->getHeight())
{ }

MultiSprite2::MultiSprite2(const MultiSprite2& s) :
  Drawable(s), 
  frames(s.frames),
  currentFrame(s.currentFrame),
  numberOfFrames( s.numberOfFrames ),
  frameInterval( s.frameInterval ),
  timeSinceLastFrame( s.timeSinceLastFrame ),
  worldWidth( s.worldWidth ),
  worldHeight( s.worldHeight ),
  frameWidth( s.frameWidth ),
  frameHeight( s.frameHeight )
  { }

void MultiSprite2::draw() const { 
  frames[currentFrame]->draw(getX(), getY());
}

/*void MultiSprite2::explode() { 
  //
}*/

void MultiSprite2::update(Uint32 ticks) { 
  advanceFrame(ticks);

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);

  if ( getY() < 150) {
    setVelocityY( fabs( getVelocityY() ) );
  }
  if ( getY() > (0.8*worldHeight)-frameHeight) {
    setVelocityY( -fabs( getVelocityY() ) );
  }

  if ( getX() < 0) {
    setVelocityX( fabs( getVelocityX() ) );
  }
  if ( getX() > worldWidth-frameWidth) {
    setVelocityX( -fabs( getVelocityX() ) );
  }  

}
