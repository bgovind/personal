#ifndef SCALEDSPRITE__H
#define SCALEDSPRITE__H
#include <string>
#include <vector>
#include <cmath>
#include "drawable.h"

class ScaledSprite : public Drawable {
public:
  ScaledSprite(const std::string&, const float);
  ScaledSprite(const ScaledSprite&);

  virtual void draw() const;
  virtual void update(Uint32 ticks);
  //virtual void explode();
  virtual const Frame* getFrame() const { 
    return frames[currentFrame]; 
  }

  //Get and set Scale
  inline float getScale()const{ return scale; }
  void  setScale(float s){ scale = s; }

protected:
  const std::vector<Frame *> frames;

  unsigned currentFrame;
  unsigned numberOfFrames;
  unsigned frameInterval;
  float timeSinceLastFrame;
  int worldWidth;
  int worldHeight;
  int frameWidth;
  int frameHeight;
  float scale;

  void advanceFrame(Uint32 ticks);

};
#endif
