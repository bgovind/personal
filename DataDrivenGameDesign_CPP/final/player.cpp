#include "player.h"
#include "gamedata.h"
#include "renderContext.h"

void Player::advanceFrame(Uint32 ticks) 
{
	timeSinceLastFrame += ticks;


	//Flip two way Player/Demenentor to move in opposite direction
	if (numberOfFrames == 4 && timeSinceLastFrame > frameInterval) 
	{
		if ( getVelocityX() <= 0)
		{
			if(currentFrame > numberOfFrames/2)
			{
    				currentFrame = (currentFrame+(numberOfFrames/2)+1) % numberOfFrames;
				
			}
			else
			{
    				currentFrame = (currentFrame+1) % numberOfFrames;
			}
		}
		else
 		{

			currentFrame = (currentFrame+1) % (numberOfFrames/2);
				
		}
		timeSinceLastFrame = 0;
	} 
	else if(numberOfFrames == 8 && timeSinceLastFrame > frameInterval)
	{
		if ( getVelocityX() <= 0)
		{
			if(currentFrame <= numberOfFrames/2)
			{
    				if(currentFrame <= numberOfFrames / 2){ 
                    			currentFrame = (currentFrame + (numberOfFrames / 2) + 1) % numberOfFrames; 
                		} 
				
			}
			else
			{
				currentFrame = (currentFrame + 1) % numberOfFrames; 
    				if(currentFrame <= numberOfFrames / 2){ 
                    			currentFrame = (currentFrame + (numberOfFrames / 2) + 1) % numberOfFrames; 
               			 } 
			}
		}
		else
 		{
			currentFrame = (currentFrame+1) % (numberOfFrames/2);
				
		}
		timeSinceLastFrame = 0;

	}
	
}

Player::Player( const std::string& name, int speedX, int speedY, int startX, int startY) :
  Playerdraw(name, 
           Vector2f(startX + (rand()%2? - 50 : 50), 
                    startY + (rand()%2? - 50 : 50)), 
           makeVelocity(speedX,speedY)
           ),
  currentMode( NORMAL ),
  explosion(NULL),
  frames( RenderContext::getInstance()->getFrames(name) ),

  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval")),
  initialVelocity(getVelocity()),
  slowdown(0.9),
  timeSinceLastFrame( 0 ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),
  frameWidth(frames[0]->getWidth()),
  frameHeight(frames[0]->getHeight()),
  explosionStatus(false),
  health(100),
  observers(),
  subjectPositionX(0),
  subjectPositionY(0),

  //Sphere of Influence of the Dementors. The dementor becomes smart and starts moving towards the player is the player is within 650 pixel range
  observerInfluence(650)
{ }

Player::Player( const std::string& name) :
  Playerdraw(name,
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/startLoc/x"), 
                    Gamedata::getInstance().getXmlInt(name+"/startLoc/y")), 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/speedX"),
                    Gamedata::getInstance().getXmlInt(name+"/speedY"))
           ),
  currentMode( NORMAL ),
  explosion(NULL),
  frames( RenderContext::getInstance()->getFrames(name) ),

  currentFrame(0),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval")),
  initialVelocity(getVelocity()),
  slowdown(0.9),
  timeSinceLastFrame( 0 ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),
  frameWidth(frames[0]->getWidth()),
  frameHeight(frames[0]->getHeight()),
  explosionStatus(false),
  health(100),
  observers(),
  subjectPositionX(0),
  subjectPositionY(0),
  
  //Sphere of Influence of the Dementors. The dementor becomes smart and starts moving towards the player is the player is within 650 pixel range
  observerInfluence(650)
{ }

Player::Player(const Player& s) :
  Playerdraw(s),
  currentMode( s.currentMode ),
  explosion(s.explosion),
  frames(s.frames),
  currentFrame(s.currentFrame),
  numberOfFrames( s.numberOfFrames ),
  frameInterval( s.frameInterval ),
  initialVelocity(s.initialVelocity),
  slowdown(s.slowdown),
  timeSinceLastFrame( s.timeSinceLastFrame ),
  worldWidth( s.worldWidth ),
  worldHeight( s.worldHeight ),
  frameWidth( s.frameWidth ),
  frameHeight( s.frameHeight ),
  explosionStatus(s.explosionStatus),
  health(s.health),
  observers(s.observers),
  subjectPositionX(s.subjectPositionX),
  subjectPositionY(s.subjectPositionY),
  observerInfluence(s.observerInfluence)
  { }

Player::~Player(){
        std::list<Playerdraw*>::iterator it = observers.begin();
	while(it != observers.end())
	{
		it = observers.erase(it);
	}
	delete explosion;
}


//Randomize movement of dementors when they are created
Vector2f Player::makeVelocity(int vx, int vy) const
{
	if(vx != 0 && vy != 0)
	{
	float a = Gamedata::getInstance().getRandInRange(vx -100, vx + 100);
	float b = Gamedata::getInstance().getRandInRange(vy - 100, vy + 100);
	a = a * (rand()%2? -1 : 1);
	b = b * (rand()%2? -1 : 1);
	vx += a;
	vy += b;
	//std::cout<< a << "," << b << std::endl;
	return Vector2f(a, b);
	}
	else
	{
	return Vector2f(vx,vy);
	}
}

float Player::getDistance(float xObs, float yObs, float xSub, float ySub) {
 float x = xObs-xSub;
 float y = yObs-ySub;
 return hypot(x, y);  
}

void Player::draw() const {

  //If Explosion is complete (Chunklist size is 0) then bring back player and draw the sprite but not dementor sprite 
  if(explosionStatus && explosion->chunkCount() > 0 && getName() == "harry")
  {
	explosion->draw();
	return;
  }
  else if(explosionStatus && getName() == "dementor")
  {
	explosion->draw();
	return;
  }
  else {
 	frames[currentFrame]->draw(getX(), getY());
  }
}

void Player::update(Uint32 ticks) {
  
  //Act as Player (Subject Class) if the class object type is Harry Player
  if(getName() == "harry") {
	//std::cout<<"Observer List size" << observers.size() << std::endl;
	std::list<Playerdraw*>::iterator it = observers.begin();
	while(it != observers.end())
	{
		static_cast<Player*>(*it)->notifyHarryPosition(getPosition());
		++it;
	}
  }

  //Act as Observer Class if the class object type is Dementor
  if(getName() == "dementor") {
	float x = getX()+getFrame()->getWidth()/2;
 	float y = getY()+getFrame()->getHeight()/2;
	float distanceToHarry = getDistance(x, y, subjectPositionX, subjectPositionY);
  	if( currentMode == NORMAL ) {
   		if( distanceToHarry < observerInfluence ) currentMode = SMART; //Set dementor mode to Smart when player is within range
  	}
 	else if( currentMode == SMART ) {
   		if( distanceToHarry > observerInfluence ) currentMode = NORMAL; //Set dementor mode to Normal when player is not in Range
  		else {
    			if( getX() >= subjectPositionX ) setVelocityX( -fabs( getVelocityX() ) ); //Change Direction of Dementor to left based on player location
    			if( getX() <= subjectPositionX ) setVelocityX( fabs( getVelocityX() ) );//Change Direction of Dementor to right based on player location
   		}
  	}
  }

  if(explosionStatus && explosion->chunkCount() > 0) 
	{ explosion -> update(ticks); return; }
  
  else{
  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  	setPosition(getPosition() + incr);
  
  advanceFrame(ticks);	
  if ( getY() < 0) {
    setVelocityY( fabs( getVelocityY() ) );
  }
  if ( getY() > worldHeight-frameHeight) {
    setVelocityY( -fabs( getVelocityY() ) );
  }

  if ( getX() < 0) {
    setVelocityX( fabs( getVelocityX() ) );
  }
  if ( getX() > worldWidth-frameWidth) {
    setVelocityX( -fabs( getVelocityX() ) );
  } 


 if(getName() == "harry") stop();
 }

}

//Explosion Method for Player and Dementor Sprites
void Player::explode(){
   if(explosionStatus) { return; }
   else{
   explosionStatus = true;
   //Create Temporary Sprite from frame for creating explosion using Chunks
   Sprite playerFrame(getName(), getPosition(), initialVelocity, getFrame());
   explosion = new ExplodingSprite(playerFrame);}
}

void Player::stop()
{
	setVelocityX(slowdown * getVelocityX());
	setVelocityY(0);
}

void Player::right()
{
	if(getX() < worldWidth-frameWidth)
	{
		setVelocityX(initialVelocity[0]);
	}

}

void Player::left()
{
	if(getX() > 0)
	{
		setVelocityX(-initialVelocity[0]);
	}
}

void Player::up()
{
	if(getY() > 150)
	{
		setVelocityY(-initialVelocity[0]);
	}
}

void Player::down()
{
	if(getY() < (worldHeight - frameHeight)*0.8)
	{
		setVelocityY(initialVelocity[0]);
	}
}

void Player::ne()
{
	if(getY() > 150 && getX() < worldWidth-frameWidth)
	{
		setVelocityY(-initialVelocity[0]);
		setVelocityX(initialVelocity[0]);
	}
	else if(getY() <= 150 && getX() < worldWidth-frameWidth)
	{
		setVelocityX(initialVelocity[0]);
	}
}

void Player::nw()
{
	if(getY() > 150 && getX() > 0)
	{
		setVelocityY(-initialVelocity[0]);
		setVelocityX(-initialVelocity[0]);
	}
	else if (getY() <= 150 && getX() > 0)
	{
		setVelocityX(-initialVelocity[0]);
	}
}

void Player::se()
{
	if(getY() < (worldHeight - frameHeight)*0.8 && getX() < worldWidth-frameWidth)
	{
		setVelocityY(initialVelocity[0]);
		setVelocityX(initialVelocity[0]);
	}
	else if (getY() >= (worldHeight - frameHeight)*0.8 && getX() < worldWidth-frameWidth)
	{
		setVelocityX(initialVelocity[0]);
	}
}

void Player::sw()
{
	if(getY() < (worldHeight - frameHeight)*0.8 && getX() > 0)
	{
		setVelocityY(initialVelocity[0]);
		setVelocityX(-initialVelocity[0]);
	}
	else if (getY() >= (worldHeight - frameHeight)*0.8 && getX() > 0)
	{
		setVelocityX(-initialVelocity[0]);
	}
}
