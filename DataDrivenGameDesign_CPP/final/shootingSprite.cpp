#include "shootingSprite.h"
#include "gamedata.h"

ShootingSprite::ShootingSprite(const std::string& name) :
  Player(name),
  bulletName( Gamedata::getInstance().getXmlStr(name+"/bullet") ),
  bullets( bulletName, name ),
  minSpeed( Gamedata::getInstance().getXmlInt(bulletName+"/speedX") )
{ }

ShootingSprite::ShootingSprite(const std::string& name, int speedX, int speedY, int StartX, int StartY) :
  Player(name, speedX, speedY, StartX, StartY),
  bulletName( Gamedata::getInstance().getXmlStr(name+"/bullet") ),
  bullets( bulletName, name ),
  minSpeed( Gamedata::getInstance().getXmlInt(bulletName+"/speedX") )
{ }


ShootingSprite::ShootingSprite(const ShootingSprite& s) :
  Player(s),
  bulletName(s.bulletName),
  bullets(s.bullets),
  minSpeed(s.minSpeed)
{ }

void ShootingSprite::shoot() { 
  float x = getX()+getFrame()->getWidth();
  float y = getY()+getFrame()->getHeight()/2;
  // I'm not adding minSpeed to y velocity:

  if(getVelocityX() > 0)
  {
  	bullets.shoot( Vector2f(x, y), 
    	Vector2f(minSpeed+getVelocityX(), 0));
  }
  else if(getVelocityX() < 0)
   {
	bullets.shoot( Vector2f((x-(frameWidth*1.3)), y), 
    	Vector2f(-(minSpeed+(-1*getVelocityX())), 0));

   }
}

bool ShootingSprite::collidedWith(const Playerdraw* obj) const {
  return bullets.collidedWith( obj );
}

void ShootingSprite::draw() const { 
  Player::draw();
  bullets.draw();
}

void ShootingSprite::update(Uint32 ticks) { 
  Player::update(ticks);
  bullets.update(ticks);
}

