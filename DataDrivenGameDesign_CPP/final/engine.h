#include <vector>
#include <SDL.h>
#include "ioMod.h"
#include "renderContext.h"
#include "clock.h"
#include "world.h"
#include "hud.h"
#include "health.h"
#include "HuDFonts.h"
#include "viewport.h"
#include "explodingSprite.h"
#include "sound.h"

class CollisionStrategy;

class Engine {
public:
  Engine ();
  ~Engine ();
  void play();
  void switchSprite();

private:
  const RenderContext* rc;
  const IOmod& io;
  const HuDFonts& ioHuD;
  Clock& clock;

  SDL_Renderer * const renderer;
  
  //Pitch, Directions to Play, Cloud and Mountain Backgrounds for parallax scrolling
  World pitch;
  World cloud;
  World mountain;
  World directions;

  Viewport& viewport;
  
  std::vector<Playerdraw*> player; //Player
  std::vector<Drawable*> sprites; //Other Ingame sprites like Flutteting Sprites and Golden Snitch
  std::vector<Playerdraw*> dementors; //Dementors
  std::vector<Drawable*> bludgers;	//Bludgers (Brown Orbs)
  std::vector<Drawable*> scaledSprites;	//Scaled Owl multisprites

  SDLSound& sound;	//Sound
  const Hud& hud;	//HUD
  Health& health;	//HealthBar
 

  int currentSprite;
  bool makeVideo;
  CollisionStrategy* strategy;
  bool textColorState;
  bool toggleHUD;
  int flagCount;
  bool keyState;
  bool hudResetUnder5;
  bool collisionDementor;
  bool collisionBludger;
  bool directHit;
  bool reset;
  bool godMode;  //Toggle Godmode
  bool result;
  bool lost;

  void draw() const;
  void update(Uint32);
  void checkForCollisions();  //Check for all kinds of collision
  void checkVictory();  //Check if player has caught the snitch
  void checkDirectHit(); //Check if player has shot spell at a dementor and explode dementor
  void generateDementor(); //Generate new dementors as the initial ones are killed.

  //Item 7: Explicitly disallow  use of compiler generated functions
  Engine(const Engine&) = delete;
  Engine& operator=(const Engine&) = delete;
};
