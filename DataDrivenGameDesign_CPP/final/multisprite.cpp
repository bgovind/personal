#include "multisprite.h"
#include "gamedata.h"
#include "renderContext.h"

void MultiSprite::advanceFrame(Uint32 ticks) 
{
	timeSinceLastFrame += ticks;

	if ( numberOfFrames == 2 && timeSinceLastFrame > frameInterval) 
	{
		currentFrame = (currentFrame+1) % numberOfFrames;
		timeSinceLastFrame = 0;
	}
	else if(numberOfFrames == 8 && timeSinceLastFrame > frameInterval)
	{
		if ( getVelocityX() <= 0)
		{
			if(currentFrame <= numberOfFrames/2)
			{
    				if(currentFrame <= numberOfFrames / 2){ 
                    			currentFrame = (currentFrame + (numberOfFrames / 2) + 1) % numberOfFrames; 
                		} 
				
			}
			else
			{
				currentFrame = (currentFrame + 1) % numberOfFrames; 
    				if(currentFrame <= numberOfFrames / 2){ 
                    			currentFrame = (currentFrame + (numberOfFrames / 2) + 1) % numberOfFrames; 
               			 } 
			}
		}
		else
 		{
			currentFrame = (currentFrame+1) % (numberOfFrames/2);
				
		}
		timeSinceLastFrame = 0;

	}
}

MultiSprite::MultiSprite( const std::string& name) :
  Drawable(name, 
           Vector2f(Gamedata::getInstance().getXmlInt(name+"/startLoc/x"), 
                    Gamedata::getInstance().getXmlInt(name+"/startLoc/y")), 
           makeVelocity(Gamedata::getInstance().getXmlInt(name+"/speedX"),
                    Gamedata::getInstance().getXmlInt(name+"/speedY"))
           ),
  frames( RenderContext::getInstance()->getFrames(name) ),

  currentFrame(1),
  numberOfFrames( Gamedata::getInstance().getXmlInt(name+"/frames") ),
  frameInterval( Gamedata::getInstance().getXmlInt(name+"/frameInterval")),
  initialVelocity(getVelocity()),
  timeSinceLastFrame( 0 ),
  worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
  worldHeight(Gamedata::getInstance().getXmlInt("world/height")),
  frameWidth(frames[0]->getWidth()),
  frameHeight(frames[0]->getHeight()),
  scale(1)

{ }

MultiSprite::MultiSprite(const MultiSprite& s) :
  Drawable(s),
  frames(s.frames),
  currentFrame(s.currentFrame),
  numberOfFrames( s.numberOfFrames ),
  frameInterval( s.frameInterval ),
  initialVelocity(s.initialVelocity),
  timeSinceLastFrame( s.timeSinceLastFrame ),
  worldWidth( s.worldWidth ),
  worldHeight( s.worldHeight ),
  frameWidth( s.frameWidth ),
  frameHeight( s.frameHeight ),
  scale(s.scale)

  { }

Vector2f MultiSprite::makeVelocity(int vx, int vy) const
{
	if(vx != 0 && vy != 0)
	{
	float a = Gamedata::getInstance().getRandInRange(vx -100, vx + 100);
	float b = Gamedata::getInstance().getRandInRange(vy - 100, vy + 100);
	a = a * (rand()%2? -1 : 1);
	b = b * (rand()%2? -1 : 1);
	vx += a;
	vy += b;
	//std::cout<< a << "," << b << std::endl;
	return Vector2f(a, b);
	}
	else
	{
	return Vector2f(vx,vy);
	}
}

void MultiSprite::draw() const {
  frames[currentFrame]->draw(getX(), getY());
}

void MultiSprite::update(Uint32 ticks) { 
  advanceFrame(ticks);

  Vector2f incr = getVelocity() * static_cast<float>(ticks) * 0.001;
  setPosition(getPosition() + incr);

  if ( getY() < -100) {
    setVelocityY( fabs( getVelocityY() ) );
  }
  if ( getY() > (worldHeight)+100) {
    //setVelocityY( -fabs( getVelocityY() ) );
    setY(-50);
  }

  if ( getX() < -100) {
    //setVelocityX( fabs( getVelocityX() ) );
    setX(Gamedata::getInstance().getXmlInt("world/width"));
  }
  if ( getX() > worldWidth-frameWidth) {
    setVelocityX( -fabs( getVelocityX() ) );
  }  

}

