#ifndef HARRY_H
#define HARRY_H

#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "animation.h"
#include <string>

class Harry
{
	private:
	const char* harryPath;
	//const char* snitchPath;
	int imageWidth;
	int imageHeight;
	

	int X1_POS;
	int Y1_POS;

	int x1P;
	int y1P;

	float X1_VEL;
	float Y1_VEL;


	const float val;
	const float lo;
	const float hi;
	const unsigned int DT;

	inline float clampX(const float val, const float lo, const float hi);
	inline float clampY(const float val, const float hi, const float lo);

	SDL_Renderer *myRenderer;
	SDL_Texture *harry;

	//SDL_Texture *mybackground;
	SDL_Texture *getHarryTexture();


	Harry(const Harry&);
	Harry operator=(const Harry&);
	
	
	public:

	Harry();
	Harry(SDL_Renderer *myRenderer);
	~Harry();
	
	void effects();
	void draw();
	void update();

};

#endif












