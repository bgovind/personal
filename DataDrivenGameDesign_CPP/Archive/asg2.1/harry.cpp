#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "harry.h"
#include<string>

Harry::~Harry() {
	std::cout<< "Harry Destroyed" << std::endl;
}

Harry::Harry(SDL_Renderer *renderer) : 
harryPath("images/harry1.png"), 
imageWidth(298), 
imageHeight(300), 
X1_POS(0), 
Y1_POS(0), 
x1P(0), 
y1P(500), 
X1_VEL(200.0), 
Y1_VEL(200.0),  
val(0.0), 
lo(0.0), 
hi(0.0), 
DT(17u), 
myRenderer(renderer), 
harry(getHarryTexture())
{
	std::cout << "Harry Created" << std::endl;	
}

void Harry::draw()
{
	SDL_Rect hSrcRect = {X1_POS, Y1_POS, imageWidth, imageHeight}; 
	SDL_Rect hDstRect = {x1P, y1P, 175, 200};

	
	SDL_RenderCopy(myRenderer, harry, &hSrcRect, &hDstRect);
	
	std::cout<<"Drawing Harry" << std::endl;
}

void Harry::update()
{
	static float x1 = x1P;
  	static float y1 = y1P;
	//static float x2 = x2P;
  	//static float y2 = y2P;
	
	static unsigned int remainder = 0u; // ***
  	static unsigned int prevTicks = SDL_GetTicks();
  	unsigned int currentTicks = SDL_GetTicks();
  	unsigned int elapsedTicks = currentTicks - prevTicks + remainder; // ***
	if( elapsedTicks < DT ) return;
	float dx1 = X1_VEL * DT * 0.001;
  	x1 += dx1;
	float dy1 = Y1_VEL * DT * 0.001;
  	y1 -= dy1;
	//float dx2 = X2_VEL * DT * 0.001;
  	//x2 += dx2;
	//float dy2 = Y2_VEL * DT * 0.001;
  	//y2 -= dy2;

	x1 = clampX(x1,0.f,1000-150);
	y1 = clampY(y1,550.f,10);

	//x2 = clampX(x2,0.f,950);
	//y2 = clampY(y2,720.f,100);

	prevTicks = currentTicks;
  	remainder = elapsedTicks - DT;
	x1P = x1;
	y1P = y1;
	//x2P = x2;
	//y2P = y2;

	
}

inline float Harry::clampX(const float val, const float lo, const float hi)
{
  return val <= lo ? lo : ( val >= hi ? hi : val);
}

inline float Harry::clampY(const float val, const float hi, const float lo)
{
  return val >= hi ? hi : ( val <= lo ? lo : val);
}

void Harry::effects()
{
	Y1_POS = Y1_POS + 315;
	//Y2_POS = Y2_POS + 562;
	if ( Y1_POS >= 600)
	{
		Y1_POS = 0;
	}
	/*if (Y2_POS >= 610)
	{
		Y2_POS = 0; 
	}*/

}




SDL_Texture* Harry::getHarryTexture()
{
  try {
    SDL_Texture *texture = IMG_LoadTexture(myRenderer, harryPath);
	std::cout<< "Harry path" << harryPath << std::endl;
 	std::cout<<"Harry Texture created" << std::endl;
    if ( texture == NULL ) {
      throw std::string("Couldn't load ") + harryPath;
    }
    return texture;
  }
  catch( const std::string& msg ) { 
    std::cout << msg << std::endl; 
    std::terminate();
  }
}

/*
SDL_Texture* Harry::getSnitchTexture()
{
  try {
    SDL_Texture *texture = IMG_LoadTexture(myRenderer, snitchPath);
	std::cout<< "Harry path" << snitchPath << std::endl;
 	std::cout<<"Harry Texture created" << std::endl;
    if ( texture == NULL ) {
      throw std::string("Couldn't load ") + snitchPath;
    }
    return texture;
  }
  catch( const std::string& msg ) { 
    std::cout << msg << std::endl; 
    std::terminate();
  }
}

*/
