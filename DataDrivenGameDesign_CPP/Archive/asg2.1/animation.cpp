#include<iostream>
#include<string>
#include "animation.h"

Animation::~Animation() 
{
	std::cout<< "Animation Destroyed" << std::endl;
}

Animation::Animation() : 
winTitle("Quidditch"), 
xPos(100), yPos(100), 
winWIDTH(1280), 
winHEIGHT(720), 
flag(0), 
done(false), 
nKeys(-1), 
frameTime(0), 
fps(17u), 
bgPath("images/axis.png"), 
makeVideo(false), 
window(SDL_CreateWindow(winTitle, xPos, yPos, winWIDTH, winHEIGHT, flag)), 
renderer(SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC)), 
background(getTexture()), 
harry(new Harry(renderer)), 
snitch(new Snitch(renderer, background)), 
shadow(new Shadow(renderer)), 
frameGen(new FrameGenerator(renderer, window))
{
	std::cout << "Animation Initialized" << std::endl;
}


void Animation::start()
{
	
	SDL_Event event;
	
  	const Uint8* keystate;

	while ( !done )

	{
		while (SDL_PollEvent(&event)){
      			if (event.type == SDL_QUIT) {
        			done = true;
      			}
      			keystate = SDL_GetKeyboardState(&nKeys);
      			if (keystate[SDL_SCANCODE_ESCAPE]) { 
        			done = true; 
      			}
			if (keystate[SDL_SCANCODE_F4]) { 
       				makeVideo = true;
      			}
			
		}
		frameTime ++;
				
		draw();
	}
	
	delete frameGen;
	delete shadow;
	delete snitch;
	delete harry;
	SDL_DestroyTexture(background);
	//SDL_DestroyTexture(harry);
	SDL_DestroyRenderer(renderer);
  	SDL_DestroyWindow(window);
  	SDL_Quit();
	

}


void Animation::draw()
{
	
	if ( makeVideo ) frameGen->makeFrame();	
	if ( fps/frameTime == 4)
	{
		frameTime = 0;
		harry->effects();
		snitch->effects();
	}
	SDL_RenderClear(renderer);
	snitch->draw();
	shadow->draw();
	harry->draw();
	snitch->update();
	shadow->update();
	harry->update();
		
	SDL_RenderPresent(renderer);
}


SDL_Texture* Animation::getTexture()
{
  try {
    SDL_Texture *texture = IMG_LoadTexture(renderer, bgPath);
	std::cout<< "image path" << bgPath << std::endl;
 	std::cout<<"Texture created" << std::endl;
    if ( texture == NULL ) {
      throw std::string("Couldn't load ") + bgPath;
    }
    return texture;
  }
  catch( const std::string& msg ) { 
    std::cout << msg << std::endl; 
    std::terminate();
  }
}
	
