#ifndef FRAME_H
#define FRAME_H

#include <string>
#include <SDL.h>

const int WIDTH  = 1280;
const int HEIGHT = 720;
const std::string USERNAME = "bgovind";
const int unsigned MAX_FRAMES = 500;


class FrameGenerator {
public:
  FrameGenerator(SDL_Renderer* const r, SDL_Window* const win) : 
    rend(r), window(win),
    frameCount(0) 
  {}
  ~FrameGenerator() {std::cout<< "FrameGen Destroyed" << std::endl;}
  void makeFrame();
  unsigned int getFrameCount() const { return frameCount; }
private:
  SDL_Renderer* const rend;
  SDL_Window* const window;
  unsigned int frameCount;
  FrameGenerator(const FrameGenerator&);
  FrameGenerator& operator=(const FrameGenerator&);
};

#endif
