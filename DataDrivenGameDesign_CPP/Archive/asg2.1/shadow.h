#ifndef SHADOW_H
#define SHADOW_H

#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "animation.h"
#include <string>

class Shadow
{
	private:
	const char* shadowPath;
	int imageWidth;
	int imageHeight;

	int X_POS;
	int Y_POS;
	int xP;
	int yP;
	float X_VEL;
	float Y_VEL;

	const float val;
	const float lo;
	const float hi;
	const unsigned int DT;

	inline float clampX(const float val, const float lo, const float hi);
	
	SDL_Renderer *myRenderer;
	SDL_Texture *shadow;

	//SDL_Texture *mybackground;
	SDL_Texture *getShadowTexture();

	Shadow(const Shadow&);
	Shadow operator = (const Shadow&);

	public:
	Shadow();
	Shadow(SDL_Renderer *myRenderer);
	~Shadow();

	void draw();
	void update();
	


};































#endif
