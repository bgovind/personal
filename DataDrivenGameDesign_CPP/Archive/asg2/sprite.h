//Sprite Class
#ifndef SPRITE__H
#define SPRITE__H

#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "harry.h"
#include "frameGenerator.h"
//#include<harry.h>
//#include<snitch.h>
//#include<shadow.h>

class Harry;

class Sprite 
{

	private:
	
	const char* winTitle;
	const int xPos;
	const int yPos;
	const int winWIDTH;
	const int winHEIGHT;
	const int flag;
	bool done;
	int nKeys;
	int frameTime;
	const unsigned int fps;
	const char* bgPath;
	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_Texture *background;
	SDL_Texture *getTexture();

	
	Harry *harry;
	//const char* bgPath;

	public:
	

	Sprite();
	~Sprite();

	//Snitch snitch;
	//Shadow shadow;
	//FrameGenerator frameGen;

	//SDL_Event event;
	void start();
	void draw();

	
	//void stop();
	
	
	

	
	
	

	//bool makevideo;
	//void draw() const;
	//void update();
	Sprite(const Sprite&);
	Sprite operator = (const Sprite&);
	
};



#endif


	
