#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "harry.h"
#include<string>

Harry::~Harry() {
	delete frameGen;
	std::cout<< "FrameGen Destroyed" << std::endl;
	std::cout<< "Harry Destroyed" << std::endl;
}

Harry::Harry(SDL_Renderer *renderer, SDL_Texture *background, SDL_Window *window) : harryPath("images/harry1.png"), imageWidth(298), imageHeight(300), X_POS(0), Y_POS(0), xP(0), yP(0), X_VEL(200.0), Y_VEL(200.0), val(0.0), lo(0.0), hi(0.0), DT(17u), makeVideo(false), myWindow(window), myRenderer(renderer), harry(getHarryTexture()), mybackground(background), frameGen(new FrameGenerator(myRenderer, myWindow))
{
	std::cout << "Harry Created" << std::endl;
}

void Harry::draw()
{
	//Sprite::dgetfrombase();
	SDL_Rect hSrcRect = {X_POS, Y_POS, imageWidth, imageHeight}; 
	SDL_Rect hDstRect = {xP, yP, 175, 200};

	SDL_RenderClear(myRenderer);
	SDL_RenderCopy(myRenderer, mybackground, NULL, NULL);
	SDL_RenderCopy(myRenderer, harry, &hSrcRect, &hDstRect);
	std::cout<<"Drawing Background and Harry" << std::endl;
	SDL_RenderPresent(myRenderer);
}

void Harry::update()
{
	static float x = X_POS;
  	static float y = Y_POS;
	static unsigned int remainder = 0u; // ***
  	static unsigned int prevTicks = SDL_GetTicks();
  	unsigned int currentTicks = SDL_GetTicks();
  	unsigned int elapsedTicks = currentTicks - prevTicks + remainder; // ***
	if( elapsedTicks < DT ) return;
	if ( makeVideo ) frameGen->makeFrame();
	float dx = X_VEL * DT * 0.001;
  	x += dx;
	float dy = Y_VEL * DT * 0.001;
  	y += dy;

	x = clamp(x,0.f,1050-175);
	y = clamp(y,0.f,720-200);

	prevTicks = currentTicks;
  	remainder = elapsedTicks - DT;
	xP = x;
	yP = y;

	
}

inline float Harry::clamp(const float val, const float lo, const float hi)
{
  return val <= lo ? lo : ( val >= hi ? hi : val);
}

void Harry::effects()
{
	Y_POS = Y_POS + 315;
	if ( Y_POS >= 600)
	{
		Y_POS = 0;
	}

}




SDL_Texture* Harry::getHarryTexture()
{
  try {
    SDL_Texture *texture = IMG_LoadTexture(myRenderer, harryPath);
	std::cout<< "Harry path" << harryPath << std::endl;
 	std::cout<<"Harry Texture created" << std::endl;
    if ( texture == NULL ) {
      throw std::string("Couldn't load ") + harryPath;
    }
    return texture;
  }
  catch( const std::string& msg ) { 
    std::cout << msg << std::endl; 
    std::terminate();
  }
}



