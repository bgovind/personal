#ifndef HARRY_H
#define HARRY_H

#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include "sprite.h"
#include "frameGenerator.h"
#include <string>

class Harry
{
	private:
	const char* harryPath;
	int imageWidth;
	int imageHeight;

	int X_POS;
	int Y_POS;
	int xP;
	int yP;
	float X_VEL;
	float Y_VEL;

	const float val;
	const float lo;
	const float hi;
	const unsigned int DT;

	inline float clamp(const float val, const float lo, const float hi);
	bool makeVideo;
	SDL_Window *myWindow;
	SDL_Renderer *myRenderer;
	SDL_Texture *harry;
	SDL_Texture *mybackground;
	SDL_Texture *getHarryTexture();
	FrameGenerator *frameGen;
	Harry(const Harry&);
	Harry operator=(const Harry&);
	
	
	public:
	//SDL_Rect hDstRect;

	Harry();
	Harry(SDL_Renderer *myRenderer, SDL_Texture *mybackground, SDL_Window *myWindow);
	~Harry();
	
	void effects();
	void draw();
	void update();
	

};

#endif












