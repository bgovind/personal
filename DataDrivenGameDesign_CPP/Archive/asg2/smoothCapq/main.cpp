// To compile and link, need to add link flag in Makefile: -lSDL2_image
// Also need to install libsdl2-image-dev

#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include "frameGenerator.h"


const int X_POS = 0;
const int Y_POS = 0;
const float X1_VEL = 165.0;
const float Y1_VEL = 165.0;
const float X2_VEL = 200.0;
const float Y2_VEL = 200.0;
const float X3_VEL = 200.0;
const float Y3_VEL = 200.0;

// Approximately 60 frames per second: 60/1000
const unsigned int DT = 17u; // ***

SDL_Texture* getTexture(SDL_Renderer* rend, const std::string& filename) 
{
  try {
    SDL_Texture *texture = IMG_LoadTexture(rend, filename.c_str());
    if ( texture == NULL ) {
      throw std::string("Couldn't load ") + filename;
    }
    return texture;
  }
  catch( const std::string& msg ) { 
    std::cout << msg << std::endl; 
    std::terminate();
  }
}

void draw(SDL_Renderer* rend, SDL_Texture* back, SDL_Texture* star, SDL_Texture* harry, SDL_Texture* shadow, const SDL_Rect& dstrect, const SDL_Rect& dsdrect,  const SDL_Rect& dhtrect, const SDL_Rect& dhstrect, const SDL_Rect& dshtrect) 
{
  SDL_RenderClear(rend);
  SDL_RenderCopy(rend, back, NULL, NULL);
  SDL_RenderCopy(rend, star, &dstrect , &dsdrect);
  SDL_RenderCopy(rend, harry, &dhstrect, &dhtrect);//harry->draw()//Harry::draw() { SDL_RenderCopy(rend, harry, &dhstrect, &dhtrect); }
  SDL_RenderCopy(rend, shadow, NULL, &dshtrect);
  SDL_RenderPresent(rend);
}

/*
void draw(SDL_Renderer* rend, SDL_Texture* back, SDL_Texture* star, SDL_Texture* harry, SDL_Texture* shadow, SDL_Texture* msg, const SDL_Rect& dstrect, const SDL_Rect& dsdrect, const SDL_Rect& dhtrect, const SDL_Rect& dhstrect, const SDL_Rect& dmtrect, const SDL_Rect& dshtrect)
{

  SDL_RenderClear(rend);
  SDL_RenderCopy(rend, back, NULL, NULL);
  SDL_RenderCopy(rend, star, &dstrect, &dsdrect);
  SDL_RenderCopy(rend, harry, &dhstrect, &dhtrect);
  SDL_RenderCopy(rend, shadow, NULL, &dshtrect);
  SDL_RenderCopy(rend, msg, NULL, &dmtrect);
  SDL_RenderPresent(rend);
} 

*/

inline float clamp(const float val, const float lo, const float hi)
{
  return val <= lo ? lo : ( val >= hi ? hi : val);
}

void update(SDL_Rect& dstrect, SDL_Rect& dhtrect, SDL_Rect& dshtrect, FrameGenerator& frameGen, bool makeVideo) 
{
  static float x1 = 250;
  static float y1 = 250;
  static float x2 = X_POS;
  static float y2 = Y_POS;
  static float x3 = X_POS;
  static float y3 = 710;
  

  static unsigned int remainder = 0u; // ***
  static unsigned int prevTicks = SDL_GetTicks();
  unsigned int currentTicks = SDL_GetTicks();
  unsigned int elapsedTicks = currentTicks - prevTicks + remainder; // ***

  if( elapsedTicks < DT ) return;

  // Generate a frame:
  if ( makeVideo ) frameGen.makeFrame();

  float dx1 = X1_VEL * DT * 0.001;
  x1 += dx1;
  float dx2 = X2_VEL * DT * 0.001;
  x2 += dx2;
  float dy1 = Y1_VEL * DT * 0.001;
  y1 += dy1;
  float dy2 = Y2_VEL * DT * 0.001;
  y2 += dy2;
  float dx3 = X3_VEL * DT * 0.001;
  x3 += dx3;
  
   
  x1 = clamp(x1,0.f,1000-dstrect.w);
  y1 = clamp(y1,0.f,650-dstrect.h);
  x2 = clamp(x2,0.f,1050-dhtrect.w);
  std::cout<< "x2 : " << x2 << std::endl;
  y2 = clamp(y2,0.f,720-dhtrect.h);
  std::cout<< "y2 : " << y2 << std::endl;
  x3 = clamp(x3,0.f,1050-dshtrect.w);
  y3 = clamp(y3,0.f,720-dshtrect.h);

  prevTicks = currentTicks;
  remainder = elapsedTicks - DT; // ***

  dstrect.x = x1;
  dstrect.y = y1;
  dhtrect.x = x2;
  dhtrect.y = y2;
  dshtrect.x = x3;
  dshtrect.y = y3;
  
}

int main( ) {
  int posX = 100, posY = 100;
  //const int fps = 60;
  int frameTime = 0;
  SDL_Init(SDL_INIT_VIDEO);

  SDL_Window *window = 
    SDL_CreateWindow("Harry Potter Animation", posX, posY, 1280, 720, 0);

  SDL_Renderer *renderer = 
    SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);

  SDL_Texture *background = getTexture(renderer, "images/axis.png");
  SDL_Texture *yellowstar = getTexture(renderer, "images/yellowstar1.png");
  SDL_Texture *harry = getTexture(renderer, "images/harry1.png");//
  //SDL_Texture *msg = getTexture(renderer, "images/msg.png");
  SDL_Texture *shadow = getTexture(renderer, "images/shadow.png");

  SDL_Event event;
  const Uint8* keystate;
  int nKeys=-1;
  SDL_Rect dstrect = {X_POS, Y_POS, 692, 562};
  SDL_Rect dsdrect = {X_POS, Y_POS, 50, 50};

  SDL_Rect dhsrect = {X_POS, Y_POS, 298, 300};
  SDL_Rect dhtrect = {X_POS, Y_POS, 175, 200};
  
  SDL_Rect dshtrect = {X_POS, Y_POS, 200, 25};
  //SDL_Rect dmtrect = {1050, 550, 150,150};
  bool makeVideo = false;	;
  bool done = false;
  FrameGenerator frameGen(renderer, window);

  while ( !done ) {
    while ( SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT) {
        done = true;
      }
      keystate = SDL_GetKeyboardState(&nKeys);
      if (keystate[SDL_SCANCODE_ESCAPE]) { 
        done = true; 
      }
      if (keystate[SDL_SCANCODE_F4]) { 
        makeVideo = true;
      }
    }
	
	frameTime++;

    if (DT/frameTime == 4)
	{
		frameTime = 0;
		dhsrect.y = dhsrect.y + 15 +  dhsrect.h;
		dstrect.y = dstrect.y + dstrect.h;
		if (dhsrect.y >= 600)
		{
			dhsrect.y = Y_POS;
		}
		if (dstrect.y >= 610)
		  {
		    dstrect.y = Y_POS;
		  }
	}
    
   
     draw(renderer, background, yellowstar, harry, shadow, dstrect, dsdrect,  dhtrect, dhsrect, dshtrect);
     update(dsdrect, dhtrect, dshtrect, frameGen, makeVideo);
    
  }

  SDL_DestroyTexture(yellowstar);
  SDL_DestroyTexture(background);
  SDL_DestroyTexture(harry);
  //SDL_DestroyTexture(msg);
  SDL_DestroyTexture(shadow);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}
