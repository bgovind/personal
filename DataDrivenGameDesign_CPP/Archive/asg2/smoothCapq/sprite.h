//Sprite Class

#include<iostream>
#include<SDL.h>
#include<SDL_image.h>
#include<harry.h>
#include<snitch.h>
#include<shadow.h>
#include<string>


class Sprite 
{

	public:
	Sprite();
	~Sprite();

	SDL_window* window;
	SDL_renderer* renderer;
	
	//Harry harry;
	//Snitch snitch;
	//Shadow shadow;
	//FrameGenerator frameGen;

	
	
	void start();
	
	
	private:
	
	static const string winTitle;
	static const int xPos;
	static const int yPos;
	static const int winWIDTH;
	static const int winHEIGHT;
	static const int flag;
	
	

	//bool makevideo;
	//void draw() const;
	//void update();
	Sprite(const Sprite&);
	Sprite operator = (const Sprite&);
	
	
	
	

};


	
