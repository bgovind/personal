Project 1: Currency Converter V2.0

-The Currency Converter is an android application that converts USD amount to Indian Rupee (INR), Great Britain Pound (GBP), Euro (EURO) and Chinese Yuan (CYN). 
-V2.0 consists of three pages, Main Landing page, History Page and Info Page. The info page can be accessed by using the Info option from overflow menu. 
-The main landing page has two EditText fields (1 to enter USD and editable, 1 to display converted exchange amount and disabled), 4 Buttons (INR, GBP, EUR, CNY & Reset). 
-Based on currency button clicked the app converts the amount in USD to field to equivalent exchange and displays it in a toast and also a non-editable EditText field 
(Non-editable field is not populated when there is no internet).
-History page gives 10 most recent exchange entries the user had done from main page.

Key Highlights:

-The app uses realtime exchange values from "fixer.io" API webservice and obtains latest currency exchange values in JSON and parses the JSON objects to get exchange amount 4
when connected to internet.

-When there is no internet the app uses approximate default exchange value for each currency and displays the exchange value along with a message to user that internet 
connectivity is needed for latest accurate exchange values.

-V2.0 now has a history page that gives you 10 of your most recent exchange entries (Read)

-These are stored in an internal SQLITE DB when you check exchange exchange amount on main page (Create)

-You can tap on each USD value to update it (Update) and get an exchange value for old exchange rate that was prevalent on that particular date (Date Column).4

-You can also clear (Delete) your history whenver you wish using Clear History button.