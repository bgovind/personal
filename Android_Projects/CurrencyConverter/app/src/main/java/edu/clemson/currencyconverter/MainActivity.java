package edu.clemson.currencyconverter;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class MainActivity extends AppCompatActivity {

    //set initial network connectivity status to 0
    private int networkStatus = 0;
    private String curr_currency;


    //Check if USD amount to be entered by the user is left empty before a button is pressed
    private boolean isEmpty(EditText inputText) {
        if (inputText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    //Generate Toast to visually notify the user that field is blank
    private void blankCheck() {
        Toast.makeText(MainActivity.this, "Cannot be Blank", Toast.LENGTH_SHORT).show();
    }

    //Inout Method Manager method to close virtual keyboard service to display entire screen after a button is pressed.
    private void vKeyboardClose() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    //Set Converted Value to Null when there is no internet connectivity
    private void setConvAmtNull() {
        EditText newAmt = (EditText) findViewById(R.id.cnv_amt);
        newAmt.setText(null);
    }

    //Implementation
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button inrHit = (Button) findViewById(R.id.inr);
        Button gbpHit = (Button) findViewById(R.id.gbp);
        Button eurHit = (Button) findViewById(R.id.euro);
        Button cnyHit = (Button) findViewById(R.id.cny);
        Button resetHit = (Button) findViewById(R.id.reset);

        //OnClick listener to convert to USD to GBP when GBP button is pressed.
        gbpHit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText usd = (EditText) findViewById(R.id.amt);
                curr_currency = "GBP";
                setConvAmtNull();
                if (isEmpty(usd)) {
                    blankCheck();
                } else {
                    //Call JASONTask to get latest GBP currency exchange value JSON from fixer.io webservice.
                    JSONTask myJSONTask = (JSONTask)new JSONTask().execute("http://api.fixer.io/latest?base=USD", "GBP");
                    networkStatus = myJSONTask.networkStatus();

                    //Convert with default approximate exchange for INR value when there is no internet
                    EditText excAmt = (EditText) findViewById(R.id.cnv_amt);
                    System.out.println("Status" + networkStatus);
                    if (networkStatus == 0) {
                        Double dollar = Double.parseDouble(usd.getText().toString());
                        Double gbpAmt = dollar * 0.83;
                        //Display Converted Value in Toast
                        Toast.makeText(MainActivity.this, "GBP " + String.format("%.2f", gbpAmt) + " (This is an approx. Exchange Value. Please connect to Internet)", Toast.LENGTH_SHORT).show();
                        EditText exchAmt = (EditText) findViewById(R.id.cnv_amt);
                        exchAmt.setText(String.format("%.2f", gbpAmt));
                    }

                    //Change Image to GBP symbol
                    ImageView image = (ImageView) findViewById(R.id.conv_img);
                    image.setImageResource(R.drawable.ukp);

                    vKeyboardClose();
                    networkStatus = 0;
                }

            }
        });

        inrHit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                EditText usd = (EditText) findViewById(R.id.amt);
                curr_currency = "INR";
                setConvAmtNull();
                if (isEmpty(usd)) {
                    blankCheck();
                } else {
                    //Call JASONTask to get latest INR currency exchange value JSON from fixer.io webservice.

                    JSONTask myJSONTask = (JSONTask) new JSONTask().execute("http://api.fixer.io/latest?base=USD", "INR");
                    networkStatus = myJSONTask.networkStatus();



                    //Convert with default approximate exchange for INR value when there is no internet
                    System.out.println("Status" + networkStatus);
                    if (networkStatus == 0) {
                        Double dollar = Double.parseDouble(usd.getText().toString());
                        Double inrAmt = dollar * 68;
                        //Display Converted Value in Toast
                        Toast.makeText(MainActivity.this, "INR " + String.format("%.2f", inrAmt) + " (This is an approx. Exchange Value. Please connect to Internet)", Toast.LENGTH_SHORT).show();
                        EditText exchAmt = (EditText) findViewById(R.id.cnv_amt);
                        exchAmt.setText(String.format("%.2f", inrAmt));
                    }
                    //EditText excAmt = (EditText) findViewById(R.id.cnv_amt);


                    //Change Image to INR symbol
                    ImageView image = (ImageView) findViewById(R.id.conv_img);
                    image.setImageResource(R.drawable.inr);

                    vKeyboardClose();
                    networkStatus = 0;
                }

            }
        });


        eurHit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText usd = (EditText) findViewById(R.id.amt);
                curr_currency = "EUR";
                setConvAmtNull();
                if (isEmpty(usd)) {
                    blankCheck();
                } else {
                    //Call JASONTask to get latest EUR currency exchange value JSON from fixer.io webservice.
                    JSONTask myJSONTask= (JSONTask) new JSONTask().execute("http://api.fixer.io/latest?base=USD", "EUR");
                    networkStatus = myJSONTask.networkStatus();

                    //Convert with default approximate exchange value for EUR when there is no internet
                    EditText excAmt = (EditText) findViewById(R.id.cnv_amt);
                    System.out.println("Status" + networkStatus);
                    if (networkStatus == 0) {
                        Double dollar = Double.parseDouble(usd.getText().toString());
                        Double eurAmt = dollar * 0.94;
                        System.out.println(eurAmt);
                        //Display Converted Value in Toast
                        Toast.makeText(MainActivity.this, "EUR " + String.format("%.2f", eurAmt) + " (This is an approx. Exchange Value. Please connect to Internet)", Toast.LENGTH_SHORT).show();
                        EditText exchAmt = (EditText) findViewById(R.id.cnv_amt);
                        exchAmt.setText(String.format("%.2f", eurAmt));
                    }

                    //Change Image to EUR symbol
                    ImageView image = (ImageView) findViewById(R.id.conv_img);
                    image.setImageResource(R.drawable.euro);

                    vKeyboardClose();
                    networkStatus = 0;
                }

            }
        });

        cnyHit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText usd = (EditText) findViewById(R.id.amt);
                curr_currency = "CNY";
                setConvAmtNull();
                if (isEmpty(usd)) {
                    blankCheck();
                } else {
                    //Call JASONTask to get latest CNY currency exchange value JSON from fixer.io webservice.

                    JSONTask myJSONTask = (JSONTask)new JSONTask().execute("http://api.fixer.io/latest?base=USD", "CNY");
                    networkStatus = myJSONTask.networkStatus();


                    //Convert with default approximate exchange value for CNY when there is no internet
                    EditText excAmt = (EditText) findViewById(R.id.cnv_amt);
                    System.out.println("Status" + networkStatus);
                    if (networkStatus == 0) {
                        Double dollar = Double.parseDouble(usd.getText().toString());
                        Double cnyAmt = dollar * 6.89;
                        //Display Converted Value in Toast
                        Toast.makeText(MainActivity.this, "CNY " + String.format("%.2f", cnyAmt) + " (This is an approx. Exchange Value. Please connect to Internet)", Toast.LENGTH_LONG).show();
                        EditText exchAmt = (EditText) findViewById(R.id.cnv_amt);
                        exchAmt.setText(String.format("%.2f", cnyAmt));
                    }

                    //Change Image to CNY symbol
                    ImageView image = (ImageView) findViewById(R.id.conv_img);
                    image.setImageResource(R.drawable.cny);
                    EditText inrAmt = (EditText) findViewById(R.id.cnv_amt);

                    vKeyboardClose();
                    networkStatus = 0;
                }

            }
        });

        //Onclick listener to reset all fields to default when reset button is pressed
        resetHit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ImageView image = (ImageView) findViewById(R.id.conv_img);
                image.setImageResource(R.drawable.usd);
                EditText resetText = (EditText) findViewById(R.id.amt);
                resetText.setText(null);
                EditText resetAmt = (EditText) findViewById(R.id.cnv_amt);
                resetAmt.setText(null);

            }
        });


    }

    //JSONTask inherited from Async Task to get currency exchange values in JSON format from fixer.io webservice

    public class JSONTask extends AsyncTask<String, String, Double> {

        //Separation of webservice connection request and responses from UI thread
        @Override
        protected Double doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                //Open connection with fixer.io webservice
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                //Store JSON values from webservice in a String buffer
                StringBuffer buffer = new StringBuffer();

                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                //Parse JSON objects from string buffer and obtain exchange value for required currency (Base currency = USD)
                if (connection != null) {
                    networkStatus = 1;
                    System.out.println("paseJSON"+networkStatus);
                    String finalJson = buffer.toString();
                    JSONObject parentObject = new JSONObject(finalJson);
                    JSONObject childObject = parentObject.getJSONObject("rates");
                    Log.i("Buffer", buffer.toString());
                    if(params[1] == null)
                    {
                        return null;
                    }
                    else {
                        Double exVal = childObject.getDouble(params[1]);
                        return exVal;
                    }
                    //return exchange value as double to post execute method

                } else {
                    networkStatus = 0;
                    System.out.println("paseJSON"+networkStatus);
                }
                System.out.println(networkStatus);
                //Catch Exceptions due to no internet or improper webservice response
            } catch (MalformedURLException e) {
                e.printStackTrace();
                networkStatus = 0;
                System.out.println("Exception"+networkStatus);
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                networkStatus = 0;
                System.out.println("Exception"+networkStatus);
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
                networkStatus = 0;
                System.out.println("Exception"+networkStatus);
                return null;
            } finally {
                if (connection != null) {
                    //Close connection to service fixer.io
                    connection.disconnect();
                }

                try {
                    if (reader != null) {
                        //close JSON reader
                        networkStatus = 1;
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    networkStatus = 0;
                    System.out.println("Exception"+networkStatus);
                    return null;
                }
            }
            return null;
        }

        //Post execute to get requested exchange value from JSONTask and display exchange value of requested USD amount
        @Override
        protected void onPostExecute(Double result) {
            super.onPostExecute(result);
            System.out.println(result);
            //When JSON response was interrupted due to no internet connectivity return value from JSONTask will be null.
            //This routine will check for null return
            if (result != null) {
                networkStatus = 1;
                System.out.println("PostExe"+networkStatus);
                EditText usd = (EditText) findViewById(R.id.amt);
                Double dollar = Double.parseDouble(usd.getText().toString());
                Double cnvAmt = dollar * result;
                Toast.makeText(MainActivity.this, "USD " + dollar.toString() + " = " + curr_currency+" " + String.format("%.2f", cnvAmt), Toast.LENGTH_SHORT).show();
                EditText exchAmt = (EditText) findViewById(R.id.cnv_amt);
                exchAmt.setText(String.format("%.2f", cnvAmt));
                String date = new SimpleDateFormat("yy-MM-dd").format(new Date());
                String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());


                //Add current action entry into internal SQLite DB for history
                try{

                    SQLiteDatabase historyDB = (MainActivity.this).openOrCreateDatabase("History", MODE_PRIVATE, null);


                    historyDB.execSQL("CREATE TABLE IF NOT EXISTS history (id INTEGER PRIMARY KEY, time VARCHAR(20), date VARCHAR(10), usd DOUBLE, currency CHARACTER(3), exc_amt DOUBLE)");

                    historyDB.execSQL("INSERT INTO history (time, date, usd, currency, exc_amt) VALUES('"+time+"', '"+date+"', '"+dollar+"', '"+curr_currency+"', '"+cnvAmt+"')");

                    Cursor c = historyDB.rawQuery("SELECT * FROM history", null);

                    int id = c.getColumnIndex("id");

                    int timeIndex = c.getColumnIndex("time");

                    int dateIndex = c.getColumnIndex("date");
                    int usdIndex = c.getColumnIndex("usd");


                    int currIndex = c.getColumnIndex("currency");
                    int exc_amtIndex = c.getColumnIndex("exc_amt");


                    c.moveToFirst();

                    while(c != null) {

                        Log.i("id:", c.getString(id));

                        Log.i("time:", c.getString(timeIndex));

                        Log.i("date:", c.getString(dateIndex));

                        Log.i("USD:", c.getString(usdIndex));

                        Log.i("Currency:", c.getString(currIndex));
                        Log.i("conv :", c.getString(exc_amtIndex));
                        c.moveToNext();
                    }



                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            //If JSON response is null networkStatus will be set to 0;
            else if (result == null) {
                networkStatus = 0;
                System.out.println("PostExe"+networkStatus);
            }
            System.out.println(networkStatus);
        }


        private int networkStatus()
        {
            if (networkStatus == 0) {
                System.out.println("netstatus"+networkStatus);
                return 0;
            }
            else {
                System.out.println("netstatus"+networkStatus);
                return 1;
            }
        }

    }

    //Options Overflow Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    //Start other activities from options overflow menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.info)
        {
            startActivity(new Intent(getApplicationContext(), Main2Activity.class));

        }

        if (id == R.id.history)
        {
            startActivity(new Intent(getApplicationContext(), Main3Activity.class));
        }

        if (id == R.id.main)
        {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }

        return true;
    }

    //Alert Dialog to get confirmation from user to exit app when back is pressed from Main Screen
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exit Currency Converter")
                .setMessage("Are you sure you want to exit Currency Converter?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }


}

//End of MainActivity




