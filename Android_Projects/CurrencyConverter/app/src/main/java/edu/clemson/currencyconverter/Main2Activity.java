package edu.clemson.currencyconverter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }

    //Options Overflow Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    //Start Info activity from options overflow menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.info)
        {
            startActivity(new Intent(getApplicationContext(), Main2Activity.class));

        }

        if (id == R.id.history)
        {
            startActivity(new Intent(getApplicationContext(), Main3Activity.class));
        }

        if (id == R.id.info)
        {
            startActivity(new Intent(getApplicationContext(), Main2Activity.class));

        }

        if (id == R.id.main)
        {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
        return true;
    }
}
