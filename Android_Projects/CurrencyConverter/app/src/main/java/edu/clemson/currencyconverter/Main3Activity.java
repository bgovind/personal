package edu.clemson.currencyconverter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Main3Activity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        Button clrHistory = (Button) findViewById(R.id.clr);

        //Delete entire History Table
        clrHistory.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                //Alert dialog to get confirmation from user to delete entire history
                new AlertDialog.Builder(Main3Activity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Delete History")
                        .setMessage("Do you want to delete the exchange history?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i)
                            {
                                try {
                                    SQLiteDatabase historyDB = (Main3Activity.this).openOrCreateDatabase("History", MODE_PRIVATE, null);
                                    historyDB.execSQL("CREATE TABLE IF NOT EXISTS history (id INTEGER PRIMARY KEY, time VARCHAR(20), date VARCHAR(10), usd DOUBLE, currency CHARACTER(3), exc_amt DOUBLE)");

                                    historyDB.execSQL("DELETE FROM history");
                                    //startActivity(new Intent(getApplicationContext(), Main3Activity.class));
                                    recreate();
                                    Toast.makeText(Main3Activity.this, "History deleted", Toast.LENGTH_SHORT).show();
                                }catch(Exception e){
                                    e.printStackTrace();
                            }
                            }
                        })
                        .setNegativeButton("No",null)
                        .show();
            }
        });


        // Populate last 10 entries in history table from Internal SQLite DB
        try {

            SQLiteDatabase historyDB = (Main3Activity.this).openOrCreateDatabase("History", MODE_PRIVATE, null);
            historyDB.execSQL("CREATE TABLE IF NOT EXISTS history (id INTEGER PRIMARY KEY, time VARCHAR(20), date VARCHAR(10), usd DOUBLE, currency CHARACTER(3), exc_amt DOUBLE)");

            Cursor c = historyDB.rawQuery("SELECT * FROM history ORDER BY time DESC LIMIT 10", null);

            int idIndex = c.getColumnIndex("id");


            int dateIndex = c.getColumnIndex("date");
            int usdIndex = c.getColumnIndex("usd");
            int timeIndex = c.getColumnIndex("time");


            int currIndex = c.getColumnIndex("currency");
            int exc_amtIndex = c.getColumnIndex("exc_amt");


            c.moveToFirst();

            int row = 2;

            while (c != null ) {

                /*if(row > 11){
                    break;
                }
                else {*/

                    String idNo = c.getString(idIndex);
                    String idCell = "r"+row+"c0";
                    int idID = getResources().getIdentifier(idCell, "id", getPackageName());
                    TextView rowID = (TextView) findViewById(idID);
                    rowID.setText(idNo);


                    //String time = c.getString(timeIndex);

                    String date = c.getString(dateIndex);
                    String dateCell = "r"+row+"c1";
                    int dateID = getResources().getIdentifier(dateCell, "id", getPackageName());
                    TextView rowDate = (TextView) findViewById(dateID);
                    rowDate.setText(date);



                    Double usdVal = c.getDouble(usdIndex);
                    String usdCell = "r"+row+"c2";
                    int usdID = getResources().getIdentifier(usdCell, "id", getPackageName());
                    TextView rowUSD = (TextView) findViewById(usdID);
                    rowUSD.setText(String.format("%.2f", usdVal));


                    String currName = c.getString(currIndex);
                    String currNameCell = "r"+row+"c3";
                    int currID = getResources().getIdentifier(currNameCell, "id", getPackageName());
                    TextView rowCurrName = (TextView) findViewById(currID);
                    rowCurrName.setText(currName);



                    Double convVal = c.getDouble(exc_amtIndex);
                    String conValCell = "r"+row+"c4";
                    int convValID = getResources().getIdentifier(conValCell, "id", getPackageName());
                    TextView rowConvVall = (TextView) findViewById(convValID);
                    rowConvVall.setText(String.format("%.2f", convVal));


                    row++;
                    c.moveToNext();
                    /*
                }*/
            }

            row = 2;

        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    //Update Dialog to edit USD value to historical entries and get old exchange rate for different USD amount

    public void editDialog(String focusID, String entryID) {
        int usdCell = getResources().getIdentifier(focusID, "id", getPackageName());
        TextView usd = (TextView) findViewById(usdCell);
        Double usdOldVal = Double.parseDouble(usd.getText().toString());

        int idCell = getResources().getIdentifier(entryID, "id", getPackageName());
        TextView id = (TextView) findViewById(idCell);
        final int idVal = Integer.parseInt(id.getText().toString());

        //Get old exchange value
        Double exchangeTotalVal = null;
        try {
            SQLiteDatabase historyDB = (Main3Activity.this).openOrCreateDatabase("History", MODE_PRIVATE, null);
            historyDB.execSQL("CREATE TABLE IF NOT EXISTS history (id INTEGER PRIMARY KEY, time VARCHAR(20), date VARCHAR(10), usd DOUBLE, currency CHARACTER(3), exc_amt DOUBLE)");
            Cursor exVal = historyDB.rawQuery("SELECT exc_amt FROM history WHERE id = '" + idVal + "'", null);
            int exValIndex = exVal.getColumnIndex("exc_amt");
            exVal.moveToFirst();
            while(exVal != null)
            {
                exchangeTotalVal = exVal.getDouble(exValIndex);
                exVal.moveToNext();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        final Double exchangeRate = exchangeTotalVal / usdOldVal;

        final Context context = this;
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.prompts, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView.findViewById(R.id.editTextDialogUserInput);
        userInput.setText(String.format("%.2f", usdOldVal));

        // Update history table or cancel
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    Double usdNewVal = Double.parseDouble(userInput.getText().toString());
                                    Double newExchangeVal = usdNewVal * exchangeRate;
                                    SQLiteDatabase historyDB = (Main3Activity.this).openOrCreateDatabase("History", MODE_PRIVATE, null);
                                    historyDB.execSQL("CREATE TABLE IF NOT EXISTS history (id INTEGER PRIMARY KEY, time VARCHAR(20), date VARCHAR(10), usd DOUBLE, currency CHARACTER(3), exc_amt DOUBLE)");
                                    historyDB.execSQL("UPDATE history SET usd = '" + usdNewVal + "' , exc_amt = '" + newExchangeVal + "' WHERE id = '" + idVal + "'");

                                    recreate();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    //Check if user clicks empty cells in history table

    Boolean checkEmpty(String cellID)
    {
        int cell = getResources().getIdentifier(cellID, "id", getPackageName());
        TextView usd = (TextView) findViewById(cell);
        if (usd.getText().toString().trim().length() > 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    //Update history row to obtain old exchange rate for different amount
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.r2c2:
                if(checkEmpty("r2c2"))
                {
                     return;
                }
                else
                {
                    editDialog("r2c2", "r2c0");
                }
                break;
            case R.id.r3c2:
                if(checkEmpty("r3c2"))
                {
                    return;
                }
                else {
                    editDialog("r3c2", "r3c0");
                }
                break;
            case R.id.r4c2:
                if(checkEmpty("r4c2"))
                {
                    return;
                }
                else {
                    editDialog("r4c2", "r4c0");
                }
                break;
            case R.id.r5c2:
                if(checkEmpty("r5c2"))
                {
                    return;
                }
                else {
                    editDialog("r5c2", "r5c0");
                }
                break;
            case R.id.r6c2:
                if(checkEmpty("r6c2"))
                {
                    return;
                }
                else {
                    editDialog("r6c2", "r6c0");
                }
                break;
            case R.id.r7c2:
                if(checkEmpty("r7c2"))
                {
                    return;
                }
                else {
                    editDialog("r7c2", "r7c0");
                }
                break;
            case R.id.r8c2:
                if(checkEmpty("r8c2"))
                {
                    return;
                }
                else {
                    editDialog("r8c2", "r8c0");
                }
                break;
            case R.id.r9c2:
                if(checkEmpty("r9c2"))
                {
                    return;
                }
                else {
                    editDialog("r9c2", "r9c0");
                }
                break;
            case R.id.r10c2:
                if(checkEmpty("r10c2"))
                {
                    return;
                }
                else {
                    editDialog("r10c2", "r10c0");
                }
                break;
            case R.id.r11c2:
                if(checkEmpty("r11c2"))
                {
                    return;
                }
                else {
                    editDialog("r11c2", "r11c0");
                }
                break;
        }
    }

    //Inflate Options Overflow menu from menu.xml
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    //Start Different activity from options overflow menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.info)
        {
            startActivity(new Intent(getApplicationContext(), Main2Activity.class));

        }
        if (id == R.id.history)
        {
            startActivity(new Intent(getApplicationContext(), Main3Activity.class));
        }
        if (id == R.id.main)
        {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
        return true;
    }
}
