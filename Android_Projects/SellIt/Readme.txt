CPSC 682 - Assignment 3 - Bharat Kumar Govindaradjou

Sell It for Android V1.0

Authoring tool Links:

Navigate to each link from the browser. Each page will have a set of instructions to be followed and expected result.

1. SignUp:

 people.cs.clemson.edu/~bgovind/cpsc682a3/phpscripts/usercontrol.php

2. Login:

 people.cs.clemson.edu/~bgovind/cpsc682a3/phpscripts/login.php

3. Post a new item as an existing user:

 people.cs.clemson.edu/~bgovind/cpsc682a3/phpscripts/postitem.php

4. Get all posts posted by a user:

 people.cs.clemson.edu/~bgovind/cpsc682a3/phpscripts/myposts.php

5. Delete an existing post as a user:

 people.cs.clemson.edu/~bgovind/cpsc682a3/phpscripts/deletepost.php

6. Update an existing post as a user:
 
 people.cs.clemson.edu/~bgovind/cpsc682a3/phpscripts/updatepost.php

7. Get all Feeds for a user:

 people.cs.clemson.edu/~bgovind/cpsc682a3/phpscripts/allposts.php

8. Get all new feeds for a user since last download:

 people.cs.clemson.edu/~bgovind/cpsc682a3/phpscripts/feedupdate.php 

