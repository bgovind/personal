<!DOCTYPE html>
<html>
<body>
</h3>Post item for sale Request</h3>
<p>-Enter Username, item name, price, quantity, description and click submit.</p>
<p>-If your credentials are correct then you will see 'success'.</p>
<p>-If you credentials are incorrect then you will see 'incorrect credentials'.</p>

<form action="" method="post">
	<p style="margin: 0.5cm 0cm auto 1cm;">Enter username, item name, item price, item quantity, item description below</p>
	<table style="margin: 0.5cm 0cm auto 1cm; text-align: left;">
	<tr><td>Username:* </td><td><input type="text" name="username"></td></tr>
	<tr><td>Item name:* </td><td><input type="text" name="name"></td></tr>
	<tr><td>Item Price:* </td><td><input type="number" step="any" name="price"></td></tr>
	<tr><td>Item quantity:* </td><td><input type="number" name="quantity"></td></tr>
	<tr><td>description:* </td><td><input type="text" name="description"></td></tr>
	<tr><td></td><td><input name="submit" type="submit" value="Submit"></td></tr>
	</table>
</form>


<?php

	require_once "connection.php";
	//header('Content-Type: application/json;charset=utf-8');
	
	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function post_item($name, $price, $quantity, $description, $username)
		{
			$query = "insert into items (name, price, quantity, description, username, last_updated, delete_status) values('".$name."','".$price."','".$quantity."','".$description."','".$username."', now()::timestamp(0) without time zone, 'false')";
			$result = pg_query($this->connection, $query);
			if ($result)
			{
				$json['success'] = 'success';
			}
			else
			{	
				$json['fail'] = 'fail';
			}
			echo json_encode($json);
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	if(isset($_POST['name'], $_POST['price'], $_POST['quantity'], $_POST['description'], $_POST['username']))
	{
		$name = $_POST['name'];
		$price = $_POST['price'];
		$quantity = $_POST['quantity'];
		$description = $_POST['description'];
		$username = $_POST['username'];
		
		if (!empty($name) && !empty($price) && !empty($quantity) && !empty($description) && !empty($username))
		{
			$user -> post_item($name, $price, $quantity, $description, $username);
		}
		else
		{
			$json['error'] = 'all fields mandatory';
			echo json_encode($json);
		}
	}
	
	?>
	</body>

</html>