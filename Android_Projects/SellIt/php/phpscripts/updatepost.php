<html>
<body>
</h3>Update an existing Post Request</h3>
<p>-Navigate to <a href="https://people.cs.clemson.edu/~bgovind/cpsc682a3/phpscripts/myposts.php">My Posts</a> and get a list of item posted by a user. Get item name, quantity, price and description.</p>
<p>-Enter Username, item name, price, quantity, description and new item name, quantity, price , description and click submit.</p>
<p>-If your credentials are correct then you will see 'item updated'.</p>
<p>-If you credentials are incorrect then you will see 'incorrect credentials'.</p>

<form action="" method="post">
	<p style="margin: 0.5cm 0cm auto 1cm;">Enter username, item name, item price, item quantity, item description below</p>
	<table style="margin: 0.5cm 0cm auto 1cm; text-align: left;">
	<tr><td>Username:* </td><td><input type="text" name="username"></td></tr>
	<tr><td>Item name:* </td><td><input type="text" name="name"></td></tr>
	<tr><td>Item Price:* </td><td><input type="number" step="any" name="price"></td></tr>
	<tr><td>Item quantity:* </td><td><input type="number" name="quantity"></td></tr>
	<tr><td>description:* </td><td><input type="text" name="description"></td></tr>
	<tr><td>New Item name:* </td><td><input type="text" name="newname"></td></tr>
	<tr><td>New Item Price:* </td><td><input type="number" step="any" name="newprice"></td></tr>
	<tr><td>New Item quantity:* </td><td><input type="number" name="newquantity"></td></tr>
	<tr><td>New description:* </td><td><input type="text" name="newdescription"></td></tr>
	<tr><td></td><td><input name="submit" type="submit" value="Submit"></td></tr>
	</table>
</form>

<?php

	require_once "connection.php";
	//header('Content-Type: application/json');

	
	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function delete_post($name, $price, $quantity, $description, $username, $newname, $newprice, $newquantity, $newdescription)
		{
			$query1 = "update items set delete_status = 'true' where name= '".$name."' and quantity='".$quantity."' and description='".$description."' and username = '".$username."'";
			$query2 = "update items set last_updated = now()::timestamp(0) without time zone where name= '".$name."' and quantity='".$quantity."' and description='".$description."' and username = '".$username."'";
			$query3 = "insert into items (name, price, quantity, description, username, last_updated, delete_status) values('".$newname."','".$newprice."','".$newquantity."','".$newdescription."','".$username."', now()::timestamp(0) without time zone, 'false')";
			$set_deleteStatus = pg_query($this->connection, $query1);
			$result = pg_query($this->connection, $query2);
			$insertUpdateRecord = pg_query($this->connection, $query3);
			if ($set_deleteStatus)
			{
				if($set_deleteStatus && $insertUpdateRecord)
				{
				$json['success'] = 'item updated';
				echo json_encode($json);
				}
			}
			else
			{	
				$json['fail'] = 'item not posted';
				echo json_encode($json);
			}
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	if(isset($_POST['name'], $_POST['price'], $_POST['quantity'], $_POST['description'], $_POST['username'], $_POST['newname'], $_POST['newprice'], $_POST['newquantity'], $_POST['newdescription']))
	{
		$name = $_POST['name'];
		$price = $_POST['price'];
		$quantity = $_POST['quantity'];
		$description = $_POST['description'];
		$username = $_POST['username'];
		$newname = $_POST['newname'];
		$newprice = $_POST['newprice'];
		$newquantity = $_POST['newquantity'];
		$newdescription = $_POST['newdescription'];
		
		if (!empty($name) && !empty($price) && !empty($quantity) && !empty($description) && !empty($username) && !empty($newname) && !empty($newprice) && !empty($newquantity) && !empty($newdescription))
		{
			$user -> delete_post($name, $price, $quantity, $description, $username, $newname, $newprice, $newquantity, $newdescription);
		}
		else
		{
			$json['error'] = 'all fields mandatory';
			echo json_encode($json);
		}
	}
	
	?>
	</body>
	</html>