<!DOCTYPE html>
<html>
<body>
</h3>Get all posts for a particular user</h3>
<p>-Enter Username and click submit.</p>
<p>-If your credentials are correct and the user already has posted some items then you will see the list of items.</p>
<p>-If your credentials are correct and but the user has not posted any item then you will see 'user has no items'.</p>
<p>-If your credentials are incorrect then you will see credentials incorrect.</p>

<form action="" method="post">
	<p style="margin: 0.5cm 0cm auto 1cm;">Enter username below</p>
	<table style="margin: 0.5cm 0cm auto 1cm; text-align: left;">
	<tr><td>Username:* </td><td><input type="text" name="username"></td></tr>
	<tr><td></td><td><input name="submit" type="submit" value="Submit"></td></tr>
	</table>
</form>




<?php

	require_once "connection.php";
	//header('Content-Type: application/json');
	
	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function get_user_posts($username)
		{
			$query = "select * from items where username = '".$username."'";
			$result = pg_query($this->connection, $query);
			if ($result)
			{
				if (pg_num_rows($result) == 0)
				{
					$json['noitems'] = 'user has no items';
					$object['1'] = $json;
					echo json_encode($object);
				}
				else{
					
				$i = 1;
				while($row = pg_fetch_row($result))
				{
					$itemNo = $i;
					$itemName = $row[1];
					$itemPrice = $row[2];
					$itemQuantity = $row[3];
					$itemDescription = $row[4];
					$itemDeleteStatus = $row[7];
					
					$json['success'] = 'posts loaded';
					$json['number'] = $itemNo ;
					$json['name'] = $itemName ;
					$json['price'] = $itemPrice ;
					$json['quantity'] = $itemQuantity ;
					$json['description'] = $itemDescription ;
					$json['delete_status'] = $itemDeleteStatus ;
					
					$object[$i] = $json;
						
					$i = $i+1;
				}
				echo json_encode($object);
				}
				
			}
			else
			{
				$json['fail'] = 'credentials incorrect';
				echo json_encode($json);
			}
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	if(isset($_POST['username']))
	{
		$username = $_POST['username'];
		
		if (!empty($username))
		{
			$user -> get_user_posts($username);
		}
		else
		{
			$json['error'] = 'all fields mandatory';
			echo json_encode($json);
		}
	}
	
	?>
	</body>

</html>