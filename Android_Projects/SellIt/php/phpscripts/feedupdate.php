<!DOCTYPE html>
<html>
<body>
</h3>Get all posts from other user to populate feeds since last download to refresh user feed:</h3>
<p>-Enter Username and click submit.</p>
<p>-If your credentials are correct and user have posted items you will see the list of items posted by other users since last download.</p>
<p>-If your credentials are correct and but nobody has posted any items then you will see blank.</p>
<p>-If your credentials are incorrect then you will see 'credentials incorrect'.</p>

<form action="" method="post">
	<p style="margin: 0.5cm 0cm auto 1cm;">Enter username below</p>
	<table style="margin: 0.5cm 0cm auto 1cm; text-align: left;">
	<tr><td>Username:* </td><td><input type="text" name="username"></td></tr>
	<tr><td></td><td><input name="submit" type="submit" value="Submit"></td></tr>
	</table>
</form>


<?php

	require_once "connection.php";
	//header('Content-Type: application/json');

	
	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function get_feed_updates($username)
		{
			$fetch_lastUpdated = "select last_updated from users where username = '".$username."'";
			$result_lastUpdated = pg_query($this->connection, $fetch_lastUpdated);
			if ($result_lastUpdated)
			{
				while($row1 = pg_fetch_row($result_lastUpdated))
				{
					$last_updated = $row1[0];
				}
			}
			//$query = "select * from items where username != '".$username."' and to_timestamp(last_updated, 'YYYY-MM-dd HH:MI:SS')::timestamp without time zone > to_timestamp('".$last_updated."', 'YYYY-MM-dd HH:MI:SS')::timestamp without time zone";
			$query = "select * from items where username != '".$username."' and last_updated > '".$last_updated."'";
			$result = pg_query($this->connection, $query);
			$timestamp_update = "update users set last_updated = now()::timestamp(0) without time zone where username = '".$username."'";
			$set_update = pg_query($this->connection, $timestamp_update);
			if ($result)
			{
				$i = 1;
				while($row = pg_fetch_row($result))
				{
					$itemNo = $i;
					$itemName = $row[1];
					$itemPrice = $row[2];
					$itemQuantity = $row[3];
					$itemDescription = $row[4];
					$itemUsername = $row[5];
					$itemLastUpdated = $row[6];
					$itemDeleteStatus = $row[7];
					$emailQuery = "select email from users where username = '".$itemUsername."'";
					$email_result = pg_query($this->connection, $emailQuery);
					while($emailRow = pg_fetch_row($email_result))
					{
						$itemEmail = $emailRow[0];
					}
					
					$json['success'] = 'posts loaded';
					$json['number'] = $itemNo ;
					$json['name'] = $itemName ;
					$json['price'] = $itemPrice ;
					$json['quantity'] = $itemQuantity ;
					$json['description'] = $itemDescription ;
					$json['username'] = $itemUsername;
					$json['last_updated'] = $itemLastUpdated;
					$json['delete_status'] = $itemDeleteStatus ;
					$json['email'] = $itemEmail;
					
					$object[$i] = $json;
						
					$i = $i+1;
				}
				echo json_encode($object);
				
			}
			else
			{
				$json['fail'] = 'credentials incorrect';
				echo json_encode($json);
			}
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	if(isset($_POST['username']))
	{
		$username = $_POST['username'];
		
		if (!empty($username))
		{
			$user -> get_feed_updates($username);
		}
		else
		{
			$json['error'] = 'all fields mandatory';
			echo json_encode($json);
		}
	}
	
	?>
	
</body>

</html>