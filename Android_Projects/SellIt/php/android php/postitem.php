<?php

	require_once "connection.php";
	header('Content-Type: application/json;charset=utf-8');
	
	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function post_item($name, $price, $quantity, $description, $username)
		{
			$query = "insert into items (name, price, quantity, description, username, last_updated, delete_status) values('".$name."','".$price."','".$quantity."','".$description."','".$username."', now()::timestamp(0) without time zone, 'false')";
			$result = pg_query($this->connection, $query);
			if ($result)
			{
				$json['success'] = 'success';
			}
			else
			{	
				$json['fail'] = 'fail';
			}
			echo json_encode($json);
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	if(isset($_POST['name'], $_POST['price'], $_POST['quantity'], $_POST['description'], $_POST['username']))
	{
		$name = $_POST['name'];
		$price = $_POST['price'];
		$quantity = $_POST['quantity'];
		$description = $_POST['description'];
		$username = $_POST['username'];
		
		if (!empty($name) && !empty($price) && !empty($quantity) && !empty($description) && !empty($username))
		{
			$user -> post_item($name, $price, $quantity, $description, $username);
		}
		else
		{
			$json['error'] = 'all fields mandatory';
			echo json_encode($json);
		}
	}
	
	?>
	</body>

</html>