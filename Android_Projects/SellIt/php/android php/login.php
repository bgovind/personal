<?php

	require_once "connection.php";
	header('Content-Type: application/json');
	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function does_user_exist($username, $password)
		{
			$query = "select * from users where username = '".$username."' and password= '".$password."'";
			$result = pg_query($this->connection, $query);
			if (pg_num_rows($result) == 1)
			{
				$timestamp_update = "update users set last_updated = now()::timestamp(0) without time zone where username = '".$username."'";
				$set_update = pg_query($this->connection, $timestamp_update);
				$json['success'] = 'logged in';
				while($row = pg_fetch_row($result))
				{
					$username = $row[1];
					
					$fn = $row[3];
					$ln = $row[4];
					$email = $row[5];
					$json['fn'] = $fn;
					$json['ln'] = $ln;
					$json['email'] = $email;
				}
				echo json_encode($json);
			}
			else
			{
				$json['fail'] = 'credentials incorrect';
				echo json_encode($json);
			}
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	if(isset($_POST['username'], $_POST['password']))
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		
		if (!empty($username) && !empty($password))
		{
			$user -> does_user_exist($username, $password);
		}
		else
		{
			$json['error'] = 'all fields mandatory';
			echo json_encode($json);
		}
	}
	
	?>