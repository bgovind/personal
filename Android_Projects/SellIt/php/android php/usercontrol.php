<?php

	require_once "connection.php";
	header('Content-Type: application/json');

	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function does_user_exist($username, $password, $fn, $ln, $email)
		{
			$query = "select * from users where email = '".$email."' or username = '".$username."'";
			$result = pg_query($this->connection, $query);
			if (pg_num_rows($result) > 0)
			{
				$json['fail'] = 'user exists';
				echo json_encode($json);
			}
			else
			{
				$register = "insert into users (username, password, fn, ln, email, last_updated) values('".$username."', '".$password."','".$fn."','".$ln."','".$email."', now()::timestamp(0) without time zone)";
				$is_registered = pg_query($this->connection, $register);
				if (!$is_registered) 
				{  
					$json['error'] = 'register error';
				}
				else
				{
					$json['success'] = 'registered';
				}
				echo json_encode($json);
			}
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	if(isset($_POST['username'], $_POST['password'], $_POST['fn'], $_POST['ln'], $_POST['email']))
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		$fn = $_POST['fn'];
		$ln = $_POST['ln'];
		$email = $_POST['email'];
		
		if (!empty($username) && !empty($password) && !empty($fn) && !empty($ln) && !empty($email))
		{
			$user -> does_user_exist($username, $password, $fn, $ln, $email);
		}
		else
		{
			$json['error'] = 'all fields mandatory';
			echo json_encode($json);
		}
	}
	
	?>