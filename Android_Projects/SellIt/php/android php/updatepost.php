<?php

	require_once "connection.php";
	header('Content-Type: application/json');

	
	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function delete_post($name, $price, $quantity, $description, $username, $newname, $newprice, $newquantity, $newdescription)
		{
			$query1 = "update items set delete_status = 'true' where name= '".$name."' and quantity='".$quantity."' and description='".$description."' and username = '".$username."'";
			$query2 = "update items set last_updated = now()::timestamp(0) without time zone where name= '".$name."' and quantity='".$quantity."' and description='".$description."' and username = '".$username."'";
			$query3 = "insert into items (name, price, quantity, description, username, last_updated, delete_status) values('".$newname."','".$newprice."','".$newquantity."','".$newdescription."','".$username."', now()::timestamp(0) without time zone, 'false')";
			$set_deleteStatus = pg_query($this->connection, $query1);
			$result = pg_query($this->connection, $query2);
			$insertUpdateRecord = pg_query($this->connection, $query3);
			if ($set_deleteStatus)
			{
				if($set_deleteStatus && $insertUpdateRecord)
				{
				$json['success'] = 'item deleted';
				echo json_encode($json);
				}
			}
			else
			{	
				$json['fail'] = 'item not posted';
				echo json_encode($json);
			}
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	if(isset($_POST['name'], $_POST['price'], $_POST['quantity'], $_POST['description'], $_POST['username'], $_POST['newname'], $_POST['newprice'], $_POST['newquantity'], $_POST['newdescription']))
	{
		$name = $_POST['name'];
		$price = $_POST['price'];
		$quantity = $_POST['quantity'];
		$description = $_POST['description'];
		$username = $_POST['username'];
		$newname = $_POST['newname'];
		$newprice = $_POST['newprice'];
		$newquantity = $_POST['newquantity'];
		$newdescription = $_POST['newdescription'];
		
		if (!empty($name) && !empty($price) && !empty($quantity) && !empty($description) && !empty($username) && !empty($newname) && !empty($newprice) && !empty($newquantity) && !empty($newdescription))
		{
			$user -> delete_post($name, $price, $quantity, $description, $username, $newname, $newprice, $newquantity, $newdescription);
		}
		else
		{
			$json['error'] = 'all fields mandatory';
			echo json_encode($json);
		}
	}
	
	?>