<?php

	require_once "connection.php";
	header('Content-Type: application/json');

	
	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function delete_post($name, $price, $quantity, $description, $username)
		{
			$query1 = "update items set delete_status = 'true' where name= '".$name."' and quantity='".$quantity."' and description='".$description."' and username = '".$username."'";
			$query2 = "update items set last_updated = now()::timestamp(0) without time zone where name= '".$name."' and quantity='".$quantity."' and description='".$description."' and username = '".$username."'";
			$set_deleteStatus = pg_query($this->connection, $query1);
			$result = pg_query($this->connection, $query2);
			if ($set_deleteStatus)
			{
				if($set_deleteStatus)
				{
				$json['success'] = 'item deleted';
				echo json_encode($json);
				}
			}
			else
			{	
				$json['fail'] = 'item not posted';
				echo json_encode($json);
			}
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	if(isset($_POST['name'], $_POST['price'], $_POST['quantity'], $_POST['description'], $_POST['username']))
	{
		$name = $_POST['name'];
		$price = $_POST['price'];
		$quantity = $_POST['quantity'];
		$description = $_POST['description'];
		$username = $_POST['username'];
		
		if (!empty($name) && !empty($price) && !empty($quantity) && !empty($description) && !empty($username))
		{
			$user -> delete_post($name, $price, $quantity, $description, $username);
		}
		else
		{
			$json['error'] = 'all fields mandatory';
			echo json_encode($json);
		}
	}
	
	?>