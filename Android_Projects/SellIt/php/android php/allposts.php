<?php

	require_once "connection.php";
	header('Content-Type: application/json');
	
	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function get_all_posts($username)
		{
			$query = "select * from items where username != '".$username."' and delete_status = 'false'";
			$result = pg_query($this->connection, $query);
			if ($result)
			{
				
				if (pg_num_rows($result) == 0)
				{
					$json['noitems'] = 'user has no items';
					$object['1'] = $json;
					echo json_encode($object);
				}
				else{
				$i = 1;
				while($row = pg_fetch_row($result))
				{
					$itemNo = $i;
					$itemName = $row[1];
					$itemPrice = $row[2];
					$itemQuantity = $row[3];
					$itemDescription = $row[4];
					$itemUsername = $row[5];
					$itemLastUpdated = $row[6];
					$itemDeleteStatus = $row[7];
					
					$emailQuery = "select email from users where username = '".$itemUsername."'";
					$email_result = pg_query($this->connection, $emailQuery);
					while($emailRow = pg_fetch_row($email_result))
					{
						$itemEmail = $emailRow[0];
					}
					
					$json['success'] = 'posts loaded';
					$json['number'] = $itemNo ;
					$json['name'] = $itemName ;
					$json['price'] = $itemPrice ;
					$json['quantity'] = $itemQuantity ;
					$json['description'] = $itemDescription ;
					$json['username'] = $itemUsername;
					$json['last_updated'] = $itemLastUpdated;
					$json['delete_status'] = $itemDeleteStatus ;
					$json['email'] = $itemEmail;
					
					$object[$i] = $json;
						
					$i = $i+1;
				}
				echo json_encode($object);
				}
				
			}
			else
			{
				$json['fail'] = 'credentials incorrect';
				echo json_encode($json);
			}
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	if(isset($_POST['username']))
	{
		$username = $_POST['username'];
		
		if (!empty($username))
		{
			$user -> get_all_posts($username);
		}
		else
		{
			$json['error'] = 'all fields mandatory';
			echo json_encode($json);
		}
	}
	
	?>