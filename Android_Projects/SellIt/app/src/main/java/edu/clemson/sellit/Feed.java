package edu.clemson.sellit;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//Display feeds of posts from other users
public class Feed extends AppCompatActivity {

    private ArrayList<String> arrayNames = new ArrayList<>();
    private ArrayList<String> arrayIDs = new ArrayList<>();
    private ArrayList<String> arrayPrice = new ArrayList<>();
    private ArrayList<String> arrayQuantity = new ArrayList<>();
    private ArrayList<String> arrayDescription= new ArrayList<>();
    private ArrayList<String> arrayEmail= new ArrayList<>();

    ListView feedList;

    private RequestQueue requestQueue;

    private static final String URL = "https://people.cs.clemson.edu/~bgovind/cpsc682a3/feedupdate.php";

    private StringRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        Button refreshHit = (Button) findViewById(R.id.refresh);

        refreshHit.setOnClickListener(new View.OnClickListener() {

            //Refresh feeds by downloading only new posts posted by other users from server since last refresh/download
            @Override
            public void onClick(View v) {
                try {
                    request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {

                                System.out.println("login feeds inserting into local");

                                SQLiteDatabase sellitDB = (Feed.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);

                                JSONObject outer = new JSONObject(response);
                                Iterator<String> keys = outer.keys();
                                while (keys.hasNext()) {
                                    String key = keys.next();
                                    JSONObject inside = outer.getJSONObject(key);

                                    if (inside.names().get(0).equals("success")) {

                                        String iNo = inside.getString("number");
                                        String iName = inside.getString("name");
                                        String iPrice = inside.getString("price");
                                        String iQuantity = inside.getString("quantity");
                                        String iDescription = inside.getString("description");
                                        String iUsername = inside.getString("username");
                                        String iLastUpdated = inside.getString("last_updated");
                                        String iDeleteStatus = inside.getString("delete_status");
                                        String iEmail = inside.getString("email");


                                        sellitDB.execSQL("CREATE TABLE IF NOT EXISTS feeds(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), last_updated text, delete_status boolean, email varchar(50), FOREIGN KEY (username) REFERENCES users(username))");

                                        Cursor c1 = sellitDB.rawQuery("select * from feeds where name = '"+iName+"' and quantity = '"+iQuantity+"' and description = '"+iDescription+"' and username = '"+iUsername+"' and email = '"+iEmail+"'", null);

                                        if(c1.getCount() > 0)
                                        {
                                            sellitDB.execSQL("update feeds set last_updated = '"+iLastUpdated+"', delete_status = '"+iDeleteStatus+"' where name = '"+iName+"' and quantity = '"+iQuantity+"' and description = '"+iDescription+"'");
                                        }
                                        else
                                        {
                                            sellitDB.execSQL("INSERT INTO feeds(name, price, quantity, description, username, last_updated, delete_status, email) VALUES ('" + iName + "', '" + iPrice + "','" + iQuantity + "','" + iDescription + "','" + iUsername + "', '" + iLastUpdated + "','" + iDeleteStatus + "', '"+iEmail+"')");
                                            System.out.println(iNo);
                                            System.out.println(iDeleteStatus);

                                            arrayNames.add(iNo);
                                            arrayIDs.add(iName);
                                            arrayPrice.add(iPrice);
                                            arrayQuantity.add(iQuantity);
                                            arrayDescription.add(iDescription);

                                        }

                                    }
                                    else if (inside.names().get(0).equals("noitems"))
                                    {
                                        Toast.makeText(Feed.this, "Your feed if up-to-date!", Toast.LENGTH_SHORT).show();

                                    }
                                    else if (outer.names().get(0).equals("fail")) {
                                        Toast.makeText(Feed.this, "Could not fetch updates. Try Later.", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                Toast.makeText(Feed.this, "Feed Refreshed!", Toast.LENGTH_SHORT).show();

                                recreate();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> hashMap = new HashMap<>();

                            try {

                                SQLiteDatabase sellitDB = (Feed.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                int usernameIndex = c.getColumnIndex("username");

                                c.moveToFirst();

                                String username = c.getString(usernameIndex);

                                hashMap.put("username", username);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return hashMap;
                        }
                    };

                    requestQueue.add(request);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        //Welcome user with their first name
        try
        {
            SQLiteDatabase sellitDB = (Feed.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
            sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
            Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);

            int f_nIndex = c.getColumnIndex("fn");

            c.moveToFirst();

            final String name = c.getString(f_nIndex);
            int nameID = getResources().getIdentifier("welcome","id",getPackageName());
            TextView welcomeName = (TextView) findViewById(nameID);
            welcomeName.setText("Hi, "+name+"!");

            Button postHit = (Button) findViewById(R.id.post);

            //Take user to post an item page when user presses Post an Item button
            postHit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), PostItem.class));
                }
            });

            requestQueue = Volley.newRequestQueue(this);





        }catch (Exception e)
        {
            e.printStackTrace();
        }

        //Populate already existing posts from internal database
        try
        {
            final SQLiteDatabase sellitDB = (Feed.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
            sellitDB.execSQL("CREATE TABLE IF NOT EXISTS feeds(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), last_updated text, delete_status boolean, email varchar(50), FOREIGN KEY (username) REFERENCES users(username))");
            Cursor c = sellitDB.rawQuery("SELECT id, name, price, quantity, description, email FROM feeds where delete_status = 'f'", null);
            int idIndex = c.getColumnIndex("id");
            int nameIndex = c.getColumnIndex("name");
            int priceIndex = c.getColumnIndex("price");
            int quantityIndex = c.getColumnIndex("quantity");
            int descriptionIndex = c.getColumnIndex("description");
            int emailIndex = c.getColumnIndex("email");

            if (c.moveToFirst())
                System.out.println("feeds true");
            else
                System.out.println("feeds false");

            c.moveToFirst();
            while(c != null && !c.isAfterLast())
            {
                String itemID = c.getString(idIndex);
                String itemName = c.getString(nameIndex);
                String itemPrice = c.getString(priceIndex);
                String itemQuantity = c.getString(quantityIndex);
                String itemDescription = c.getString(descriptionIndex);
                String itemEmail = c.getString(emailIndex);
                System.out.println(itemID);
                System.out.println(itemName);
                arrayNames.add(itemName);
                arrayIDs.add(itemID);
                arrayPrice.add(itemPrice);
                arrayQuantity.add(itemQuantity);
                arrayDescription.add(itemDescription);
                arrayEmail.add(itemEmail);
                c.moveToNext();
            }

        }catch(Exception e)
        {
            System.out.println("Exception thrown by DB");
            e.printStackTrace();
        }

        CustomListAdapterFeed adapter=new CustomListAdapterFeed(this, arrayNames, arrayIDs, arrayPrice, arrayQuantity, arrayDescription, arrayEmail);
        feedList=(ListView)findViewById(R.id.feedlist);
        feedList.setAdapter(adapter);


        feedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                String Slecteditem= arrayNames.get(position).toString();
                Toast.makeText(getApplicationContext(), Slecteditem, Toast.LENGTH_SHORT).show();
            }
        });


    }

    //Alert diallog to confirm exit when user presses back
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exit Currency Converter")
                .setMessage("Are you sure you want to exit Sell it?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int i)
                    {
                        Intent a = new Intent(Intent.ACTION_MAIN);
                        a.addCategory(Intent.CATEGORY_HOME);
                        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(a);


                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    //Inflate options menu to display overflow mennu options to navigate to other pages in the app
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    //Start other activities like my posts, deleted posts, inforfrom options overflow menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.myitems)
        {
            startActivity(new Intent(getApplicationContext(), MyPosts.class));
        }
        else if (id == R.id.info) {
            startActivity(new Intent(getApplicationContext(), InfoPage.class));
        }
        else if (id == R.id.mydeleted)
        {
            startActivity(new Intent(getApplicationContext(), DeletedPosts.class));
        }
        else if (id == R.id.logout) {
            try {
                SQLiteDatabase sellitDB = (Feed.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                sellitDB.execSQL("DELETE FROM users");
                sellitDB.execSQL("CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), delete_status boolean, FOREIGN KEY (username) REFERENCES users(username))");
                sellitDB.execSQL("DELETE FROM items");
                sellitDB.execSQL("CREATE TABLE IF NOT EXISTS feeds(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), last_updated text, delete_status boolean, email varchar(50), FOREIGN KEY (username) REFERENCES users(username))");
                sellitDB.execSQL("DELETE FROM feeds");
                Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                Cursor c1 = sellitDB.rawQuery("SELECT * FROM items", null);
                Cursor c2 = sellitDB.rawQuery("SELECT * FROM feeds", null);
                if (c.getCount() == 0 && c1.getCount() == 0 && c2.getCount() == 0) {
                    Toast.makeText(Feed.this, "You have logged out of Sell It!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return true;
    }
}
