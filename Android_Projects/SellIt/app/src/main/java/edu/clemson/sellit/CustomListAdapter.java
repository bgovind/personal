package edu.clemson.sellit;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomListAdapter extends ArrayAdapter<String>{

    private final Activity context;
    private final ArrayList<String> itemname;
    private final ArrayList<String> id;
    private final ArrayList<String> price;
    private final ArrayList<String> quantity;
    private final ArrayList<String> description;

    public CustomListAdapter(Activity context, ArrayList<String> itemname, ArrayList<String> id, ArrayList<String> price, ArrayList<String> quantity, ArrayList<String> description)
    {
        super(context, R.layout.mylist, itemname);

        this.context=context;
        this.itemname=itemname;
        this.id=id;
        this.price=price;
        this.quantity = quantity;
        this.description = description;
    }

    public View getView(int position, View v,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.mylist, null,true);

        TextView txtID = (TextView) rowView.findViewById(R.id.item);
        TextView txtName = (TextView) rowView.findViewById(R.id.name);
        TextView txtPrice = (TextView) rowView.findViewById(R.id.price);
        TextView txtQuantity = (TextView) rowView.findViewById(R.id.itemQuantity);
        TextView txtDescription = (TextView) rowView.findViewById(R.id.itemDescription);

        txtID.setText(id.get(position).toString());
        txtName.setText("Item Name : "+itemname.get(position).toString());
        txtPrice.setText("Price : $"+price.get(position).toString());
        txtQuantity.setText("Quantity Available : "+quantity.get(position).toString());
        txtDescription.setText("Description : "+description.get(position).toString());
        return rowView;

    };
}
