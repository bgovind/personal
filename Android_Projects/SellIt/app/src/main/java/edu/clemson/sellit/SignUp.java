package edu.clemson.sellit;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//Signup activity
public class SignUp extends AppCompatActivity
{
    private RequestQueue requestQueue;
    private static final String URL = "https://people.cs.clemson.edu/~bgovind/cpsc682a3/usercontrol.php";
    private static final String URL2 = "https://people.cs.clemson.edu/~bgovind/cpsc682a3/myposts.php";
    private static final String URL3 = "https://people.cs.clemson.edu/~bgovind/cpsc682a3/allposts.php";

    private ArrayList<String> arrayNames = new ArrayList<>();
    private ArrayList<String> arrayIDs = new ArrayList<>();
    private ArrayList<String> arrayPrice = new ArrayList<>();
    private ArrayList<String> arrayQuantity = new ArrayList<>();
    private ArrayList<String> arrayDescription= new ArrayList<>();

    private ArrayList<String> feedarrayNames = new ArrayList<>();
    private ArrayList<String> feedarrayIDs = new ArrayList<>();
    private ArrayList<String> feedarrayPrice = new ArrayList<>();
    private ArrayList<String> feedarrayQuantity = new ArrayList<>();
    private ArrayList<String> feedarrayDescription= new ArrayList<>();
    private ArrayList<String> feedarrayEmail= new ArrayList<>();

    private StringRequest request;
    private StringRequest request2;
    private StringRequest request3;

    //Check if field is blank
    private boolean isEmpty(EditText inputText)
    {
        if (inputText.getText().toString().trim().length() > 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    //Display toast if field is blank
    private void blankAlert()
    {
        Toast.makeText(SignUp.this, "All fields are mandatory.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        Button registerHit = (Button) findViewById(R.id.register);

        requestQueue = Volley.newRequestQueue(this);

        //Upload user data to server during registration and take user to feeds page if user has
        //entered correct drtails in sign up page
        registerHit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                EditText uname = (EditText) findViewById(R.id.username);
                EditText fn = (EditText) findViewById(R.id.fn);
                EditText ln = (EditText) findViewById(R.id.ln);
                EditText email = (EditText) findViewById(R.id.email);
                EditText pwd1 = (EditText) findViewById(R.id.password1);
                EditText pwd2 = (EditText) findViewById(R.id.password2);
                final String un = uname.getText().toString();
                final String f_n = fn.getText().toString();
                final String l_n = ln.getText().toString();
                final String p1 = pwd1.getText().toString();
                String p2 = pwd2.getText().toString();
                final String em = email.getText().toString();


                if(isEmpty(uname) || isEmpty(pwd1) || isEmpty(pwd2) || isEmpty(fn) || isEmpty(ln) || isEmpty(email))
                {
                    blankAlert();
                }
                else if (uname.getText().toString().trim().length() < 5 || uname.getText().toString().trim().length() > 10)
                {
                    Toast.makeText(SignUp.this, "Username min 5 and maximum 10 characters.", Toast.LENGTH_SHORT).show();
                }
                else if(pwd1.getText().toString().trim().length() < 5 || pwd1.getText().toString().trim().length() > 10)
                {
                    Toast.makeText(SignUp.this, "Password min 5 and maximum 10 characters.", Toast.LENGTH_SHORT).show();
                }
                else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(em).matches())
                {
                    Toast.makeText(SignUp.this, "Invalid Email ID format.", Toast.LENGTH_SHORT).show();
                }
                else if (!p1.equals(p2))
                {
                    Toast.makeText(SignUp.this, "Passwords don't match", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    try
                    {
                        //Register User
                        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    System.out.println(jsonObject.names().get(0));
                                    if(jsonObject.names().get(0).equals("success"))
                                    {
                                        SQLiteDatabase sellitDB = (SignUp.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                        sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username varchar(10), password varchar(10), fn varchar(50), ln varchar(50), email varchar(50), last_updated text)");
                                        sellitDB.execSQL("Delete FROM users");
                                        sellitDB.execSQL("INSERT INTO users(username, password, fn, ln, email, last_updated) VALUES ('" + un + "', '" + p1 + "','" + f_n + "','" + l_n + "','" + em + "', CURRENT_TIMESTAMP)");

                                        try{

                                            //final SQLiteDatabase sellitDB = (LoginActivity.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                            sellitDB.execSQL("CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), delete_status boolean, FOREIGN KEY (username) REFERENCES users(username))");
                                            sellitDB.execSQL("CREATE TABLE IF NOT EXISTS feeds(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), last_updated text, delete_status boolean, email varchar(50), FOREIGN KEY (username) REFERENCES users(username))");
                                            Cursor c = sellitDB.rawQuery("SELECT id, name FROM items", null);
                                            final Cursor c1 = sellitDB.rawQuery("SELECT id, name FROM feeds", null);
                                            if (c.moveToFirst())
                                                System.out.println("login items true");
                                            else
                                                System.out.println("login items false");

                                            if (c1.moveToFirst())
                                                System.out.println("login feed true");
                                            else
                                                System.out.println("login feed false");

                                            if (!c.moveToFirst())
                                            {
                                                //Get all user's posts from server
                                                //Since user is new this will return blank response from server
                                                try{
                                                    request2 = new StringRequest(Request.Method.POST, URL2, new Response.Listener<String>()
                                                    {
                                                        @Override
                                                        public void onResponse(String response) {
                                                            try {
                                                                System.out.println("login user items inserting into local");

                                                                JSONObject outer = new JSONObject(response);
                                                                Iterator<String> keys =outer.keys();
                                                                while(keys.hasNext())
                                                                {
                                                                    String key = keys.next();
                                                                    JSONObject inside = outer.getJSONObject(key);

                                                                    if(inside.names().get(0).equals("success"))
                                                                    {
                                                                        SQLiteDatabase sellitDB = (SignUp.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                                        String iNo = inside.getString("number");
                                                                        String iName = inside.getString("name");
                                                                        String iPrice = inside.getString("price");
                                                                        String iQuantity = inside.getString("quantity");
                                                                        String iDescription = inside.getString("description");
                                                                        String iDeleteStatus = inside.getString("delete_status");
                                                                        sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                                                        Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                                                        int usernameIndex = c.getColumnIndex("username");



                                                                        c.moveToFirst();

                                                                        String username = c.getString(usernameIndex);
                                                                        sellitDB.execSQL("CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), delete_status boolean, FOREIGN KEY (username) REFERENCES users(username))");

                                                                        sellitDB.execSQL("INSERT INTO items(name, price, quantity, description, username, delete_status) VALUES ('" + iName + "', '" + iPrice + "','" + iQuantity + "','" + iDescription + "','" + username + "', '" + iDeleteStatus +"')");
                                                                        System.out.println(iNo);
                                                                        System.out.println(iName);
                                                                        arrayNames.add(iNo);
                                                                        arrayIDs.add(iName);
                                                                        arrayPrice.add(iPrice);
                                                                        arrayQuantity.add(iQuantity);
                                                                        arrayDescription.add(iDescription);


                                                                    }
                                                                    else if (inside.names().get(0).equals("noitems"))
                                                                    {
                                                                        Toast.makeText(SignUp.this, "Congrats! You can start using Sell It!", Toast.LENGTH_SHORT).show();
                                                                        startActivity(new Intent(getApplicationContext(), Feed.class));
                                                                    }
                                                                    else if(outer.names().get(0).equals("fail"))
                                                                    {
                                                                        Toast.makeText(SignUp.this, "Could not fetch your posts. Try Later.", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }, new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error)
                                                        {

                                                        }
                                                    }){
                                                        @Override
                                                        protected Map<String, String> getParams() throws AuthFailureError {
                                                            HashMap<String, String> hashMap = new HashMap<>();

                                                            try {

                                                                SQLiteDatabase sellitDB = (SignUp.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                                sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                                                Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                                                int usernameIndex = c.getColumnIndex("username");

                                                                c.moveToFirst();

                                                                String username = c.getString(usernameIndex);

                                                                hashMap.put("username", username);
                                                            }catch(Exception e)
                                                            {
                                                                e.printStackTrace();
                                                            }


                                                            return hashMap;
                                                        }
                                                    };

                                                    requestQueue.add(request2);

                                                    if (!c1.moveToFirst())
                                                    {
                                                        //Get all posts from other users from server and populate feeds page
                                                        try{
                                                            request3 = new StringRequest(Request.Method.POST, URL3, new Response.Listener<String>()
                                                            {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    try {

                                                                        System.out.println("login feeds inserting into local");

                                                                        JSONObject outer = new JSONObject(response);
                                                                        Iterator<String> keys =outer.keys();
                                                                        while(keys.hasNext()){
                                                                            String key = keys.next();
                                                                            JSONObject inside = outer.getJSONObject(key);

                                                                            if(inside.names().get(0).equals("success"))
                                                                            {
                                                                                SQLiteDatabase sellitDB = (SignUp.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                                                String iNo = inside.getString("number");
                                                                                String iName = inside.getString("name");
                                                                                String iPrice = inside.getString("price");
                                                                                String iQuantity = inside.getString("quantity");
                                                                                String iDescription = inside.getString("description");
                                                                                String iUsername = inside.getString("username");
                                                                                String iLastUpdated = inside.getString("last_updated");
                                                                                String iDeleteStatus = inside.getString("delete_status");
                                                                                String iEmail = inside.getString("email");


                                                                                sellitDB.execSQL("CREATE TABLE IF NOT EXISTS feeds(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), last_updated text, delete_status boolean, email varchar(50), FOREIGN KEY (username) REFERENCES users(username))");

                                                                                sellitDB.execSQL("INSERT INTO feeds(name, price, quantity, description, username, last_updated, delete_status, email) VALUES ('" + iName + "', '" + iPrice + "','" + iQuantity + "','" + iDescription + "','" + iUsername + "', '" + iLastUpdated + "','" + iDeleteStatus +"', '"+iEmail+"')");
                                                                                System.out.println(iNo);
                                                                                System.out.println(iDeleteStatus);
                                                                                feedarrayNames.add(iNo);
                                                                                feedarrayIDs.add(iName);
                                                                                feedarrayPrice.add(iPrice);
                                                                                feedarrayQuantity.add(iQuantity);
                                                                                feedarrayDescription.add(iDescription);
                                                                                feedarrayEmail.add(iEmail);



                                                                            }
                                                                            else if (inside.names().get(0).equals("noitems"))
                                                                            {
                                                                                Toast.makeText(SignUp.this, "Congrats! You can start using Sell It!", Toast.LENGTH_SHORT).show();
                                                                                startActivity(new Intent(getApplicationContext(), Feed.class));
                                                                            }
                                                                            else if(outer.names().get(0).equals("fail"))
                                                                            {
                                                                                Toast.makeText(SignUp.this, "Could not fetch feeds. Try Later.", Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        }

                                                                        Toast.makeText(SignUp.this, "Congrats! You can start using Sell It!", Toast.LENGTH_SHORT).show();
                                                                        startActivity(new Intent(getApplicationContext(), Feed.class));



                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }, new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error)
                                                                {

                                                                }
                                                            }){
                                                                @Override
                                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                                    HashMap<String, String> hashMap = new HashMap<>();

                                                                    try {

                                                                        SQLiteDatabase sellitDB = (SignUp.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                                        sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                                                        Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                                                        int usernameIndex = c.getColumnIndex("username");

                                                                        c.moveToFirst();

                                                                        String username = c.getString(usernameIndex);

                                                                        hashMap.put("username", username);
                                                                    }catch(Exception e)
                                                                    {
                                                                        e.printStackTrace();
                                                                    }


                                                                    return hashMap;
                                                                }
                                                            };

                                                            requestQueue.add(request3);



                                                        }catch (Exception e)
                                                        {
                                                            e.printStackTrace();
                                                        }
                                                    }



                                                }catch (Exception e)
                                                {
                                                    e.printStackTrace();
                                                }
                                            }



                                        }catch (Exception e)
                                        {
                                            e.printStackTrace();
                                        }
                                    }
                                    else if(jsonObject.names().get(0).equals("fail"))
                                    {
                                        Toast.makeText(SignUp.this, "This user ID or email is taken. Use different IDs.", Toast.LENGTH_SHORT).show();
                                    }
                                    else
                                    {
                                        Toast.makeText(SignUp.this, "Poor or no Internet. Try once you are connected.", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {

                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError{
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("username", un);
                                hashMap.put("password", p1);
                                hashMap.put("fn", f_n);
                                hashMap.put("ln", l_n);
                                hashMap.put("email", em);


                                return hashMap;
                            }
                        };

                        requestQueue.add(request);

                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
            }

        });

    }
}
