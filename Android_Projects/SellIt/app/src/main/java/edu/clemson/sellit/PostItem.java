package edu.clemson.sellit;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

//Post new item
public class PostItem extends AppCompatActivity {

    private RequestQueue requestQueue;
    private static final String URL = "https://people.cs.clemson.edu/~bgovind/cpsc682a3/postitem.php";
    private StringRequest request;

    //Check if field is blank
    private boolean isEmpty(EditText inputText)
    {
        if (inputText.getText().toString().trim().length() > 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    //Display toast if field is blank
    private void blankAlert()
    {
        Toast.makeText(PostItem.this, "All fields are mandatory.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_item);

        Button postHit = (Button) findViewById(R.id.postItem);

        requestQueue = Volley.newRequestQueue(this);

        postHit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText name = (EditText) findViewById(R.id.itemName);
                EditText price = (EditText) findViewById(R.id.itemPrice);
                EditText quantity = (EditText) findViewById(R.id.quantity);
                EditText description = (EditText) findViewById(R.id.description);

                final String itemName = name.getText().toString();
                final String itemPrice = price.getText().toString();
                final String itemQuantity = quantity.getText().toString();
                final String itemDescription = description.getText().toString();

                if(isEmpty(name) || isEmpty(price) || isEmpty(quantity) || isEmpty(description))
                {
                    blankAlert();
                }
                else if (itemName.length() > 50)
                {
                    Toast.makeText(PostItem.this, "Item Name cannot exceed 50 characters.", Toast.LENGTH_SHORT).show();
                }
                else if (itemDescription.length() > 250)
                {
                    Toast.makeText(PostItem.this, "Item Description cannot exceed 250 characters.", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    //Send post to server so that other users can see the posts on their feed when they refresh
                    try
                    {
                        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    System.out.println(jsonObject.names().get(0));
                                    //String jsonResult = response.getString("success");
                                    if(jsonObject.names().get(0).equals("success"))
                                    {
                                        SQLiteDatabase sellitDB = (PostItem.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                        sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                        Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                        int usernameIndex = c.getColumnIndex("username");

                                        c.moveToFirst();

                                        String username = c.getString(usernameIndex);
                                        sellitDB.execSQL("CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), delete_status boolean, FOREIGN KEY (username) REFERENCES users(username))");
                                        sellitDB.execSQL("INSERT INTO items(name, price, quantity, description, username, delete_status) VALUES ('" + itemName + "', '" + itemPrice + "','" + itemQuantity + "','" + itemDescription + "','" + username + "', 'f')");

                                        Toast.makeText(PostItem.this, "Congrats! Your item has been posted for sale.", Toast.LENGTH_SHORT).show();
                                        startActivity(new Intent(getApplicationContext(), Feed.class));
                                    }
                                    else if(jsonObject.names().get(0).equals("fail"))
                                    {
                                        Toast.makeText(PostItem.this, "Cannot post item now. Try again later.", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {

                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String, String> hashMap = new HashMap<>();

                                try {

                                    SQLiteDatabase sellitDB = (PostItem.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                    sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                    Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                    int usernameIndex = c.getColumnIndex("username");

                                    c.moveToFirst();

                                    String username = c.getString(usernameIndex);

                                    hashMap.put("name", itemName);
                                    hashMap.put("price", itemPrice);
                                    hashMap.put("quantity", itemQuantity);
                                    hashMap.put("description", itemDescription);
                                    hashMap.put("username", username);
                                }catch(Exception e)
                                {
                                    e.printStackTrace();
                                }


                                return hashMap;
                            }
                        };

                        requestQueue.add(request);



                    }catch(Exception e)
                    {
                        e.printStackTrace();
                    }



                }


            }

        });

    }
}
