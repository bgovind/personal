package edu.clemson.sellit;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//Login Activity
public class LoginActivity extends AppCompatActivity
{

    private ArrayList<String> arrayNames = new ArrayList<>();
    private ArrayList<String> arrayIDs = new ArrayList<>();
    private ArrayList<String> arrayPrice = new ArrayList<>();
    private ArrayList<String> arrayQuantity = new ArrayList<>();
    private ArrayList<String> arrayDescription= new ArrayList<>();

    private ArrayList<String> feedarrayNames = new ArrayList<>();
    private ArrayList<String> feedarrayIDs = new ArrayList<>();
    private ArrayList<String> feedarrayPrice = new ArrayList<>();
    private ArrayList<String> feedarrayQuantity = new ArrayList<>();
    private ArrayList<String> feedarrayDescription= new ArrayList<>();
    private ArrayList<String> feedarrayEmail= new ArrayList<>();
    private ArrayList<String> feedarrayUsername= new ArrayList<>();
    //private ArrayList<String> feedarrayLastUpdated= new ArrayList<>();

    private RequestQueue requestQueue;

    private static final String URL1 = "https://people.cs.clemson.edu/~bgovind/cpsc682a3/login.php";
    private static final String URL2 = "https://people.cs.clemson.edu/~bgovind/cpsc682a3/myposts.php";
    private static final String URL3 = "https://people.cs.clemson.edu/~bgovind/cpsc682a3/allposts.php";


    private StringRequest request1;
    private StringRequest request2;
    private StringRequest request3;



    //Check if field is blank
    private boolean isEmpty(EditText inputText)
    {
        if (inputText.getText().toString().trim().length() > 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    //Display toast if field is blank
    private void blankAlert()
    {
        Toast.makeText(LoginActivity.this, "Username and Password cannot be blank", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {


        super.onCreate(savedInstanceState);

        if( getIntent().getBooleanExtra("Exit me", false)){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
        try {

            SQLiteDatabase sellitDB = (LoginActivity.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
            sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
            Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
            System.out.println(c.getCount());

            if (c.getCount() == 0)
            {

                setContentView(R.layout.activity_login);
                Button loginHit = (Button) findViewById(R.id.login);
                Button signupHit = (Button) findViewById(R.id.signup);

                requestQueue = Volley.newRequestQueue(this);
                //requestQueue2 = Volley.newRequestQueue(this);


                //Login When uses enters username, password and clicks login button
                loginHit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText uname = (EditText) findViewById(R.id.uname);
                        EditText pwd = (EditText) findViewById(R.id.pwd);
                        final String username = uname.getText().toString();
                        final String password = pwd.getText().toString();
                        if (isEmpty(uname) || isEmpty(pwd)) {
                            blankAlert();
                        } else
                        {
                            try
                            {
                                request1 = new StringRequest(Request.Method.POST, URL1, new Response.Listener<String>()
                                {
                                    //Send request to server and look for response if user has entered correct credentials
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            System.out.println(jsonObject.names().get(0));
                                            if(jsonObject.names().get(0).equals("success"))
                                            {
                                                SQLiteDatabase sellitDB = (LoginActivity.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                String db_fn = jsonObject.getString("fn");
                                                String db_ln = jsonObject.getString("ln");
                                                String db_email = jsonObject.getString("email");
                                                sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username varchar(10), password varchar(10), fn varchar(50), ln varchar(50), email varchar(50), last_updated text)");
                                                sellitDB.execSQL("DELETE FROM users");
                                                sellitDB.execSQL("INSERT INTO users(username, password, fn, ln, email) VALUES ('" + username + "', '" + password + "','" + db_fn + "','" + db_ln + "','" + db_email + "')");


                                                try{

                                                    //final SQLiteDatabase sellitDB = (LoginActivity.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                    sellitDB.execSQL("CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), delete_status boolean, FOREIGN KEY (username) REFERENCES users(username))");
                                                    sellitDB.execSQL("CREATE TABLE IF NOT EXISTS feeds(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), last_updated text, delete_status boolean, email varchar(50), FOREIGN KEY (username) REFERENCES users(username))");
                                                    Cursor c = sellitDB.rawQuery("SELECT id, name FROM items", null);
                                                    final Cursor c1 = sellitDB.rawQuery("SELECT id, name FROM feeds", null);
                                                    if (c.moveToFirst())
                                                        System.out.println("login items true");
                                                    else
                                                        System.out.println("login items false");

                                                    if (c1.moveToFirst())
                                                        System.out.println("login feed true");
                                                    else
                                                        System.out.println("login feed false");

                                                    if (!c.moveToFirst())
                                                    {
                                                        //Fetch posts alreadt posted by user from server
                                                        try{
                                                            request2 = new StringRequest(Request.Method.POST, URL2, new Response.Listener<String>()
                                                            {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    try {
                                                                        System.out.println("login user items inserting into local");

                                                                        JSONObject outer = new JSONObject(response);
                                                                        Iterator<String> keys =outer.keys();
                                                                        while(keys.hasNext())
                                                                        {
                                                                            String key = keys.next();
                                                                            JSONObject inside = outer.getJSONObject(key);

                                                                            if(inside.names().get(0).equals("success"))
                                                                            {
                                                                                SQLiteDatabase sellitDB = (LoginActivity.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                                                String iNo = inside.getString("number");
                                                                                String iName = inside.getString("name");
                                                                                String iPrice = inside.getString("price");
                                                                                String iQuantity = inside.getString("quantity");
                                                                                String iDescription = inside.getString("description");
                                                                                String iDeleteStatus = inside.getString("delete_status");
                                                                                sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                                                                Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                                                                int usernameIndex = c.getColumnIndex("username");



                                                                                c.moveToFirst();

                                                                                String username = c.getString(usernameIndex);
                                                                                sellitDB.execSQL("CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), delete_status boolean, FOREIGN KEY (username) REFERENCES users(username))");

                                                                                sellitDB.execSQL("INSERT INTO items(name, price, quantity, description, username, delete_status) VALUES ('" + iName + "', '" + iPrice + "','" + iQuantity + "','" + iDescription + "','" + username + "', '" + iDeleteStatus +"')");
                                                                                System.out.println(iNo);
                                                                                System.out.println(iName);
                                                                                arrayNames.add(iNo);
                                                                                arrayIDs.add(iName);
                                                                                arrayPrice.add(iPrice);
                                                                                arrayQuantity.add(iQuantity);
                                                                                arrayDescription.add(iDescription);


                                                                            }
                                                                            else if (inside.names().get(0).equals("noitems"))
                                                                            {
                                                                                Toast.makeText(LoginActivity.this, "Welcome back to Sell It!", Toast.LENGTH_SHORT).show();
                                                                                startActivity(new Intent(getApplicationContext(), Feed.class));
                                                                            }
                                                                            else if(outer.names().get(0).equals("fail"))
                                                                            {
                                                                                Toast.makeText(LoginActivity.this, "Could not fetch your posts. Try Later.", Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }, new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error)
                                                                {

                                                                }
                                                            }){
                                                                @Override
                                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                                    HashMap<String, String> hashMap = new HashMap<>();

                                                                    try {

                                                                        SQLiteDatabase sellitDB = (LoginActivity.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                                        sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                                                        Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                                                        int usernameIndex = c.getColumnIndex("username");

                                                                        c.moveToFirst();

                                                                        String username = c.getString(usernameIndex);

                                                                        hashMap.put("username", username);
                                                                    }catch(Exception e)
                                                                    {
                                                                        e.printStackTrace();
                                                                    }


                                                                    return hashMap;
                                                                }
                                                            };

                                                            requestQueue.add(request2);

                                                            if (!c1.moveToFirst())
                                                            {
                                                                //Fetch all posts from server to populate user feed
                                                                try{
                                                                    request3 = new StringRequest(Request.Method.POST, URL3, new Response.Listener<String>()
                                                                    {
                                                                        @Override
                                                                        public void onResponse(String response) {
                                                                            try {

                                                                                System.out.println("login feeds inserting into local");

                                                                                JSONObject outer = new JSONObject(response);
                                                                                Iterator<String> keys =outer.keys();
                                                                                while(keys.hasNext()){
                                                                                    String key = keys.next();
                                                                                    JSONObject inside = outer.getJSONObject(key);

                                                                                    if(inside.names().get(0).equals("success"))
                                                                                    {
                                                                                        SQLiteDatabase sellitDB = (LoginActivity.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                                                        String iNo = inside.getString("number");
                                                                                        String iName = inside.getString("name");
                                                                                        String iPrice = inside.getString("price");
                                                                                        String iQuantity = inside.getString("quantity");
                                                                                        String iDescription = inside.getString("description");
                                                                                        String iUsername = inside.getString("username");
                                                                                        String iLastUpdated = inside.getString("last_updated");
                                                                                        String iDeleteStatus = inside.getString("delete_status");
                                                                                        String iEmail = inside.getString("email");


                                                                                        sellitDB.execSQL("CREATE TABLE IF NOT EXISTS feeds(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), last_updated text, delete_status boolean, email varchar(50), FOREIGN KEY (username) REFERENCES users(username))");

                                                                                        sellitDB.execSQL("INSERT INTO feeds(name, price, quantity, description, username, last_updated, delete_status, email) VALUES ('" + iName + "', '" + iPrice + "','" + iQuantity + "','" + iDescription + "','" + iUsername + "', '" + iLastUpdated + "','" + iDeleteStatus +"', '"+iEmail+"')");
                                                                                        System.out.println(iNo);
                                                                                        System.out.println(iDeleteStatus);
                                                                                        feedarrayNames.add(iNo);
                                                                                        feedarrayIDs.add(iName);
                                                                                        feedarrayPrice.add(iPrice);
                                                                                        feedarrayQuantity.add(iQuantity);
                                                                                        feedarrayDescription.add(iDescription);
                                                                                        feedarrayEmail.add(iEmail);



                                                                                    }
                                                                                    else if (inside.names().get(0).equals("noitems"))
                                                                                    {
                                                                                        Toast.makeText(LoginActivity.this, "Welcome back to Sell It!", Toast.LENGTH_SHORT).show();
                                                                                        startActivity(new Intent(getApplicationContext(), Feed.class));
                                                                                    }
                                                                                    else if(outer.names().get(0).equals("fail"))
                                                                                    {
                                                                                        Toast.makeText(LoginActivity.this, "Could not fetch feeds. Try Later.", Toast.LENGTH_SHORT).show();
                                                                                    }
                                                                                }

                                                                                Toast.makeText(LoginActivity.this, "Welcome back to Sell It!", Toast.LENGTH_SHORT).show();
                                                                                startActivity(new Intent(getApplicationContext(), Feed.class));



                                                                            } catch (Exception e) {
                                                                                e.printStackTrace();
                                                                            }
                                                                        }
                                                                    }, new Response.ErrorListener() {
                                                                        @Override
                                                                        public void onErrorResponse(VolleyError error)
                                                                        {

                                                                        }
                                                                    }){
                                                                        @Override
                                                                        protected Map<String, String> getParams() throws AuthFailureError {
                                                                            HashMap<String, String> hashMap = new HashMap<>();

                                                                            try {

                                                                                SQLiteDatabase sellitDB = (LoginActivity.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                                                sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                                                                Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                                                                int usernameIndex = c.getColumnIndex("username");

                                                                                c.moveToFirst();

                                                                                String username = c.getString(usernameIndex);

                                                                                hashMap.put("username", username);
                                                                            }catch(Exception e)
                                                                            {
                                                                                e.printStackTrace();
                                                                            }


                                                                            return hashMap;
                                                                        }
                                                                    };

                                                                    requestQueue.add(request3);



                                                                }catch (Exception e)
                                                                {
                                                                    e.printStackTrace();
                                                                }
                                                            }



                                                        }catch (Exception e)
                                                        {
                                                            e.printStackTrace();
                                                        }
                                                    }



                                                }catch (Exception e)
                                                {
                                                    e.printStackTrace();
                                                }
                                            }
                                            else if(jsonObject.names().get(0).equals("fail"))
                                            {
                                                Toast.makeText(LoginActivity.this, "Incorrect Username or Password.", Toast.LENGTH_SHORT).show();
                                            }
                                            else
                                            {
                                                Toast.makeText(LoginActivity.this, "Poor or no Internet. Try once you are connected.", Toast.LENGTH_SHORT).show();
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error)
                                    {

                                    }
                                }){
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        HashMap<String, String> hashMap = new HashMap<>();
                                        hashMap.put("username", username);
                                        hashMap.put("password", password);

                                        return hashMap;
                                    }
                                };
                                requestQueue.add(request1);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    }


                });
                //Take user to signup screen if user doesn't have a Sell It account
                signupHit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getApplicationContext(), SignUp.class));
                    }


                });
            }

            //Take user directly to Feed screen if user has Sell It installed and signed in on the device before
            else
            {
                System.out.println("old user");
                startActivity(new Intent(getApplicationContext(), Feed.class));
                System.out.println("old user");
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    //Inflate menu options for overflow menu
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_login, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.info) {
            startActivity(new Intent(getApplicationContext(), InfoPage.class));
        }
        return true;
    }

    //Exit Application when user taps back button from Login screen
    @Override
    public void onBackPressed()
    {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

}
