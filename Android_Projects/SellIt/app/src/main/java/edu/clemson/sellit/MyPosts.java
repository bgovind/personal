package edu.clemson.sellit;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static edu.clemson.sellit.R.layout.updatepost;

//View User's post. delete or edit posts
public class MyPosts extends AppCompatActivity {

    private ArrayList<String> arrayNames = new ArrayList<>();
    private ArrayList<String> arrayIDs = new ArrayList<>();
    private ArrayList<String> arrayPrice = new ArrayList<>();
    private ArrayList<String> arrayQuantity = new ArrayList<>();
    private ArrayList<String> arrayDescription= new ArrayList<>();

    private RequestQueue requestQueue;
    private static final String URL = "https://people.cs.clemson.edu/~bgovind/cpsc682a3/deletepost.php";
    private static final String URL2 = "https://people.cs.clemson.edu/~bgovind/cpsc682a3/updatepost.php";
    private StringRequest request;
    private StringRequest request2;

    ListView list;

    //Check if field is blank
    private boolean isEmpty(EditText inputText)
    {
        if (inputText.getText().toString().trim().length() > 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    //Display toast if field is blank
    private void blankAlert()
    {
        Toast.makeText(MyPosts.this, "All fields are mandatory.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_posts);

        requestQueue = Volley.newRequestQueue(this);

        //Populate user's posts from internal database
        try
        {
            final SQLiteDatabase sellitDB = (MyPosts.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
            sellitDB.execSQL("CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), delete_status boolean, FOREIGN KEY (username) REFERENCES users(username))");
            Cursor c = sellitDB.rawQuery("SELECT id, name, price, quantity, description, delete_status FROM items where delete_status='f'", null);
            int idIndex = c.getColumnIndex("id");
            int nameIndex = c.getColumnIndex("name");
            int priceIndex = c.getColumnIndex("price");
            int quantityIndex = c.getColumnIndex("quantity");
            int descriptionIndex = c.getColumnIndex("description");
            int deleteIndex = c.getColumnIndex("delete_status");

            if (c.moveToFirst())
                System.out.println("true");
            else
                System.out.println("false");

            c.moveToFirst();
            while(c != null && !c.isAfterLast())
            {
                String itemID = c.getString(idIndex);
                String itemName = c.getString(nameIndex);
                String itemPrice = c.getString(priceIndex);
                String itemQuantity = c.getString(quantityIndex);
                String itemDescription = c.getString(descriptionIndex);
                String itemDeleteStatus = c.getString(deleteIndex);
                System.out.println(itemID);
                System.out.println(itemName);
                System.out.println(itemDeleteStatus);
                arrayNames.add(itemName);
                arrayIDs.add(itemID);
                arrayPrice.add(itemPrice);
                arrayQuantity.add(itemQuantity);
                arrayDescription.add(itemDescription);

                c.moveToNext();
            }

            c.close();

        }catch(Exception e)
        {
            System.out.println("Exception thrown by DB");
            e.printStackTrace();
        }

        CustomListAdapter adapter=new CustomListAdapter(this, arrayNames, arrayIDs, arrayPrice, arrayQuantity, arrayDescription);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);

        //Display edit post dialog when user taps on a item in the list
       list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           //Display edit post dialog when user taps on a item in the list
           @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                final String SlecteditemName= arrayNames.get(position).toString();
                final String SlecteditemPrice = arrayPrice.get(position).toString();
                final String SlecteditemQuantity = arrayQuantity.get(position).toString();
                final String SlecteditemDescription = arrayDescription.get(position).toString();

                final Context context = MyPosts.this;
                LayoutInflater li = LayoutInflater.from(context);
                final View updatedView = li.inflate(updatepost, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setView(updatedView);

                final EditText nameInput = (EditText) updatedView.findViewById(R.id.updateitemName);
                nameInput.setText(SlecteditemName);
                final EditText priceInput = (EditText) updatedView.findViewById(R.id.updateitemPrice);
                priceInput.setText(SlecteditemPrice);
                final EditText quantityInput = (EditText) updatedView.findViewById(R.id.updatequantity);
                quantityInput.setText(SlecteditemQuantity);
                final EditText descriptionInput = (EditText) updatedView.findViewById(R.id.updatedescription);
                descriptionInput.setText(SlecteditemDescription);

                //Update post and upload to server when user taps updarte from dialog
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Update",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        try {

                                            EditText newname = (EditText) updatedView.findViewById(R.id.updateitemName);
                                            EditText newprice = (EditText) updatedView.findViewById(R.id.updateitemPrice);
                                            EditText newquantity = (EditText) updatedView.findViewById(R.id.updatequantity);
                                            EditText newdescription = (EditText) updatedView.findViewById(R.id.updatedescription);

                                            final String newitemName = newname.getText().toString();
                                            final String newitemPrice = newprice.getText().toString();
                                            final String newitemQuantity = newquantity.getText().toString();
                                            final String newitemDescription = newdescription.getText().toString();

                                            if(isEmpty(newname) || isEmpty(newprice) || isEmpty(newquantity) || isEmpty(newdescription))
                                            {
                                                blankAlert();
                                            }
                                            else if (newitemName.length() > 50)
                                            {
                                                Toast.makeText(MyPosts.this, "Item Name cannot exceed 50 characters.", Toast.LENGTH_SHORT).show();
                                            }
                                            else if (newitemDescription.length() > 250)
                                            {
                                                Toast.makeText(MyPosts.this, "Item Description cannot exceed 250 characters.", Toast.LENGTH_SHORT).show();
                                            }

                                            else {

                                                try {
                                                    try {
                                                        request2 = new StringRequest(Request.Method.POST, URL2, new Response.Listener<String>() {
                                                            @Override
                                                            public void onResponse(String response) {
                                                                try {
                                                                    JSONObject jsonObject = new JSONObject(response);
                                                                    System.out.println(jsonObject.names().get(0));
                                                                    if (jsonObject.names().get(0).equals("success")) {
                                                                        SQLiteDatabase sellitDB = (MyPosts.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                                        sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                                                        Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                                                        int usernameIndex = c.getColumnIndex("username");

                                                                        c.moveToFirst();

                                                                        String username = c.getString(usernameIndex);
                                                                        sellitDB.execSQL("CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), delete_status boolean, FOREIGN KEY (username) REFERENCES users(username))");
                                                                        sellitDB.execSQL("UPDATE items set delete_status = 't' where name = '" + SlecteditemName + "' and price = '" + SlecteditemPrice + "' and quantity = '" + SlecteditemQuantity + "' and description = '" + SlecteditemDescription + "' and username = '" + username + "'");
                                                                        sellitDB.execSQL("INSERT INTO items(name, price, quantity, description, username, delete_status) VALUES ('" + newitemName + "', '" + newitemPrice + "','" + newitemQuantity + "','" + newitemDescription + "','" + username + "', 'f')");


                                                                        Toast.makeText(MyPosts.this, "Your post has been updated successfully!", Toast.LENGTH_SHORT).show();
                                                                        startActivity(new Intent(getApplicationContext(), Feed.class));
                                                                    } else if (jsonObject.names().get(0).equals("fail")) {
                                                                        Toast.makeText(MyPosts.this, "Cannot post item now. Try again later.", Toast.LENGTH_SHORT).show();
                                                                    }

                                                                } catch (Exception e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                        }, new Response.ErrorListener() {
                                                            @Override
                                                            public void onErrorResponse(VolleyError error) {

                                                            }
                                                        }) {
                                                            @Override
                                                            protected Map<String, String> getParams() throws AuthFailureError {
                                                                HashMap<String, String> hashMap = new HashMap<>();

                                                                try {

                                                                    SQLiteDatabase sellitDB = (MyPosts.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                                    sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                                                    Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                                                    int usernameIndex = c.getColumnIndex("username");

                                                                    c.moveToFirst();

                                                                    String username = c.getString(usernameIndex);

                                                                    hashMap.put("name", SlecteditemName);
                                                                    hashMap.put("price", SlecteditemPrice);
                                                                    hashMap.put("quantity", SlecteditemQuantity);
                                                                    hashMap.put("description", SlecteditemDescription);
                                                                    hashMap.put("newname", newitemName);
                                                                    hashMap.put("newprice", newitemPrice);
                                                                    hashMap.put("newquantity", newitemQuantity);
                                                                    hashMap.put("newdescription", newitemDescription);
                                                                    hashMap.put("username", username);
                                                                } catch (Exception e) {
                                                                    e.printStackTrace();
                                                                }


                                                                return hashMap;
                                                            }
                                                        };

                                                        requestQueue.add(request2);


                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }


                                                    recreate(); // delete entry in  db

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }


                                                recreate(); // update entry in db
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });



                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });

        //Display delete post dialog when users long presses an item on the lost of posts
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> parent, View v,
                                           int position, long id) {

                final String SlecteditemName = arrayNames.get(position).toString();
                final String SlecteditemPrice = arrayPrice.get(position).toString();
                final String SlecteditemQuantity = arrayQuantity.get(position).toString();
                final String SlecteditemDescription = arrayDescription.get(position).toString();
                System.out.println(SlecteditemPrice);



                final Context context = MyPosts.this;
                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.prompts, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setView(promptsView);


                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Delete",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        try {
                                            try
                                            {
                                                request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>()
                                                {
                                                    @Override
                                                    public void onResponse(String response) {
                                                        try {
                                                            JSONObject jsonObject = new JSONObject(response);
                                                            System.out.println(jsonObject.names().get(0));
                                                            if(jsonObject.names().get(0).equals("success"))
                                                            {
                                                                SQLiteDatabase sellitDB = (MyPosts.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                                sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                                                Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                                                int usernameIndex = c.getColumnIndex("username");

                                                                c.moveToFirst();

                                                                String username = c.getString(usernameIndex);
                                                                sellitDB.execSQL("CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), delete_status boolean, FOREIGN KEY (username) REFERENCES users(username))");
                                                                sellitDB.execSQL("UPDATE items set delete_status = 't' where name = '"+SlecteditemName+"' and price = '"+SlecteditemPrice+"' and quantity = '"+SlecteditemQuantity+"' and description = '"+SlecteditemDescription+"' and username = '"+username+"'");

                                                                Toast.makeText(MyPosts.this, "Your post has been deleted!", Toast.LENGTH_SHORT).show();
                                                                startActivity(new Intent(getApplicationContext(), Feed.class));
                                                            }
                                                            else if(jsonObject.names().get(0).equals("fail"))
                                                            {
                                                                Toast.makeText(MyPosts.this, "Cannot post item now. Try again later.", Toast.LENGTH_SHORT).show();
                                                            }

                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }, new Response.ErrorListener() {
                                                    @Override
                                                    public void onErrorResponse(VolleyError error)
                                                    {

                                                    }
                                                }){
                                                    @Override
                                                    protected Map<String, String> getParams() throws AuthFailureError {
                                                        HashMap<String, String> hashMap = new HashMap<>();

                                                        try {

                                                            SQLiteDatabase sellitDB = (MyPosts.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
                                                            sellitDB.execSQL("CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username VARCHAR(10), password VARCHAR(10), fn VARCHAR(50), ln VARCHAR(50), email VARCHAR(50), last_updated text)");
                                                            Cursor c = sellitDB.rawQuery("SELECT * FROM users", null);
                                                            int usernameIndex = c.getColumnIndex("username");

                                                            c.moveToFirst();

                                                            String username = c.getString(usernameIndex);

                                                            hashMap.put("name", SlecteditemName);
                                                            hashMap.put("price", SlecteditemPrice);
                                                            hashMap.put("quantity", SlecteditemQuantity);
                                                            hashMap.put("description", SlecteditemDescription);
                                                            hashMap.put("username", username);
                                                        }catch(Exception e)
                                                        {
                                                            e.printStackTrace();
                                                        }


                                                        return hashMap;
                                                    }
                                                };

                                                requestQueue.add(request);



                                            }catch(Exception e)
                                            {
                                                e.printStackTrace();
                                            }




                                            recreate(); // delete entry in  db
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });



                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

                return true;
            }
        });
    }

}