package edu.clemson.sellit;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

//Display all posts deleted by user
public class DeletedPosts extends AppCompatActivity {

    private ArrayList<String> arrayNames = new ArrayList<>();
    private ArrayList<String> arrayIDs = new ArrayList<>();
    private ArrayList<String> arrayPrice = new ArrayList<>();
    private ArrayList<String> arrayQuantity = new ArrayList<>();
    private ArrayList<String> arrayDescription= new ArrayList<>();

    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deleted_posts);

        //populate all deleted posts from internal database
        try
        {
            final SQLiteDatabase sellitDB = (DeletedPosts.this).openOrCreateDatabase("sellit", MODE_PRIVATE, null);
            sellitDB.execSQL("CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY, name varchar(50), price numeric(20,2), quantity integer, description varchar(250), username varchar(10), delete_status boolean, FOREIGN KEY (username) REFERENCES users(username))");
            Cursor c = sellitDB.rawQuery("SELECT id, name, price, quantity, description, delete_status FROM items where delete_status='t'", null);
            int idIndex = c.getColumnIndex("id");
            int nameIndex = c.getColumnIndex("name");
            int priceIndex = c.getColumnIndex("price");
            int quantityIndex = c.getColumnIndex("quantity");
            int descriptionIndex = c.getColumnIndex("description");
            int deleteIndex = c.getColumnIndex("delete_status");

            if (c.moveToFirst())
                System.out.println("true");
            else
                System.out.println("false");

            c.moveToFirst();
            while(c != null && !c.isAfterLast())
            {
                String itemID = c.getString(idIndex);
                String itemName = c.getString(nameIndex);
                String itemPrice = c.getString(priceIndex);
                String itemQuantity = c.getString(quantityIndex);
                String itemDescription = c.getString(descriptionIndex);
                String itemDeleteStatus = c.getString(deleteIndex);
                System.out.println(itemID);
                System.out.println(itemName);
                System.out.println(itemDeleteStatus);
                arrayNames.add(itemName);
                arrayIDs.add(itemID);
                arrayPrice.add(itemPrice);
                arrayQuantity.add(itemQuantity);
                arrayDescription.add(itemDescription);

                c.moveToNext();
            }

            c.close();

        }catch(Exception e)
        {
            System.out.println("Exception thrown by DB");
            e.printStackTrace();
        }

        CustomListAdapter adapter=new CustomListAdapter(this, arrayNames, arrayIDs, arrayPrice, arrayQuantity, arrayDescription);
        list=(ListView)findViewById(R.id.deletedlist);
        list.setAdapter(adapter);




    }
}
