package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

//Info for Stickman App
public class stickmaninfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stickmaninfo);

        TextView info = (TextView)findViewById(R.id.sminfo);
        String temp = "Welcome to StickMan!  This game is revolved around the classic" +
                "game of hangman.  From the splash screen you can create a custom word list, " +
                "or play the game.  If you select to create a custom word list, press the create " +
                "custom button.  This will bring up another screen where you can add, edit, and " +
                "delete words from your custom list.  Once you are finished, press back and hit the play button." +
                "This will bring up a dialog to select between custom or normal mode.  Select custom." +
                "If you want to play with a standard library built into the app, select normal mode. " +
                "Enjoy!";
        info.setText(temp);
        info.setTextSize(18f);
        info.setTypeface(Typeface.create("casual", Typeface.NORMAL));
        
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, stickManSplashScreen.class);
        startActivity(intent);
        finish();
    }
}
