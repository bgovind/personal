package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

//Start SlapJack
public class slapjackstart extends AppCompatActivity {

    private MediaPlayer slapsound;
    private boolean start = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slapjackstart);
        final TextView textView = (TextView)findViewById(R.id.slaptext);
        ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.slapview);
        slapsound = MediaPlayer.create(this, R.raw.slapsound);
        slapsound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();

            }});

        constraintLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                start = true;
                Intent intent = new Intent(getApplicationContext(), SlapJackSplashScreen.class);
                startActivity(intent);
                slapsound.start();
                finish();
            }
        });
        final Animation animationFadeIn =  AnimationUtils.loadAnimation(this, R.anim.in);
        final Animation animationFadeOut = AnimationUtils.loadAnimation(this, R.anim.out);
        //animationFadeIn.setDuration(2000);
        //animationFadeOut.setDuration(2000);
        animationFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textView.startAnimation(animationFadeOut);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animationFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textView.startAnimation(animationFadeIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        textView.startAnimation(animationFadeOut);

    }


    //Handle Back Button
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, mainMenuGameSelect.class);
        startActivity(intent);
        finish();
    }


}
