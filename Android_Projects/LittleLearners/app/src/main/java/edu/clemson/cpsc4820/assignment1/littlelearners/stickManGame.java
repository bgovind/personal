package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.app.AlertDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;


//Stickman Game Logic
public class stickManGame extends AppCompatActivity implements View.OnClickListener {

    private char[] wordArray;
    private TextView[] tvID4later;
    private TextView[] scoreBoardTV;
    private int numGuesses = 0;
    private int numRight = 0;
    private final int easy = 10;
    private Context context;
    private ImageView hangMan;
    private int totalWins = 0;
    private int numGuessLeft = easy - numGuesses;
    private TextToSpeech t2speach;
    private Button speak;
    private MediaPlayer soundeffect;
    private Intent intent;
    private SQLiteDatabase mydb;
    private int mode;


    @Override
    protected void onResume() {
        super.onResume();
        MusicManager.start(this, 2);
    }

    public void noWords()
    {
        Intent intent = new Intent(this, create_a_level.class);
        Toast.makeText(this, "No words in the list, add some here!", Toast.LENGTH_SHORT).show();
        startActivity(intent);
        MusicManager.pause();
        finish();
    }

    //Restart Activity
    public void restart() {
        intent = new Intent(stickManGame.this, stickManGame.class);
        intent.putExtra("mode", mode);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.loadingdialog, null);
        final AlertDialog alertD = new AlertDialog.Builder(this).create();
        alertD.setView(promptView);
        alertD.show();
        MusicManager.pause();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
                alertD.dismiss();
            }
        }, 1500);

    }

    public void gameOver() {
        //Intent intent = new Intent(this, gameOver.class);
        //startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        Button temp = (Button) v;
        String buttonText = temp.getText().toString();
        temp.setVisibility(View.INVISIBLE);
        checkGuess(buttonText);

    }

    //Handle Back Button
    @Override
    public void onBackPressed() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.smexit, null);

        final AlertDialog alertD = new AlertDialog.Builder(this).create();

        Button btnAdd1 = (Button) promptView.findViewById(R.id.btnYes);

        Button btnAdd2 = (Button) promptView.findViewById(R.id.btnNo);

        btnAdd1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertD.dismiss();
                Intent intent = new Intent(getApplicationContext(), stickManSplashScreen.class);
                startActivity(intent);
                //MusicManager.pause();
                MusicManager.release();
                finish();


            }
        });

        btnAdd2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                alertD.dismiss();

            }
        });

        alertD.setView(promptView);

        alertD.show();

    }


    public String generateWord(int mode) {
        String word = "null";
        switch (mode) {
            case 1: {
                try {
                    mydb = this.openOrCreateDatabase("wordDB", MODE_PRIVATE, null);
                    mydb.execSQL("CREATE TABLE IF NOT EXISTS wordTable (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, word VARCHAR(20)UNIQUE, isdeleted INT, lastsync TIMESTAMP)");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Cursor cursor = mydb.rawQuery("Select * FROM wordTable where isdeleted = '0'", null);
                if(cursor.getCount() > 0) {
                    Random randomGenerator = new Random();
                    int random = randomGenerator.nextInt(cursor.getCount());
                    int column = cursor.getColumnIndexOrThrow("word");
                    int isDeleted = cursor.getColumnIndexOrThrow("isdeleted");
                    cursor.moveToPosition(random);
                    while (word.equals("null")) {
                        if (cursor.getInt(isDeleted) == 0) {
                            word = cursor.getString(column);
                        } else {
                            cursor.moveToPosition(randomGenerator.nextInt(cursor.getCount()));
                        }
                    }
                    //  randomWord = cursor.getString(column);
                    mydb.close();
                }
                else{
                    noWords();
                }
                break;
            }
            case 2: {
                BufferedReader readWords = new BufferedReader(new InputStreamReader(this.getResources().openRawResource(R.raw.wordlist)));
                ArrayList<String> wordList = new ArrayList<>();

                try {
                    while ((word = readWords.readLine()) != null) {
                        if (word.length() > 1) {
                            wordList.add(word);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Random randomNumber = new Random();
                word = wordList.get(randomNumber.nextInt(wordList.size()));
                break;
            }
        }
        return word;
    }

    //Check Guess
    public void checkGuess(String resourceName)
    {

        soundeffect = MediaPlayer.create(this, R.raw.success);
        soundeffect.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();

            }});
        boolean inWord = false;
        for(int index = 0; index < wordArray.length; index++)
        {
            String temp = String.valueOf(wordArray[index]);

            if(temp.equalsIgnoreCase(resourceName))
            {
                tvID4later[index].setText(resourceName);
                numRight++;
                inWord = true;
                //soundeffect = MediaPlayer.create(this, R.raw.success);


            }
        }
        if(!inWord)
        {
            numGuesses++;
            soundeffect = MediaPlayer.create(this, R.raw.fail);
            soundeffect.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();

                }});
            soundeffect.start();
            numGuessLeft = easy - numGuesses;
            scoreBoardTV[3].setText(String.valueOf(numGuessLeft));
            switch(numGuesses)
            {
                case 1:
                    hangMan.setImageResource(R.drawable.head);
                    break;
                case 2:
                    hangMan.setImageResource(R.drawable.body);
                    break;
                case 3:
                    hangMan.setImageResource(R.drawable.arm1);
                    break;
                case 4:
                    hangMan.setImageResource(R.drawable.arm2);
                    break;
                case 5:
                    hangMan.setImageResource(R.drawable.leg1);
                    break;
                case 6:
                    hangMan.setImageResource(R.drawable.leg2);
                    break;
                case 7:
                    hangMan.setImageResource(R.drawable.eye1);
                    break;
                case 8:
                    hangMan.setImageResource(R.drawable.eye2);
                    break;
                case 9:
                    hangMan.setImageResource(R.drawable.mouth);
                    break;
                case 10:
                    hangMan.setImageResource(R.drawable.gameover);
                    break;

            }
        }

        int cScore = 0;
        String level = "";

        soundeffect.start();
        int duration = Toast.LENGTH_LONG;
        if(numGuesses < easy)
        {
            if(numRight == wordArray.length)
            {
                String youWin = "You Win!! Congratulations!";
                try{
                    SQLiteDatabase llDB = (stickManGame.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                    llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer, foreign key (game_name) references game_list(game_name))");
                    Cursor c = llDB.rawQuery("SELECT * FROM progress where game_name = 'stickman'", null);
                    int cScoreIndex = c.getColumnIndex("cScore");
                    if (c.getCount()!=0) {
                        System.out.println("Stickman Score exists");
                        c.moveToFirst();
                        cScore = c.getInt(cScoreIndex);


                    } else {
                        System.out.println("Error");
                    }
                    c.close();
                    llDB.close();
                }catch(Exception e)
                {
                    e.printStackTrace();
                }

                System.out.println("Old Score: "+cScore);

                cScore = cScore + 1;

                if(cScore >= 1000)
                {
                    cScore = 1000;
                }

                if(cScore < 5)
                {
                    level = "Level 1";
                }
                else if (cScore >= 5 || cScore < 25)
                {
                    level = "Level 2";
                }
                else if (cScore >= 25 || cScore < 50)
                {
                    level = "Level 3";
                }
                else if (cScore >= 50 || cScore < 80)
                {
                    level = "Level 4";
                }
                else if (cScore >= 80 || cScore < 100)
                {
                    level = "Level 5";
                }
                else if (cScore >= 100 || cScore < 120)
                {
                    level = "Level 6";
                }
                else if (cScore >= 120 || cScore < 140)
                {
                    level = "Level 7";
                }
                else if (cScore >= 140 || cScore < 160)
                {
                    level = "Level 8";
                }
                else if (cScore >= 160 || cScore < 180)
                {
                    level = "Level 9";
                }
                else if (cScore >= 180 || cScore < 200)
                {
                    level = "Level 10";
                }
                else if (cScore >= 200 || cScore < 250)
                {
                    level = "Level 11";
                }
                else if (cScore >= 250 || cScore < 300)
                {
                    level = "Level 12";
                }
                else if (cScore >= 300 || cScore < 350)
                {
                    level = "Level 13";
                }
                else if (cScore >= 350 || cScore < 400)
                {
                    level = "Level 14";
                }
                else if (cScore >= 400 || cScore < 450)
                {
                    level = "Level 15";
                }
                else if (cScore >= 450 || cScore < 500)
                {
                    level = "Level 16";
                }
                else if (cScore >= 500 || cScore < 550)
                {
                    level = "Level 17";
                }
                else if (cScore >= 550 || cScore < 650)
                {
                    level = "Level 18";
                }
                else if (cScore >= 650 || cScore < 800)
                {
                    level = "Level 19";
                }
                else if (cScore >= 800 || cScore <= 1000)
                {
                    level = "Level 20";
                }

                System.out.println("Current Level : "+level);

                try{
                    SQLiteDatabase llDB = (stickManGame.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                    System.out.println("update stickman cScore");
                    llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer, foreign key (game_name) references game_list(game_name))");
                    llDB.execSQL("UPDATE progress set cScore = '"+cScore+"' where game_name = 'stickman'");
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
                try{
                    SQLiteDatabase llDB = (stickManGame.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                    System.out.println("update stickman cScore");
                    Cursor c = llDB.rawQuery("SELECT * FROM progress WHERE game_name = 'stickman'", null);
                    if(c.getCount() == 0)
                    {
                        llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer, foreign key (game_name) references game_list(game_name))");
                        llDB.execSQL("INSERT INTO progress(game_name, cScore) VALUES ( 'stickman', '"+ cScore +"')");
                    }
                    else {

                        llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer, foreign key (game_name) references game_list(game_name))");
                        llDB.execSQL("UPDATE progress set cScore = '"+cScore+"' where game_name = 'stickman'");
                    }
                    llDB.close();
                }catch(Exception e)
                {
                    e.printStackTrace();
                }

                System.out.println("New Stickman Score: "+cScore);

                soundeffect = MediaPlayer.create(this, R.raw.win);
                soundeffect.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();

                    }});
                soundeffect.start();
                Toast toast = Toast.makeText(context, youWin, duration);
                toast.show();
                totalWins++;
                SharedPreferences sp = getSharedPreferences("myPref", stickManGame.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putInt("winsKey", totalWins);
                editor.apply();
                restart();
            }
        }
        else
        {
            String youLose = "Sorry you lose....";
            Toast toast = Toast.makeText(context, youLose, duration);
            toast.show();
            soundeffect = MediaPlayer.create(this, R.raw.lose);
            soundeffect.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();

                }});
            soundeffect.start();
            //gameOver();
            restart();
        }

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stickmangame);
        mode = getIntent().getIntExtra("mode",2);
        SharedPreferences sp = getSharedPreferences("myPref", stickManGame.MODE_PRIVATE);
        totalWins = sp.getInt("winsKey", 0);
        MusicManager.start(this,2);
        context = getApplicationContext();


        LinearLayout wordHolder = (LinearLayout) findViewById(R.id.wordHolder);

        Button a = (Button) findViewById(R.id.A);
        a.setOnClickListener(this);
        Button b = (Button) findViewById(R.id.B);
        b.setOnClickListener(this);
        Button c = (Button) findViewById(R.id.C);
        c.setOnClickListener(this);
        Button d = (Button) findViewById(R.id.D);
        d.setOnClickListener(this);
        Button e = (Button) findViewById(R.id.E);
        e.setOnClickListener(this);
        Button f = (Button) findViewById(R.id.F);
        f.setOnClickListener(this);
        Button g = (Button) findViewById(R.id.G);
        g.setOnClickListener(this);
        Button h = (Button) findViewById(R.id.H);
        h.setOnClickListener(this);
        Button i = (Button) findViewById(R.id.I);
        i.setOnClickListener(this);
        Button j = (Button) findViewById(R.id.J);
        j.setOnClickListener(this);
        Button k = (Button) findViewById(R.id.K);
        k.setOnClickListener(this);
        Button l = (Button) findViewById(R.id.L);
        l.setOnClickListener(this);
        Button m = (Button) findViewById(R.id.M);
        m.setOnClickListener(this);
        Button n = (Button) findViewById(R.id.N);
        n.setOnClickListener(this);
        Button o = (Button) findViewById(R.id.O);
        o.setOnClickListener(this);
        Button p = (Button) findViewById(R.id.P);
        p.setOnClickListener(this);
        Button q = (Button) findViewById(R.id.Q);
        q.setOnClickListener(this);
        Button r = (Button) findViewById(R.id.R);
        r.setOnClickListener(this);
        Button s = (Button) findViewById(R.id.S);
        s.setOnClickListener(this);
        Button t = (Button) findViewById(R.id.T);
        t.setOnClickListener(this);
        Button u = (Button) findViewById(R.id.U);
        u.setOnClickListener(this);
        Button v = (Button) findViewById(R.id.V);
        v.setOnClickListener(this);
        Button w = (Button) findViewById(R.id.W);
        w.setOnClickListener(this);
        Button x = (Button) findViewById(R.id.X);
        x.setOnClickListener(this);
        Button y = (Button) findViewById(R.id.Y);
        y.setOnClickListener(this);
        Button z = (Button) findViewById(R.id.Z);
        z.setOnClickListener(this);

        final String newWord = generateWord(mode);

        wordArray = newWord.toCharArray();

        tvID4later = new TextView[newWord.length()];
        scoreBoardTV = new TextView[4];

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT, (1/newWord.length()));
        param.gravity = Gravity.CENTER_HORIZONTAL;
        param.setMargins(25,0,25,0);



        for(int index = 0; index < newWord.length(); index++)
        {

            TextView newText = new TextView(this);
            tvID4later[index] = newText;
            newText.setLayoutParams(param);
            newText.setId(index);
            newText.setText("_");
            newText.setTypeface(Typeface.create("casual",Typeface.NORMAL));
            newText.setAllCaps(true);
            newText.setTextSize(40);
            wordHolder.addView(newText);

        }

        hangMan = (ImageView)findViewById(R.id.hangmanView);
        hangMan.setImageResource(R.drawable.gallows);

        TextView wins = (TextView)findViewById(R.id.textView9);
        TextView guessLeft = (TextView)findViewById(R.id.textView10);
        TextView numWins = (TextView)findViewById(R.id.textView11);
        TextView numGuesses = (TextView)findViewById(R.id.textView12);

        // 4 is the amount of text views I need to make the scoreboard


        String stringWins = "Wins: ";
        String guessesLeft = "Guesses Left: ";

        wins.setText(stringWins);
        numWins.setText(String.valueOf(totalWins));
        guessLeft.setText(guessesLeft);
        numGuesses.setText(String.valueOf(numGuessLeft));

        scoreBoardTV[0] = wins;
        scoreBoardTV[1] = guessLeft;
        scoreBoardTV[2] = numWins;
        scoreBoardTV[3] = numGuesses;

        t2speach = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                t2speach.setLanguage(Locale.US);
                t2speach.setPitch(1f);


            }
        });
        speak = (Button)findViewById(R.id.speak);
        speak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(), newWord,Toast.LENGTH_SHORT).show();
                t2speach.speak(newWord, TextToSpeech.QUEUE_FLUSH, null);
            }
        });
    }

    //Handle Pause
    public void onPause(){
        if(t2speach!=null){
            t2speach.stop();
            t2speach.shutdown();
        }
        super.onPause();
        MusicManager.pause();
    }
}

