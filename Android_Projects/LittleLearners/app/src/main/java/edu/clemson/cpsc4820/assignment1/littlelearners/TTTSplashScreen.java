package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.lang.reflect.InvocationTargetException;


//Tic Tac Toe Splash Screen
public class TTTSplashScreen extends AppCompatActivity {

    private Button play;
    private Button info;
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(TTTSplashScreen.this, mainMenuGameSelect.class);
        startActivity(intent);
        finish();
    }
    public void playTTT()
    {
        Intent intent = new Intent(this, ticTacToeGame.class);
        startActivity(intent);
        finish();
    }
    public void infoTTT()
    {
        Intent intent = new Intent(this, TTTinfo.class);
        startActivity(intent);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tttsplash_screen);

        play = (Button)findViewById(R.id.tttplaybutton);
        play.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                playTTT();
            }
        });

        info = (Button)findViewById(R.id.tttinfobutton);
        info.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                infoTTT();
            }
        });



    }
}
