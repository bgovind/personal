package edu.clemson.cpsc4820.assignment1.littlelearners;

/**
 * Created by gfranci on 3/13/2017.
 */

//Set Parameter for Accesing Word DB
public class wordDB {

    private static final int SYNC_STATUS_OKAY = 0;
    private static final int SYNC_STATUS_FAILED = 1;
    public static final String SERVER_URL = "mysql1.cs.clemson.edu";
    public static final String DATABASE_NAME = "wordDB";
    public static final String TABLE_NAME ="wordTable";
    public static final String ID = "id";
    public static final String WORD ="word";
    public static final String IS_DELETED ="isdeleted";
    public static final String LAST_SYNC ="lastsync";
}
