package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;

import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

//Display quiz content with timer once started and grade quiz once submitted
public class quizContent extends AppCompatActivity {

    private ArrayList<String> arrayOptions = new ArrayList<>();
    private ArrayList<String> arrayAnswers = new ArrayList<>();
    private ArrayList<String> postedAnswers = new ArrayList<>();

    //private ArrayList<String> arrayQuestion = new ArrayList<>();

    private ArrayList<String> arrayOption1 = new ArrayList<>();
    private ArrayList<String> arrayOption2 = new ArrayList<>();
    private ArrayList<String> arrayOption3 = new ArrayList<>();
    private ArrayList<String> arrayOption4 = new ArrayList<>();


    CountDownTimer mCountDownTimer = null;
    /*private ArrayList<String> arrayAnswer = new ArrayList<>();


    ListView quizContentList;*/

    //Check for unanswered questions
    private boolean checkempty(int buttonStatus){
        if (buttonStatus == -1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_content);

        final String s = getIntent().getStringExtra("quizName");

        mCountDownTimer = new CountDownTimer(60000*5, 1000) {

            //Count down timer for quiz
            public void onTick(long millisUntilFinished) {
                TextView timer = (TextView) findViewById(R.id.timer);
                timer.setText("Time remaining "+String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

            }
            public void onFinish() {
                new AlertDialog.Builder(quizContent.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Time is up!")
                        .setMessage("Would you like to try this quiz again?")
                        .setPositiveButton("Yes. Try again", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                Intent intent = new Intent(getBaseContext(), quizContent.class);
                                intent.putExtra("quizName", s);
                                startActivity(intent);
                                finish();
                            }

                        })
                        .setNegativeButton("No. Try another ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                startActivity(new Intent(getApplicationContext(), quizList.class));
                                finish();
                            }
                        })
                        .show();
            }
        }.start();

        //Fetch quiz content form local SQLITE DB
        try
        {
            System.out.println("fetching quiz from local");
            final SQLiteDatabase llDB = (quizContent.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
            llDB.execSQL("CREATE TABLE IF NOT EXISTS quizcontent(id INTEGER PRIMARY KEY, name varchar(50), question varchar(100), op1 varchar(20), op2 varchar(20), op3 varchar(20), op4 varchar(20), answer INTEGER)");
            Cursor c = llDB.rawQuery("SELECT * FROM quizcontent where name = '"+s+"'", null);

            int questionIndex = c.getColumnIndex("question");
            int op1Index = c.getColumnIndex("op1");
            int op2Index = c.getColumnIndex("op2");
            int op3Index = c.getColumnIndex("op3");
            int op4Index = c.getColumnIndex("op4");
            int answerIndex = c.getColumnIndex("answer");


            /*if (c.moveToFirst())
                System.out.println("feeds true");
            else
                System.out.println("feeds false");*/

            int row=1;

            c.moveToFirst();
            while(c != null)
            {
                String question = c.getString(questionIndex);
                String questionRow = "question"+row;
                int questionRowID = getResources().getIdentifier(questionRow, "id", getPackageName());
                TextView questionCell = (TextView) findViewById(questionRowID);
                questionCell.setText("Challenge "+row+" : "+question);

                String op1 = c.getString(op1Index);
                String op2 = c.getString(op2Index);
                String op3 = c.getString(op3Index);
                String op4 = c.getString(op4Index);
                String answer = c.getString(answerIndex);


                arrayOptions.add(op1);
                arrayOptions.add(op2);
                arrayOptions.add(op3);
                arrayOptions.add(op4);
                arrayAnswers.add(answer);


                String radioGroupRow = "radiogroup"+row;
                int radioGroupRowID = getResources().getIdentifier(radioGroupRow, "id", getPackageName());
                RadioGroup rbtnGrp = (RadioGroup)findViewById(radioGroupRowID);
                for (int i = 0; i < rbtnGrp.getChildCount(); i++) {
                    ((RadioButton) rbtnGrp.getChildAt(i)).setText(arrayOptions.get(i));
                }

                arrayOptions.clear();

                /*String option1Row = "op"+row+"1";
                int option1RowID = getResources().getIdentifier(option1Row, "id", getPackageName());
                RadioButton op1Cell = (RadioButton) findViewById(option1RowID);
                op1Cell.setText(op1);


                String option2Row = "op"+row+"2";
                int option2RowID = getResources().getIdentifier(option2Row, "id", getPackageName());
                RadioButton op2Cell = (RadioButton) findViewById(option2RowID);
                op2Cell.setText(op2);

                String option3Row = "op"+row+"3";
                int option3RowID = getResources().getIdentifier(option3Row, "id", getPackageName());
                RadioButton op3Cell = (RadioButton) findViewById(option3RowID);
                op3Cell.setText(op3);

                String option4Row = "op"+row+"4";
                int option4RowID = getResources().getIdentifier(option4Row, "id", getPackageName());
                RadioButton op4Cell = (RadioButton) findViewById(option4RowID);
                op4Cell.setText(op4);*/



                System.out.println(question);
                System.out.println(op1);
                System.out.println(op2);
                System.out.println(op3);
                System.out.println(op4);
                System.out.println(answer);;

                //arrayQuestion.add(question);
                arrayOption1.add(op1);
                arrayOption2.add(op2);
                arrayOption3.add(op3);
                arrayOption4.add(op4);

                row=row+1;
                c.moveToNext();
            }

        }catch(Exception e)
        {
            System.out.println("Exception thrown by DB");
            e.printStackTrace();
        }

        /*CustomListAdapterQuizContent adapter=new CustomListAdapterQuizContent(this, arrayQuestion, arrayOption1, arrayOption2, arrayOption3, arrayOption4, arrayAnswer);
        quizContentList=(ListView)findViewById(R.id.quizcontentlist);
        quizContentList.setAdapter(adapter);*/

        Button submitQuiz = (Button) findViewById(R.id.submit);

        final Context context = this;
        LayoutInflater li = LayoutInflater.from(context);
        final View promptsView = li.inflate(R.layout.quizresultprompt, null);


        //Submit Quiz for grading
        submitQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCountDownTimer.cancel();
                RadioGroup rg1 = (RadioGroup) findViewById(R.id.radiogroup1);
                RadioGroup rg2 = (RadioGroup) findViewById(R.id.radiogroup2);
                RadioGroup rg3 = (RadioGroup) findViewById(R.id.radiogroup3);
                RadioGroup rg4 = (RadioGroup) findViewById(R.id.radiogroup4);
                RadioGroup rg5 = (RadioGroup) findViewById(R.id.radiogroup5);
                if(checkempty(rg1.getCheckedRadioButtonId()))
                {
                    Toast.makeText(quizContent.this, "You haven't answered challenge 1", Toast.LENGTH_SHORT).show();
                }
                else if(checkempty(rg2.getCheckedRadioButtonId())) {
                    Toast.makeText(quizContent.this, "You haven't answered challenge 2", Toast.LENGTH_SHORT).show();
                }else if(checkempty(rg3.getCheckedRadioButtonId())){
                    Toast.makeText(quizContent.this, "You haven't answered challenge 3", Toast.LENGTH_SHORT).show();
                }else if(checkempty(rg4.getCheckedRadioButtonId())){
                    Toast.makeText(quizContent.this, "You haven't answered challenge 4", Toast.LENGTH_SHORT).show();
                }else if(checkempty(rg5.getCheckedRadioButtonId())){
                    Toast.makeText(quizContent.this, "You haven't answered challenge 5", Toast.LENGTH_SHORT).show();
                }else {

                    RadioButton rb1 = (RadioButton) findViewById(rg1.getCheckedRadioButtonId());
                    System.out.println(rb1.getText().toString());
                    String answer1 = rb1.getText().toString();
                    postedAnswers.add(answer1);

                    RadioButton rb2 = (RadioButton) findViewById(rg2.getCheckedRadioButtonId());
                    System.out.println(rb2.getText().toString());
                    String answer2 = rb2.getText().toString();
                    postedAnswers.add(answer2);

                    RadioButton rb3 = (RadioButton) findViewById(rg3.getCheckedRadioButtonId());
                    System.out.println(rb3.getText().toString());
                    String answer3 = rb3.getText().toString();
                    postedAnswers.add(answer3);

                    RadioButton rb4 = (RadioButton) findViewById(rg4.getCheckedRadioButtonId());
                    System.out.println(rb4.getText().toString());
                    String answer4 = rb4.getText().toString();
                    postedAnswers.add(answer4);

                    RadioButton rb5 = (RadioButton) findViewById(rg5.getCheckedRadioButtonId());
                    System.out.println(rb5.getText().toString());
                    String answer5 = rb5.getText().toString();
                    postedAnswers.add(answer5);

                    int grade = 0;
                    for (int k = 0; k < 5; k++) {
                        String correctAnswerID = arrayAnswers.get(k);
                        //System.out.println(correctAnswerID);
                        String answerChosen = postedAnswers.get(k);
                        switch (correctAnswerID) {
                            case "1":
                                String option1 = arrayOption1.get(k);
                                //System.out.println(option1);
                                if (option1 == answerChosen) {
                                    grade++;
                                }
                                break;
                            case "2":
                                String option2 = arrayOption2.get(k);
                                //System.out.println(option2);
                                if (option2 == answerChosen) {
                                    grade++;
                                }
                                break;
                            case "3":
                                String option3 = arrayOption3.get(k);
                                //System.out.println(option3);
                                if (option3 == answerChosen) {
                                    grade++;
                                }
                                break;
                            case "4":
                                String option4 = arrayOption4.get(k);
                                //System.out.println(option4);
                                if (option4 == answerChosen) {
                                    grade++;
                                }
                                break;

                        }
                    }
                    System.out.println("Grade" + grade);

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setView(promptsView);

                    final TextView userInput = (TextView) promptsView.findViewById(R.id.textView1);
                    userInput.setText("Your have scored "+grade+"/5 in this Challenge!!");

                    int cScore = 0;
                    String level = "";

                    //Set Scrore in DB for Progress Tracking
                    try{
                        SQLiteDatabase llDB = (quizContent.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                        llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer)");
                        Cursor c = llDB.rawQuery("SELECT * FROM progress where game_name = 'quiz'", null);
                        int cScoreIndex = c.getColumnIndex("cScore");
                        if (c.moveToFirst()) {
                            System.out.println("Quiz Score exists");
                            c.moveToFirst();
                            while(c != null)
                            {
                                cScore = c.getInt(cScoreIndex);
                                c.moveToNext();
                            }
                            c.close();
                        } else {
                            System.out.println("Error");
                        }

                    }catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                    System.out.println("Old Score: "+cScore);

                    cScore = cScore + grade;

                    if(cScore >= 100)
                    {
                        cScore = 100;
                    }


                    if(cScore < 5)
                    {
                        level = "Level 1";
                    }
                    else if (cScore >= 5 || cScore < 10)
                    {
                        level = "Level 2";
                    }
                    else if (cScore >= 10 || cScore < 15)
                    {
                        level = "Level 3";
                    }
                    else if (cScore >= 15 || cScore < 20)
                    {
                        level = "Level 4";
                    }
                    else if (cScore >= 20 || cScore < 25)
                    {
                        level = "Level 5";
                    }
                    else if (cScore >= 25 || cScore < 30)
                    {
                        level = "Level 6";
                    }
                    else if (cScore >= 30 || cScore < 35)
                    {
                        level = "Level 7";
                    }
                    else if (cScore >= 35 || cScore < 40)
                    {
                        level = "Level 8";
                    }
                    else if (cScore >= 40 || cScore < 45)
                    {
                        level = "Level 9";
                    }
                    else if (cScore >= 45 || cScore < 50)
                    {
                        level = "Level 10";
                    }
                    else if (cScore >= 50 || cScore < 55)
                    {
                        level = "Level 11";
                    }
                    else if (cScore >= 55 || cScore < 60)
                    {
                        level = "Level 12";
                    }
                    else if (cScore >= 60 || cScore < 65)
                    {
                        level = "Level 13";
                    }
                    else if (cScore >= 65 || cScore < 70)
                    {
                        level = "Level 14";
                    }
                    else if (cScore >= 70 || cScore < 75)
                    {
                        level = "Level 15";
                    }
                    else if (cScore >= 75 || cScore < 80)
                    {
                        level = "Level 16";
                    }
                    else if (cScore >= 80 || cScore < 85)
                    {
                        level = "Level 17";
                    }
                    else if (cScore >= 85 || cScore < 90)
                    {
                        level = "Level 18";
                    }
                    else if (cScore >= 90 || cScore < 95)
                    {
                        level = "Level 19";
                    }
                    else if (cScore >= 95 || cScore <= 100)
                    {
                        level = "Level 20";
                    }

                    System.out.println("Currenet Level : "+level);

                    try{
                        SQLiteDatabase llDB = (quizContent.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                        System.out.println("update quiz cScore");
                        llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer)");
                        llDB.execSQL("UPDATE progress set cScore = '"+cScore+"' where game_name = 'quiz'");
                    }catch(Exception e)
                    {
                        e.printStackTrace();
                    }

                    try{
                        SQLiteDatabase llDB = (quizContent.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                        llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer)");
                        Cursor c = llDB.rawQuery("SELECT * FROM progress where game_name = 'quiz'", null);
                        int cScoreIndex = c.getColumnIndex("cScore");
                        if (c.moveToFirst()) {
                            System.out.println("Quiz Score exists");
                            c.moveToFirst();
                            while(c != null)
                            {
                                cScore = c.getInt(cScoreIndex);
                                c.moveToNext();
                            }
                            c.close();
                        } else {
                            System.out.println("Error");
                        }

                    }catch(Exception e)
                    {
                        e.printStackTrace();
                    }

                    System.out.println("New Score: "+cScore);


                    //Alert dialog displayed after submitting quiz. Contains Grade and options to navigate
                    alertDialogBuilder
                            .setMessage("You did it!")
                            .setCancelable(false)
                            .setPositiveButton("Try more Quizzes",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    startActivity(new Intent(getApplicationContext(), quizList.class));
                                    finish();
                                }
                            })
                            .setNegativeButton("Try this Quiz Again",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    Intent intent = new Intent(getBaseContext(), quizContent.class);
                                    intent.putExtra("quizName", s);
                                    startActivity(intent);
                                    finish();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();


                }

            }

        });
    }

    //Back Button routine
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exit Challenge")
                .setMessage("Are you sure you want to exit this challenge?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        mCountDownTimer.cancel();
                        startActivity(new Intent(getApplicationContext(), quizList.class));
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }
}
