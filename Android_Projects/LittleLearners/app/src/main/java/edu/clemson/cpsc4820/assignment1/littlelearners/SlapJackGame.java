package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import java.util.Random;

//Slapjack Game Logic
public class SlapJackGame extends AppCompatActivity {
    private MediaPlayer slapsound;
    static int incorrect = 0;
    static int number = 0;
    private int correct = 0;
    private int numtapsneeded = 0;
    private long startTime = 0L;
    private int count = 0;
    private int numberOfTaps = 0;
    private int mode = 0;
    private long touchDownMs = 0L;
    private Runnable updateTaskTimer;
    private Handler updateTimeHandler;
    private long timeInMilliseconds = 0L;
    private TextView taps;
    private long maxTime = 5000L;
    public void restart()
    {
        if(count < 9) {
            count++;
            Intent intent = new Intent(this, SlapJackGame.class);
            intent.putExtra("count", count);
            intent.putExtra("correct",correct);
            intent.putExtra("incorrect",incorrect);
            intent.putExtra("slapmode", mode);
            slapsound.start();
            updateTimeHandler.removeCallbacksAndMessages(getApplicationContext());
            startActivity(intent);
            finish();
        }
        else
        {
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View promptView = layoutInflater.inflate(R.layout.slapjacksummary, null);

            final AlertDialog alertD = new AlertDialog.Builder(this).create();
            TextView right = (TextView)promptView.findViewById(R.id.rightsj);
            TextView wrong = (TextView)promptView.findViewById(R.id.wrongsj);
            String temp = Integer.toString(correct);
            String temp1 = Integer.toString(incorrect);
            right.setText(temp);
            wrong.setText(temp1);
            Button btnAdd1 = (Button) promptView.findViewById(R.id.playagain);

            Button btnAdd2 = (Button) promptView.findViewById(R.id.exitsk);

            btnAdd1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    alertD.dismiss();
                    Intent intent = new Intent(getApplicationContext(), SlapJackGame.class);
                    intent.putExtra("slapmode",mode);
                    startActivity(intent);
                    finish();


                }
            });

            btnAdd2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    alertD.dismiss();
                    Intent intent = new Intent(getApplicationContext(), SlapJackSplashScreen.class);
                    startActivity(intent);
                    finish();

                }
            });

            alertD.setView(promptView);
            alertD.show();

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slap_jack_game);
        slapsound = MediaPlayer.create(this, R.raw.slapsound);
        slapsound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();

            }});
        count = getIntent().getIntExtra("count", 0);
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.progressBarsj);
        correct = getIntent().getIntExtra("correct", 0);
        progressBar.setMax(9);
        progressBar.getProgressDrawable().setColorFilter(
                Color.BLACK, android.graphics.PorterDuff.Mode.SRC_IN);
        progressBar.setProgress(count);
        taps = (TextView)findViewById(R.id.taps);

        incorrect = getIntent().getIntExtra("incorrect", 0);
        mode = getIntent().getIntExtra("slapmode", 0);
        final TextView view = (TextView)findViewById(R.id.slapview);
        final TextView view2 = (TextView)findViewById(R.id.slaptext2);
        String temp = "";
        switch(mode)
        {

            case 1:
                temp = "Even/Odd Mode:\n" +
                        " Even = 2 Taps, Odd = 1 Tap";
                view2.setText(temp);
                maxTime = 2000L;
                break;
            case 2:
                temp = "Multiples Mode:\n" +
                        " Multiples of 3 = 3 Taps, 4 = 4 , 5 = 5";
                view2.setText(temp);
                maxTime = 4000L;
                break;
            case 3:
                temp = "Prime Mode:\n " +
                        "Prime Numbers = 2 Taps, NOT Prime = 1 Tap";
                view2.setText(temp);
                maxTime = 3000L;
                break;
        }

        numtapsneeded = 0;
        newNumber();
        System.out.println(numtapsneeded);
        updateTimeHandler = new Handler();
        temp = Integer.toString(number);
        view.setText(temp);


        startTime = System.currentTimeMillis();
        updateTaskTimer = new Runnable() {

            public void run() {

                timeInMilliseconds = System.currentTimeMillis() - startTime;

                if(timeInMilliseconds > maxTime)
                {
                    updateTimeHandler.removeCallbacksAndMessages(this);
                    if(numberOfTaps == numtapsneeded)
                    {
                        correct++;
                    }
                    else{
                        incorrect++;
                    }
                    restart();
                }
                else
                {
                    view.setOnTouchListener(new View.OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {

                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN:
                                    touchDownMs = System.currentTimeMillis();
                                    break;
                                case MotionEvent.ACTION_UP:
                                    if ((System.currentTimeMillis() - touchDownMs) < ViewConfiguration.getTapTimeout()) {
                                        //lastTapTimeMs = System.currentTimeMillis();
                                        numberOfTaps++;
                                        taps.setText("" + numberOfTaps);
                                        System.out.println("tap");
                                    }
                                    break;
                            }
                            return true;
                        }
                    });
                    updateTimeHandler.postDelayed(this, 0);
                }
            }
        };
        updateTimeHandler.postDelayed(updateTaskTimer, 100);



    }


    boolean isPrime(int n) {
        for(int i=2;2*i<n;i++) {
            if(n%i==0)
                return false;
        }
        return true;
    }

    //Random New Number
    private void newNumber() {
        Random random = new Random();
        while(numtapsneeded == 0) {
            number = random.nextInt(100) + 1;
            switch (mode) {
                case 1:
                    if (number % 2 == 1) {
                        numtapsneeded = 1;
                    } else {
                        numtapsneeded = 2;
                    }
                    break;
                case 2:
                    if (number % 3 == 0) {
                        numtapsneeded = 3;
                    }
                    if (number % 4 == 0) {
                        numtapsneeded = 4;
                    }
                    if (number % 5 == 0) {
                        numtapsneeded = 5;
                    }
                    if(number % 4 ==0 && number % 5 == 0)
                    {
                        numtapsneeded = 0;
                    }
                    if(number % 3 == 0 && number % 4 == 0)
                    {
                        numtapsneeded = 0;
                    }
                    if(number % 3 ==0 && number % 5 == 0)
                    {
                        numtapsneeded = 0;
                    }
                    break;
                case 3:
                    Random prime = new Random();
                    int temp = prime.nextInt(9);
                    if(temp % 2 == 0)
                    {
                        while(!isPrime(number))
                        {
                            number = random.nextInt(100) + 1;
                        }
                        numtapsneeded = 2;
                    }
                    else {
                        if (isPrime(number)) {
                            numtapsneeded = 2;
                        } else {
                            numtapsneeded = 1;
                        }
                    }
                    break;
            }
        }
    }

    //Handle Back Button
    @Override
    public void onBackPressed()
    {
        /*updateTimeHandler.removeCallbacksAndMessages(getApplicationContext());
        Intent intent = new Intent(this, SlapJackSplashScreen.class);
        startActivity(intent);
        finish();*/
    }
}
