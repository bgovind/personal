package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;

import android.speech.tts.TextToSpeech;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;

import static edu.clemson.cpsc4820.assignment1.littlelearners.flashCardSetUp.myQuestions;
import static edu.clemson.cpsc4820.assignment1.littlelearners.R.id.listenButton;


//FlashCards Game Logic and Screen Layout
public class flashCardGame extends AppCompatActivity {

    private ArrayList<String> keys;
    private TextView question;
    private TextView answer;
    private String questionNow="";
    private String answerNow="";
    private boolean front;
    private int count = 0;
    private AnimatorSet mSetRightOut;
    private AnimatorSet mSetLeftIn;
    private boolean mIsBackVisible = false;
    private View mCardFrontLayout;
    private View mCardBackLayout;
    private boolean start = false;
    private int choice = 0;
    private ArrayList<spanishWordTable> wordList;
    private static TextToSpeech t2speach;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flashcardgame);
        choice = getIntent().getIntExtra("wordlistchoice", 4);
        findViews();
        loadAnimations();
        changeCameraDistance();
        wordList = new ArrayList<>();
        final Locale locale = new Locale("spa", "ESP");
        progressBar = (ProgressBar)findViewById(R.id.progressfc);

        t2speach = new TextToSpeech(flashCardGame.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {

            }
        });


        question = (TextView) findViewById(R.id.frontTV);
        question.setText(" ");

        answer = (TextView) findViewById(R.id.backTV);
        answer.setText(" ");

        //Choice Cases
        switch (choice) {
            case 1: {
                BufferedReader readWords = new BufferedReader(new InputStreamReader(this.getResources().openRawResource(R.raw.spanisharverbs)));
                String line;
                String english;
                String spanish;
                try {
                    while((line = readWords.readLine())!= null)
                    {
                        String[]parts = line.split("\t",2);
                        parts[0] = parts[0].trim();
                        parts[1] = parts[1].trim();
                        spanish = parts[0];
                        english = parts[1];
                        spanishWordTable temp = new spanishWordTable(english,spanish);
                        wordList.add(temp);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                question.setText("Click next to start");
                front = true;
                break;
            }
            case 2: {
                BufferedReader readWords = new BufferedReader(new InputStreamReader(this.getResources().openRawResource(R.raw.spanisherverbs)));
                String line;
                String english;
                String spanish;
                try {
                    while((line = readWords.readLine())!= null)
                    {
                        String[]parts = line.split("\t",2);
                        parts[0] = parts[0].trim();
                        parts[1] = parts[1].trim();
                        spanish = parts[0];
                        english = parts[1];
                        spanishWordTable temp = new spanishWordTable(english,spanish);
                        wordList.add(temp);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                question.setText("Click next to start");
                front = true;
                break;
            }
            case 3: {
                BufferedReader readWords = new BufferedReader(new InputStreamReader(this.getResources().openRawResource(R.raw.spanishirverbs)));
                String line;
                String english;
                String spanish;
                try {
                    while((line = readWords.readLine())!= null)
                    {
                        String[]parts = line.split("\t",2);
                        parts[0] = parts[0].trim();
                        parts[1] = parts[1].trim();
                        spanish = parts[0];
                        english = parts[1];
                        spanishWordTable temp = new spanishWordTable(english,spanish);
                        wordList.add(temp);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                question.setText("Click next to start");
                front = true;
                break;
            }
            case 4: {
                HashMap<String,String> temp =  (HashMap<String, String>) getIntent().getSerializableExtra("wordlist");
                if (temp == null) {
                    int count = myQuestions.size();
                    if(count == 0)
                    {
                        question.setText("Add words to the list before you begin!");
                    }
                    else {
                        keys = new ArrayList<>();
                        while (count > 0) {
                            String key = (String) myQuestions.keySet().toArray()[count - 1];
                            keys.add(key);
                            count -= 1;
                            Log.i("keys in quiz", keys.toString());
                            front = true;
                            question.setText("Click next to start!");
                        }
                    }
                }
                else {
                    question.setText("Click Next to start.");
                    myQuestions.putAll(temp);
                    front = true;
                }
            }
        }
        Button speak = (Button) findViewById(listenButton);
        speak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (front) {
                    t2speach.setLanguage(Locale.US);
                    t2speach.speak(questionNow, TextToSpeech.QUEUE_FLUSH, null);

                } else {
                    t2speach.setLanguage(locale);
                    t2speach.speak(answerNow, TextToSpeech.QUEUE_FLUSH, null);

                }
            }
        });
        progressBar.setMax(wordList.size());
        progressBar.setProgress(count);
    }

    //from from front to back or back to front
    public void flipCardClickFunction(View view) {
        if (front) {
            flipCard(view);
            front = false;
        } else {
            flipCard(view);
            front = true;
        }

    }

    //switch cards
    public void nextCardClickFunction(View view) {

        int size;

        switch(choice)
        {
            case 1: {
                Collections.shuffle(wordList);
                size = wordList.size();
                if(size > 0) {
                    questionNow = wordList.get(size - 1).getEnglishWord();
                    answerNow = wordList.get(size - 1).getSpanishWord();
                    wordList.remove(size - 1);

                    if (!front) {
                        flipCard(view);
                        front = true;
                    }
                    question.setText(questionNow);
                }
                else
                {
                    question.setText("Quiz over.");
                    startOver();
                }
                    break;
            }
            case 2: {
                Collections.shuffle(wordList);
                size = wordList.size();
                if(size > 0) {
                    questionNow = wordList.get(size - 1).getEnglishWord();
                    answerNow = wordList.get(size - 1).getSpanishWord();
                    wordList.remove(size - 1);

                    if (!front) {
                        flipCard(view);
                        front = true;
                    }
                    question.setText(questionNow);
                }
                else
                {
                    question.setText("Quiz over.");
                    startOver();
                }
                break;
            }
            case 3: {
                Collections.shuffle(wordList);
                size = wordList.size();
                if(size > 0) {
                    questionNow = wordList.get(size - 1).getEnglishWord();
                    answerNow = wordList.get(size - 1).getSpanishWord();
                    wordList.remove(size - 1);

                    if (!front) {
                        flipCard(view);
                        front = true;
                    }
                    question.setText(questionNow);
                }
                else
                {
                    question.setText("Quiz over.");
                    startOver();
                }
                break;
            }
            case 4: {
                Collections.shuffle(keys);
                size = keys.size();

                if (size>0) {
                    questionNow = keys.get(size - 1);
                    answerNow = myQuestions.get(questionNow);


                    keys.remove(size - 1);
                    if(!front)
                    {
                        flipCard(view);
                        front = true;
                    }
                    question.setText(questionNow);

                } else {
                    question.setText("Quiz over.");
                    startOver();
                }
                progressBar.setMax(myQuestions.size());
                break;
            }
        }
        count++;
        progressBar.setProgress(count);
    }

    public void startOver() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(flashCardGame.this);
        dialogBuilder.setTitle("Quiz is Over");
        dialogBuilder.setMessage("Would you like to select another quiz?");
        //Confirm Dialog to comfirm deletion.
        dialogBuilder.setPositiveButton("Yes, new Quiz.", new DialogInterface.OnClickListener() {
            /** Prompt the user for deletion. **/
            @Override
            public void onClick(DialogInterface dialog, int which) {
                restart();
                }

        });
        //Cancel Button
        dialogBuilder.setNegativeButton("No, I am done.", new DialogInterface.OnClickListener() {
            /**
             * Prompt the user for deletion.
             **/
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), flashCardSplashScreen.class);
                startActivity(intent);
                finish();
            }
        });
        dialogBuilder.show();
    }

    public void onBackPressed()
    {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.exitfcdialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(this).create();

        Button btnAdd1 = (Button) promptView.findViewById(R.id.btnYes);

        Button btnAdd2 = (Button) promptView.findViewById(R.id.btnNo);

        btnAdd1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertD.dismiss();
                Intent intent = new Intent(getApplicationContext(), flashCardSplashScreen.class);
                intent.putExtra("wordlist", myQuestions);
                startActivity(intent);
                t2speach.shutdown();
                finish();


            }
        });

        btnAdd2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                alertD.dismiss();

            }
        });

        alertD.setView(promptView);

        alertD.show();

    }


    private void changeCameraDistance() {
        int distance = 8000;
        float scale = getResources().getDisplayMetrics().density * distance;
        mCardFrontLayout.setCameraDistance(scale);
        mCardBackLayout.setCameraDistance(scale);
    }

    private void loadAnimations() {
        mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.out_animation);
        mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.in_animation);
    }

    private void findViews() {
        mCardBackLayout = findViewById(R.id.card_back);
        mCardFrontLayout = findViewById(R.id.card_front);
    }

    //Flip Cards
    public void flipCard(View view) {

        MediaPlayer mediaPlayer = MediaPlayer.create(this,R.raw.fcflip);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();

            }});
        mediaPlayer.start();
        if (!mIsBackVisible) {
                mSetRightOut.setTarget(mCardFrontLayout);
                mSetLeftIn.setTarget(mCardBackLayout);
                mSetRightOut.start();
                mSetLeftIn.start();
                answer.setText(answerNow);
                mIsBackVisible = true;
                front = false;

        } else {
            mSetRightOut.setTarget(mCardBackLayout);
            mSetLeftIn.setTarget(mCardFrontLayout);
            mSetRightOut.start();
            mSetLeftIn.start();
            question.setText(questionNow);
            mIsBackVisible = false;
            front = true;
        }
    }

    //Restart Activity
    public void restart()
    {
        Intent intent = new Intent(this, flashcardmode.class);
        intent.putExtra("wordlist", myQuestions);
        startActivity(intent);
        finish();
    }

}
