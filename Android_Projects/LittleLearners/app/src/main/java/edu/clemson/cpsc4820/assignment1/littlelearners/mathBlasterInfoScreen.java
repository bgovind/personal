package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

//Info Screen for Math Blaster
public class mathBlasterInfoScreen extends AppCompatActivity {

    @Override
    public void onBackPressed()
    {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_math_blaster_info_screen);
    }
}
