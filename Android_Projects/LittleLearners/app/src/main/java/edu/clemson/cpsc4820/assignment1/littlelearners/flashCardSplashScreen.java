package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//Flash Card Splash Screen
public class flashCardSplashScreen extends AppCompatActivity {

    //Play Game
    public void playFlashCards()
    {
        Intent intent = new Intent(this, flashcardmode.class);
        startActivity(intent);
        finish();

    }

    //Info Flash Cards
    public void flashCardInfo()
    {
        Intent intent = new Intent(this, flashCardInfoScreen.class);
        startActivity(intent);
        finish();

    }

    //Handle BackButton
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, mainMenuGameSelect.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flashcardsplashscreen);

        Button start = (Button)findViewById(R.id.fcplay);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playFlashCards();

            }
        });
        Button info = (Button)findViewById(R.id.fcInfo);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               flashCardInfo();

            }
        });
    }
}
