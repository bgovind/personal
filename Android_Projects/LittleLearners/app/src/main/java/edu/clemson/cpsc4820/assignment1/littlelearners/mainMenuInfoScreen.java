package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

//Info Screen
public class mainMenuInfoScreen extends AppCompatActivity {

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, HomeScreen.class);
        startActivity(intent);
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainmenuinfoscreen);
    }
}
