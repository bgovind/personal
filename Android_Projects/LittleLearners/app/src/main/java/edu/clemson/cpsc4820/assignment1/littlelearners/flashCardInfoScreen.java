package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


//Flash Cards Infor Screen
public class flashCardInfoScreen extends AppCompatActivity {

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, flashCardSplashScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flashcardinfoscreen);

        TextView info = (TextView)findViewById(R.id.infotextView);
        info.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        info.setTextSize(18f);
        info.setText("\n Flash cards was designed to help you master the spanish language." +
                " You will find pre-loaded libraries of spanish verbs sorted by ending." +
                " If you have successfully mastered the verb libraries, you may also" +
                " create your own vocabulary list within the app itself." +
                " To create your own vocab list, choose the custom list option." +
                " You will then have the option to add words to the list." +
                " To change or delete words in the list, click on the word." +
                " Enjoy!");
    }
}
