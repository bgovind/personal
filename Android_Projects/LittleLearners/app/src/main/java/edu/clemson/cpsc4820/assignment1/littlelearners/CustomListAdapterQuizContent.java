package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Bharat Kumar on 4/6/2017.
 */

//Helper class for Custom Adapter for QuizList
public class CustomListAdapterQuizContent extends ArrayAdapter<String> {

    private final Activity context;
    private ArrayList<String> arrayQuestion = new ArrayList<>();
    private ArrayList<String> arrayOption1 = new ArrayList<>();
    private ArrayList<String> arrayOption2 = new ArrayList<>();
    private ArrayList<String> arrayOption3 = new ArrayList<>();
    private ArrayList<String> arrayOption4 = new ArrayList<>();
    private ArrayList<String> arrayAnswer = new ArrayList<>();


    public CustomListAdapterQuizContent(Activity context, ArrayList<String> arrayQuestion, ArrayList<String> arrayOption1, ArrayList<String> arrayOption2, ArrayList<String> arrayOption3, ArrayList<String> arrayOption4, ArrayList<String> arrayAnswer)
    {
        super(context, R.layout.quizcontentlist, arrayQuestion);

        this.context=context;
        this.arrayQuestion=arrayQuestion;
        this.arrayOption1= arrayOption1;
        this.arrayOption2= arrayOption2;
        this.arrayOption3= arrayOption3;
        this.arrayOption4= arrayOption4;
        this.arrayAnswer= arrayAnswer;

    }

    public View getView(int position, View v, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.quizcontentlist, null, true);
        RecyclerView.ViewHolder holder;

        if(rowView == null) {

            TextView question = (TextView) rowView.findViewById(R.id.question);
            RadioButton op1 = (RadioButton) rowView.findViewById(R.id.option1);
            RadioButton op2 = (RadioButton) rowView.findViewById(R.id.option2);
            RadioButton op3 = (RadioButton) rowView.findViewById(R.id.option3);
            RadioButton op4 = (RadioButton) rowView.findViewById(R.id.option4);

            question.setText(arrayQuestion.get(position).toString());
            op1.setText(arrayOption1.get(position).toString());
            op2.setText(arrayOption2.get(position).toString());
            op3.setText(arrayOption3.get(position).toString());
            op4.setText(arrayOption4.get(position).toString());
        }else
        {
            holder = (RecyclerView.ViewHolder)rowView.getTag();
        }








        return rowView;
    }
}
