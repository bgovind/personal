package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

//Info for Slap Jack
public class slapinfo extends AppCompatActivity {

    private MediaPlayer slapsound;
    private Intent intent;

    @Override
    public void onBackPressed()
    {
        intent = new Intent(this, SlapJackSplashScreen.class);
        startActivity(intent);
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slapinfo);
        TextView textView = (TextView)findViewById(R.id.slapinfotext);
        String slapinfo = "Welcome to Slap Jack.  There are 3 modes to choose from while playing this game." +
                " First you can choose multiples mode where you will be tested on your knowledge on multiples" +
                " of 3, 4, and 5.  This app will not give you numbers that are multiples of 3 and 4 or 3 and 5 or 4 and 5." +
                "  So there is only ever 1 Choice.  You will have to tap the screen the corresponding amount of times in order to " +
                " input your answer.  For example if the number is 9, that is a multiple of 3, so I will tap the screen 3 times." +
                "  Also there is Prime mode, where you will be tested on prime numbers.  If the number is prime, tap the screen 2 times," +
                " and if it is not, tap the screen 1 time.  Lastly there is Even/Odd mode.  If the number is even tap the screen 2 times, " +
                " and if it is odd, tap the screen 1 time.  These instructions will be displayed in their cooresponding game modes as a reminder. " +
                "  Enjoy!";
        textView.setText(slapinfo);

        slapsound = MediaPlayer.create(this, R.raw.slapsound);
        slapsound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();

            }});
    }
}
