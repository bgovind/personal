package edu.clemson.cpsc4820.assignment1.littlelearners;

/**
 * Created by gfranci on 3/13/2017.
 */


//Helper class for Managing Word List
public class words {

    private int id;
    private String word;
    private int isDeleted;
    private String lastSync;


    words(int id, String word, int isDeleted, String lastSync) {
        this.id = id;
        this.word = word;
        this.isDeleted = isDeleted;
        this.lastSync = lastSync;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getId() {
        return id;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }


    public String getLastSync() {
        return lastSync;
    }

    public void setLastSync(String lastSync) {
        this.lastSync = lastSync;
    }
}








