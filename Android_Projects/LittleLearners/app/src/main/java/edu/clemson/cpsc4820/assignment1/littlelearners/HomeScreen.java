package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//Little Learner HomeScreen
public class HomeScreen extends AppCompatActivity {

    /*private ArrayList<String> arrayQuizNames = new ArrayList<>();
    private ArrayList<String> arrayQuizPosted = new ArrayList<>();
    private ArrayList<String> arrayquizPostedOn = new ArrayList<>();*/

    //ListView feedList;

    private RequestQueue requestQueue;

    private static final String URL = "https://people.cs.clemson.edu/~bgovind/ll/allquizlist.php";

    private StringRequest request;


    @Override
    public void onBackPressed() {

        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.exitgamecustomdialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(this).create();

        Button btnAdd1 = (Button) promptView.findViewById(R.id.btnYes);

        Button btnAdd2 = (Button) promptView.findViewById(R.id.btnNo);

        btnAdd1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertD.dismiss();
                Intent a = new Intent(Intent.ACTION_MAIN);
                a.addCategory(Intent.CATEGORY_HOME);
                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);


            }
        });

        btnAdd2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                alertD.dismiss();

            }
        });

        alertD.setView(promptView);

        alertD.show();

    }

    //function to run info activity
    public void info() {
        Intent intent = new Intent(this, mainMenuInfoScreen.class);
        startActivity(intent);
    }

    //function to run the game select activity
    public void gameSelect() {
        Intent intent = new Intent(this, mainMenuGameSelect.class);
        startActivity(intent);
    }

    //function to run the progress monitoring activity
    public void progress() {
        Intent intent = new Intent(this, progresshome.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homescreen);

        //set info button active
        Button infoButton = (Button) findViewById(R.id.infobutton);
        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                info();
            }


        });

        //set progress button active
        Button progressButton = (Button) findViewById(R.id.progressbutton);
        progressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress();
            }


        });

        //set play button active
        Button playButton = (Button) findViewById(R.id.playbutton);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameSelect();
                finish();
            }
        });

        requestQueue = Volley.newRequestQueue(this);

        //Setup and initialize DB when using for first time by getting response from Server
        try {
            request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        System.out.println("login feeds inserting into local");

                        SQLiteDatabase llDB = (HomeScreen.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                        llDB.execSQL("CREATE TABLE IF NOT EXISTS game_list(id INTEGER PRIMARY KEY, game_name varchar(20))");

                        Cursor create = llDB.rawQuery("SELECT * FROM game_list", null);
                        if (create.getCount() > 0) {
                            System.out.println("game list data exists true");
                        }
                        else{
                            System.out.println("creating game list table");
                            llDB.execSQL("INSERT INTO game_list(game_name) VALUES ('quiz')");
                            llDB.execSQL("INSERT INTO game_list(game_name) VALUES ('stickman')");
                            llDB.execSQL("INSERT INTO game_list(game_name) VALUES ('math')");
                        }


                        llDB.execSQL("CREATE TABLE IF NOT EXISTS quizlist(id INTEGER PRIMARY KEY, name varchar(50), postedby varchar(50), postedon text)");
                        llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer, foreign key (game_name) references game_list(game_name))");
                        Cursor c = llDB.rawQuery("SELECT * FROM progress", null);
                        if (c.getCount() > 0) {
                            System.out.println("progress data exists true");
                        }
                        else{
                            System.out.println("creating progress table");
                            llDB.execSQL("INSERT INTO progress(game_name, cScore) VALUES ('quiz', '0')");
                            llDB.execSQL("INSERT INTO progress(game_name, cScore) VALUES ('stickman', '0')");
                            llDB.execSQL("INSERT INTO progress(game_name, cScore) VALUES ('math', '0')");
                        }

                        llDB.execSQL("DELETE FROM quizlist");

                        JSONObject outer = new JSONObject(response);
                        Iterator<String> keys = outer.keys();
                        while (keys.hasNext())
                        {
                            String key = keys.next();
                            JSONObject inside = outer.getJSONObject(key);

                            if (inside.names().get(0).equals("success"))
                            {

                                String quizName = inside.getString("quizName");
                                String quizPostedBy = inside.getString("posted_by");
                                String quizPostedOn = inside.getString("posted_on");

                                try{

                                    llDB.execSQL("INSERT INTO quizlist(name, postedby, postedon) VALUES ('" + quizName + "', '" + quizPostedBy + "','" + quizPostedOn + "')");
                                    System.out.println(quizName);
                                    System.out.println(quizPostedBy);
                                    System.out.println(quizPostedOn);


                                    /*arrayQuizNames.add(quizName);
                                    arrayQuizPosted.add(quizPostedBy);
                                    arrayquizPostedOn.add(quizPostedOn);*/

                                }catch(Exception e)
                                {
                                    e.printStackTrace();
                                }

                            }
                            else if (inside.names().get(0).equals("fail")) {
                                //Toast.makeText(HomeScreen.this, "", Toast.LENGTH_SHORT).show();

                            }
                        }

                        //Toast.makeText(HomeScreen.this, "Welcome!", Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> hashMap = new HashMap<>();

                    String authorkey = "2017";
                    hashMap.put("authorkey", authorkey);

                    return hashMap;
                }
            };

            requestQueue.add(request);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}