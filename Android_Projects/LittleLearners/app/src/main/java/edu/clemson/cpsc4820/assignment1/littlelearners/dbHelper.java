package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by gfranci on 3/13/2017.
 */

//Helper class for Accessing DB for Stickman
public class dbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS wordTable (id INTEGER PRIMARY KEY, word VARCHAR(20)UNIQUE, isdeleted INT DEFAULT 0, lastsync TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";
    public static final String DROP_TABLE = "DROP TABLE IF EXISTS wordTable";
    public static final String TABLE_NAME ="wordTable";
    public static final String ID = "id";
    public static final String WORD ="word";
    public static final String IS_DELETED ="isdeleted";
    public static final String LAST_SYNC ="lastsync";
    ArrayList<words> wordList;
    public dbHelper(Context context)
    {
        super(context,wordDB.DATABASE_NAME,null,DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }


    public void saveToLocalDB(String word) {

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        contentValues.put("word", word);

        //db.execSQL("INSERT INTO wordTable word VALUES('word')");
        db.insert(TABLE_NAME, null, contentValues);


        db.close();

    }

    public Cursor readFromLocalDB(SQLiteDatabase db)
    {
        String[] projection = {ID, WORD, IS_DELETED, LAST_SYNC};
        return (db.query(TABLE_NAME, projection,null,null,null,null,null));
    }

    public void updateLocalDB(int id, String word, int isdeleted, String lastsync, SQLiteDatabase db)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(LAST_SYNC,lastsync);
        String selection = WORD + "LIKE ?";
        String[] selectionArgs = {word};
        db.update(TABLE_NAME,contentValues,selection,selectionArgs);
        db.close();
    }

    public ArrayList<HashMap<String, String>> getAllUsers() {
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT  * FROM wordTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", cursor.getString(0));
                map.put("word", cursor.getString(1));
                map.put("isdeleted", cursor.getString(2));
                map.put("lastsync", cursor.getString(3));
                wordList.add(map);
            } while (cursor.moveToNext());
        }
        database.close();
        return wordList;
    }


}
