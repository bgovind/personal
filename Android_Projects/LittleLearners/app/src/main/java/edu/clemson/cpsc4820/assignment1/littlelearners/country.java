package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

//Geo App Country screen Layout
public class country extends AppCompatActivity {


    @Override
    protected void onPause() {
        super.onPause();
        MusicManager.pause();

    }
    @Override
    protected void onResume() {
        super.onResume();
        MusicManager.start(this, 2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        MusicManager.start(this,2);
        Bundle extras = getIntent().getExtras();
        String continent = extras.getString("continent");
        String country = extras.getString("country");
        String capital = extras.getString("capital");
        String currency = extras.getString("currency");
        String region = extras.getString("region");



        System.out.println(country);
        System.out.println(capital);
        System.out.println(currency);
        System.out.println(region);

        TextView countryName = (TextView) findViewById(R.id.country);
        countryName.setText(country);

        ImageView countryImage= (ImageView) findViewById(R.id.countryimage);
        ImageView countryFlag= (ImageView) findViewById(R.id.flagimage);

        //Set screen content based on country
        switch(country)
        {
            case "India":
                countryImage.setImageResource(R.drawable.india);
                countryFlag.setImageResource(R.drawable.indiaflag);
                break;
            case "Afghanistan":
                countryImage.setImageResource(R.drawable.afghanistan);
                countryFlag.setImageResource(R.drawable.afghanistanflag);
                break;
            case "China":
                countryImage.setImageResource(R.drawable.china);
                countryFlag.setImageResource(R.drawable.chinaflag);
                break;
            case "Indonesia":
                countryImage.setImageResource(R.drawable.indonesia);
                countryFlag.setImageResource(R.drawable.indonesiaflag);
                break;
            case "Iran":
                countryImage.setImageResource(R.drawable.iran);
                countryFlag.setImageResource(R.drawable.iranflag);
                break;
            case "Japan":
                countryImage.setImageResource(R.drawable.japan);
                countryFlag.setImageResource(R.drawable.japanflag);
                break;
            case "Kazakhstan":
                countryImage.setImageResource(R.drawable.kazhakstan);
                countryFlag.setImageResource(R.drawable.kazhakflag);
                break;
            case "Mongolia":
                countryImage.setImageResource(R.drawable.mongolia);
                countryFlag.setImageResource(R.drawable.mongoliaflag);
                break;
            case "Pakistan":
                countryImage.setImageResource(R.drawable.pakistan);
                countryFlag.setImageResource(R.drawable.pakistanflag);
                break;
            case  "Philippines":
                countryImage.setImageResource(R.drawable.phily);
                countryFlag.setImageResource(R.drawable.phillyflag);
                break;
            case "Saudi Arabia":
                countryImage.setImageResource(R.drawable.saudi);
                countryFlag.setImageResource(R.drawable.saudiflag);
                break;
            case "South Korea":
                countryImage.setImageResource(R.drawable.southkorea);
                countryFlag.setImageResource(R.drawable.southkoreaflag);
                break;
            case "Turkey":
                countryImage.setImageResource(R.drawable.turkey);
                countryFlag.setImageResource(R.drawable.turkeyflag);
                break;
            case "Ukraine":
                countryImage.setImageResource(R.drawable.ukraine);
                countryFlag.setImageResource(R.drawable.ukraineflag);
                break;
            case "Democratic Republic of the Congo":
                countryImage.setImageResource(R.drawable.congo);
                countryFlag.setImageResource(R.drawable.congoflag);
                break;
            case "Egypt":
                countryImage.setImageResource(R.drawable.egypt);
                countryFlag.setImageResource(R.drawable.egyptflag);
                break;
            case "Ethiopia":
                countryImage.setImageResource(R.drawable.ethiopia);
                countryFlag.setImageResource(R.drawable.ethiopiaflag);
                break;
            case "Kenya":
                countryImage.setImageResource(R.drawable.kenya);
                countryFlag.setImageResource(R.drawable.kenyaflag);
                break;
            case "Libya":
                countryImage.setImageResource(R.drawable.libya);
                countryFlag.setImageResource(R.drawable.libyaflag);
                break;
            case "Madagascar":
                countryImage.setImageResource(R.drawable.madagascar);
                countryFlag.setImageResource(R.drawable.madagascarflag);
                break;
            case "Morocco":
                countryImage.setImageResource(R.drawable.morocco);
                countryFlag.setImageResource(R.drawable.morrocoflag);
                break;
            case "South Africa":
                countryImage.setImageResource(R.drawable.saf);
                countryFlag.setImageResource(R.drawable.safflag);
                break;
            case "Tunisia":
                countryImage.setImageResource(R.drawable.tunisia);
                countryFlag.setImageResource(R.drawable.tunisiaflag);
                break;
            case "Zimbabwe":
                countryImage.setImageResource(R.drawable.zimbabwe);
                countryFlag.setImageResource(R.drawable.zimflag);
                break;
            case "Finland":
                countryImage.setImageResource(R.drawable.finland);
                countryFlag.setImageResource(R.drawable.finflag);
                break;
            case "France":
                countryImage.setImageResource(R.drawable.france);
                countryFlag.setImageResource(R.drawable.franceflag);
                break;
            case "Germany":
                countryImage.setImageResource(R.drawable.germany);
                countryFlag.setImageResource(R.drawable.germanyflag);
                break;
            case "Greece":
                countryImage.setImageResource(R.drawable.greece);
                countryFlag.setImageResource(R.drawable.greeceflag);
                break;
            case "Ireland":
                countryImage.setImageResource(R.drawable.ireland);
                countryFlag.setImageResource(R.drawable.irelandfla);
                break;
            case "Italy":
                countryImage.setImageResource(R.drawable.italy);
                countryFlag.setImageResource(R.drawable.italyflag);
                break;
            case "Norway":
                countryImage.setImageResource(R.drawable.norway);
                countryFlag.setImageResource(R.drawable.norwayflaf);
                break;
            case "Portugal":
                countryImage.setImageResource(R.drawable.portugal);
                countryFlag.setImageResource(R.drawable.portugalflag);
                break;
            case "Spain":
                countryImage.setImageResource(R.drawable.spain);
                countryFlag.setImageResource(R.drawable.spainflag);
                break;
            case "Sweden":
                countryImage.setImageResource(R.drawable.sweden);
                countryFlag.setImageResource(R.drawable.swedenflag);
                break;
            case "Switzerland":
                countryImage.setImageResource(R.drawable.swiss);
                countryFlag.setImageResource(R.drawable.swissflag);
                break;
            case "United Kingdom":
                countryImage.setImageResource(R.drawable.uk);
                countryFlag.setImageResource(R.drawable.ukflag);
                break;
            case "Australia":
                countryImage.setImageResource(R.drawable.australia);
                countryFlag.setImageResource(R.drawable.australiaflag);
                break;
            case "New Zealand":
                countryImage.setImageResource(R.drawable.nz);
                countryFlag.setImageResource(R.drawable.nzflag);
                break;
            case "Canada":
                countryImage.setImageResource(R.drawable.canada);
                countryFlag.setImageResource(R.drawable.canadaflag);
                break;
            case "United States of America":
                countryImage.setImageResource(R.drawable.usa);
                countryFlag.setImageResource(R.drawable.usaflag);
                break;
            case "Mexico":
                countryImage.setImageResource(R.drawable.mexico);
                countryFlag.setImageResource(R.drawable.mexicoflag);
                break;
            case "Cuba":
                countryImage.setImageResource(R.drawable.cuba);
                countryFlag.setImageResource(R.drawable.cubaimage);
                break;
            case "Brazil":
                countryImage.setImageResource(R.drawable.brazil);
                countryFlag.setImageResource(R.drawable.brazilflag);
                break;
            case "Argentina":
                countryImage.setImageResource(R.drawable.argentina);
                countryFlag.setImageResource(R.drawable.argflag);
                break;
            case "Paraguay":
                countryImage.setImageResource(R.drawable.paraguay);
                countryFlag.setImageResource(R.drawable.paraguayflag);
                break;

        }

        TextView capitalName = (TextView) findViewById(R.id.capital);
        capitalName.setText("Capital : " + capital);

        TextView currencyName = (TextView) findViewById(R.id.currency);
        currencyName.setText("Currency : " + currency);

        TextView regionName = (TextView) findViewById(R.id.region);
        regionName.setText("Location in "+ continent+" : " + region);

        Button tryagain = (Button) findViewById(R.id.tryagain);
        tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle extras = getIntent().getExtras();
                String continent = extras.getString("continent");
                Intent intent = new Intent(getBaseContext(), continent.class);
                intent.putExtra("continent", continent);
                startActivity(intent);
                finish();
            }
        });

    }

    //Handle Back Button
    public void onBackPressed() {
        Bundle extras = getIntent().getExtras();
        String continent = extras.getString("continent");
        Intent intent = new Intent(getBaseContext(), continent.class);
        intent.putExtra("continent", continent);
        startActivity(intent);
        MusicManager.pause();
        finish();
    }
}
