package edu.clemson.cpsc4820.assignment1.littlelearners;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;

//Flash Card Game Setup
public class flashCardSetUp extends AppCompatActivity {

    static HashMap<String, String> myQuestions = new HashMap<>();
    ArrayList<String> keys;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Your Questions");
        setContentView(R.layout.flashcardsetup);
        keys = new ArrayList<String>();

        //sample flashcards
        myQuestions.put("Red", "Rojo");
        myQuestions.put("Blue", "Azul");
        myQuestions.put("Dog", "Perro");
        myQuestions.put("Cat", "Gato");


        final ListView myListView = (ListView) findViewById(R.id.myListView);
        int count = myQuestions.size();
        while (count > 0) {
            String key = (String) myQuestions.keySet().toArray()[count - 1];
            String value = (String) myQuestions.values().toArray()[count - 1];
            keys.add(key);
            count -= 1;
        }

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, keys);
        myListView.setAdapter(arrayAdapter);


        //Alert for managing the Questions
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final AlertDialog.Builder optionsBuilder = new AlertDialog.Builder(flashCardSetUp.this);
                String title = keys.get(position);
                optionsBuilder.setTitle("Options for " + title);

                optionsBuilder.setItems(new String[]{"Edit", "Delete"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String item = keys.get(position);

                        /** Creates a dialog for editing the selected question. **/
                        if (which == 0) {
                            AlertDialog.Builder editBuilder = new AlertDialog.Builder(flashCardSetUp.this);
                            TextView QuestionLabel = new TextView(flashCardSetUp.this);
                            TextView AnswerLabel = new TextView(flashCardSetUp.this);
                            final EditText Question = new EditText(flashCardSetUp.this);
                            final EditText Answer = new EditText(flashCardSetUp.this);

                            editBuilder.setTitle("Edit");


                            //** Formatting for the dialog text. **/
                            String title2 = item;
                            String feed = myQuestions.get(keys.get(position));
                            Question.setText(title2);
                            Answer.setText(feed);
                            Question.setTextColor(Color.BLACK);
                            Answer.setTextColor(Color.BLACK);

                            QuestionLabel.setText("Question");
                            AnswerLabel.setText("Answer");

                            /** Formating for the layout of the dialog **/
                            LinearLayout layout = new LinearLayout(getApplicationContext());
                            layout.setOrientation(LinearLayout.VERTICAL);
                            layout.setGravity(Gravity.CENTER_VERTICAL);
                            layout.addView(QuestionLabel);
                            layout.addView(Question);
                            layout.addView(AnswerLabel);
                            layout.addView(Answer);
                            editBuilder.setView(layout);
                            editBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String title = Question.getText().toString();
                                    String answer = Answer.getText().toString();
                                    if (!title.isEmpty() && !answer.isEmpty()) {
                                        myQuestions.remove(keys.get(position));
                                        keys.remove(position);
                                        keys.add(title);
                                        myQuestions.put(title, answer);
                                        Log.i("edit",myQuestions.toString());
                                    }
                                }
                            });
                            editBuilder.setNegativeButton("Cancel", null);
                            editBuilder.show();
                        }
                        /** End of edit dialog. **/

                        /** Creates a dialog for deleting a question **/
                        if (which == 1) {
                            AlertDialog.Builder deleteBuilder = new AlertDialog.Builder(flashCardSetUp.this);
                            deleteBuilder.setTitle("Confirm Delete");
                            deleteBuilder.setMessage("Are you sure you want to delete this?");

                            //Confirm Dialog to comfirm deletion.
                            deleteBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                /** Prompt the user for deletion. **/
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    myQuestions.remove(keys.get(position));
                                    keys.remove(position);
                                    arrayAdapter.notifyDataSetChanged();
                                    Log.i("delete",myQuestions.toString());
                                }
                            });
                            //Cancel Button
                            deleteBuilder.setNegativeButton("Cancel", null);
                            //Closes the dialog for Builder
                            dialog.dismiss();
                            deleteBuilder.show();
                        }
                    }
                });
                optionsBuilder.show();
            }
        });


        /**Allows the user to add a RSS Feed using Alert Dialog */
        Button addButton = (Button) findViewById(R.id.addQuestion);
        addButton.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {

                                             AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(flashCardSetUp.this);
                                             dialogBuilder.setTitle("New Question");
                                             final EditText newQues = new EditText(getApplicationContext());

                                             newQues.setHint("Question");
                                             newQues.setTextColor(Color.BLACK);
                                             newQues.setHintTextColor(Color.GRAY);

                                             final EditText newAns = new EditText(getApplicationContext());
                                             newAns.setHint("Answer");
                                             newAns.setTextColor(Color.BLACK);
                                             newAns.setHintTextColor(Color.GRAY);

                                             LinearLayout layout = new LinearLayout(getApplicationContext());
                                             layout.setOrientation(LinearLayout.VERTICAL);
                                             layout.setGravity(Gravity.CENTER_VERTICAL);
                                             layout.addView(newQues);
                                             layout.addView(newAns);

                                             dialogBuilder.setView(layout);
                                             dialogBuilder.setNegativeButton("Cancel", null);
                                             dialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                                                         @Override
                                                         public void onClick(DialogInterface dialog, int which) {
                                                             String title = newQues.getText().toString();
                                                             String link = newAns.getText().toString();
                                                             if (!title.isEmpty() && !link.isEmpty()) {
                                                                 myQuestions.put(title, link);
                                                                 keys.add(title);
                                                                 arrayAdapter.notifyDataSetChanged();
                                                                 Toast.makeText(getApplicationContext(), "Added", Toast.LENGTH_SHORT).show();
                                                                 Log.i("add new",myQuestions.toString());
                                                             }
                                                         }
                                                     }

                                             );
                                             dialogBuilder.show();
                                         }
                                     }
        );

    }

    public void infoClick(View v) {
        startActivity(new Intent(flashCardSetUp.this, flashCardInfoScreen.class));
        finish();
    }

    public void quizzzClick(View w) {

        Intent intent = new Intent(this, flashCardGame.class);
        intent.putExtra("wordlistchoice", 4);
        startActivity(intent);
        finish();
    }

    //Handle Back Button
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, flashCardSplashScreen.class);
        startActivity(intent);
        finish();
    }
}
