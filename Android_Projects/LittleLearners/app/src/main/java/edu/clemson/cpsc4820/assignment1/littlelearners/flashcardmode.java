package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.HashMap;

/**
 * Created by gfranci on 4/19/2017.
 */



public class flashcardmode extends AppCompatActivity {
    private HashMap<String, String> temp;
    private int choice = 0;
    public void playFlashCards()
    {
        Intent intent = new Intent(this, flashCardGame.class);
        intent.putExtra("wordlistchoice", choice);
        startActivity(intent);
        finish();
    }
    public void customList()
    {
        Intent intent = new Intent(this, flashCardSetUp.class);
        intent.putExtra("wordlistchoice", choice);
        intent.putExtra("wordlist", temp);
        startActivity(intent);
        finish();
    }
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, flashCardSplashScreen.class);
        startActivity(intent);
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flashcardmode);
        temp = (HashMap<String, String>) getIntent().getSerializableExtra("wordlist");
        Button arVerbs = (Button)findViewById(R.id.arverbs);
        arVerbs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                choice = 1;
                playFlashCards();

            }
        });
        Button erVerbs = (Button)findViewById(R.id.erverbs);
        erVerbs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choice = 2;
                playFlashCards();

            }
        });
        Button irVerbs = (Button)findViewById(R.id.irverbs);
        irVerbs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choice = 3;
                playFlashCards();
            }
        });
        Button customList = (Button)findViewById(R.id.customlist);
        customList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choice = 4;
                customList();
            }
        });
    }
}
