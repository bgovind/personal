package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

//Splash Screen for Gep App
public class geoSplash extends AppCompatActivity {

    @Override
    protected void onPause() {
        super.onPause();
        MusicManager.pause();

    }
    @Override
    protected void onResume() {
        super.onResume();
        MusicManager.start(this, 3);
    }
    public static String AssetJSONFile (String filename, Context context) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream file = manager.open(filename);
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();

        return new String(formArray);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_splash);
        MusicManager.start(this,3);
        animationForGeo gifImageView = (animationForGeo) findViewById(R.id.GifImageView);
        gifImageView.setGifImageResource(R.raw.geo);

        Button startGeo = (Button) findViewById(R.id.startGeo);

        startGeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), geo.class));
                finish();
            }
        });

        //Access DB for getting country and continent data

        try
        {
            SQLiteDatabase llDB = (geoSplash.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
            llDB.execSQL("CREATE TABLE IF NOT EXISTS countries(id INTEGER PRIMARY KEY, name varchar(50), capital varchar(50), currency varchar(100), region varchar(100))");
            Cursor c = llDB.rawQuery("SELECT * FROM countries", null);
            if (c.moveToFirst()) {
                System.out.println("feeds true");
                System.out.println("Country data already exists");

            }
            else {
                int j = 0;
                try
                {
                    String jsonLocation = AssetJSONFile("countries.JSON", geoSplash.this);
                    JSONObject jsonobject = new JSONObject(jsonLocation);
                    JSONArray jarray = (JSONArray) jsonobject.getJSONArray("Asia");

                    System.out.println("loading Asian inserting into local");
                    //SQLiteDatabase llDB = (geoSplash.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                    llDB.execSQL("CREATE TABLE IF NOT EXISTS countries(id INTEGER PRIMARY KEY, name varchar(50), capital varchar(50), currency varchar(100), region varchar(100))");
                    for(int i=0;i<jarray.length();i++)
                    {
                        JSONObject jb =(JSONObject) jarray.get(i);
                        String name = jb.getString("name");
                        String capital = jb.getString("capital");
                        String currency = jb.getString("currency");
                        String region = jb.getString("Region");
                        llDB.execSQL("INSERT INTO countries(name, capital, currency, region) VALUES ('" + name + "', '" + capital + "','" + currency + "','" + region + "')");
                        //System.out.println(name);
                        //System.out.println(capital);
                        //System.out.println(currency);
                        //System.out.println(region);
                        ++j;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("Asia :" + j);

                int k = 0;

                try
                {
                    String jsonLocation = AssetJSONFile("countries.JSON", geoSplash.this);
                    JSONObject jsonobject = new JSONObject(jsonLocation);
                    JSONArray jarray = (JSONArray) jsonobject.getJSONArray("Africa");

                    System.out.println("loading Africa inserting into local");
                    //SQLiteDatabase llDB = (geoSplash.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                    llDB.execSQL("CREATE TABLE IF NOT EXISTS countries(id INTEGER PRIMARY KEY, name varchar(50), capital varchar(50), currency varchar(100), region varchar(100))");
                    for(int i=0;i<jarray.length();i++)
                    {
                        JSONObject jb =(JSONObject) jarray.get(i);
                        String name = jb.getString("name");
                        String capital = jb.getString("capital");
                        String currency = jb.getString("currency");
                        String region = jb.getString("Region");
                        llDB.execSQL("INSERT INTO countries(name, capital, currency, region) VALUES ('" + name + "', '" + capital + "','" + currency + "','" + region + "')");
                        //System.out.println(name);
                        //System.out.println(capital);
                        //System.out.println(currency);
                        //System.out.println(region);
                        ++k;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("Africa :" + k);

                int l = 0;


                try
                {
                    String jsonLocation = AssetJSONFile("countries.JSON", geoSplash.this);
                    JSONObject jsonobject = new JSONObject(jsonLocation);
                    JSONArray jarray = (JSONArray) jsonobject.getJSONArray("Europe");

                    System.out.println("loading European countries inserting into local");

                    //SQLiteDatabase llDB = (geoSplash.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                    llDB.execSQL("CREATE TABLE IF NOT EXISTS countries(id INTEGER PRIMARY KEY, name varchar(50), capital varchar(50), currency varchar(100), region varchar(100))");
                    for(int i=0;i<jarray.length();i++)
                    {
                        JSONObject jb =(JSONObject) jarray.get(i);
                        String name = jb.getString("name");
                        String capital = jb.getString("capital");
                        String currency = jb.getString("currency");
                        String region = jb.getString("Region");
                        llDB.execSQL("INSERT INTO countries(name, capital, currency, region) VALUES ('" + name + "', '" + capital + "','" + currency + "','" + region + "')");
                        //System.out.println(name);
                        //System.out.println(capital);
                        //System.out.println(currency);
                        //System.out.println(region);
                        ++l;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("Europe :" + l);

                int m = 0;

                try
                {
                    String jsonLocation = AssetJSONFile("countries.JSON", geoSplash.this);
                    JSONObject jsonobject = new JSONObject(jsonLocation);
                    JSONArray jarray = (JSONArray) jsonobject.getJSONArray("Australia");

                    System.out.println("loading Australian countries inserting into local");

                    //SQLiteDatabase llDB = (geoSplash.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                    llDB.execSQL("CREATE TABLE IF NOT EXISTS countries(id INTEGER PRIMARY KEY, name varchar(50), capital varchar(50), currency varchar(100), region varchar(100))");
                    for(int i=0;i<jarray.length();i++)
                    {
                        JSONObject jb =(JSONObject) jarray.get(i);
                        String name = jb.getString("name");
                        String capital = jb.getString("capital");
                        String currency = jb.getString("currency");
                        String region = jb.getString("Region");
                        llDB.execSQL("INSERT INTO countries(name, capital, currency, region) VALUES ('" + name + "', '" + capital + "','" + currency + "','" + region + "')");
                        //System.out.println(name);
                        //System.out.println(capital);
                        //System.out.println(currency);
                        //System.out.println(region);
                        ++m;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("Australia :" + m);

                int n = 0;

                try
                {
                    String jsonLocation = AssetJSONFile("countries.JSON", geoSplash.this);
                    JSONObject jsonobject = new JSONObject(jsonLocation);
                    JSONArray jarray = (JSONArray) jsonobject.getJSONArray("NorthAmerica");

                    System.out.println("loading North American countries inserting into local");

                    //SQLiteDatabase llDB = (geoSplash.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                    llDB.execSQL("CREATE TABLE IF NOT EXISTS countries(id INTEGER PRIMARY KEY, name varchar(50), capital varchar(50), currency varchar(100), region varchar(100))");
                    for(int i=0;i<jarray.length();i++)
                    {
                        JSONObject jb =(JSONObject) jarray.get(i);
                        String name = jb.getString("name");
                        String capital = jb.getString("capital");
                        String currency = jb.getString("currency");
                        String region = jb.getString("Region");
                        llDB.execSQL("INSERT INTO countries(name, capital, currency, region) VALUES ('" + name + "', '" + capital + "','" + currency + "','" + region + "')");
                        //System.out.println(name);
                        //System.out.println(capital);
                        //System.out.println(currency);
                        //System.out.println(region);
                        ++n;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("North :" + n);

                int o = 0;

                try
                {
                    String jsonLocation = AssetJSONFile("countries.JSON", geoSplash.this);
                    JSONObject jsonobject = new JSONObject(jsonLocation);
                    JSONArray jarray = (JSONArray) jsonobject.getJSONArray("SouthAmerica");

                    System.out.println("South American countries inserting into local");

                    //SQLiteDatabase llDB = (geoSplash.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                    llDB.execSQL("CREATE TABLE IF NOT EXISTS countries(id INTEGER PRIMARY KEY, name varchar(50), capital varchar(50), currency varchar(100), region varchar(100))");
                    for(int i=0;i<jarray.length();i++)
                    {
                        JSONObject jb =(JSONObject) jarray.get(i);
                        String name = jb.getString("name");
                        String capital = jb.getString("capital");
                        String currency = jb.getString("currency");
                        String region = jb.getString("Region");
                        llDB.execSQL("INSERT INTO countries(name, capital, currency, region) VALUES ('" + name + "', '" + capital + "','" + currency + "','" + region + "')");
                        //System.out.println(name);
                        //System.out.println(capital);
                        //System.out.println(currency);
                        //System.out.println(region);
                        ++o;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("South :" + o);

            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }





    }


    //Handle Back Button
    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), mainMenuGameSelect.class));
        MusicManager.pause();
        finish();
    }
}
