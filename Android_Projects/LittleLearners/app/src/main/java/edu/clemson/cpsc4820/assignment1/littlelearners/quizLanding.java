package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//Landing page implementation once a quiz is chosen by user
public class quizLanding extends AppCompatActivity {

    private RequestQueue requestQueue;

    private static final String URL = "https://people.cs.clemson.edu/~bgovind/ll/getquizcontent.php";

    private StringRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_landing);

        final String s = getIntent().getStringExtra("quizID");

        TextView quizName = (TextView) findViewById(R.id.quizName);
        quizName.setText(s);

        requestQueue = Volley.newRequestQueue(this);

        //Fetch quiz content from external postgresql DB and populate internal DB
        try {

            final SQLiteDatabase llDB = (quizLanding.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
            llDB.execSQL("CREATE TABLE IF NOT EXISTS quizcontent(id INTEGER PRIMARY KEY, name varchar(50), question varchar(100), op1 varchar(20), op2 varchar(20), op3 varchar(20), op4 varchar(20), answer INTEGER)");
            Cursor c = llDB.rawQuery("SELECT * FROM quizcontent where name = '"+s+"'", null);

            if(c.getCount() == 0) {
                try {
                    request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {

                                System.out.println("inserting quizcontent into local");

                                SQLiteDatabase llDB = (quizLanding.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);

                                JSONObject outer = new JSONObject(response);
                                Iterator<String> keys = outer.keys();
                                while (keys.hasNext()) {
                                    String key = keys.next();
                                    JSONObject inside = outer.getJSONObject(key);

                                    if (inside.names().get(0).equals("success")) {

                                        String question = inside.getString("question");
                                        String op1 = inside.getString("op1");
                                        String op2 = inside.getString("op2");
                                        String op3 = inside.getString("op3");
                                        String op4 = inside.getString("op4");
                                        String answer = inside.getString("answer");

                                        try {

                                            llDB.execSQL("INSERT INTO quizcontent(name, question, op1, op2, op3, op4, answer) VALUES ('" + s + "', '" + question + "','" + op1 + "', '" + op2 + "', '" + op3 + "', '" + op4 + "', '" + answer + "')");
                                            System.out.println(question);
                                            System.out.println(op1);
                                            System.out.println(op2);
                                            System.out.println(op3);
                                            System.out.println(op4);
                                            System.out.println(answer);


                                    /*arrayQuizNames.add(quizName);
                                    arrayQuizPosted.add(quizPostedBy);
                                    arrayquizPostedOn.add(quizPostedOn);*/

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    } else if (inside.names().get(0).equals("fail")) {
                                        Toast.makeText(quizLanding.this, "", Toast.LENGTH_SHORT).show();

                                    }
                                }

                                Toast.makeText(quizLanding.this, "Quiz List Refreshed!", Toast.LENGTH_SHORT).show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("quizname", s);

                            return hashMap;
                        }
                    };

                    requestQueue.add(request);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else
            {
                System.out.println(s+" content already exists");
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }

        //Start the Quiz
        Button startQuiz = (Button) findViewById(R.id.startQuiz);
        startQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), quizContent.class);
                intent.putExtra("quizName", s);
                startActivity(intent);
                finish();
            }


        });

    }
}
