package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//Start Game by Topic
public class mainMenuTopicSelect extends AppCompatActivity {

    //Start Flash Cards
    public void playFlashCards()
    {
        //change mainmenuinfoscreen.class to yourclass.class when finished
        Intent intent = new Intent(this, flashCardSplashScreen.class);
        startActivity(intent);
    }
    //Start Stickman
    public void playStickMan()
    {
        Intent intent = new Intent(this, stickManSplashScreen.class);
        startActivity(intent);
    }
    //Start Tic Tac Toe
    public void playTicTacToe()
    {
        Intent intent = new Intent(this, TTTSplashScreen.class);
        startActivity(intent);
    }
    //Start MathBlaster
    public void playMathBlaster()
    {
        //change mainmenuinfoscreen.class to yourclass.class when finished
        Intent intent = new Intent(this, mathBlasterSplashScreen.class);
        startActivity(intent);
    }
    //Start SlapJack
    public void playSlapJack()
    {
        //change mainmenuinfoscreen.class to yourclass.class when finished
        Intent intent = new Intent(this, slapjackstart.class);
        startActivity(intent);
    }
    //Start Geography
    public void playGeography()
    {
        //change mainmenuinfoscreen.class to yourclass.class when finished
        Intent intent = new Intent(this, geoSplash.class);
        startActivity(intent);
    }
    //Handle Back Button
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, mainMenuGameSelect.class);
        startActivity(intent);
        finish();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainmenutopicselect);

        Button flashcard = (Button) findViewById(R.id.pronunciation);
        flashcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playFlashCards();
                finish();
            }


        });

        Button stickman = (Button) findViewById(R.id.spelling);
        stickman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playStickMan();
                finish();
            }


        });

        Button tictactoe = (Button) findViewById(R.id.strategy);
        tictactoe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playTicTacToe();
                finish();
            }


        });

        Button mathblaster = (Button) findViewById(R.id.math);
        mathblaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playMathBlaster();
                finish();
            }


        });

        Button slapjack = (Button) findViewById(R.id.memory);
        slapjack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playSlapJack();
                finish();
            }


        });

        Button geography = (Button) findViewById(R.id.geography);
        geography.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playGeography();
                finish();
            }


        });
    }
}
