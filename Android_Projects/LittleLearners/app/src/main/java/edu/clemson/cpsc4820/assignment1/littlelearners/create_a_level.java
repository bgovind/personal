package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;


//Helper Class for creating a level in Stickman
public class create_a_level extends Activity {

    //declare variables
    private Button add;
    private Button remove;
    private Button edit;
    private Button exit;
    private Button viewDeleted;
    private String userInput;
    private String UI;
    private SQLiteDatabase mydb;
    private ArrayList<words> arrayList;

    @Override
    protected void onPause() {
        super.onPause();
        MusicManager.pause();

    }
    @Override
    protected void onResume() {
        super.onResume();
        MusicManager.start(this, 2);
    }

    public boolean checkNetworkStatus() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public void readLocalDB() {
        arrayList.clear();
        dbHelper dbHelper = new dbHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = dbHelper.readFromLocalDB(db);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int id = cursor.getInt(cursor.getColumnIndex(wordDB.ID));
            String word = cursor.getString(cursor.getColumnIndex(wordDB.WORD));
            int isdeleted = cursor.getInt(cursor.getColumnIndex(wordDB.IS_DELETED));
            String lastsync = cursor.getString(cursor.getColumnIndex(wordDB.LAST_SYNC));
            arrayList.add(new words(id, word, isdeleted, lastsync));
            cursor.moveToNext();
        }
        cursor.close();
        dbHelper.close();


    }
    public ArrayList<HashMap<String, String>> getAllUsers() {
        ArrayList<HashMap<String, String>> wordList;
        wordList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT  * FROM wordTable";
        try {
            mydb = this.openOrCreateDatabase("wordDB",MODE_PRIVATE, null);
            //mydb.execSQL(dbHelper.DROP_TABLE);
            mydb.execSQL("CREATE TABLE IF NOT EXISTS wordTable (id INTEGER PRIMARY KEY, word VARCHAR(20)UNIQUE, isdeleted INT DEFAULT 0, lastsync TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
        }
        catch(Exception e){
            e.printStackTrace();
        }
        Cursor cursor = mydb.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                //map.put("id", cursor.getString(0));
                map.put("word", cursor.getString(1));
                //map.put("isdeleted", cursor.getString(2));
               // map.put("lastsync", cursor.getString(3));
                wordList.add(map);
            } while (cursor.moveToNext());
        }
        mydb.close();
        return wordList;
    }










    public void header() {
        //get table id
        TableLayout stk = (TableLayout) findViewById(R.id.table_main);

        //create a new row
        TableRow tbrow = new TableRow(this);

        //create a new column
        TextView tv0 = new TextView(this);

        //set values in the text view
        tv0.setText("WORD DATABASE");
        tv0.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1));
        tv0.setTextColor(Color.WHITE);
        tv0.setGravity(Gravity.CENTER);

        //add column to row
        tbrow.addView(tv0);


        //add row to table
        stk.addView(tbrow);
    }


    public void add(String word, String lastsync) {

        //get table id
        TableLayout stk = (TableLayout) findViewById(R.id.table_main);

        //create a new row
        TableRow tbrow = new TableRow(create_a_level.this);


        int count = stk.getChildCount();
        if (count % 2 == 0) {
            tbrow.setBackgroundColor(Color.DKGRAY);
        } else {
            tbrow.setBackgroundColor(Color.BLACK);
        }


        TextView t4v = new TextView(this);

        //set values, must set id of the column here so we can modify its value later
        t4v.setId(R.id.tv4);
        t4v.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, .5f));
        t4v.setText(word);
        t4v.setTextColor(Color.WHITE);
        t4v.setGravity(Gravity.CENTER);

        //add column to row
        tbrow.addView(t4v);


        TextView t1v = new TextView(this);

        //set values, must set id of the column here so we can modify its value later
        t1v.setId(R.id.tv1);
        t1v.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, .5f));
        t1v.setText(lastsync);
        t1v.setTextColor(Color.WHITE);
        t1v.setGravity(Gravity.CENTER);

        //add column to row
        tbrow.addView(t1v);

        //add row to table
        stk.addView(tbrow);


    }

    public void viewDeleted() {
        TableLayout stk = (TableLayout) findViewById(R.id.table_main);
        stk.removeAllViews();
        readLocalDB();
        header();
        for (int index = 0; index < arrayList.size(); index++) {
            if (arrayList.get(index).getIsDeleted() == 1) {
                add(arrayList.get(index).getWord(), arrayList.get(index).getLastSync());
            }
        }
    }


    public void generateWordTable() {
            readLocalDB();
            TableLayout stk = (TableLayout)findViewById(R.id.table_main);
            stk.removeAllViews();
            header();
          for(int index = 0; index < arrayList.size(); index++) {
              if (arrayList.get(index).getIsDeleted()  == 0) {
                  add(arrayList.get(index).getWord(), arrayList.get(index).getLastSync());
              }
          }


    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, stickManSplashScreen.class);
        startActivity(intent);
        MusicManager.pause();
        finish();
    }


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_a_level);
        arrayList = new ArrayList();
        dbHelper help = new dbHelper(this);
        ArrayList<HashMap<String, String>> userList =  help.getAllUsers();
        //readLocalDB();
        generateWordTable();
        System.out.println(arrayList.size());
        MusicManager.start(this,2);


        System.out.println("Sync successful??");

        try {
            mydb = this.openOrCreateDatabase("wordDB",MODE_PRIVATE, null);
            //mydb.execSQL(dbHelper.DROP_TABLE);
            mydb.execSQL("CREATE TABLE IF NOT EXISTS wordTable (id INTEGER PRIMARY KEY, word VARCHAR(20)UNIQUE, isdeleted INT DEFAULT 0, lastsync TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
        }
        catch(Exception e){
            e.printStackTrace();
        }


            //instantiate the add button
            add = (Button) findViewById(R.id.buttonadd);

            //add button listener
            add.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    //create a new text field for the alert
                    final EditText userName = new EditText(create_a_level.this);

                    //set hint for text field
                    userName.setHint("Add a new word");

                    //create a new dialog
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            create_a_level.this);

                    // set title
                    alertDialogBuilder.setTitle("Add words to Game");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Click yes to save!")
                            .setView(userName)
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // if this button is clicked, call add
                                    // method to put the new word in the table
                                    ContentValues values = new ContentValues();
                                    values.put("word",userName.getText().toString());

                                    UI = "word = '" + userName.getText().toString()+"'";
                                    String[] columns = new String []{"word", "isdeleted"};
                                    Cursor cursor = mydb.query("wordTable", columns, UI, null, null, null, null);
                                    cursor.moveToFirst();
                                    if (cursor.getCount() == 0) {
                                        mydb.insert("wordTable", null, values);
                                        Toast.makeText(create_a_level.this, "Word Created!", Toast.LENGTH_LONG).show();
                                        generateWordTable();
                                    }

                                    if(cursor.getCount() == 1)
                                    {
                                        int isdeleted = cursor.getColumnIndexOrThrow("isdeleted");
                                        if (cursor.getInt(isdeleted) == 1) {
                                            try {
                                                mydb.execSQL("UPDATE wordTable SET isdeleted = 0 WHERE word = '" + userName.getText().toString() + "'");
                                            } catch (Exception e) {
                                                Toast.makeText(create_a_level.this, "Failed to Update status of word!", Toast.LENGTH_LONG).show();

                                            }
                                            Toast.makeText(create_a_level.this, "Changed state of word: " + userName.getText().toString() + ", to not deleted!", Toast.LENGTH_SHORT).show();
                                            generateWordTable();
                                        }
                                        else{
                                            Toast.makeText(create_a_level.this, "Cannot duplicate words!", Toast.LENGTH_LONG).show();
                                        }

                                    }





                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            });
            //instantiate the remove button
            remove = (Button) findViewById(R.id.removeButton);

            //remove listener
            remove.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    //create text field for the alert
                    final EditText input = new EditText(create_a_level.this);

                    //set hint for the text field
                    input.setHint("Which word would you like to remove?");

                    //create a new dialog
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            create_a_level.this);

                    // set title
                    alertDialogBuilder.setTitle("Word Removal");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Click yes to save!")
                            .setView(input)
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // if this button is clicked, close
                                    // dialog box
                                    UI = "SELECT word FROM wordTable WHERE word = '" + input.getText().toString()+"'";
                                    Cursor cursor = mydb.rawQuery(UI,null);

                                    cursor.moveToFirst();

                                    if (cursor.getCount() > 0) {
                                        mydb.execSQL("UPDATE wordTable SET isdeleted = 1 WHERE word = '" + input.getText().toString() + "'");
                                        Toast.makeText(create_a_level.this, input.getText().toString() +  " Deleted from Database", Toast.LENGTH_LONG).show();
                                        generateWordTable();
                                    } else {
                                        Toast.makeText(create_a_level.this, "No word found", Toast.LENGTH_LONG).show();
                                        dialog.dismiss();
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            });

            //instantiate the edit button
            edit = (Button) findViewById(R.id.editButton);

            //edit listener
            edit.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {


                    //create the select dialog
                    final AlertDialog.Builder selectEntry = new AlertDialog.Builder(
                            create_a_level.this);

                    //create a new linear layout for the selection alert
                    final LinearLayout ll = new LinearLayout(create_a_level.this);
                    //set orientation
                    ll.setOrientation(LinearLayout.VERTICAL);
                    // create an edit text for the selection input
                    final EditText input = new EditText(create_a_level.this);
                    //specify input type for the selection alert
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    //add text fields to selection linear layout
                    ll.addView(input);


                    //create the modify dialog
                    final AlertDialog.Builder modifyEntry = new AlertDialog.Builder(
                            create_a_level.this);
                    //create a new linear layout for the modify alert
                    LinearLayout ll2 = new LinearLayout(create_a_level.this);
                    //set orientation
                    ll.setOrientation(LinearLayout.VERTICAL);
                    //create an edit text for the modify input
                    final EditText change = new EditText(create_a_level.this);
                    //specify input type for the modify alert
                    change.setInputType(InputType.TYPE_CLASS_TEXT);

                    //add text fields to linear layout
                    ll2.addView(change);


                    // set title
                    selectEntry.setTitle("Modify Database Word");

                    // set dialog message
                    selectEntry
                            .setMessage("Please enter the word you want to modify:")
                            .setView(ll)
                            .setCancelable(false)
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    userInput = input.getText().toString();
                                    change.setText(userInput);
                                    change.setSelection(userInput.length());
                                    dialog.dismiss();

                                    modifyEntry.create();
                                    modifyEntry.show();


                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });

                    // create alert dialog
                    selectEntry.create();
                    selectEntry.show();


                    // set title
                    modifyEntry.setTitle("Modify current word");

                    // set dialog message
                    modifyEntry
                            .setMessage("Press save to finish")
                            .setView(ll2)
                            .setCancelable(false)
                            .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    try {
                                        mydb.execSQL("UPDATE wordTable SET word = '" + change.getText().toString() + "'" + "WHERE word = '" +input.getText().toString() + "'");
                                    }catch(Exception e)
                                    {
                                        Toast.makeText(create_a_level.this,"ERROR: Changed word already exists in the database!", Toast.LENGTH_SHORT).show();

                                    }

                                    generateWordTable();
                                    dialog.dismiss();

                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });

                }
            });

            //instantiate the exit button
            exit = (Button) findViewById(R.id.exitButton);

            //exit listener
            exit.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //return to main screen
                    mydb.close();
                    Intent intent = new Intent(getApplicationContext(), SlapJackSplashScreen.class);
                    startActivity(intent);
                    finish();
                }
            });

            viewDeleted = (Button)findViewById(R.id.vDel);

            viewDeleted.setOnClickListener(new View.OnClickListener()
            {
                 public void onClick(View view)
                {
                    if(viewDeleted.getText().toString().equals("View Deleted")) {
                        viewDeleted();
                        viewDeleted.setText("Return");
                    }
                    else
                    {
                        generateWordTable();
                        viewDeleted.setText("View Deleted");
                    }
                }
            });
    }
}


