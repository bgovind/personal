package edu.clemson.cpsc4820.assignment1.littlelearners;

/**
 * Created by gfranci on 4/19/2017.
 */

//Helper Class
public class spanishWordTable {

    private String englishWord;
    private String spanishWord;

    public spanishWordTable()
    {
        this.englishWord = "";
        this.spanishWord = "";
    }
    public spanishWordTable(String english, String spanish)
    {
        this.englishWord = english;
        this.spanishWord = spanish;
    }

    public String getEnglishWord() {
        return englishWord;
    }

    public void setEnglishWord(String englishWord) {
        this.englishWord = englishWord;
    }

    public String getSpanishWord() {
        return spanishWord;
    }

    public void setSpanishWord(String spanishWord) {
        this.spanishWord = spanishWord;
    }
}
