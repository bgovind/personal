package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


//Quiz List implementation
public class quizList extends AppCompatActivity {

    private ArrayList<String> arrayQuizNames = new ArrayList<>();
    private ArrayList<String> arrayQuizPosted = new ArrayList<>();
    private ArrayList<String> arrayquizPostedOn = new ArrayList<>();

    ListView quizList;

    private RequestQueue requestQueue;

    private static final String URL = "https://people.cs.clemson.edu/~bgovind/ll/allquizlist.php";

    private StringRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_list);

        requestQueue = Volley.newRequestQueue(this);

        Button refreshList = (Button) findViewById(R.id.refresh);
        refreshList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                requestQueue = Volley.newRequestQueue(quizList.this);

                //Fetch quiz list from external DB and populate external DB when refreshed
                try {
                    request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {

                                System.out.println("login feeds inserting into local");

                                SQLiteDatabase llDB = (quizList.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                                llDB.execSQL("CREATE TABLE IF NOT EXISTS quizlist(id INTEGER PRIMARY KEY, name varchar(50), postedby varchar(50), postedon text)");

                                llDB.execSQL("DELETE FROM quizlist");

                                JSONObject outer = new JSONObject(response);
                                Iterator<String> keys = outer.keys();
                                while (keys.hasNext())
                                {
                                    String key = keys.next();
                                    JSONObject inside = outer.getJSONObject(key);

                                    if (inside.names().get(0).equals("success"))
                                    {

                                        String quizName = inside.getString("quizName");
                                        String quizPostedBy = inside.getString("posted_by");
                                        String quizPostedOn = inside.getString("posted_on");

                                        try{

                                            llDB.execSQL("INSERT INTO quizlist(name, postedby, postedon) VALUES ('" + quizName + "', '" + quizPostedBy + "','" + quizPostedOn + "')");
                                            System.out.println(quizName);
                                            System.out.println(quizPostedBy);
                                            System.out.println(quizPostedOn);

                                        }catch(Exception e)
                                        {
                                            e.printStackTrace();
                                        }

                                    }
                                    else if (inside.names().get(0).equals("fail")) {
                                        Toast.makeText(quizList.this, "", Toast.LENGTH_SHORT).show();

                                    }
                                }

                                Toast.makeText(quizList.this, "Quiz List Refreshed!", Toast.LENGTH_SHORT).show();
                                recreate();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> hashMap = new HashMap<>();

                            String authorkey = "2017";
                            hashMap.put("authorkey", authorkey);

                            return hashMap;
                        }
                    };

                    requestQueue.add(request);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        //Fetch quiz list from internal SQLITE DB
        try
        {
            final SQLiteDatabase llDB = (quizList.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
            llDB.execSQL("CREATE TABLE IF NOT EXISTS quizlist(id INTEGER PRIMARY KEY, name varchar(50), postedby varchar(50), postedon text)");
            Cursor c = llDB.rawQuery("SELECT * FROM quizlist order by postedon desc", null);

            int nameIndex = c.getColumnIndex("name");
            int postedbyIndex = c.getColumnIndex("postedby");
            int postedonIndex = c.getColumnIndex("postedon");

            if (c.moveToFirst())
                System.out.println("feeds true");
            else
                System.out.println("feeds false");

            c.moveToFirst();
            while(c != null && !c.isAfterLast())
            {
                String quizName = c.getString(nameIndex);
                String quizPostedBy = c.getString(postedbyIndex);
                String quizPostedOn = c.getString(postedonIndex);

                System.out.println(quizName);
                System.out.println(quizPostedBy);
                System.out.println(quizPostedOn);

                arrayQuizNames.add(quizName);
                arrayQuizPosted.add(quizPostedBy);
                arrayquizPostedOn.add(quizPostedOn);
                c.moveToNext();
            }

        }catch(Exception e)
        {
            System.out.println("Exception thrown by DB");
            e.printStackTrace();
        }

        CustomListAdapterQuiz adapter=new CustomListAdapterQuiz(this, arrayQuizNames, arrayQuizPosted, arrayquizPostedOn);
        quizList=(ListView)findViewById(R.id.quizlist);
        quizList.setAdapter(adapter);


        //Select a quiz
        quizList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id)
            {

                String Slecteditem= arrayQuizNames.get(position).toString();
                Intent intent = new Intent(getBaseContext(), quizLanding.class);
                intent.putExtra("quizID", Slecteditem);
                startActivity(intent);
            }
        });


    }

    //Back Button routine
    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), mainMenuGameSelect.class));
        finish();
    }


}

