package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.app.AlertDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

//Stickman Splash Screen Activity
public class stickManSplashScreen extends AppCompatActivity {

    public void playStickMan()
    {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.stickmanmode, null);

        final AlertDialog alertD = new AlertDialog.Builder(this).create();

        Button btnAdd1 = (Button) promptView.findViewById(R.id.btnYes);

        Button btnAdd2 = (Button) promptView.findViewById(R.id.btnNo);

        btnAdd1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), stickManGame.class);
                intent.putExtra("mode", 1);
                mPlayer.stop();
                startActivity(intent);
                finish();


            }
        });

        btnAdd2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), stickManGame.class);
                intent.putExtra("mode", 2);
                mPlayer.stop();
                startActivity(intent);
                finish();

            }
        });

        alertD.setView(promptView);

        alertD.show();

    }
    public void info()
    {
        Intent intent = new Intent(this, stickmaninfo.class);
        mPlayer.stop();
        startActivity(intent);
        finish();
    }
    public void levelCreate()
    {
        Intent intent = new Intent(this, create_a_level.class);
        mPlayer.stop();
        startActivity(intent);
        finish();
    }

    //declare variables
    private Button startButton;
    private MediaPlayer mPlayer;
    private ImageButton info;
    private ImageButton musictoggle;
    private Button levelCreate;

    //Handle Back Button
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, mainMenuGameSelect.class);
        mPlayer.stop();
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stickmansplashscreen);

        //set variables to resources
        startButton = (Button) findViewById(R.id.startButton);
        info = (ImageButton)findViewById(R.id.imageButton);
        animatorForStickMan gifImageView = (animatorForStickMan) findViewById(R.id.dancer);
        gifImageView.setGifImageResource(R.raw.dancingstick);
        musictoggle = (ImageButton)findViewById(R.id.imageButton2);
        levelCreate = (Button)findViewById(R.id.levelcreatebutton);
        mPlayer = MediaPlayer.create(this,R.raw.thememusic);
        mPlayer.start();

        //wait for button clicks, on click do this
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playStickMan();
            }
        });
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                info();
            }
        });
        levelCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                levelCreate();
            }
        });
        musictoggle.setOnClickListener(new View.OnClickListener() {
            int count = 1;
            @Override
            public void onClick(View view) {
                if(count%2 == 0)
                {
                    musictoggle.setBackgroundResource(R.drawable.soundon);
                    mPlayer.start();
                    count++;
                }
                else
                {
                    musictoggle.setBackgroundResource(R.drawable.soundmute);
                    mPlayer.pause();
                    count++;
                }
            }
        });
    }
}