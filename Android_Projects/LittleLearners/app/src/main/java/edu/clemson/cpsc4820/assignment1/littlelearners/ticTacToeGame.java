package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.app.AlertDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import static edu.clemson.cpsc4820.assignment1.littlelearners.flashCardSetUp.myQuestions;

//Tic Tac Toe Game Logic
public class ticTacToeGame extends AppCompatActivity implements View.OnClickListener{

    private int player = 1;
    private int count = 0;
    private TextView playerTurn = null;
    private Intent intent;
    private MediaPlayer soundeffect;

    //Handle Back Button
    @Override
    public void onBackPressed()
    {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.exittttcustomdialog, null);

        final AlertDialog alertD = new AlertDialog.Builder(this).create();

        Button btnAdd1 = (Button) promptView.findViewById(R.id.btnYes);

        Button btnAdd2 = (Button) promptView.findViewById(R.id.btnNo);

        btnAdd1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                intent = new Intent(getApplicationContext(), TTTSplashScreen.class);
                startActivity(intent);
                finish();
                alertD.dismiss();


            }
        });

        btnAdd2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                alertD.dismiss();

            }
        });

        alertD.setView(promptView);

        alertD.show();

    }

    //Restart Activity
    public void restart()
    {
        intent = new Intent(ticTacToeGame.this , ticTacToeGame.class);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.loadingdialog, null);
        final AlertDialog alertD = new AlertDialog.Builder(this).create();
        alertD.setView(promptView);
        alertD.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
                alertD.dismiss();
            }
        },2000);



    }

    public void onClick(View v) {


        Button temp = (Button)v;
        temp.setClickable(false);
        switch (player % 2)
        {
            case 0:
                temp.setBackgroundResource(R.drawable.opic);
                 temp.setTag("R.drawable.opic");
                player ++;
                playerTurn.setText("Player " + (player % 2) + "\n Your Turn");
                break;
            case 1:
                temp.setBackgroundResource(R.drawable.xpic);
                temp.setTag("R.drawable.xpic");
                player ++;
                playerTurn.setText("Player " + ((player % 2) + 2) +  "\n Your Turn");
                break;
        }

        boolean hasWon = false;
        if(hasWon())
        {
            switch(player % 2) {
                case 0:
                    Toast.makeText(this, "Player 1 wins the match", Toast.LENGTH_LONG).show();
                    break;
                case 1:
                    Toast.makeText(this, "Player 2 wins the match", Toast.LENGTH_LONG).show();
                    break;
            }
            hasWon = true;
            soundeffect.start();
            restart();

        }

        count++;
        if((count == 9) && !hasWon)
        {
            Toast.makeText(this, "Match ends in a Draw!", Toast.LENGTH_LONG).show();
            restart();
        }
        //soundeffect.start();
    }




    //Handle Win Result
    public boolean hasWon() {
        TableLayout gameBoard = (TableLayout)findViewById(R.id.gameBoard);

        for(int index = 0; index < gameBoard.getChildCount(); index++)
        {
            View child = gameBoard.getChildAt(index);
            TableRow row = (TableRow)child;
            String name1 = (String)row.getChildAt(0).getTag();
            String name2 = (String)row.getChildAt(1).getTag();
            String name3 = (String)row.getChildAt(2).getTag();
            if((name1 != null) && (name2 != null) && (name3 != null)) {
                switch (player % 2) {
                    case 0:
                        if ((!name1.equals("R.drawable.opic")) && (!name2.equals("R.drawable.opic")) && (!name3.equals("R.drawable.opic"))) {
                            return true;
                        }
                        break;
                    case 1:
                        if ((!name1.equals("R.drawable.xpic")) && (!name2.equals("R.drawable.xpic")) && (!name3.equals("R.drawable.xpic"))) {
                            return true;
                        }
                        break;
                }
            }
        }
        for(int index = 0; index < gameBoard.getChildCount(); index++)
        {
            TableRow row1 = (TableRow)gameBoard.getChildAt(0);
            TableRow row2 = (TableRow)gameBoard.getChildAt(1);
            TableRow row3 = (TableRow)gameBoard.getChildAt(2);
            String name1 = (String)row1.getChildAt(index).getTag();
            String name2 = (String)row2.getChildAt(index).getTag();
            String name3 = (String)row3.getChildAt(index).getTag();
            if((name1 != null) && (name2 != null) && (name3 != null)) {
                switch (player % 2) {
                    case 0:
                        if ((!name1.equals("R.drawable.opic")) && (!name2.equals("R.drawable.opic")) && (!name3.equals("R.drawable.opic"))) {
                            return true;
                        }
                        break;
                    case 1:
                        if ((!name1.equals("R.drawable.xpic")) && (!name2.equals("R.drawable.xpic")) && (!name3.equals("R.drawable.xpic"))) {
                            return true;
                        }
                        break;
                }
            }
        }

        for(int index = 0; index < 1; index++)
        {
            TableRow row1 = (TableRow)gameBoard.getChildAt(0);
            TableRow row2 = (TableRow)gameBoard.getChildAt(1);
            TableRow row3 = (TableRow)gameBoard.getChildAt(2);
            String name1 = (String)row1.getChildAt(0).getTag();
            String name2 = (String)row2.getChildAt(1).getTag();
            String name3 = (String)row3.getChildAt(2).getTag();

            if((name1 != null) && (name2 != null) && (name3 != null)) {
                switch (player % 2) {
                    case 0:
                        if ((!name1.equals("R.drawable.opic")) && (!name2.equals("R.drawable.opic")) && (!name3.equals("R.drawable.opic"))) {
                            return true;
                        }
                        break;

                    case 1:
                        if ((!name1.equals("R.drawable.xpic")) && (!name2.equals("R.drawable.xpic")) && (!name3.equals("R.drawable.xpic"))) {
                            return true;
                        }
                        break;
                }
            }
        }
        for(int index = 0; index < 1; index++)
        {
            TableRow row1 = (TableRow)gameBoard.getChildAt(0);
            TableRow row2 = (TableRow)gameBoard.getChildAt(1);
            TableRow row3 = (TableRow)gameBoard.getChildAt(2);
            String name1 = (String)row1.getChildAt(2).getTag();
            String name2 = (String)row2.getChildAt(1).getTag();
            String name3 = (String)row3.getChildAt(0).getTag();

            if((name1 != null) && (name2 != null) && (name3 != null)) {
                switch (player % 2) {
                    case 0:
                        if ((!name1.equals("R.drawable.opic")) && (!name2.equals("R.drawable.opic")) && (!name3.equals("R.drawable.opic"))) {
                            return true;
                        }
                        break;

                    case 1:
                        if ((!name1.equals("R.drawable.xpic")) && (!name2.equals("R.drawable.xpic")) && (!name3.equals("R.drawable.xpic"))) {
                            return true;
                        }
                        break;
                }
            }
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tictactoegame);
        soundeffect = MediaPlayer.create(this,R.raw.turnindicator);
        //soundeffect.start();

        Button buttonOne = (Button) findViewById(R.id.button1);
        buttonOne.setOnClickListener(this);
        Button buttonTwo = (Button) findViewById(R.id.button2);
        buttonTwo.setOnClickListener(this);
        Button buttonThree = (Button) findViewById(R.id.button3);
        buttonThree.setOnClickListener(this);
        Button buttonFour = (Button) findViewById(R.id.button4);
        buttonFour.setOnClickListener(this);
        Button buttonFive = (Button) findViewById(R.id.button5);
        buttonFive.setOnClickListener(this);
        Button buttonSix = (Button) findViewById(R.id.button6);
        buttonSix.setOnClickListener(this);
        Button buttonSeven = (Button) findViewById(R.id.button7);
        buttonSeven.setOnClickListener(this);
        Button buttonEight = (Button) findViewById(R.id.button8);
        buttonEight.setOnClickListener(this);
        Button buttonNine = (Button) findViewById(R.id.button9);
        buttonNine.setOnClickListener(this);
        playerTurn = (TextView)findViewById(R.id.moveTextView);
        playerTurn.setText("Player " + (player % 2) + "\n Your Turn");
    }
}
