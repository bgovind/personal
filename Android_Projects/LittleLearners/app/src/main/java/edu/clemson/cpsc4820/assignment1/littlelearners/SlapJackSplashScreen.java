package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//Slapjack Splash Screen
public class SlapJackSplashScreen extends AppCompatActivity {

    private MediaPlayer slapsound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slap_jack_splash_screen);

        Button evenodd = (Button)findViewById(R.id.evenodd);
        evenodd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSJ(1);

            }
        });
        Button multiples = (Button)findViewById(R.id.multiples);
        multiples.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSJ(2);

            }
        });
        Button prime = (Button)findViewById(R.id.prime);
        prime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSJ(3);

            }
        });

        Button info = (Button)findViewById(R.id.infoSJ);
        info.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), slapinfo.class);
            startActivity(intent);
            finish();

            }
        });
        slapsound = MediaPlayer.create(this, R.raw.slapsound);
        slapsound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();

            }});


    }

    public void playSJ(int mode)
    {
        Intent intent = new Intent(this, SlapJackGame.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        slapsound.start();
        intent.putExtra("slapmode", mode);
        startActivity(intent);
        finish();

    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, slapjackstart.class);
        slapsound.start();
        startActivity(intent);
        finish();
    }
}
