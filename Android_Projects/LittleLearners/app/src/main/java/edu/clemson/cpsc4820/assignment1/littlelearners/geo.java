package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class geo extends AppCompatActivity {

    Integer[] imageIDs = {
            R.drawable.sa,
            R.drawable.na,
            R.drawable.oc,
            R.drawable.africa,
            R.drawable.asia,
            R.drawable.eu
    };
    @Override
    protected void onPause() {
        super.onPause();
        MusicManager.pause();

    }
    @Override
    protected void onResume() {
        super.onResume();
        MusicManager.start(this, 3);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo);
        MusicManager.start(this,2);
        GridView gridView = (GridView) findViewById(R.id.gridview);
        gridView.setAdapter(new ImageAdapter(this));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id)
            {
                String nameSelected = "";
                if(position == 0)
                {
                    nameSelected = "South America";
                }
                else if(position == 1)
                {
                    nameSelected =  "North America";
                }
                else if (position == 2)
                {
                    nameSelected =  "Australia";
                }
                else if(position == 3)
                {
                    nameSelected =  "Africa";
                }
                else if(position == 4)
                {
                    nameSelected =  "Asia";
                }
                else if(position == 5)
                {
                    nameSelected =  "Europe";
                }
                Toast.makeText(getBaseContext(),
                        "Continent selected is " + nameSelected,
                        Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getBaseContext(), continent.class);
                intent.putExtra("continent", nameSelected);
                startActivity(intent);
            }
        });
    }

    public class ImageAdapter extends BaseAdapter
    {
        private Context context;

        public ImageAdapter(Context c)
        {
            context = c;
        }

        //returns the number of images
        public int getCount() {
            return imageIDs.length;
        }

        //returns the ID of an item
        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        //returns an ImageView view
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(context);
                imageView.setLayoutParams(new GridView.LayoutParams(285, 400));
                imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                imageView.setPadding(5, 5, 5, 5);
            } else {
                imageView = (ImageView) convertView;
            }
            imageView.setImageResource(imageIDs[position]);
            return imageView;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), geoSplash.class));
        MusicManager.pause();
        finish();
    }
}