package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

//Prgress Tracker Class
public class progresshome extends AppCompatActivity {

    private int progressStatusQuiz = 0;
    private int progressStatusStickman = 0;
    private int progressStatusMath = 0;
    private Handler handler1 = new Handler();
    private Handler handler2 = new Handler();
    private Handler handler3 = new Handler();
    int cScoreQuiz = 0;
    int cScoreStickman = 0;
    int cScoreMath = 0;
    String levelQuiz = "";
    String levelStickman = "";
    String levelMath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progresshome);

        // Get the widgets reference from XML layout
        final TextView quizScore = (TextView) findViewById(R.id.quizScore);
        final TextView quizLevel = (TextView) findViewById(R.id.quizLevel);
        final TextView quizTarget = (TextView) findViewById(R.id.quizTarget);
        final ProgressBar pb1 = (ProgressBar) findViewById(R.id.pb1);

        final TextView stickmanScore = (TextView) findViewById(R.id.stickmanScore);
        final TextView stickmanLevel = (TextView) findViewById(R.id.stickmanLevel);
        final TextView stickmanTarget = (TextView) findViewById(R.id.stickmanTarget);
        final ProgressBar pb2 = (ProgressBar) findViewById(R.id.pb2);

        final TextView mathScore = (TextView) findViewById(R.id.mathScore);
        final TextView mathLevel = (TextView) findViewById(R.id.mathLevel);
        final TextView mathTarget = (TextView) findViewById(R.id.mathTarget);
        final ProgressBar pb3 = (ProgressBar) findViewById(R.id.pb3);

        pb1.setScaleY(3f);
        pb2.setScaleY(3f);
        pb3.setScaleY(3f);

        //Set Progress for Quiz
        try{
            SQLiteDatabase llDB = (progresshome.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
            llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer, foreign key (game_name) references game_list(game_name))");
            Cursor c = llDB.rawQuery("SELECT * FROM progress where game_name = 'quiz'", null);
            int cScoreIndex = c.getColumnIndex("cScore");
            if (c.getCount() > 0) {
                System.out.println("Quiz Score exists");
                c.moveToFirst();
                cScoreQuiz = c.getInt(cScoreIndex);
                c.close();
            } else {
                System.out.println("Error");
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }

        if(cScoreQuiz < 5)
        {
            levelQuiz = "Level 1";
        }
        else if (cScoreQuiz >= 5 &&  cScoreQuiz < 10)
        {
            levelQuiz = "Level 2";
        }
        else if (cScoreQuiz >= 10 && cScoreQuiz < 15)
        {
            levelQuiz = "Level 3";
        }
        else if (cScoreQuiz >= 15 && cScoreQuiz < 20)
        {
            levelQuiz = "Level 4";
        }
        else if (cScoreQuiz >= 20 && cScoreQuiz < 25)
        {
            levelQuiz = "Level 5";
        }
        else if (cScoreQuiz >= 25 && cScoreQuiz < 30)
        {
            levelQuiz = "Level 6";
        }
        else if (cScoreQuiz >= 30 && cScoreQuiz < 35)
        {
            levelQuiz = "Level 7";
        }
        else if (cScoreQuiz >= 35 && cScoreQuiz < 40)
        {
            levelQuiz = "Level 8";
        }
        else if (cScoreQuiz >= 40 && cScoreQuiz < 45)
        {
            levelQuiz = "Level 9";
        }
        else if (cScoreQuiz >= 45 && cScoreQuiz < 50)
        {
            levelQuiz = "Level 10";
        }
        else if (cScoreQuiz >= 50 && cScoreQuiz < 55)
        {
            levelQuiz = "Level 11";
        }
        else if (cScoreQuiz >= 55 && cScoreQuiz < 60)
        {
            levelQuiz = "Level 12";
        }
        else if (cScoreQuiz >= 60 && cScoreQuiz < 65)
        {
            levelQuiz = "Level 13";
        }
        else if (cScoreQuiz >= 65 && cScoreQuiz < 70)
        {
            levelQuiz = "Level 14";
        }
        else if (cScoreQuiz >= 70 && cScoreQuiz < 75)
        {
            levelQuiz = "Level 15";
        }
        else if (cScoreQuiz >= 75 && cScoreQuiz < 80)
        {
            levelQuiz = "Level 16";
        }
        else if (cScoreQuiz >= 80 && cScoreQuiz < 85)
        {
            levelQuiz = "Level 17";
        }
        else if (cScoreQuiz >= 85 && cScoreQuiz < 90)
        {
            levelQuiz = "Level 18";
        }
        else if (cScoreQuiz >= 90 && cScoreQuiz < 95)
        {
            levelQuiz = "Level 19";
        }
        else if (cScoreQuiz >= 95 && cScoreQuiz <= 100)
        {
            levelQuiz = "Level 20";
        }

        System.out.println("Currenet Level in Quiz : "+levelQuiz);


                // Set the progress status zero on each button click
        progressStatusQuiz = 0;

                /*
                    A Thread is a concurrent unit of execution. It has its own call stack for
                    methods being invoked, their arguments and local variables. Each application
                    has at least one thread running when it is started, the main thread,
                    in the main ThreadGroup. The runtime keeps its own threads
                    in the system thread group.
                */
                // Start the lengthy operation in a background thread
        quizScore.setText("Your Score : "+progressStatusQuiz);
        quizLevel.setText("Your Level : "+levelQuiz);
        quizTarget.setText((100-progressStatusQuiz)+" more points to Level 20");

        new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while(progressStatusQuiz < cScoreQuiz){
                            // Update the progress status
                            progressStatusQuiz +=1;

                            // Try to sleep the thread for 20 milliseconds
                            try{
                                Thread.sleep(40);
                            }catch(InterruptedException e){
                                e.printStackTrace();
                            }

                            // Update the progress bar
                            handler1.post(new Runnable() {
                                @Override
                                public void run() {
                                    pb1.setProgress(progressStatusQuiz);
                                    // Show the progress on TextView
                                    quizScore.setText("Your Score : "+progressStatusQuiz);
                                    quizLevel.setText("Your Level : "+levelQuiz);
                                    if(cScoreQuiz == 100) {
                                        quizTarget.setText("Congrats! You have reached level 20!");
                                    }else{
                                        quizTarget.setText((100 - cScoreQuiz) + " more points to Level 20");
                                    }

                                }
                            });
                        }
                    }
                }).start(); // Start the operation


        //Set Progress for Stickman
        try{
            SQLiteDatabase llDB = (progresshome.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
            llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer, foreign key (game_name) references game_list(game_name))");
            Cursor c = llDB.rawQuery("SELECT * FROM progress where game_name = 'stickman'", null);
            int cScoreIndex = c.getColumnIndex("cScore");
            if (c.getCount() > 0 ) {
                System.out.println("Stickman Score exists");
                c.moveToFirst();
                cScoreStickman = c.getInt(cScoreIndex);
                c.close();
            } else {
                System.out.println("Error");
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }

        if(cScoreStickman < 5)
        {
            levelStickman = "Level 1";
        }
        else if (cScoreStickman >= 5 && cScoreStickman < 25)
        {
            levelStickman = "Level 2";
        }
        else if (cScoreStickman >= 25 && cScoreStickman < 50)
        {
            levelStickman = "Level 3";
        }
        else if (cScoreStickman >= 50 && cScoreStickman < 80)
        {
            levelStickman = "Level 4";
        }
        else if (cScoreStickman >= 80 && cScoreStickman < 100)
        {
            levelStickman = "Level 5";
        }
        else if (cScoreStickman >= 100 && cScoreStickman < 120)
        {
            levelStickman = "Level 6";
        }
        else if (cScoreStickman >= 120 && cScoreStickman < 140)
        {
            levelStickman = "Level 7";
        }
        else if (cScoreStickman >= 140 && cScoreStickman < 160)
        {
            levelStickman = "Level 8";
        }
        else if (cScoreStickman >= 160 && cScoreStickman < 180)
        {
            levelStickman = "Level 9";
        }
        else if (cScoreStickman >= 180 && cScoreStickman < 200)
        {
            levelStickman = "Level 10";
        }
        else if (cScoreStickman >= 200 && cScoreStickman < 250)
        {
            levelStickman = "Level 11";
        }
        else if (cScoreStickman >= 250 && cScoreStickman < 300)
        {
            levelStickman = "Level 12";
        }
        else if (cScoreStickman >= 300 && cScoreStickman < 350)
        {
            levelStickman = "Level 13";
        }
        else if (cScoreStickman >= 350 && cScoreStickman < 400)
        {
            levelStickman = "Level 14";
        }
        else if (cScoreStickman >= 400 && cScoreStickman < 450)
        {
            levelStickman = "Level 15";
        }
        else if (cScoreStickman >= 450 && cScoreStickman < 500)
        {
            levelStickman = "Level 16";
        }
        else if (cScoreStickman >= 500 && cScoreStickman < 550)
        {
            levelStickman = "Level 17";
        }
        else if (cScoreStickman >= 550 && cScoreStickman < 650)
        {
            levelStickman = "Level 18";
        }
        else if (cScoreStickman >= 650 && cScoreStickman < 800)
        {
            levelStickman = "Level 19";
        }
        else if (cScoreStickman >= 800 && cScoreStickman <= 1000)
        {
            levelStickman = "Level 20";
        }

        System.out.println("Current Level in Stickman: "+levelStickman);


        progressStatusStickman = 0;

                /*
                    A Thread is a concurrent unit of execution. It has its own call stack for
                    methods being invoked, their arguments and local variables. Each application
                    has at least one thread running when it is started, the main thread,
                    in the main ThreadGroup. The runtime keeps its own threads
                    in the system thread group.
                */
        // Start the lengthy operation in a background thread
        stickmanScore.setText("Your Score : "+progressStatusStickman);
        stickmanLevel.setText("Your Level : "+levelStickman);
        stickmanTarget.setText((1000-progressStatusStickman)+" more points to Level 20");

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(progressStatusStickman < cScoreStickman){
                    // Update the progress status
                    progressStatusStickman +=1;

                    // Try to sleep the thread for 20 milliseconds
                    try{
                        Thread.sleep(40);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }

                    // Update the progress bar
                    handler2.post(new Runnable() {
                        @Override
                        public void run() {
                            pb2.setProgress(progressStatusStickman);
                            // Show the progress on TextView
                            stickmanScore.setText("Your Score : "+progressStatusStickman);
                            stickmanLevel.setText("Your Level : "+levelStickman);
                            if(cScoreStickman == 1000) {
                                stickmanTarget.setText("Congrats! You have reached level 20!");
                            }else{
                                stickmanTarget.setText((1000 - cScoreStickman) + " more points to Level 20");
                            }

                        }
                    });
                }
            }
        }).start(); // Start the operation

        //Set Progress for Math Blaster
        try{
            SQLiteDatabase llDB = (progresshome.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
            llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer, foreign key (game_name) references game_list(game_name))");
            Cursor c = llDB.rawQuery("SELECT * FROM progress where game_name = 'math'", null);
            int cScoreIndex = c.getColumnIndex("cScore");
            if (c.moveToFirst()) {
                System.out.println("Math Score exists");
                c.moveToFirst();
                while(c != null)
                {
                    cScoreMath = c.getInt(cScoreIndex);
                    c.moveToNext();
                }
                c.close();
            } else {
                System.out.println("Error");
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }

        if(cScoreMath < 5)
        {
            levelMath = "Level 1";
        }
        else if (cScoreMath >= 5 && cScoreMath < 15)
        {
            levelMath = "Level 2";
        }
        else if (cScoreMath >= 15 && cScoreMath < 30)
        {
            levelMath = "Level 3";
        }
        else if (cScoreMath >= 30 && cScoreMath < 50)
        {
            levelMath = "Level 4";
        }
        else if (cScoreMath >= 50 && cScoreMath < 75)
        {
            levelMath = "Level 5";
        }
        else if (cScoreMath >= 75 && cScoreMath < 105)
        {
            levelMath = "Level 6";
        }
        else if (cScoreMath >= 105 && cScoreMath < 140)
        {
            levelMath = "Level 7";
        }
        else if (cScoreMath >= 140 && cScoreMath < 180)
        {
            levelMath = "Level 8";
        }
        else if (cScoreMath >= 180 && cScoreMath < 225)
        {
            levelMath = "Level 9";
        }
        else if (cScoreMath >= 225 && cScoreMath < 275)
        {
            levelMath = "Level 10";
        }
        else if (cScoreMath >= 275 && cScoreMath < 330)
        {
            levelMath = "Level 11";
        }
        else if (cScoreMath >= 330 && cScoreMath < 390)
        {
            levelMath = "Level 12";
        }
        else if (cScoreMath >= 390 && cScoreMath < 455)
        {
            levelMath = "Level 13";
        }
        else if (cScoreMath >= 455 && cScoreMath < 525)
        {
            levelMath = "Level 14";
        }
        else if (cScoreMath >= 525 && cScoreMath < 600)
        {
            levelMath = "Level 15";
        }
        else if (cScoreMath >= 600 && cScoreMath < 680)
        {
            levelMath = "Level 16";
        }
        else if (cScoreMath >= 680 && cScoreMath < 765)
        {
            levelMath = "Level 17";
        }
        else if (cScoreMath >= 765 && cScoreMath < 855)
        {
            levelMath = "Level 18";
        }
        else if (cScoreMath >= 855 && cScoreMath < 955)
        {
            levelMath = "Level 19";
        }
        else if (cScoreMath >= 955 && cScoreMath <= 1000)
        {
            levelMath = "Level 20";
        }

        System.out.println("Current Level in Math: "+levelMath);


        progressStatusMath = 0;

                /*
                    A Thread is a concurrent unit of execution. It has its own call stack for
                    methods being invoked, their arguments and local variables. Each application
                    has at least one thread running when it is started, the main thread,
                    in the main ThreadGroup. The runtime keeps its own threads
                    in the system thread group.
                */
        // Start the lengthy operation in a background thread
        mathScore.setText("Your Score : "+progressStatusMath);
        mathLevel.setText("Your Level : "+levelMath);
        mathTarget.setText((1000-progressStatusMath)+" more points to Level 20");

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(progressStatusMath < cScoreMath){
                    // Update the progress status
                    progressStatusMath +=1;

                    // Try to sleep the thread for 20 milliseconds
                    try{
                        Thread.sleep(40);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }

                    // Update the progress bar
                    handler3.post(new Runnable() {
                        @Override
                        public void run() {
                            pb3.setProgress(progressStatusMath);
                            // Show the progress on TextView
                            mathScore.setText("Your Score : "+progressStatusMath);
                            mathLevel.setText("Your Level : "+levelMath);
                            if(cScoreStickman == 1000) {
                                mathTarget.setText("Congrats! You have reached level 20!");
                            }else{
                                mathTarget.setText((1000 - cScoreMath) + " more points to Level 20");
                            }

                        }
                    });
                }
            }
        }).start(); // Start the operation


    }
}
