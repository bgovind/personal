package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Random;

//Geo App Content screen Layout
public class continent extends AppCompatActivity {

    String country = "";
    String capital = "";
    String currency = "";
    String region = "";


    @Override
    protected void onPause() {
        super.onPause();
        MusicManager.pause();

    }
    @Override
    protected void onResume() {
        super.onResume();
        MusicManager.start(this, 2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_continent);
        final String s = getIntent().getStringExtra("continent");
        Random r = new Random();
        int randomInt = 0;
        MusicManager.start(this,2);

        TextView continentName = (TextView) findViewById(R.id.header);
        continentName.setText(s);

        TextView description = (TextView) findViewById(R.id.desc);

        //Continent Selection
        switch(s) {
            case "Asia":
                description.setText("Asia is Earth's largest and most populous continent, located primarily in the eastern and northern hemispheres and sharing the continental landmass of Eurasia with the continent of Europe and shares the continental landmass of Afro-Eurasia with both Europe and Africa. Asia covers an area of 44,579,000 square kilometres (17,212,000 sq mi), about 30% of Earth's total land area and 8.7% of the Earth's total surface area. The continent, which has long been home to the majority of the human population, was the site of many of the first civilizations. Asia is notable for not only its overall large size and population, but also dense and large settlements as well as vast barely populated regions within the continent of 4.4 billion people. Tap the button below to know about a country located in Asia.");
                randomInt = r.nextInt((14 - 1)+ 1) + 1;
                System.out.println(randomInt);
                break;
            case "Africa":
                description.setText("Africa is the world's second-largest and second-most-populous continent. At about 30.3 million km² (11.7 million square miles) including adjacent islands, it covers 6% of Earth's total surface area and 20.4 % of its total land area.With 1.2 billion people as of 2016, it accounts for about 16% of the world's human population.The continent is surrounded by the Mediterranean Sea to the north, both the Suez Canal and the Red Sea along the Sinai Peninsula to the northeast, the Indian Ocean to the southeast, and the Atlantic Ocean to the west. The continent includes Madagascar and various archipelagos.  Tap the button below to know about a country located in Africa.");
                randomInt = r.nextInt((24-15)+1) + 15;
                System.out.println(randomInt);
                break;
            case "Europe":
                description.setText("Europe is a continent that comprises the westernmost part of Eurasia. Europe is bordered by the Arctic Ocean to the north, the Atlantic Ocean to the west, and the Mediterranean Sea to the south. The eastern boundary with Asia is a historical and cultural construct, as there is no clear physical and geographical separation between them; Europe is generally considered as separated from Asia by the watershed divides of the Ural and Caucasus Mountains, the Ural River, the Caspian and Black Seas, and the waterways of the Turkish Straits. Tap the button below to know about a country located in Europe.");
                randomInt = r.nextInt((36 - 25)+1) + 25;
                System.out.println(randomInt);
                break;
            case "Australia":
                description.setText("Australia, sometimes known in technical contexts by the names Sahul, Australinea or Meganesia, to distinguish it from the Australian mainland, is a continent comprising mainland Australia, Tasmania, New Guinea, Seram, possibly Timor, and neighbouring islands. It is the smallest of the seven traditional continents in the English conception. The continent lies on a continental shelf overlain by shallow seas which divide it into several landmasses - the Arafura Sea and Torres Strait between mainland Australia and New Guinea, and Bass Strait between mainland Australia and Tasmania. When sea levels were lower during the Pleistocene ice age, including the Last Glacial Maximum about 18,000 BC, they were connected by dry land. During the past 10,000 years, rising sea levels overflowed the lowlands and separated the continent into today's low-lying arid to semi-arid mainland and the two mountainous islands of New Guinea and Tasmania. Tap the button below to know about a country located in Australia.");
                randomInt = r.nextInt((38 - 37 ) +1) + 37;
                System.out.println(randomInt);
                break;
            case "North America":
                description.setText("North America is a continent entirely within the Northern Hemisphere and almost all within the Western Hemisphere. It can also be considered a northern subcontinent of the Americas. It is bordered to the north by the Arctic Ocean, to the east by the Atlantic Ocean, to the west and south by the Pacific Ocean, and to the southeast by South America and the Caribbean Sea. North America covers an area of about 24,709,000 square kilometers (9,540,000 square miles), about 16.5% of the earth's land area and about 4.8% of its total surface. North America is the third largest continent by area, following Asia and Africa, and the fourth by population.  Tap the button below to know about a country located in North America");
                randomInt = r.nextInt((42 - 39) + 1) + 39;
                System.out.println(randomInt);
                break;
            case "South America":
                description.setText("South America is a continent located in the western hemisphere, mostly in the southern hemisphere, with a relatively small portion in the northern hemisphere. It is bordered on the west by the Pacific Ocean and on the north and east by the Atlantic Ocean; North America and the Caribbean Sea lie to the northwest. South America has an area of 17,840,000 square kilometers (6,890,000 sq mi). Its population as of 2005 has been estimated at more than 371,090,000. Tap the button below to know about a country located in South America");
                randomInt = r.nextInt((45 - 43)+1) + 43;
                System.out.println(randomInt);
                break;
        }

        System.out.println(randomInt);


        //Get Random Country Id from DB
        try {
            SQLiteDatabase llDB = (continent.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
            llDB.execSQL("CREATE TABLE IF NOT EXISTS countries(id INTEGER PRIMARY KEY, name varchar(50), capital varchar(50), currency varchar(100), region varchar(100))");
            Cursor c = llDB.rawQuery("SELECT * FROM countries where id = '" + randomInt + "'", null);

            int countryNameIndex = c.getColumnIndex("name");
            int capitalIndex = c.getColumnIndex("capital");
            int currencyIndex = c.getColumnIndex("currency");
            int regionIndex = c.getColumnIndex("region");
            if (c.moveToFirst()) {
                System.out.println("feeds true");
                c.moveToFirst();
                while(c != null)
                {

                    country = c.getString(countryNameIndex);
                    capital = c.getString(capitalIndex);
                    currency = c.getString(currencyIndex);
                    region = c.getString(regionIndex);

                    c.moveToNext();
                }

            } else {
                System.out.println("Error");
            }

        }catch(Exception e)
        {
            e.printStackTrace();
        }

        System.out.println(country);
        System.out.println(capital);
        System.out.println(currency);
        System.out.println(region);

        Button viewCountry = (Button) findViewById(R.id.go);

        viewCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), country.class);
                Bundle extras = new Bundle();
                extras.putString("continent",s);
                extras.putString("country", country);
                extras.putString("capital", capital);
                extras.putString("currency", currency);
                extras.putString("region", region);
                intent.putExtras(extras);
                startActivity(intent);
                finish();
            }
        });

    }

    //Handle Back Button
    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), geo.class));
        MusicManager.pause();
        finish();
    }
}
