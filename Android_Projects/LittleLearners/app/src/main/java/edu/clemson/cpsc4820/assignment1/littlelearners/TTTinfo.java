package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

//Info for Tic Tac Toe App
public class TTTinfo extends AppCompatActivity {

    @Override
    public void onBackPressed()
    {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tttinfo);
    }
}
