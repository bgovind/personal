package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.StringTokenizer;

//Math Blaster Game
public class mathBlasterGame extends AppCompatActivity {

        private int[] operators;
        private String expression;
        private int valueOne;
        private int valueTwo;
        private int answer;
        private int randomNum;
        private MediaPlayer correctSound;
        private MediaPlayer incorrectSound;
        private int height;
        private Runnable updateTaskTimer;
        private Handler updateTimeHandler;
        long timeInMilliseconds = 0L;
        long maxTime = 15000L;
        long updatedTime = 0L;
        long startTime = 0L;
        private TextView timerView;
        private int secs;
        private int points;
        private int correct;
        private int incorrect;
        private int roundPoints;
        private static Random random;
        private MediaPlayer mPlayer;
        private boolean isPlaying = false;

        @Override
        protected void onPause() {
            super.onPause();
            MusicManager.pause();

        }
        @Override
        protected void onResume() {
            super.onResume();
            MusicManager.start(this, 2);
        }

        //Handle Back Button
        @Override
        public void onBackPressed()
        {
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View promptView = layoutInflater.inflate(R.layout.mbexit, null);

            final AlertDialog alertD = new AlertDialog.Builder(this).create();

            Button btnAdd1 = (Button) promptView.findViewById(R.id.btnYes);

            Button btnAdd2 = (Button) promptView.findViewById(R.id.btnNo);

            btnAdd1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    alertD.dismiss();
                    updateTimeHandler.removeCallbacksAndMessages(null);
                    Intent intent = new Intent(getApplicationContext(), mathBlasterSplashScreen.class);
                    startActivity(intent);
                    //MusicManager.pause();
                    MusicManager.release();
                    finish();


                }
            });

            btnAdd2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    alertD.dismiss();

                }
            });

            alertD.setView(promptView);
            alertD.show();

        }


        //Restart Activity
        public void restart()
        {
            updateTimeHandler.removeCallbacksAndMessages(null);
            Intent intent = new Intent(this,mathBlasterGame.class);
            try {
                final SQLiteDatabase llDB = (mathBlasterGame.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                llDB.execSQL("CREATE TABLE IF NOT EXISTS mbcontent(localuser INTEGER PRIMARY KEY, points int, correct int, incorrect int)");
                Cursor c = llDB.rawQuery("SELECT * FROM mbcontent ORDER BY localuser ASC LIMIT 1", null);
                c.moveToFirst();
                if (c.getCount() == 0) {
                    try {
                        llDB.execSQL("INSERT INTO mbcontent(points, correct, incorrect) VALUES ('" + points + "', '" + correct + "','" + incorrect + "')");
                        System.out.println("insert new score");
                    } catch (SQLiteException e) {
                        System.out.println("Error adding new score");
                    }
                } else {
                    try {
                        int id = c.getInt(0);
                        llDB.execSQL("UPDATE mbcontent set points = '" + points + "' where localuser = '" + id + "'");
                        llDB.execSQL("UPDATE mbcontent set correct = '" + correct + "' where localuser = '" + id + "'");
                        llDB.execSQL("UPDATE mbcontent set incorrect = '" + incorrect + "' where localuser = '" + id + "'");
                        System.out.println("update score");
                    } catch (SQLiteException e) {
                        System.out.println("Error updating score");
                    }
                }
                c.close();
                llDB.close();
            }
            catch(SQLiteException e)
            {
                System.out.println("Start debug in restart");
            }

            //intent.putExtra("points", points);
            //intent.putExtra("correct", correct);
            //intent.putExtra("incorrect",incorrect);
            //intent.putExtra("music", (Serializable) mPlayer);
            MusicManager.pause();
            startActivity(intent);
            finish();
        }







        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_math_blaster_game);
            MusicManager.start(this,2);

            points = 0;
            correct = 0;
            incorrect = 0;
            try {
                final SQLiteDatabase llDB = (mathBlasterGame.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                llDB.execSQL("CREATE TABLE IF NOT EXISTS mbcontent(localuser INTEGER PRIMARY KEY, points int, correct int, incorrect int)");
                Cursor c = llDB.rawQuery("SELECT * FROM mbcontent ORDER BY localuser ASC LIMIT 1", null);
                if (c.moveToFirst()) {
                    int temp = 0;
                    temp = c.getColumnIndex("points");
                    points = c.getInt(temp);
                    temp = c.getColumnIndex("correct");
                    correct = c.getInt(temp);
                    temp = c.getColumnIndex("incorrect");
                    incorrect = c.getInt(temp);
                    System.out.println(points + " " + correct + " " + incorrect);
                    c.close();
                    llDB.close();
                }
            }
            catch(SQLiteException e)
            {
                System.out.println("Start debug in onCreate, something went wrong.");
            }

            //points = getIntent().getIntExtra("points", 0);
            //correct = getIntent().getIntExtra("correct", 0);
            //incorrect = getIntent().getIntExtra("incorrect", 0);
            TextView mbSB = (TextView)findViewById(R.id.mbsb);
            mbSB.setText("Points: " +  points + " #Right: " + correct + " #Missed: " + incorrect);
            mbSB.setTypeface(Typeface.create("casual",Typeface.NORMAL));

            answer = 100;

            //initialize the sound effects
            correctSound = MediaPlayer.create(this, R.raw.mbcorrect);
            correctSound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();

                }});
            incorrectSound = MediaPlayer.create(this, R.raw.mbwrong);
            incorrectSound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();

                }});
            //initialize the array to hold ascii values of the basic math operators
            operators = new int[4];
            operators[0] = 42; operators[1] = 43; operators[2] = 45; operators[3] = 47;

            //set random number variable to zero
            randomNum = 0;

            //initialize a new random number generator
            random = new Random();

            //generate and initialize 2 random values for the equation
            //25 is just a base number I chose for the generator, I didn't
            //want the multiplication to be too hard


            //generate and initialize the operator for the equation
            randomNum = random.nextInt(4);

            while(answer >= 100) {
                //decide how to compute the answer based on the operator generated by the RNG and calculate answer
                switch (operators[randomNum]) {
                    case 42: {
                        valueOne = random.nextInt(10) + 1;
                        valueTwo = random.nextInt(10) + 1;
                        answer = valueOne * valueTwo;
                        expression = valueOne + " " + Character.toString((char) operators[randomNum]) + " " + valueTwo + " = ";
                        break;
                    }
                    case 43: {
                        valueOne = random.nextInt(48) + 1;
                        valueTwo = random.nextInt(48) + 1;
                        answer = valueOne + valueTwo;
                        expression = valueOne + " " + Character.toString((char) operators[randomNum]) + " " + valueTwo + " = ";
                        break;
                    }
                    case 45: {
                        valueOne = random.nextInt(98) + 1;
                        valueTwo = random.nextInt(98) + 1;
                        if (valueOne >= valueTwo) {
                            answer = valueOne - valueTwo;
                            expression = valueOne + " " + Character.toString((char) operators[randomNum]) + " " + valueTwo + " = ";
                            break;
                        } else {
                            answer = valueTwo - valueOne;
                            expression = valueTwo + " " + Character.toString((char) operators[randomNum]) + " " + valueOne + " = ";
                            break;
                        }
                    }
                    case 47: {
                        valueOne = random.nextInt(100) + 1;
                        valueTwo = random.nextInt(100) + 1;
                        while ((valueOne % valueTwo) != 0){
                            valueOne = random.nextInt(100) + 1;
                            valueTwo = random.nextInt(100) + 1;
                            if((valueOne == 0) || (valueTwo == 0))
                            {
                                valueOne = random.nextInt(100) + 1;
                                valueTwo = random.nextInt(100) + 1;
                            }
                        }
                        expression = valueOne + " " + Character.toString((char) operators[randomNum]) + " " + valueTwo + " = ";
                        answer = valueOne / valueTwo;
                        break;
                    }
                }
            }
            System.out.println(expression + " " + answer);

            //get the view that stores the expression
            LinearLayout expressionHolder = (LinearLayout) findViewById(R.id.expressionholder);

            //set parameters for that view and its children
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, .25f);
            param.gravity = Gravity.CENTER;
            param.setMargins(25,25,25,0);

            //initialize the array that will hold the text views that display the equation to the user
            TextView[] expressions = new TextView[4];

            //split the expression into individual pieces
            StringTokenizer tokens = new StringTokenizer(expression);

            //for each element of the expression, create a new text view, set the text and add it to the parent view
            for (int i = 0; i < 4; i++) {
                expressions[i] = new TextView(this);
                expressions[i].setText("" + tokens.nextToken());
                expressions[i].setTextSize(25);
                expressions[i].setTextColor(Color.BLACK);
                expressions[i].setLayoutParams(param);
                expressionHolder.addView(expressions[i]);
            }

            //get the device height
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

            //set height equal to the device height
            //this is so I can control how high or low the buttons travel on the screen
            height = displayMetrics.heightPixels;


            //initialize the buttons that will be used for the game
            Button one = (Button)findViewById(R.id.value1);
            Button two = (Button)findViewById(R.id.value2);
            Button three = (Button)findViewById(R.id.value3);
            Button four = (Button)findViewById(R.id.value4);
            Button five = (Button)findViewById(R.id.value5);

            //initialize the array that will hold 4 random values and the answer
            ArrayList<Integer> shuffle = new ArrayList<>();

            //for each button generate a random value within a range of the answer
            //no real method to this, just some arbitrary values
            for(int i = 0; i < 4; i++)
            {
                if(answer < 15)
                {
                    if(answer == 0) {
                        randomNum = random.nextInt(10) + 1;
                    }
                    else
                    {
                        int tripleAnswer = answer * 3;
                        randomNum = random.nextInt(tripleAnswer) + 1;
                    }
                }
                else if(answer < 50)
                {
                    if(answer == 0) {
                        randomNum = random.nextInt(10) + 1;
                    }
                    else
                    {
                        int doubleAnswer = answer * 2;
                        randomNum = random.nextInt(doubleAnswer) + 1;
                    }

                }
                else
                {
                    randomNum = random.nextInt(100) + 1;
                }

                //add each value to the array
                shuffle.add(randomNum);
            }

            //add the answer to the array
            shuffle.add(answer);

            //shuffle the array with the collections library
            Collections.shuffle(shuffle);

            //initialize the array that will hold the buttons used for the game
            final Button [] buttonNames = new Button[5];

            //declare the object animator
            ObjectAnimator animationOne;

            //set a reference to each of the buttons
            buttonNames[0] = one; buttonNames[1] = two; buttonNames[2] = three; buttonNames[3] = four; buttonNames[4] = five;

            //for each button, set its display value and its onclick listener
            //this also checks to see if the guess is right or wrong
            for (int i = 0; i < 5; i++) {
                buttonNames[i].setText("" + shuffle.get(i));
                buttonNames[i].setTextSize(24);
                buttonNames[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Button temp = (Button) v;
                        Toast toast;
                        if (Integer.parseInt(temp.getText().toString()) == answer) {
                            //toast = Toast.makeText(getApplicationContext(), "Correct! Loading next question....", Toast.LENGTH_SHORT);
                            correctSound.start();
                            //toast.show();
                            correct++;
                            randomNum = random.nextInt(secs) + 1;
                            roundPoints = randomNum * secs;
                            points += roundPoints;

                            int cScore = 0;
                            String level = "";

                            //Set Score in DB for Tracking Progress
                            try{
                                SQLiteDatabase llDB = (mathBlasterGame.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                                llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer, foreign key (game_name) references game_list(game_name))");
                                Cursor c = llDB.rawQuery("SELECT * FROM progress WHERE game_name = 'math'", null);
                                int cScoreIndex = c.getColumnIndex("cScore");
                                if (c.getCount()!=0) {
                                    System.out.println("Mathblaster Score exists");
                                    c.moveToFirst();
                                    cScore = c.getInt(cScoreIndex);


                                } else {
                                    System.out.println("Error");
                                }
                                c.close();
                                llDB.close();
                            }catch(Exception e)
                            {
                                e.printStackTrace();
                            }
                            System.out.println("Old Math Score: "+cScore);

                            cScore = cScore + 1;

                            if(cScore >= 1000)
                            {
                                cScore = 1000;
                            }

                            if(cScore < 5)
                            {
                                level = "Level 1";
                            }
                            else if (cScore >= 5 || cScore < 15)
                            {
                                level = "Level 2";
                            }
                            else if (cScore >= 15 || cScore < 30)
                            {
                                level = "Level 3";
                            }
                            else if (cScore >= 30 || cScore < 50)
                            {
                                level = "Level 4";
                            }
                            else if (cScore >= 50 || cScore < 75)
                            {
                                level = "Level 5";
                            }
                            else if (cScore >= 75 || cScore < 105)
                            {
                                level = "Level 6";
                            }
                            else if (cScore >= 105 || cScore < 140)
                            {
                                level = "Level 7";
                            }
                            else if (cScore >= 140 || cScore < 180)
                            {
                                level = "Level 8";
                            }
                            else if (cScore >= 180 || cScore < 225)
                            {
                                level = "Level 9";
                            }
                            else if (cScore >= 225 || cScore < 275)
                            {
                                level = "Level 10";
                            }
                            else if (cScore >= 275 || cScore < 330)
                            {
                                level = "Level 11";
                            }
                            else if (cScore >= 330 || cScore < 390)
                            {
                                level = "Level 12";
                            }
                            else if (cScore >= 390 || cScore < 455)
                            {
                                level = "Level 13";
                            }
                            else if (cScore >= 455 || cScore < 525)
                            {
                                level = "Level 14";
                            }
                            else if (cScore >= 525 || cScore < 600)
                            {
                                level = "Level 15";
                            }
                            else if (cScore >= 600 || cScore < 680)
                            {
                                level = "Level 16";
                            }
                            else if (cScore >= 680 || cScore < 765)
                            {
                                level = "Level 17";
                            }
                            else if (cScore >= 765 || cScore < 855)
                            {
                                level = "Level 18";
                            }
                            else if (cScore >= 855 || cScore < 955)
                            {
                                level = "Level 19";
                            }
                            else if (cScore >= 955 || cScore <= 1000)
                            {
                                level = "Level 20";
                            }

                            System.out.println("Current Level : "+level);

                            try{
                                SQLiteDatabase llDB = (mathBlasterGame.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                                System.out.println("update stickman cScore");
                                llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer, foreign key (game_name) references game_list(game_name))");
                                Cursor c = llDB.rawQuery("SELECT * FROM progress WHERE game_name = 'math'", null);
                                if(c.getCount() == 0)
                                {
                                    llDB.execSQL("INSERT INTO progress(game_name, cScore) VALUES ( 'math', '"+ cScore +"')");
                                }
                                else {
                                    llDB.execSQL("UPDATE progress set cScore = '"+cScore+"' where game_name = 'math'");
                                }
                                llDB.close();
                            }catch(Exception e)
                            {
                                e.printStackTrace();
                            }

                            System.out.println("New mathBlaster Score: "+cScore);
                        } else {
                            //toast = Toast.makeText(getApplicationContext(), "Wrong answer! Here is a new Question!", Toast.LENGTH_SHORT);
                            incorrectSound.start();
                            incorrect++;
                            //toast.show();
                            randomNum = random.nextInt(secs) + 1;
                            roundPoints = randomNum * secs;
                            points -= roundPoints;

                            int cScore = 0;
                            String level = "";

                            try {
                                SQLiteDatabase llDB = (mathBlasterGame.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                                llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer, foreign key (game_name) references game_list(game_name))");
                                Cursor c = llDB.rawQuery("SELECT * FROM progress where game_name = 'math'", null);
                                int cScoreIndex = c.getColumnIndex("cScore");
                                if (c.getCount() > 0) {
                                    System.out.println("Mathblaster Score exists");
                                    c.moveToFirst();
                                    cScore = c.getInt(cScoreIndex);
                                    c.close();
                                } else {
                                    System.out.println("Error");
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            System.out.println("Old Math Score: " + cScore);


                            if (cScore != 0) cScore = cScore - 1;

                            if (cScore >= 1000) {
                                cScore = 1000;
                            }

                            if (cScore < 5) {
                                level = "Level 1";
                            } else if (cScore >= 5 || cScore < 15) {
                                level = "Level 2";
                            } else if (cScore >= 15 || cScore < 30) {
                                level = "Level 3";
                            } else if (cScore >= 30 || cScore < 50) {
                                level = "Level 4";
                            } else if (cScore >= 50 || cScore < 75) {
                                level = "Level 5";
                            } else if (cScore >= 75 || cScore < 105) {
                                level = "Level 6";
                            } else if (cScore >= 105 || cScore < 140) {
                                level = "Level 7";
                            } else if (cScore >= 140 || cScore < 180) {
                                level = "Level 8";
                            } else if (cScore >= 180 || cScore < 225) {
                                level = "Level 9";
                            } else if (cScore >= 225 || cScore < 275) {
                                level = "Level 10";
                            } else if (cScore >= 275 || cScore < 330) {
                                level = "Level 11";
                            } else if (cScore >= 330 || cScore < 390) {
                                level = "Level 12";
                            } else if (cScore >= 390 || cScore < 455) {
                                level = "Level 13";
                            } else if (cScore >= 455 || cScore < 525) {
                                level = "Level 14";
                            } else if (cScore >= 525 || cScore < 600) {
                                level = "Level 15";
                            } else if (cScore >= 600 || cScore < 680) {
                                level = "Level 16";
                            } else if (cScore >= 680 || cScore < 765) {
                                level = "Level 17";
                            } else if (cScore >= 765 || cScore < 855) {
                                level = "Level 18";
                            } else if (cScore >= 855 || cScore < 955) {
                                level = "Level 19";
                            } else if (cScore >= 955 || cScore <= 1000) {
                                level = "Level 20";
                            }

                            System.out.println("Current Level : " + level);

                            try {
                                SQLiteDatabase llDB = (mathBlasterGame.this).openOrCreateDatabase("llDB", MODE_PRIVATE, null);
                                llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer, foreign key (game_name) references game_list(game_name))");
                                Cursor c = llDB.rawQuery("SELECT * FROM progress WHERE game_name = 'math'", null);
                                if (c.getCount() == 0) {
                                    //llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer)");
                                    llDB.execSQL("INSERT INTO mbcontent(game_name, cScore) VALUES (math '" + cScore + "')");
                                } else {
                                    //llDB.execSQL("CREATE TABLE IF NOT EXISTS progress(id INTEGER PRIMARY KEY, game_name varchar(20), cScore integer)");
                                    llDB.execSQL("UPDATE progress set cScore = '" + cScore + "' where game_name = 'math'");
                                }
                                llDB.close();
                            }
                            catch(SQLiteException e)
                            {
                                System.out.println("error 3");
                            }
                            System.out.println("New mathBlaster Score: "+cScore);
                        }


                        restart();
                        //toast.cancel();
                    }
                });

                //set the animation for each button, movement speed is random, although position is fixed
                animationOne = ObjectAnimator.ofFloat(buttonNames[i], "translationY", 0, height - 400);
                animationOne.setDuration(randomNum = random.nextInt((10000 - 6000) + 1) + 5000);
                animationOne.setRepeatCount(ObjectAnimator.INFINITE);
                animationOne.setRepeatMode(ObjectAnimator.REVERSE);
                animationOne.start();

                updateTimeHandler = new Handler();
                timerView = (TextView)findViewById(R.id.mbtimer);
                startTime = SystemClock.uptimeMillis();
                timerView.setText("" + String.format("%02d", 15));
                updateTaskTimer = new Runnable() {

                    public void run() {

                        timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

                        if(timeInMilliseconds > 14000L)
                        {
                            for (Button temp:buttonNames) {
                                temp.setEnabled(false);
                            }
                        }

                        if(timeInMilliseconds >= 15000L)
                        {
                                                        updateTimeHandler.removeCallbacks(this);
                            Toast.makeText(getBaseContext(),"Sorry time is up!", Toast.LENGTH_SHORT).show();
                            incorrect++;
                            restart();
                        }
                        else
                        {
                            updatedTime = maxTime - timeInMilliseconds;
                            secs = (int) (updatedTime / 1000);
                            secs = secs % 60;
                            timerView.setText("" + String.format("%02d", secs));
                            updateTimeHandler.postDelayed(this, 500);
                        }
                    }
                };
                updateTimeHandler.postDelayed(updateTaskTimer, 500);

            }
        }


    }


