package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import static android.R.attr.id;

/**
 * Created by Bharat Kumar on 4/6/2017.
 */

//Helper Adapter Class for Customer ListView in Quiz
public class CustomListAdapterQuiz extends ArrayAdapter<String>{

    private final Activity context;
    private final ArrayList<String> quizname;
    private final ArrayList<String> postedby;
    private final ArrayList<String> postedon;


    public CustomListAdapterQuiz(Activity context, ArrayList<String> quizname, ArrayList<String> postedby, ArrayList<String> postedon)
    {
        super(context, R.layout.quizlist, quizname);

        this.context=context;
        this.quizname=quizname;
        this.postedby= postedby;
        this.postedon= postedon;
    }

    public View getView(int position, View v, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.quizlist, null,true);

        TextView quizName = (TextView) rowView.findViewById(R.id.quizitem);
        TextView postedBy = (TextView) rowView.findViewById(R.id.quizpostedby);
        TextView postedOn = (TextView) rowView.findViewById(R.id.customquizpostedon);


        quizName.setText("Quiz Name :"+quizname.get(position).toString());
        postedBy.setText("Quiz Posted By : "+postedby.get(position).toString());
        postedOn.setText("Quiz Posted on : "+postedon.get(position).toString());

        return rowView;

    };
}
