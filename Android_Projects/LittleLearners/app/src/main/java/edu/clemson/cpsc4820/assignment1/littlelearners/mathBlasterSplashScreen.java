package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//Splash Screen for Math Blaster
public class mathBlasterSplashScreen extends AppCompatActivity {

    private Button start;
    private Button info;

    //Handle Back Button
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, mainMenuGameSelect.class);
        startActivity(intent);
        MusicManager.release();
        finish();
    }

    //Start MB
    public void playMB()
    {
        Intent intent = new Intent(this, mathBlasterGame.class);
        startActivity(intent);
        MusicManager.release();
        finish();
    }

    //MB info
    public void mbInfo()
    {
        Intent intent = new Intent(this, mathBlasterInfoScreen.class);
        startActivity(intent);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_math_blaster_splash_screen);
        start = (Button)findViewById(R.id.mbStart);
        start.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                playMB();
            }
        });
        info = (Button)findViewById(R.id.mbinfo);
        info.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mbInfo();
            }
        });
        MusicManager.start(this,0);
    }
}
