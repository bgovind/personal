package edu.clemson.cpsc4820.assignment1.littlelearners;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//Main Menu Select Game/Activity
public class mainMenuGameSelect extends AppCompatActivity {

    //Start FlashCards
    public void playFlashCards()
    {
        Intent intent = new Intent(this, flashCardSplashScreen.class);
        startActivity(intent);
    }
    //Start Stickman
    public void playStickMan()
    {
        Intent intent = new Intent(this, stickManSplashScreen.class);
        startActivity(intent);
    }
    //Start Tic Tac Toe
    public void playTicTacToe()
    {
        Intent intent = new Intent(this, TTTSplashScreen.class);
        startActivity(intent);
    }
    //Play Math Blaster
    public void playMathBlaster()
    {
        //change mainmenuinfoscreen.class to yourclass.class when finished
        Intent intent = new Intent(this, mathBlasterSplashScreen.class);
        startActivity(intent);
    }
    //Play SlapJack
    public void playSlapJack()
    {
        //change mainmenuinfoscreen.class to yourclass.class when finished
        Intent intent = new Intent(this, slapjackstart.class);
        startActivity(intent);
    }
    //Play Geography
    public void playGeography()
    {
        //change mainmenuinfoscreen.class to yourclass.class when finished
        Intent intent = new Intent(this, geoSplash.class);
        startActivity(intent);
    }
    //Take a Quiz
    public void playQuiz()
    {
        Intent intent = new Intent(this, quizList.class);
        startActivity(intent);
    }
    //Select Game By Topic
    public void selectTopic()
    {
        //change mainmenuinfoscreen.class to yourclass.class when finished
        Intent intent = new Intent(this, mainMenuTopicSelect.class);
        startActivity(intent);
    }
    //Handle BackButton
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, HomeScreen.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainmenugameselect);

        Button flashcard = (Button) findViewById(R.id.flashcards);
        flashcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playFlashCards();
                finish();
            }


        });

        Button otherOptions = (Button) findViewById(R.id.otheroption);
        otherOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectTopic();
                finish();
            }


        });

        Button stickman = (Button) findViewById(R.id.stickman);
        stickman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playStickMan();
                finish();
            }


        });

        Button tictactoe = (Button) findViewById(R.id.tictactoe);
        tictactoe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playTicTacToe();
                finish();
            }


        });

        Button mathblaster = (Button) findViewById(R.id.mathblaster);
        mathblaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playMathBlaster();
                finish();
            }


        });

        Button slapjack = (Button) findViewById(R.id.slapjack);
        slapjack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playSlapJack();
                finish();
            }


        });

        Button geography = (Button) findViewById(R.id.geography);
        geography.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playGeography();
                finish();
            }
        });

        Button quiz = (Button) findViewById(R.id.quiz);
        quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playQuiz();
                finish();
            }
        });
    }
}
