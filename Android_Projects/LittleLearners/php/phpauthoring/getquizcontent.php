<!DOCTYPE html>
<html>
<body>
</h3>Get quiz contents for a particular quiz</h3>
<p>-Enter quizname and click submit.</p>
<p>-If quiz name is correct and already exists you will see table below containing quiz name, questions 1 to 5, options 1 - 4 and correct answer option ID for each question .</p>
<p>-If quiz name is incorrect then you will see fail - response error.</p>

<form action="" method="post">
	<p style="margin: 0.5cm 0cm auto 1cm;">Enter Quiz Name below</p>
	<table style="margin: 0.5cm 0cm auto 1cm; text-align: left;">
	<tr><td>Quiz Name:* </td><td><input type="text" name="quizname"></td></tr>
	<tr><td></td><td><input name="submit" type="submit" value="Submit"></td></tr>
	</table>
</form>

<h4> Quiz content </h4>
<table border="1" style="text-align:center;">

<?php

	require_once "connection.php";
	//header('Content-Type: application/json');
	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function get_quizcontent($quizname)
		{
			$query = "select * from quiz_content where quizname = '".$quizname."'";
			$result = pg_query($this->connection, $query);
			if ($result)
			{
				if (pg_num_rows($result) == 0)
				{
					$json['noitems'] = 'no quiz of this name exists';
					$object['1'] = $json;
					echo json_encode($object);
				}
				else{
					
				$itr = 1;
				?>
				<table border="1" style="text-align:center;">
				<tr></th><th><b>Question</b></th><th><b>Option1</b></th><th><b>Option2</b></th><th><b>Option3</b></th><th><b>Option4</b></th><th><b>Correct Option</b></th></tr>
				<?php
				
				while($row = pg_fetch_row($result))
				{
					$question = $row[2];
					$op1 = $row[3];
					$op2 = $row[4];
					$op3 = $row[5];
					$op4 = $row[6];
					$answer = $row[7];
					
					$json['success'] = 'success';
					$json['qid'] = $itr;
					$json['question'] = $question;
					$json['op1'] = $op1;
					$json['op2'] = $op2;
					$json['op3'] = $op3;
					$json['op4'] = $op4;
					$json['answer'] = $answer ;
					
					?>
					<tr>
					<td><?php echo $question; ?></td>
					<td><?php echo $op1; ?></td>
					<td><?php echo $op2; ?></td>
					<td><?php echo $op3; ?></td>
					<td><?php echo $op4; ?></td>
					<td><?php echo $answer; ?></td>
					</tr>
					<?php
					
					$object[$itr] = $json;
						
					$itr = $itr+1;
				}
				?>
				</table>
				<?php
				//echo json_encode($object);
				}
				
			}
			else
			{
				$json['fail'] = 'response error';
				echo json_encode($json);
			}
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	if(isset($_POST['quizname']))
	{
		$quizname = $_POST['quizname'];
		
		if (!empty($quizname))
		{
			$user -> get_quizcontent($quizname);
		}
		else
		{
			$json['error'] = 'invalid request';
			echo json_encode($json);
		}
	}
		
	
?>

</body>

</html>