
<html>
<title>Littlet Learner - Get all Quiz List</title>
<body>
<h3>CPSC 682 - Spring 2017 - Assignment 3a</h3>
<h4>Bharat Kumar Govindaradjou, Hannah Haire, Greg Francis</h4>

<h4> All Quizzes Posted </h4>
<?php

	require_once "connection.php";
	//header('Content-Type: application/json');
	
	$_POST['authorkey'] = "2017";
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function get_quizlist()
		{
			$query = "select * from posted_quizzes";
			$result = pg_query($this->connection, $query);
			if ($result)
			{
				if (pg_num_rows($result) == 0)
				{
					$json['noitems'] = 'not quizzes posted so far';
					$object['1'] = $json;
					echo json_encode($object);
				}
				else{
					?>
				<table border="1" style="text-align:center;">
				<tr></th><th><b>Quiz Name</b></th><th><b>Posted by</b></th><th><b>Posted On</b></th></tr>
				<?php
					
				$itr = 1;
				while($row = pg_fetch_row($result))
				{
					$quizname = $row[1];
					$posted_by = $row[2];
					$posted_on = $row[3];
					
					
					$json['success'] = 'quizzes loaded';
					$json['id'] = $itr ;
					$json['quizName'] = $quizname ;
					$json['posted_by'] = $posted_by ;
					$json['posted_on'] = $posted_on ;
					?>
					<tr>
					<td><?php echo $quizname; ?></td>
					<td><?php echo $posted_by; ?></td>
					<td><?php echo $posted_on; ?></td>
					</tr>
					<?php
					
					$object[$itr] = $json;
						
					$itr = $itr+1;
					
					
				}
				?>
				</table>
				<?php
				//echo json_encode($object);
				}
				
			}
			else
			{
				$json['fail'] = 'response error';
				echo json_encode($json);
			}
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	
	if(isset($_POST['authorkey']))
	{
		$authorkey = $_POST['authorkey'];
		
		if (!empty($authorkey))
		{
			$user -> get_quizlist();
		}
		else
		{
			$json['error'] = 'invalid request';
			echo json_encode($json);
		}
	}
	
	
		
	
?>