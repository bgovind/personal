<?php

	require_once "connection.php";
	header('Content-Type: application/json');
	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function get_quizcontent($quizname)
		{
			$query = "select * from quiz_content where quizname = '".$quizname."'";
			$result = pg_query($this->connection, $query);
			if ($result)
			{
				if (pg_num_rows($result) == 0)
				{
					$json['noitems'] = 'no quiz of this name exists';
					$object['1'] = $json;
					echo json_encode($object);
				}
				else{
					
				$itr = 1;
				while($row = pg_fetch_row($result))
				{
					$question = $row[2];
					$op1 = $row[3];
					$op2 = $row[4];
					$op3 = $row[5];
					$op4 = $row[6];
					$answer = $row[7];
					
					$json['success'] = 'success';
					$json['qid'] = $itr;
					$json['question'] = $question;
					$json['op1'] = $op1;
					$json['op2'] = $op2;
					$json['op3'] = $op3;
					$json['op4'] = $op4;
					$json['answer'] = $answer ;
					
					$object[$itr] = $json;
						
					$itr = $itr+1;
				}
				echo json_encode($object);
				}
				
			}
			else
			{
				$json['fail'] = 'response error';
				echo json_encode($json);
			}
			
			pg_close($this->connection);
		}
	}
	
	$user = new User();
	if(isset($_POST['quizname']))
	{
		$quizname = $_POST['quizname'];
		
		if (!empty($quizname))
		{
			$user -> get_quizcontent($quizname);
		}
		else
		{
			$json['error'] = 'invalid request';
			echo json_encode($json);
		}
	}
		
	
?>