<!DOCTYPE html>
<html>
<body>
</h3>Post your quiz here for Little Learner</h3>
<p>-Each quiz will have 5 questions and each question will have 4 options.</p>

<form action="" method="post">
	<p style="margin: 0.5cm 0cm auto 1cm;">Enter quiz content here. Fields marked * are mandatory.</p>
	<table style="margin: 0.5cm 0cm auto 1cm; text-align: left;">
	<tr><td>Your Name:* </td><td><input type="text" name="postedby"></td></tr>
	<tr><td>Quiz Name:* </td><td><input type="text" name="quizname"></td></tr>
	</table>
	<br><br>
	<table>
	<tr><td>Question 1:* </td><td><input type="text" name="q1" size="100"></td></tr>
	<tr><td>Option 1:* </td><td><input type="text" name="op11" size="20"></td></tr>
	<tr><td>Option 2:* </td><td><input type="text" name="op12" size="20"></td></tr>
	<tr><td>Option 3:* </td><td><input type="text" name="op13" size="20"></td></tr>
	<tr><td>Option 4:* </td><td><input type="text" name="op14" size="20"></td></tr>
	<tr><td>Answer Question 1:* </td><td> 
			<select name="a1">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
		</select></td></tr>
	</table>
	<br><br>
	<table>
	<tr><td>Question 2:* </td><td><input type="text" name="q2" size="100"></td></tr>
	<tr><td>Option 1:* </td><td><input type="text" name="op21" size="20"></td></tr>
	<tr><td>Option 2:* </td><td><input type="text" name="op22" size="20"></td></tr>
	<tr><td>Option 3:* </td><td><input type="text" name="op23" size="20"></td></tr>
	<tr><td>Option 4:* </td><td><input type="text" name="op24" size="20"></td></tr>
	<tr><td>Answer Question 2:* </td><td> 
			<select name="a2">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
		</select></td></tr>
	</table>
	<br><br>
	<table>
	<tr><td>Question 3:* </td><td><input type="text" name="q3" size="100"></td></tr>
	<tr><td>Option 1:* </td><td><input type="text" name="op31" size="20"></td></tr>
	<tr><td>Option 2:* </td><td><input type="text" name="op32" size="20"></td></tr>
	<tr><td>Option 3:* </td><td><input type="text" name="op33" size="20"></td></tr>
	<tr><td>Option 4:* </td><td><input type="text" name="op34" size="20"></td></tr>
	<tr><td>Answer Question 3:* </td><td> 
			<select name="a3">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
		</select></td></tr>
	</table>
	<br><br>
	<table>
	<tr><td>Question 4:* </td><td><input type="text" name="q4" size="100"></td></tr>
	<tr><td>Option 1:* </td><td><input type="text" name="op41" size="20"></td></tr>
	<tr><td>Option 2:* </td><td><input type="text" name="op42" size="20"></td></tr>
	<tr><td>Option 3:* </td><td><input type="text" name="op43" size="20"></td></tr>
	<tr><td>Option 4:* </td><td><input type="text" name="op44" size="20"></td></tr>
	<tr><td>Answer Question 4:* </td><td> 
			<select name="a4">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
		</select></td></tr>
	</table>
	<br><br>
	<table>
	<tr><td>Question 5:* </td><td><input type="text" name="q5" size="100"></td></tr>
	<tr><td>Option 1:* </td><td><input type="text" name="op51" size="20"></td></tr>
	<tr><td>Option 2:* </td><td><input type="text" name="op52" size="20"></td></tr>
	<tr><td>Option 3:* </td><td><input type="text" name="op53" size="20"></td></tr>
	<tr><td>Option 4:* </td><td><input type="text" name="op54" size="20"></td></tr>
	<tr><td>Answer Question 5:* </td><td> 
			<select name="a5">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
		</select></td></tr>
	<tr><td></td><td></td></tr>
	<tr><td></td><td><input name="submit" type="submit" value="Post Quiz"></td></tr>
	</table>
</form>


<?php

	require_once "connection.php";
	//header('Content-Type: application/json;charset=utf-8');
	
	
	class User 
	{
		private $db;
		private $connection;
		
		
		function __construct()
		{
			$this->db = new DB_Connection();
			$this->connection = $this->db->get_connection();
		}
		
		public function post_quiz($postedby, $quizname, $q1, $q2, $q3, $q4, $q5, $op11, $op12, $op13, $op14, $op21, $op22, $op23, $op24,
							   $op31, $op32, $op33, $op34, $op41, $op42, $op43, $op44, $op51, $op52, $op53, $op54,
							   $a1, $a2, $a3, $a4, $a5)
		{
			$checkQuizname  = "select * from posted_quizzes where name='".$quizname."'";
			$result = pg_query($checkQuizname);
			if (pg_num_rows($result) > 0)
			{
				echo "Quiz Name already exists. Post a differet name.";
			}
			else
			{
				$createQuiz = "insert into posted_quizzes(name, posted_by, posted_on) values('".$quizname."', '".$postedby."', now()::timestamp(0) without time zone)";
				pg_query($createQuiz);
				$postq1 = "insert into quiz_content (quizname, question, op1, op2, op3, op4, answer) values ('".$quizname."', '".$q1."', '".$op11."', '".$op12."', '".$op13."', '".$op14."', '".$a1."')";
				$postq2 = "insert into quiz_content (quizname, question, op1, op2, op3, op4, answer) values ('".$quizname."', '".$q2."', '".$op21."', '".$op22."', '".$op23."', '".$op24."', '".$a2."')";
				$postq3 = "insert into quiz_content (quizname, question, op1, op2, op3, op4, answer) values ('".$quizname."', '".$q3."', '".$op31."', '".$op32."', '".$op33."', '".$op34."', '".$a3."')";
				$postq4 = "insert into quiz_content (quizname, question, op1, op2, op3, op4, answer) values ('".$quizname."', '".$q4."', '".$op41."', '".$op42."', '".$op43."', '".$op44."', '".$a4."')";
				$postq5 = "insert into quiz_content (quizname, question, op1, op2, op3, op4, answer) values ('".$quizname."', '".$q5."', '".$op51."', '".$op52."', '".$op53."', '".$op54."', '".$a5."')";
				pg_query($postq1);
				pg_query($postq2);
				pg_query($postq3);
				pg_query($postq4);
				pg_query($postq5);
				echo "Quiz has been posted successfully";
			}
			
			pg_close($this->connection);
		}
		
	}
	
	$user = new User();
	if(isset($_POST['postedby'], $_POST['quizname'], 
				$_POST['q1'], $_POST['q2'], $_POST['q3'], $_POST['q4'], $_POST['q5'], 
				$_POST['op11'], $_POST['op12'], $_POST['op13'], $_POST['op14'], 
				$_POST['op21'], $_POST['op22'], $_POST['op23'], $_POST['op24'],
				$_POST['op31'], $_POST['op32'], $_POST['op33'], $_POST['op34'],
				$_POST['op41'], $_POST['op42'], $_POST['op43'], $_POST['op44'],
				$_POST['op51'], $_POST['op52'], $_POST['op53'], $_POST['op54'],
				$_POST['a1'], $_POST['a2'], $_POST['a3'], $_POST['a4']))
	{
		$postedby = $_POST['postedby'];
		$quizname = $_POST['quizname'];
		$q1 = $_POST['q1'];
		$q2 = $_POST['q2'];
		$q3 = $_POST['q3'];
		$q4 = $_POST['q4'];
		$q5 = $_POST['q5'];
		$op11 = $_POST['op11'];
		$op12 = $_POST['op12'];
		$op13 = $_POST['op13'];
		$op14 = $_POST['op14'];
		$op21 = $_POST['op21'];
		$op22 = $_POST['op22'];
		$op23 = $_POST['op23'];
		$op24 = $_POST['op24'];
		$op31 = $_POST['op31'];
		$op32 = $_POST['op32'];
		$op33 = $_POST['op33'];
		$op34 = $_POST['op34'];
		$op41 = $_POST['op41'];
		$op42 = $_POST['op32'];
		$op43 = $_POST['op43'];
		$op44 = $_POST['op44'];
		$op51 = $_POST['op51'];
		$op52 = $_POST['op52'];
		$op53 = $_POST['op53'];
		$op54 = $_POST['op54'];
		$a1 = $_POST['a1'];
		$a2 = $_POST['a2'];
		$a3 = $_POST['a3'];
		$a4 = $_POST['a4'];
		$a5 = $_POST['a5'];
		
		
		if (!empty($postedby) && !empty($quizname) && !empty($q1) && !empty($q2) && !empty($q3) && !empty($q4) &&
			!empty($op11) && !empty($op12) && !empty($op13) && !empty($op14) &&
			!empty($op21) && !empty($op22) && !empty($op23) && !empty($op24) &&
			!empty($op31) && !empty($op32) && !empty($op33) && !empty($op34) &&
			!empty($op41) && !empty($op42) && !empty($op43) && !empty($op44) &&
			!empty($op51) && !empty($op52) && !empty($op53) && !empty($op54) &&
			!empty($a1) && !empty($a2) && !empty($a3) && !empty($a4) && !empty($a5))
		{
			$user -> post_quiz($postedby, $quizname, $q1, $q2, $q3, $q4, $q5, $op11, $op12, $op13, $op14, $op21, $op22, $op23, $op24,
							   $op31, $op32, $op33, $op34, $op41, $op42, $op43, $op44, $op51, $op52, $op53, $op54,
							   $a1, $a2, $a3, $a4, $a5);			
		}
		else
		{
			echo "All Fields mamdatory";
		}
	}
	
	?>
	</body>

</html>