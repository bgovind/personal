MeTube - A Database Driven online Media sharing Website:

MeTube is a database driven multimedia content hosting website on the lines on Youtube. MeTube can host a wide variety of content including Videos, Images and Audio files. Some key highlights of its capabilities are its ability to form word  cloud and provide a holistic view of top trending search terms on the website, media recommendation system, categorized  playlists. On top of this MeTube provides a social network like platform to chat with other users, add them as contact/friends  and get involved in public conversations on topics of interest.

The website and the database are currently hosted on the School of Computing Lab Machines and is available on the open internet @ https://people.cs.clemson.edu/~bgovind

Implemented using - HTML, CSS, bootstrap, javascript for the front end and PHP, MySQL on the server side.

Thanks to my team member Vashist Suresh, who helped me with the project.

The website is still evolving and will include additional functionalities in the future as current open bugs are fixed.